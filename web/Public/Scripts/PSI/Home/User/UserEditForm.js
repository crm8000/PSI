/**
 * 新建或编辑用户
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.Home.User.UserEditForm", {
  extend: "PSI.AFX.Form.EditForm",

  config: {
    defaultOrg: null
  },

  /**
   * 初始化组件
   * 
   * @override
   */
  initComponent() {
    const me = this;

    const entity = me.getEntity();
    me.adding = entity == null;

    const t = entity == null ? "新建用户" : "编辑用户";
    const logoHtml = me.genLogoHtml(entity, t);

    const width1 = 600;
    const width2 = 300;
    PCL.apply(me, {
      header: {
        title: me.formatTitle(PSI.Const.PROD_NAME),
        height: 40
      },
      width: 650,
      height: me.adding ? 500 : 470,
      layout: "border",
      items: [{
        region: "north",
        border: 0,
        height: 70,
        html: logoHtml
      }, {
        region: "center",
        border: 0,
        id: me.buildId(me, "editForm"),
        xtype: "form",
        layout: {
          type: "table",
          columns: 2,
          tableAttrs: PSI.Const.TABLE_LAYOUT,
        },
        height: "100%",
        bodyPadding: 5,
        defaultType: 'textfield',
        fieldDefaults: {
          labelWidth: 65,
          labelAlign: "right",
          labelSeparator: "",
          msgTarget: 'side'
        },
        items: [{
          xtype: "hidden",
          name: "id",
          value: entity === null ? null : entity.get("id")
        }, {
          id: me.buildId(me, "editLoginName"),
          fieldLabel: "登录名",
          allowBlank: false,
          blankText: "没有输入登录名",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          name: "loginName",
          colspan: 2,
          width: width1,
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          }
        }, {
          id: me.buildId(me, "editName"),
          fieldLabel: "姓名",
          width: width2,
          allowBlank: false,
          blankText: "没有输入姓名",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          name: "name",
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          }
        }, {
          id: me.buildId(me, "editOrgCode"),
          fieldLabel: "编码",
          width: width2,
          allowBlank: false,
          blankText: "没有输入编码",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          name: "orgCode",
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          },
          colspan: 1
        }, {
          id: me.buildId(me, "editOrgName"),
          xtype: "PSI_org_editor",
          fieldLabel: "所属组织",
          allowBlank: false,
          blankText: "没有选择组织机构",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          parentItem: me,
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          },
          colspan: 2,
          width: width1
        }, {
          id: me.buildId(me, "editOrgId"),
          xtype: "hidden",
          name: "orgId",
        }, {
          id: me.buildId(me, "editBirthday"),
          fieldLabel: "生日",
          width: width2,
          xtype: "datefield",
          format: "Y-m-d",
          name: "birthday",
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          }
        }, {
          id: me.buildId(me, "editIdCardNumber"),
          fieldLabel: "身份证号",
          width: width2,
          name: "idCardNumber",
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          }
        }, {
          id: me.buildId(me, "editTel"),
          fieldLabel: "联系电话",
          width: width2,
          name: "tel",
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          }
        }, {
          id: me.buildId(me, "editTel02"),
          fieldLabel: "备用电话",
          width: width2,
          name: "tel02",
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          }
        }, {
          id: me.buildId(me, "editAddress"),
          fieldLabel: "家庭住址",
          name: "address",
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          },
          colspan: 2,
          width: width1
        }, {
          id: me.buildId(me, "editGenderId"),
          xtype: "hidden",
          name: "gender",
          value: 0,
        }, {
          fieldLabel: "性别",
          xtype: "psi_sysdictfield",
          tableName: "t_sysdict_sln0000_gender",
          callbackFunc: me._genderCallback,
          callbackScope: me,
          id: me.buildId(me, "editGender"),
          width: width2,
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          },
        }, {
          id: me.buildId(me, "editEnabledId"),
          xtype: "hidden",
          name: "enabled",
          value: 1,
        }, {
          xtype: "psi_sysdictfield",
          tableName: "t_sysdict_sln0000_user_enabled",
          callbackFunc: me._enabledCallback,
          callbackScope: me,
          fieldLabel: "能否登录",
          id: me.buildId(me, "editEnabled"),
          width: width2,
          allowBlank: false,
          blankText: "没有选择是否允许登录",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          value: "允许登录",
          listeners: {
            specialkey: {
              fn: me._onLastEditSpecialKey,
              scope: me
            }
          },
        }, {
          xtype: "displayfield",
          fieldLabel: "说明",
          colspan: 2,
          value: me.toFieldNoteText("新用户的默认登录密码是 123456"),
          hidden: !me.adding
        }],
        buttons: [{
          text: "确定",
          ...PSI.Const.BTN_STYLE,
          formBind: true,
          iconCls: "PSI-button-ok",
          handler: me._onOK,
          scope: me
        }, {
          text: "取消",
          ...PSI.Const.BTN_STYLE,
          handler() {
            const info = !me.getEntity() ? "新建用户" : "编辑用户";
            me.confirm(`请确认是否取消： ${info} ?`,
              () => {
                me.close();
              });
          },
          scope: me
        }]
      }],
      listeners: {
        show: {
          fn: me._onWndShow,
          scope: me
        },
        close: {
          fn: me._onWndClose,
          scope: me
        }
      }
    });

    me.callParent(arguments);

    me.editLoginName = PCL.getCmp(me.buildId(me, "editLoginName"));
    me.editName = PCL.getCmp(me.buildId(me, "editName"));
    me.editOrgCode = PCL.getCmp(me.buildId(me, "editOrgCode"));
    me.editOrgId = PCL.getCmp(me.buildId(me, "editOrgId"));
    me.editOrgName = PCL.getCmp(me.buildId(me, "editOrgName"));
    me.editBirthday = PCL.getCmp(me.buildId(me, "editBirthday"));
    me.editIdCardNumber = PCL.getCmp(me.buildId(me, "editIdCardNumber"));
    me.editTel = PCL.getCmp(me.buildId(me, "editTel"));
    me.editTel02 = PCL.getCmp(me.buildId(me, "editTel02"));
    me.editAddress = PCL.getCmp(me.buildId(me, "editAddress"));
    me.editGenderId = PCL.getCmp(me.buildId(me, "editGenderId"));
    me.editGender = PCL.getCmp(me.buildId(me, "editGender"));
    me.editEnabledId = PCL.getCmp(me.buildId(me, "editEnabledId"));
    me.editEnabled = PCL.getCmp(me.buildId(me, "editEnabled"));

    // AFX
    me.__editorList = [
      me.editLoginName, me.editName, me.editOrgCode,
      me.editOrgName, me.editBirthday, me.editIdCardNumber, me.editTel,
      me.editTel02, me.editAddress, me.editGender, me.editEnabled];

    if (me.getDefaultOrg()) {
      const org = me.getDefaultOrg();
      me.setOrg.apply(me, [{
        id: org.get("id"),
        fullName: org.get("fullName")
      }]);
    }
  },

  /**
   * @private
   */
  _onWndClose() {
    const me = this;

    PCL.WindowManager.hideAll();

    // AFX
    PCL.get(window).un('beforeunload', me.__onWindowBeforeUnload);

    const parentForm = me.getParentForm();
    if (parentForm) {
      parentForm._keyMapMain.enable();
    }
  },

  /**
   * @private
   */
  _onWndShow() {
    const me = this;

    // AFX
    PCL.get(window).on('beforeunload', me.__onWindowBeforeUnload);

    const parentForm = me.getParentForm();
    if (parentForm) {
      parentForm._keyMapMain.disable();
    }

    if (me.adding) {
      me.setFocusAndCursorPosToLast(me.editLoginName);
      return;
    }

    // 下面的业务逻辑是编辑用户

    const el = me.getEl();
    el.mask(PSI.Const.LOADING);
    me.ajax({
      url: me.URL("Home/User/userInfo"),
      params: {
        id: me.getEntity().get("id")
      },
      callback(options, success, response) {
        el.unmask();
        if (success) {

          const data = me.decodeJSON(response.responseText);

          me.editLoginName.setValue(data.loginName);

          const name = data.name;
          me.editName.setValue(name);
          me.setFocusAndCursorPosToLast(me.editName);

          me.editOrgCode.setValue(data.orgCode);
          me.editBirthday.setValue(data.birthday);
          me.editIdCardNumber.setValue(data.idCardNumber);
          me.editTel.setValue(data.tel);
          me.editTel02.setValue(data.tel02);
          me.editAddress.setValue(data.address);
          me.editGenderId.setValue(data.genderId);
          me.editGender.setValue(data.gender);
          me.editGender.setIdValue(data.genderId);
          me.editEnabledId.setValue(data.enabledId);
          me.editEnabled.setValue(data.enabled);
          me.editEnabled.setIdValue(data.enabledId);
          me.editOrgId.setValue(data.orgId);
          me.editOrgName.setValue(data.orgFullName);
        }
      }
    });
  },

  // xtype: "PSI_org_editor"回调本方法
  setOrg(data) {
    const me = this;

    me.editOrgName.setValue(data.fullName);
    me.editOrgId.setValue(data.id);
  },

  /**
   * @private
   */
  _onOK() {
    const me = this;
    const f = PCL.getCmp(me.buildId(me, "editForm"));
    const el = f.getEl();
    el.mask("数据保存中...");
    f.submit({
      url: me.URL("Home/User/editUser"),
      method: "POST",
      success(form, action) {
        el.unmask();
        me.close();
        const parentForm = me.getParentForm();
        parentForm?.freshUserGrid?.apply(parentForm, []);
        me.tip("数据保存成功", true);
      },
      failure(form, action) {
        el.unmask();
        me.showInfo(action.result.msg, () => {
          PCL.getCmp("editName").focus();
        });
      }
    });
  },

  /**
   * @private
   */
  _onLastEditSpecialKey(field, e) {
    const me = this;

    if (e.getKey() === e.ENTER) {
      const f = PCL.getCmp(me.buildId(me, "editForm"));
      if (f.getForm().isValid()) {
        me._onOK();
      }
    }
  },

  /**
   * 性别字段回调本方法
   * @private
   */
  _genderCallback(data, scope) {
    const me = scope;

    let id = data ? data.get("id") : null;
    if (!id) {
      id = 0;
    }
    me.editGenderId.setValue(id);
  },

  /**
   * 能否登录字段回调本方法
   * @private
   */
  _enabledCallback(data, scope) {
    const me = scope;

    let id = data ? data.get("id") : null;
    if (!id) {
      id = 0;
    }
    me.editEnabledId.setValue(id);
  },
});
