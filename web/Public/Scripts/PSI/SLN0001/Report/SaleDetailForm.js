/**
 * 销售出库明细表
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.Report.SaleDetailForm", {
  extend: "PSI.AFX.Form.MainForm",

  /**
   * @override
   */
  initComponent() {
    var me = this;

    var store = me.getMainGrid().getStore();

    PCL.apply(me, {
      items: [{
        tbar: [{
          text: "查询",
          ...PSI.Const.BTN_STYLE,
          iconCls: "PSI-tb-query",
          handler: me.onQuery,
          scope: me
        }, "-", {
          text: "打印",
          ...PSI.Const.BTN_STYLE,
          menu: [{
            text: "打印预览",
            iconCls: "PSI-button-print-preview",
            scope: me,
            handler: me.onPrintPreview
          }, "-", {
            text: "直接打印",
            iconCls: "PSI-button-print",
            scope: me,
            handler: me.onPrint
          }]
        }, {
          text: "导出",
          ...PSI.Const.BTN_STYLE,
          menu: [{
            text: "导出PDF",
            iconCls: "PSI-button-pdf",
            scope: me,
            handler: me.onPDF
          }, "-", {
            text: "导出Excel",
            iconCls: "PSI-button-excel",
            scope: me,
            handler: me.onExcel
          }]
        }, "-", {
          iconCls: "PSI-tb-close",
          text: "关闭",
          ...PSI.Const.BTN_STYLE,
          handler() {
            me.closeWindow();
          }
        }, "->", {
          id: "pagingToobar",
          cls: "PSI-toolbox",
          xtype: "pagingtoolbar",
          border: 0,
          store: store
        }, "-", {
          xtype: "displayfield",
          value: "每页显示"
        }, {
          id: "comboCountPerPage",
          cls: "PSI-toolbox",
          xtype: "combobox",
          editable: false,
          width: 60,
          store: PCL.create("PCL.data.ArrayStore", {
            fields: ["text"],
            data: [["20"], ["50"], ["100"],
            ["300"], ["1000"]]
          }),
          value: 100,
          listeners: {
            change: {
              fn() {
                store.pageSize = PCL.getCmp("comboCountPerPage").getValue();
                store.currentPage = 1;
                PCL.getCmp("pagingToobar").doRefresh();
              },
              scope: me
            }
          }
        }, {
          xtype: "displayfield",
          value: "条记录"
        }].concat(me.getShortcutCmp()),
        id: "panelQueryCmp",
        region: "north",
        height: 95,
        layout: "fit",
        border: 0,
        header: false,
        collapsible: true,
        collapseMode: "mini",
        layout: {
          type: "table",
          columns: 5
        },
        bodyCls: "PSI-Query-Panel",
        items: me.getQueryCmp()
      }, {
        region: "center",
        layout: "fit",
        border: 0,
        ...PSI.Const.BODY_PADDING,
        items: [me.getMainGrid()]
      }]
    });

    me.callParent(arguments);
  },

  /**
   * 快捷访问
   * 
   * @private
   */
  getShortcutCmp() {
    return ["|", " ",
      {
        cls: "PSI-Shortcut-Cmp",
        labelWidth: 0,
        emptyText: "快捷访问",
        xtype: "psi_mainmenushortcutfield",
        width: 90
      }
    ];
  },

  getQueryCmp() {
    var me = this;

    PCL.define("PSILogCategory", {
      extend: "PCL.data.Model",
      fields: ["id", "name"]
    });

    return [{
      xtype: "container",
      height: 26,
      html: `<h2 style='margin-left:20px;margin-top:18px;color:#595959;display:inline-block'>销售出库明细表</h2>
              &nbsp;&nbsp;<span style='color:#8c8c8c'>查询式报表</span>
              <div style='float:right;display:inline-block;margin:10px 0px 0px 20px;border-left:1px solid #e5e6e8;height:40px'>&nbsp;</div>
              `
    }, {
      id: "editQueryCustomer",
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "客户",
      labelWidth: 110,
      margin: "5, 0, 0, 0",
      showModal: true,
      xtype: "psi_customerfield"
    }, {
      id: "editQueryWarehouse",
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "出库仓库",
      margin: "5, 0, 0, 0",
      showModal: true,
      xtype: "psi_warehousefield"
    }, {
      xtype: "container",
      items: [{
        xtype: "button",
        text: "查询",
        cls: "PSI-Query-btn1",
        width: 100,
        height: 26,
        margin: "5 0 0 10",
        handler: me.onQuery,
        scope: me
      }, {
        xtype: "button",
        text: "清空查询条件",
        cls: "PSI-Query-btn2",
        width: 100,
        height: 26,
        margin: "5, 0, 0, 10",
        handler: me.onClearQuery,
        scope: me
      }]
    }, {
      xtype: "container",
      items: [{
        xtype: "button",
        iconCls: "PSI-button-hide",
        text: "隐藏工具栏",
        cls: "PSI-Query-btn3",
        width: 110,
        height: 26,
        margin: "5 0 0 20",
        handler() {
          PCL.getCmp("panelQueryCmp").collapse();
        },
        scope: me
      }]
    }, { xtype: "container" }, {
      id: "editQueryFromDT",
      xtype: "datefield",
      margin: "5, 0, 0, 0",
      format: "Y-m-d",
      labelAlign: "right",
      labelSeparator: "",
      labelWidth: 110,
      fieldLabel: "业务日期（起）"
    }, {
      id: "editQueryToDT",
      xtype: "datefield",
      margin: "5, 0, 0, 0",
      format: "Y-m-d",
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "业务日期（止）"
    }];
  },

  getMainGrid() {
    var me = this;
    if (me.__mainGrid) {
      return me.__mainGrid;
    }

    var modelName = "PSISaleDetailReport";
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["customerName", "soBillRef", "wsBillRef", "bizDate", "warehouseName", "goodsCode",
        "goodsName", "goodsSpec", "unitName", "goodsCount", "goodsMoney",
        "goodsPrice", "memo", "taxRate", "tax",
        "moneyWithTax", "goodsPriceWithTax"]
    });
    var store = PCL.create("PCL.data.Store", {
      autoLoad: false,
      model: modelName,
      data: [],
      pageSize: 20,
      proxy: {
        type: "ajax",
        actionMethods: {
          read: "POST"
        },
        url: me.URL("SLN0001/Report/saleDetailQueryData"),
        reader: {
          root: "dataList",
          totalProperty: "totalCount"
        }
      }
    });
    store.on("beforeload", () => {
      store.proxy.extraParams = me.getQueryParam();
    });

    me.__mainGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-FC",
      viewConfig: {
        enableTextSelection: true
      },
      border: 1,
      columnLines: true,
      columns: {
        defaults: {
          menuDisabled: true,
          sortable: false
        },
        items: [{
          xtype: "rownumberer",
          width: 50
        }, {
          header: "销售订单号",
          dataIndex: "soBillRef",
          width: 120,
          renderer(value, md, record) {
            const refType = encodeURIComponent("销售订单");
            const ref = encodeURIComponent(record.get("soBillRef"));
            const href = `${PSI.Const.BASE_URL}SLN0001/Bill/viewIndex?fid=2027&refType=${refType}&ref=${ref}`;
            return `<a href='${href}' target='_blank'>
                      ${value}
                    </a>`;
          }
        }, {
          header: "出库单单号",
          dataIndex: "wsBillRef",
          width: 120,
          renderer(value, md, record) {
            const refType = encodeURIComponent("销售出库");
            const ref = encodeURIComponent(record.get("wsBillRef"));
            const href = `${PSI.Const.BASE_URL}SLN0001/Bill/viewIndex?fid=2001&refType=${refType}&ref=${ref}`;
            return `<a href='${href}' target='_blank'>
                      ${value}
                    </a>`;
          }
        }, {
          header: "出库单业务日期",
          dataIndex: "bizDate",
          width: 120
        }, {
          header: "出库仓库",
          dataIndex: "warehouseName",
          width: 120
        }, {
          header: "客户",
          dataIndex: "customerName",
          width: 200
        }, {
          header: "商品编码",
          dataIndex: "goodsCode",
          width: 120
        }, {
          header: "商品名称",
          dataIndex: "goodsName",
          width: 200
        }, {
          header: "规格型号",
          dataIndex: "goodsSpec",
          width: 200
        }, {
          header: "出库数量",
          width: 120,
          dataIndex: "goodsCount",
          align: "right"
        }, {
          header: "单位",
          dataIndex: "unitName",
          width: 60
        }, {
          header: "单价",
          dataIndex: "goodsPrice",
          align: "right",
          xtype: "numbercolumn",
          width: 150
        }, {
          header: "销售金额",
          dataIndex: "goodsMoney",
          align: "right",
          xtype: "numbercolumn",
          width: 150
        }, {
          header: "税率(%)",
          dataIndex: "taxRate",
          align: "right",
          xtype: "numbercolumn",
          format: "0"
        }, {
          header: "税金",
          dataIndex: "tax",
          align: "right",
          xtype: "numbercolumn",
          width: 150
        }, {
          header: "价税合计",
          dataIndex: "moneyWithTax",
          align: "right",
          xtype: "numbercolumn",
          width: 150
        }, {
          header: "含税价",
          dataIndex: "goodsPriceWithTax",
          align: "right",
          xtype: "numbercolumn",
          width: 150
        }, {
          header: "备注",
          dataIndex: "memo",
          width: 200
        }]
      },
      store: store
    });

    return me.__mainGrid;
  },

  onQuery() {
    const me = this;

    me.focus();
    me.refreshMainGrid();
  },

  getQueryParam() {
    var result = {
      customerId: PCL.getCmp("editQueryCustomer").getIdValue(),
      warehouseId: PCL.getCmp("editQueryWarehouse").getIdValue()
    };

    var fromDT = PCL.getCmp("editQueryFromDT").getValue();
    if (fromDT) {
      result.fromDT = PCL.Date.format(fromDT, "Y-m-d");
    }

    var toDT = PCL.getCmp("editQueryToDT").getValue();
    if (toDT) {
      result.toDT = PCL.Date.format(toDT, "Y-m-d");
    }

    return result;
  },

  onClearQuery() {
    var me = this;

    PCL.getCmp("editQueryCustomer").clearIdValue();
    PCL.getCmp("editQueryWarehouse").clearIdValue();
    PCL.getCmp("editQueryFromDT").setValue(null);
    PCL.getCmp("editQueryToDT").setValue(null);

    me.getMainGrid().getStore().currentPage = 1;

    me.onQuery();
  },

  refreshMainGrid(id) {
    PCL.getCmp("pagingToobar").doRefresh();
  },

  onPrintPreview() {
    var me = this;
    me.showInfo("TODO");
    return;

    var lodop = getLodop();
    if (!lodop) {
      PSI.MsgBox.showInfo("没有安装Lodop控件，无法打印");
      return;
    }

    var me = this;

    var el = PCL.getBody();
    el.mask("数据加载中...");
    var r = {
      url: me.URL("SLN0001/Report/genInventoryUpperPrintPage"),
      params: {
        limit: -1
      },
      callback(options, success, response) {
        el.unmask();

        if (success) {
          var data = response.responseText;
          me.previewReport("采购入库明细表", data);
        }
      }
    };
    me.ajax(r);
  },

  PRINT_PAGE_WIDTH: "200mm",
  PRINT_PAGE_HEIGHT: "95mm",

  previewReport(ref, data) {
    var me = this;

    var lodop = getLodop();
    if (!lodop) {
      PSI.MsgBox.showInfo("Lodop打印控件没有正确安装");
      return;
    }

    lodop.PRINT_INIT(ref);
    lodop.SET_PRINT_PAGESIZE(1, me.PRINT_PAGE_WIDTH, me.PRINT_PAGE_HEIGHT,
      "");
    lodop.ADD_PRINT_HTM("0mm", "0mm", "100%", "100%", data);
    var result = lodop.PREVIEW("_blank");
  },

  onPrint() {
    var me = this;
    me.showInfo("TODO");
    return;

    var lodop = getLodop();
    if (!lodop) {
      PSI.MsgBox.showInfo("没有安装Lodop控件，无法打印");
      return;
    }

    var me = this;

    var el = PCL.getBody();
    el.mask("数据加载中...");
    var r = {
      url: me.URL("SLN0001/Report/genPurchaseDetailPrintPage"),
      params: {
        limit: -1
      },
      callback(options, success, response) {
        el.unmask();

        if (success) {
          var data = response.responseText;
          me.printReport("采购入库明细表", data);
        }
      }
    };
    me.ajax(r);
  },

  printReport(ref, data) {
    var me = this;

    var lodop = getLodop();
    if (!lodop) {
      me.showInfo("Lodop打印控件没有正确安装");
      return;
    }

    lodop.PRINT_INIT(ref);
    lodop.SET_PRINT_PAGESIZE(1, me.PRINT_PAGE_WIDTH, me.PRINT_PAGE_HEIGHT,
      "");
    lodop.ADD_PRINT_HTM("0mm", "0mm", "100%", "100%", data);
    var result = lodop.PRINT();
  },

  onPDF() {
    var me = this;

    var url = "SLN0001/Report/saleDetailPdf?limit=-1";
    var customerId = PCL.getCmp("editQueryCustomer").getIdValue();
    if (customerId) {
      url = url + "&customerId=" + customerId;
    }
    var warehouseId = PCL.getCmp("editQueryWarehouse").getIdValue();
    if (warehouseId) {
      url = url + "&warehouseId=" + warehouseId;
    }
    var fromDT = PCL.getCmp("editQueryFromDT").getValue();
    if (fromDT) {
      var dt = PCL.Date.format(fromDT, "Y-m-d");
      url = url + "&fromDT=" + dt;
    }
    var toDT = PCL.getCmp("editQueryToDT").getValue();
    if (toDT) {
      var dt = PCL.Date.format(toDT, "Y-m-d");
      url = url + "&toDT=" + dt;
    }

    window.open(me.URL(url));
  },

  onExcel() {
    var me = this;

    var url = "SLN0001/Report/saleDetailExcel?limit=-1";
    var customerId = PCL.getCmp("editQueryCustomer").getIdValue();
    if (customerId) {
      url = url + "&customerId=" + customerId;
    }
    var warehouseId = PCL.getCmp("editQueryWarehouse").getIdValue();
    if (warehouseId) {
      url = url + "&warehouseId=" + warehouseId;
    }
    var fromDT = PCL.getCmp("editQueryFromDT").getValue();
    if (fromDT) {
      var dt = PCL.Date.format(fromDT, "Y-m-d");
      url = url + "&fromDT=" + dt;
    }
    var toDT = PCL.getCmp("editQueryToDT").getValue();
    if (toDT) {
      var dt = PCL.Date.format(toDT, "Y-m-d");
      url = url + "&toDT=" + dt;
    }

    window.open(me.URL(url));
  }
});
