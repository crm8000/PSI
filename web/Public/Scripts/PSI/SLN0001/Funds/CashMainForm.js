/**
 * 现金收支查询界面
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.Funds.CashMainForm", {
  extend: "PSI.AFX.Form.MainForm",

  /**
   * @override
   */
  initComponent() {
    const me = this;

    PCL.apply(me, {
      tbar: [{
        xtype: "displayfield",
        margin: "5 0 0 0",
        value: "业务日期 从"
      }, {
        cls: "PSI-toolbox",
        id: "dtFrom",
        xtype: "datefield",
        format: "Y-m-d",
        width: 100
      }, {
        xtype: "displayfield",
        margin: "5 0 0 0",
        value: " 到 "
      }, {
        cls: "PSI-toolbox",
        id: "dtTo",
        xtype: "datefield",
        format: "Y-m-d",
        width: 100,
        value: new Date()
      }, {
        text: "查询",
        ...PSI.Const.BTN_STYLE,
        iconCls: "PSI-tb-query",
        handler: me.onQuery,
        scope: me
      }, "-", {
        iconCls: "PSI-tb-close",
        text: "关闭",
        ...PSI.Const.BTN_STYLE,
        handler() {
          me.closeWindow();
        }
      }].concat(me.getShortcutCmp()),
      layout: "border",
      border: 0,
      bodyPadding: "0 20 5 15",
      items: [{
        region: "north",
        height: 55,
        border: 0,
        margin: 0,
        bodyPadding: "0 20 0 0",
        html: `<h2 style='color:#595959;display:inline-block'>现金收支查询</h2>
                &nbsp;&nbsp;<span style='color:#8c8c8c'>查询式报表</span>`,
      }, {
        region: "center",
        layout: "fit",
        border: 0,
        items: [me.getMainGrid()]
      }, {
        region: "south",
        layout: "fit",
        border: 0,
        split: true,
        height: "50%",
        items: [me.getDetailGrid()]
      }]
    });

    me.callParent(arguments);

    const dt = new Date();
    dt.setDate(dt.getDate() - 7);
    PCL.getCmp("dtFrom").setValue(dt);
  },

  /**
   * 快捷访问
   * 
   * @private
   */
  getShortcutCmp() {
    return ["->",
      {
        cls: "PSI-Shortcut-Cmp",
        labelWidth: 0,
        emptyText: "快捷访问",
        xtype: "psi_mainmenushortcutfield",
        width: 90
      }
    ];
  },

  getMainGrid() {
    const me = this;
    if (me.__mainGrid) {
      return me.__mainGrid;
    }

    const modelName = "PSICash";
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["bizDT", "inMoney", "outMoney", "balanceMoney"]
    });

    const store = PCL.create("PCL.data.Store", {
      model: modelName,
      pageSize: 20,
      proxy: {
        type: "ajax",
        actionMethods: {
          read: "POST"
        },
        url: PSI.Const.BASE_URL + "SLN0001/Funds/cashList",
        reader: {
          root: 'dataList',
          totalProperty: 'totalCount'
        }
      },
      autoLoad: false,
      data: []
    });

    store.on("beforeload", () => {
      PCL.apply(store.proxy.extraParams, {
        dtFrom: PCL.Date.format(PCL.getCmp("dtFrom").getValue(), "Y-m-d"),
        dtTo: PCL.Date.format(PCL.getCmp("dtTo").getValue(), "Y-m-d")
      });
    });

    me.__mainGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-FC",
      bbar: ["->", {
        xtype: "pagingtoolbar",
        border: 0,
        store: store
      }],
      columnLines: true,
      columns: [{
        header: "业务日期",
        dataIndex: "bizDT",
        menuDisabled: true,
        sortable: false
      }, {
        header: "收",
        dataIndex: "inMoney",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 160
      }, {
        header: "支",
        dataIndex: "outMoney",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 160
      }, {
        header: "余额",
        dataIndex: "balanceMoney",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 160
      }],
      store: store,
      listeners: {
        select: {
          fn: me.onMainGridSelect,
          scope: me
        }
      }
    });

    return me.__mainGrid;
  },

  getDetailGrid() {
    const me = this;
    if (me.__detailGrid) {
      return me.__detailGrid;
    }

    const modelName = "PSICashDetail";
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["bizDT", "inMoney", "outMoney", "balanceMoney",
        "refType", "refNumber", "dateCreated"]
    });

    const store = PCL.create("PCL.data.Store", {
      model: modelName,
      pageSize: 20,
      proxy: {
        type: "ajax",
        actionMethods: {
          read: "POST"
        },
        url: me.URL("SLN0001/Funds/cashDetailList"),
        reader: {
          root: 'dataList',
          totalProperty: 'totalCount'
        }
      },
      autoLoad: false,
      data: []
    });

    store.on("beforeload", () => {
      const item = me.getMainGrid().getSelectionModel().getSelection();
      let c = null;
      if (item == null || item.length != 1) {
        c = null;
      } else {
        c = item[0];
      }

      PCL.apply(store.proxy.extraParams, {
        bizDT: c == null ? null : c.get("bizDT")
      });
    });

    me.__detailGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-HL",
      header: {
        height: 30,
        title: me.formatGridHeaderTitle("现金收支流水明细")
      },
      bbar: ["->", {
        xtype: "pagingtoolbar",
        border: 0,
        store: store
      }],
      columnLines: true,
      columns: [{
        header: "业务类型",
        dataIndex: "refType",
        menuDisabled: true,
        sortable: false,
        width: 120
      }, {
        header: "单号",
        dataIndex: "refNumber",
        menuDisabled: true,
        sortable: false,
        width: 120,
        renderer(value, md, record) {
          const refType = encodeURIComponent(record.get("refType"));
          const ref = encodeURIComponent(record.get("refNumber"));
          const href = `${PSI.Const.BASE_URL}SLN0001/Bill/viewIndex?fid=2024&refType=${refType}&ref=${ref}`;

          return `
            <a href='${href}' target='_blank'>
              ${value}
            </a>`;
        }
      }, {
        header: "业务日期",
        dataIndex: "bizDT",
        menuDisabled: true,
        sortable: false
      }, {
        header: "收",
        dataIndex: "inMoney",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn"
      }, {
        header: "支",
        dataIndex: "outMoney",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn"
      }, {
        header: "余额",
        dataIndex: "balanceMoney",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn"
      }, {
        header: "创建时间",
        dataIndex: "dateCreated",
        menuDisabled: true,
        sortable: false,
        width: 150
      }],
      store: store
    });

    return me.__detailGrid;
  },

  onQuery() {
    const me = this;
    me.getDetailGrid().getStore().removeAll();

    me.getMainGrid().getStore().loadPage(1);
  },

  onMainGridSelect() {
    this.getDetailGrid().getStore().loadPage(1);
  }
});
