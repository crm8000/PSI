/**
 * 成品委托生产订单 - 主界面
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.DMO.DMOMainForm", {
  extend: "PSI.AFX.Form.MainForm",

  config: {
    permission: null
  },

  /**
   * @override
   */
  initComponent() {
    const me = this;

    PCL.apply(me, {
      items: [{
        tbar: me.getToolbarCmp(),
        id: me.buildId(me, "panelQueryCmp"),
        region: "north",
        height: 95,
        layout: "fit",
        border: 0,
        header: false,
        collapsible: true,
        collapseMode: "mini",
        layout: {
          type: "table",
          columns: 5
        },
        bodyCls: "PSI-Query-Panel",
        items: me.getQueryCmp()
      }, {
        region: "center",
        layout: "border",
        border: 0,
        ...PSI.Const.BODY_PADDING,
        items: [{
          region: "center",
          layout: "fit",
          border: 0,
          items: [me.getMainGrid()]
        }, {
          region: "south",
          height: "60%",
          split: true,
          collapsible: true,
          header: false,
          layout: "border",
          border: 0,
          items: [{
            region: "north",
            bodyStyle: "border-width:1px 1px 0px 1px",
            height: 40,
            border: 1,
            layout: {
              type: "table",
              columns: 5
            },
            bodyPadding: 10,
            items: me.getBillSummaryCmp()
          }, {
            region: "center",
            border: 0,
            xtype: "tabpanel",
            bodyStyle: { borderWidth: 0 },
            items: [me.getDetailGrid(), me.getDMWGrid()]
          }
          ]
        }]
      }]
    });

    me.callParent();

    me.editSummaryRef = PCL.getCmp(me.buildId(me, "editSummaryRef"));
    me.editSummaryFactory = PCL.getCmp(me.buildId(me, "editSummaryFactory"));
    me.editSummaryDealDate = PCL.getCmp(me.buildId(me, "editSummaryDealDate"));

    me.editQueryBillStatus = PCL.getCmp(me.buildId(me, "editQueryBillStatus"));
    me.editQueryRef = PCL.getCmp(me.buildId(me, "editQueryRef"));
    me.editQueryFromDT = PCL.getCmp(me.buildId(me, "editQueryFromDT"));
    me.editQueryToDT = PCL.getCmp(me.buildId(me, "editQueryToDT"));
    me.editQueryFactory = PCL.getCmp(me.buildId(me, "editQueryFactory"));
    me.editQueryGoods = PCL.getCmp(me.buildId(me, "editQueryGoods"));

    me.buttonEdit = PCL.getCmp(me.buildId(me, "buttonEdit"));
    me.buttonDelete = PCL.getCmp(me.buildId(me, "buttonDelete"));
    me.buttonCommit = PCL.getCmp(me.buildId(me, "buttonCommit"));
    me.buttonCancelConfirm = PCL.getCmp(me.buildId(me, "buttonCancelConfirm"));
    me.buttonGenDMWBill = PCL.getCmp(me.buildId(me, "buttonGenDMWBill"));

    me.panelQueryCmp = PCL.getCmp(me.buildId(me, "panelQueryCmp"));
    me.comboCountPerPage = PCL.getCmp(me.buildId(me, "comboCountPerPage"));
    me.pagingToobar = PCL.getCmp(me.buildId(me, "pagingToobar"));

    // AFX: 查询控件input List
    me.__editorList = [
      me.editQueryBillStatus,
      me.editQueryRef,
      me.editQueryFromDT,
      me.editQueryToDT,
      me.editQueryFactory,
      me.editQueryGoods];

    me._keyMapMain = PCL.create("PCL.util.KeyMap", PCL.getBody(), [{
      key: "N",
      ctrl: false,
      shift: false,
      alt: true,
      fn() {
        if (PSI.Const.msgBoxShowing) {
          return;
        }

        // 判断权限
        if (me.getPermission().add != "1") {
          return;
        }

        me._onAddBill.apply(me, []);
      },
      scope: me
    }, {
      key: "Q",
      ctrl: false,
      shift: false,
      alt: true,
      fn() {
        if (PSI.Const.msgBoxShowing) {
          return;
        }
        me.editQueryRef?.focus();
      },
      scope: me
    }, {
      key: "H",
      ctrl: false,
      shift: false,
      alt: true,
      fn() {
        if (PSI.Const.msgBoxShowing) {
          return;
        }

        const panel = me.panelQueryCmp;
        if (panel.getCollapsed()) {
          panel.expand();
        } else {
          panel.collapse();
        };
      },
      scope: me
    },
    ]);

    me.refreshMainGrid();
  },

  /**
   * @private
   */
  getBillSummaryCmp() {
    const me = this;

    const fieldProps = {
      xtype: "textfield",
      readOnly: true,
      labelSeparator: "",
      labelAlign: "right",
    };
    return [{
      xtype: "container",
      height: 22,
      html: "<span style='padding:0px;margin:0px 0px 0px -5px;border-left: 5px solid #9254de'></span>",
    }, {
      id: me.buildId(me, "editSummaryRef"),
      fieldLabel: "生产订单号",
      labelWidth: 75,
      value: "",
      ...fieldProps,
    }, {
      id: me.buildId(me, "editSummaryFactory"),
      fieldLabel: "工厂",
      labelWidth: 70,
      value: "",
      colspan: 2,
      width: 430,
      ...fieldProps,
    }, {
      id: me.buildId(me, "editSummaryDealDate"),
      fieldLabel: "交货日期",
      labelWidth: 70,
      value: "",
      ...fieldProps,
    }];
  },

  /**
   * @private
   */
  resetSummaryInput() {
    const me = this;

    me.editSummaryRef.setValue("");
    me.editSummaryFactory.setValue("");
    me.editSummaryDealDate.setValue("");
  },


  /**
   * @private
   */
  getToolbarCmp() {
    const me = this;

    let result = [];
    let list = [];

    // 新建
    if (me.getPermission().add == "1") {
      result.push({
        iconCls: "PSI-tb-new",
        text: "新建成品委托生产订单 <span class='PSI-shortcut-DS'>Alt + N</span>",
        tooltip: me.buildTooltip("快捷键：Alt + N"),
        ...PSI.Const.BTN_STYLE,
        scope: me,
        handler: me._onAddBill,
        id: "buttonAdd"
      });
    }

    // 变更
    list = [];
    if (me.getPermission().edit == "1") {
      list.push({
        text: "编辑成品委托生产订单",
        scope: me,
        handler: me._onEditBill,
        id: me.buildId(me, "buttonEdit")
      });
    }
    if (me.getPermission().del == "1") {
      list.push({
        text: "删除成品委托生产订单",
        scope: me,
        handler: me._onDeleteBill,
        id: me.buildId(me, "buttonDelete")
      });
    }
    if (list.length > 0) {
      if (result.length > 0) {
        result.push("-");
      }

      result.push({
        text: "变更",
        ...PSI.Const.BTN_STYLE,
        menu: list
      });
    }

    // 审核
    list = [];
    if (me.getPermission().commit == "1") {
      list.push({
        text: "审核",
        scope: me,
        handler: me._onCommit,
        id: me.buildId(me, "buttonCommit")
      }, {
        text: "取消审核",
        scope: me,
        handler: me._onCancelConfirm,
        id: me.buildId(me, "buttonCancelConfirm")
      });
    }
    if (list.length > 0) {
      if (result.length > 0) {
        result.push("-");
      }

      result.push({
        text: "审核",
        ...PSI.Const.BTN_STYLE,
        menu: list
      });
    }

    // 生成
    list = [];
    if (me.getPermission().genDMWBill == "1") {
      list.push({
        text: "生成成品委托生产入库单",
        scope: me,
        handler: me._onGenDMWBill,
        id: me.buildId(me, "buttonGenDMWBill")
      });
    }
    if (list.length > 0) {
      if (result.length > 0) {
        result.push("-");
      }

      result.push({
        text: "生成",
        ...PSI.Const.BTN_STYLE,
        menu: list
      });
    }

    // 终结
    list = [];
    if (me.getPermission().closeBill == "1") {
      list.push({
        text: "关闭成品委托生产订单",
        iconCls: "PSI-button-commit",
        scope: me,
        handler: me._onCloseDMO
      }, {
        text: "取消成品委托生产订单关闭状态",
        iconCls: "PSI-button-cancelconfirm",
        scope: me,
        handler: me._onCancelClosedDMO
      });
    }
    if (list.length > 0) {
      if (result.length > 0) {
        result.push("-");
      }

      result.push({
        text: "终结",
        ...PSI.Const.BTN_STYLE,
        menu: list
      });
    }

    // 导出
    list = [];
    if (me.getPermission().genPDF == "1") {
      list.push({
        text: "单据生成pdf",
        id: "buttonPDF",
        iconCls: "PSI-button-pdf",
        scope: me,
        handler: me._onPDF
      });
    }
    if (list.length > 0) {
      if (result.length > 0) {
        result.push("-");
      }

      result.push({
        text: "导出",
        ...PSI.Const.BTN_STYLE,
        menu: list
      });
    }

    // 打印
    list = [];
    if (me.getPermission().print == "1") {
      list.push({
        text: "打印预览",
        iconCls: "PSI-button-print-preview",
        scope: me,
        handler: me._onPrintPreview
      }, {
        text: "直接打印",
        iconCls: "PSI-button-print",
        scope: me,
        handler: me._onPrint
      });
    }
    if (list.length > 0) {
      if (result.length > 0) {
        result.push("-");
      }

      result.push({
        text: "打印",
        ...PSI.Const.BTN_STYLE,
        menu: list
      });
    }

    // 指南
    // 关闭
    if (result.length > 0) {
      result.push("-");
    }
    result.push({
      iconCls: "PSI-tb-help",
      text: "指南",
      ...PSI.Const.BTN_STYLE,
      handler() {
        me.focus();
        window.open(me.URL("Home/Help/index?t=dmobill"));
      }
    }, "-", {
      iconCls: "PSI-tb-close",
      text: "关闭",
      ...PSI.Const.BTN_STYLE,
      handler() {
        me.focus();
        me.closeWindow();
      }
    });

    result = result.concat(me.getPagination(), me.getShortcutCmp());

    return result;
  },

  /**
   * @private
   */
  getShortcutCmp() {
    return ["|", " ",
      {
        cls: "PSI-Shortcut-Cmp",
        labelWidth: 0,
        emptyText: "快捷访问",
        xtype: "psi_mainmenushortcutfield",
        width: 90
      }
    ];
  },

  /**
   * @private
   */
  getQueryCmp() {
    const me = this;
    return [{
      xtype: "container",
      height: 26,
      html: `<h2 style='margin-left:20px;margin-top:18px;color:#595959;display:inline-block'>成品委托生产订单</h2>
              &nbsp;&nbsp;<span style='color:#8c8c8c'>单据列表</span>
              <div style='float:right;display:inline-block;margin:10px 0px 0px 20px;border-left:1px solid #e5e6e8;height:40px'>&nbsp;</div>
              `
    }, {
      id: me.buildId(me, "editQueryBillStatus"),
      xtype: "combo",
      queryMode: "local",
      editable: false,
      valueField: "id",
      labelWidth: 50,
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "状态",
      margin: "5, 0, 0, 0",
      store: PCL.create("PCL.data.ArrayStore", {
        fields: ["id", "text"],
        data: [[-1, "全部"], [0, "待审核"], [1000, "已审核"],
        [2000, "部分入库"], [3000, "全部入库"], [4000, "订单关闭"]]
      }),
      value: -1,
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      }
    }, {
      id: me.buildId(me, "editQueryRef"),
      labelWidth: 90,
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "单号 <span class='PSI-shortcut-DS'>Alt + Q</span>",
      width: 220,
      margin: "5, 0, 0, 0",
      xtype: "textfield",
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      }
    }, {
      id: me.buildId(me, "editQueryFromDT"),
      xtype: "datefield",
      margin: "5, 0, 0, 0",
      format: "Y-m-d",
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "交货日期（起）",
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      }
    }, {
      id: me.buildId(me, "editQueryToDT"),
      xtype: "datefield",
      margin: "5, 0, 0, 0",
      format: "Y-m-d",
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "交货日期（止）",
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      }
    }, { xtype: "container" }, {
      id: me.buildId(me, "editQueryFactory"),
      xtype: "psi_factoryfield",
      parentCmp: me,
      showModal: true,
      labelAlign: "right",
      labelSeparator: "",
      labelWidth: 50,
      margin: "5, 0, 0, 0",
      fieldLabel: "工厂",
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      }
    }, {
      id: me.buildId(me, "editQueryGoods"),
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "物料",
      labelWidth: 60,
      margin: "5, 0, 0, 0",
      xtype: "psi_goodsfield",
      showModal: true,
      width: 220,
      listeners: {
        specialkey: {
          fn: me.__onLastEditSpecialKey,
          scope: me
        }
      }
    }, {
      xtype: "container",
      items: [{
        xtype: "button",
        text: "查询",
        cls: "PSI-Query-btn1",
        width: 100,
        height: 26,
        margin: "5 0 0 10",
        handler: me._onQuery,
        scope: me
      }, {
        xtype: "button",
        text: "清空查询条件",
        cls: "PSI-Query-btn2",
        width: 100,
        height: 26,
        margin: "5, 0, 0, 10",
        handler: me._onClearQuery,
        scope: me
      }]
    }, {
      xtype: "container",
      items: [{
        xtype: "button",
        iconCls: "PSI-button-hide",
        text: "隐藏工具栏 <span class='PSI-shortcut-DS'>Alt + H</span>",
        cls: "PSI-Query-btn3",
        width: 140,
        height: 26,
        margin: "5 0 0 20",
        handler() {
          me.panelQueryCmp.collapse();
        },
        scope: me
      }]
    }];
  },

  /**
    * @private
    */
  getPagination() {
    const me = this;
    const store = me.getMainGrid().getStore();
    const result = ["->", {
      id: me.buildId(me, "pagingToobar"),
      xtype: "pagingtoolbar",
      cls: "PSI-Pagination",
      border: 0,
      store: store
    }, "-", {
        xtype: "displayfield",
        fieldStyle: "font-size:13px",
        value: "每页显示"
      }, {
        id: me.buildId(me, "comboCountPerPage"),
        xtype: "combobox",
        cls: "PSI-Pagination",
        editable: false,
        width: 60,
        store: PCL.create("PCL.data.ArrayStore", {
          fields: ["text"],
          data: [["20"], ["50"], ["100"], ["300"],
          ["1000"]]
        }),
        value: 20,
        listeners: {
          change: {
            fn() {
              store.pageSize = me.comboCountPerPage.getValue();
              store.currentPage = 1;
              me.pagingToobar.doRefresh();
            },
            scope: me
          }
        }
      }, {
        xtype: "displayfield",
        fieldStyle: "font-size:13px",
        value: "张单据"
      }];

    return result;
  },

  /**
   * @private
   */
  getMainGrid() {
    const me = this;
    if (me._mainGrid) {
      return me._mainGrid;
    }

    const modelName = me.buildModelName(me, "DMOBill");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "ref", "factoryName", "contact", "tel",
        "fax", "inputUserName", "bizUserName",
        "billStatus", "goodsMoney", "dateCreated",
        "paymentType", "tax", "moneyWithTax", "dealDate",
        "dealAddress", "orgName", "confirmUserName",
        "confirmDate", "billMemo"]
    });
    const store = PCL.create("PCL.data.Store", {
      autoLoad: false,
      model: modelName,
      data: [],
      pageSize: 20,
      proxy: {
        type: "ajax",
        actionMethods: {
          read: "POST"
        },
        url: me.URL("SLN0001/DMO/dmobillList"),
        reader: {
          root: 'dataList',
          totalProperty: 'totalCount'
        }
      }
    });
    store.on("beforeload", () => {
      store.proxy.extraParams = me.getQueryParam();
    });
    store.on("load", (e, records, successful) => {
      if (successful) {
        me.gotoMainGridRecord(me._lastId);
      }
    });

    me._mainGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-FC",
      viewConfig: {
        enableTextSelection: true
      },
      border: 1,
      columnLines: true,
      columns: {
        defaults: {
          menuDisabled: true,
          sortable: false
        },
        items: [{
          xtype: "rownumberer",
          text: "#",
          width: 50
        }, {
          header: "状态",
          dataIndex: "billStatus",
          width: 110,
          renderer(value) {
            if (value == 0) {
              return "<span style='color:red'>待审核</span>";
            } else if (value == 1000) {
              return "已审核";
            } else if (value == 2000) {
              return "<span style='color:green'>部分入库</span>";
            } else if (value == 3000) {
              return "全部入库";
            } else if (value == 4000) {
              return "关闭(未入库)";
            } else if (value == 4001) {
              return "关闭(部分入库)";
            } else if (value == 4002) {
              return "关闭(全部入库)";
            } else {
              return "";
            }
          }
        }, {
          header: "成品委托生产订单号",
          dataIndex: "ref",
          width: 140
        }, {
          header: "交货日期",
          dataIndex: "dealDate",
          width: 90,
          algin: "center"
        }, {
          header: "交货地址",
          dataIndex: "dealAddress",
          width: 200
        }, {
          header: "工厂",
          dataIndex: "factoryName",
          width: 300
        }, {
          header: "工厂联系人",
          dataIndex: "contact"
        }, {
          header: "工厂电话",
          dataIndex: "tel"
        }, {
          header: "工厂传真",
          dataIndex: "fax"
        }, {
          header: "金额",
          dataIndex: "goodsMoney",
          align: "right",
          xtype: "numbercolumn",
          width: 90
        }, {
          header: "税金",
          dataIndex: "tax",
          align: "right",
          xtype: "numbercolumn",
          width: 90
        }, {
          header: "价税合计",
          dataIndex: "moneyWithTax",
          align: "right",
          xtype: "numbercolumn",
          width: 90
        }, {
          header: "付款方式",
          dataIndex: "paymentType",
          width: 100,
          renderer(value) {
            if (value == 0) {
              return "记应付账款";
            } else {
              return "";
            }
          }
        }, {
          header: "业务员",
          dataIndex: "bizUserName"
        }, {
          header: "组织机构",
          dataIndex: "orgName",
          width: 300,
        }, {
          header: "制单人",
          dataIndex: "inputUserName"
        }, {
          header: "制单时间",
          dataIndex: "dateCreated",
          width: 150
        }, {
          header: "审核人",
          dataIndex: "confirmUserName"
        }, {
          header: "审核时间",
          dataIndex: "confirmDate",
          width: 150
        }, {
          header: "备注",
          dataIndex: "billMemo"
        }]
      },
      store,
      listeners: {
        select: {
          fn: me._onMainGridSelect,
          scope: me
        },
        itemdblclick: {
          fn: me.getPermission().edit == "1"
            ? me._onEditBill
            : PCL.emptyFn,
          scope: me
        }
      }
    });

    return me._mainGrid;
  },

  /**
   * @private
   */
  getDetailGrid() {
    const me = this;
    if (me._detailGrid) {
      return me._detailGrid;
    }

    const modelName = me.buildModelName(me, "DMOBillDetail");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "goodsCode", "goodsName", "goodsSpec",
        "unitName", "goodsCount", "goodsMoney",
        "goodsPrice", "taxRate", "tax", "moneyWithTax",
        "dmwCount", "leftCount", "memo",
        "goodsPriceWithTax"]
    });
    const store = PCL.create("PCL.data.Store", {
      autoLoad: false,
      model: modelName,
      data: []
    });

    me._detailGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-HL",
      title: "订单明细",
      viewConfig: {
        enableTextSelection: true
      },
      columnLines: true,
      columns: {
        defaults: {
          menuDisabled: true,
          sortable: false
        },
        items: [PCL.create("PCL.grid.RowNumberer", {
          text: "#",
          width: 50
        }), {
          header: "物料编码",
          dataIndex: "goodsCode"
        }, {
          menuDisabled: true,
          draggable: false,
          sortable: false,
          header: "品名/规格型号",
          dataIndex: "goodsName",
          width: 330,
          renderer(value, metaData, record) {
            return record.get("goodsName") + " " + record.get("goodsSpec");
          }
        }, {
          header: "生产数量",
          dataIndex: "goodsCount",
          align: "right",
          width: 90
        }, {
          header: "入库数量",
          dataIndex: "dmwCount",
          align: "right",
          width: 90
        }, {
          header: "未入库数量",
          dataIndex: "leftCount",
          align: "right",
          width: 100,
          renderer(value) {
            if (value > 0) {
              return `<span style='color:red'>${value}</span>`;
            } else {
              return value;
            }
          }
        }, {
          header: "单位",
          dataIndex: "unitName",
          width: 60,
          align: "center"
        }, {
          header: "加工单价",
          dataIndex: "goodsPrice",
          align: "right",
          xtype: "numbercolumn",
          width: 90
        }, {
          header: "金额",
          dataIndex: "goodsMoney",
          align: "right",
          xtype: "numbercolumn",
          width: 90
        }, {
          header: "税率(%)",
          dataIndex: "taxRate",
          align: "right",
          xtype: "numbercolumn",
          format: "0",
          width: 60
        }, {
          header: "税金",
          dataIndex: "tax",
          align: "right",
          xtype: "numbercolumn",
          width: 90
        }, {
          header: "价税合计",
          dataIndex: "moneyWithTax",
          align: "right",
          xtype: "numbercolumn",
          width: 90
        }, {
          header: "含税价",
          dataIndex: "goodsPriceWithTax",
          align: "right",
          xtype: "numbercolumn",
          width: 90
        }, {
          header: "备注",
          dataIndex: "memo",
          width: 120
        }]
      },
      store,
    });

    return me._detailGrid;
  },

  /**
   * @private
   */
  refreshMainGrid(id) {
    const me = this;

    me.resetSummaryInput();

    me.buttonEdit?.setDisabled(true);
    me.buttonDelete?.setDisabled(true);
    me.buttonCommit?.setDisabled(true);
    me.buttonCancelConfirm?.setDisabled(true);
    me.buttonGenDMWBill?.setDisabled(true);

    const gridDetail = me.getDetailGrid();
    gridDetail.getStore().removeAll();

    me.pagingToobar.doRefresh();
    me._lastId = id;
  },

  /**
   * @private
   */
  _onAddBill() {
    const me = this;

    const form = PCL.create("PSI.SLN0001.DMO.DMOEditForm", {
      parentForm: me,
      showAddGoodsButton: me.getPermission().showAddGoodsButton,
      showAddFactoryButton: me.getPermission().showAddFactoryButton
    });
    form.show();
  },

  /**
   * @private
   */
  _onEditBill() {
    const me = this;
    const item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择要编辑的生产订单");
      return;
    }

    const bill = item[0];

    const form = PCL.create("PSI.SLN0001.DMO.DMOEditForm", {
      parentForm: me,
      entity: bill,
      showAddGoodsButton: me.getPermission().showAddGoodsButton,
      showAddFactoryButton: me.getPermission().showAddFactoryButton
    });
    form.show();
  },

  /**
   * @private
   */
  _onDeleteBill() {
    const me = this;
    const item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("请选择要删除的生产订单");
      return;
    }

    const bill = item[0];

    if (bill.get("billStatus") > 0) {
      me.showInfo("当前生产订单已经审核，不能删除");
      return;
    }

    const store = me.getMainGrid().getStore();
    let index = store.findExact("id", bill.get("id"));
    index--;
    let preIndex = null;
    const preItem = store.getAt(index);
    if (preItem) {
      preIndex = preItem.get("id");
    }

    const info = `请确认是否删除单号为：<span style='color:red'>${bill.get("ref")}</span> 的生产订单？`;
    const funcConfirm = () => {
      const el = PCL.getBody();
      el.mask("正在删除中...");
      const r = {
        url: me.URL("SLN0001/DMO/deleteDMOBill"),
        params: {
          id: bill.get("id")
        },
        callback(options, success, response) {
          el.unmask();

          if (success) {
            const data = me.decodeJSON(response.responseText);
            if (data.success) {
              me.refreshMainGrid(preIndex);
              me.tip("成功完成删除操作", true);
            } else {
              me.showInfo(data.msg);
            }
          } else {
            me.showInfo("网络错误");
          }
        }
      };
      me.ajax(r);
    };

    me.confirm(info, funcConfirm);
  },

  /**
   * @private
   */
  _onMainGridSelect() {
    const me = this;

    const item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.buttonEdit?.setDisabled(true);
      me.buttonDelete?.setDisabled(true);
      me.buttonCommit?.setDisabled(true);
      me.buttonCancelConfirm?.setDisabled(true);
      me.buttonGenDMWBill?.setDisabled(true);

      return;
    }

    const bill = item[0];
    const commited = bill.get("billStatus") >= 1000;

    const buttonEdit = me.buttonEdit;
    buttonEdit?.setDisabled(false);
    if (commited) {
      buttonEdit?.setText("查看成品委托生产订单");
    } else {
      buttonEdit?.setText("编辑成品委托生产订单");
    }

    me.buttonDelete?.setDisabled(commited);
    me.buttonCommit?.setDisabled(commited);
    me.buttonCancelConfirm?.setDisabled(!commited);
    me.buttonGenDMWBill?.setDisabled(!commited);

    me.editSummaryRef.setValue(bill.get("ref"));
    me.editSummaryFactory.setValue(bill.get("factoryName"));
    me.editSummaryDealDate.setValue(bill.get("dealDate"));

    me.refreshDetailGrid();
    me.refreshDMWGrid();
  },

  /**
   * @private
   */
  refreshDetailGrid(id) {
    const me = this;

    const item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      return;
    }

    const bill = item[0];

    const grid = me.getDetailGrid();
    const el = grid.getEl();
    el?.mask(PSI.Const.LOADING);

    const r = {
      url: me.URL("SLN0001/DMO/dmoBillDetailList"),
      params: {
        id: bill.get("id")
      },
      callback(options, success, response) {
        const store = grid.getStore();

        store.removeAll();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          store.add(data);

          if (store.getCount() > 0) {
            if (id) {
              const r = store.findExact("id", id);
              if (r != -1) {
                grid.getSelectionModel().select(r);
              }
            }
          }
        }

        el?.unmask();
      }
    };
    me.ajax(r);
  },

  /**
   * @private
   */
  _onCommit() {
    const me = this;
    const item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择要审核的生产订单");
      return;
    }
    const bill = item[0];

    if (bill.get("billStatus") > 0) {
      me.showInfo("当前生产订单已经审核，不能再次审核");
      return;
    }

    const detailCount = me.getDetailGrid().getStore().getCount();
    if (detailCount == 0) {
      me.showInfo("当前生产订单没有录入物料明细，不能审核");
      return;
    }

    const info = `请确认是否审核单号为 <span style='color:red'>${bill.get("ref")}</span> 的生产订单?`;
    const id = bill.get("id");

    const funcConfirm = () => {
      const el = PCL.getBody();
      el.mask("正在提交中...");
      const r = {
        url: me.URL("SLN0001/DMO/commitDMOBill"),
        params: {
          id: id
        },
        callback(options, success, response) {
          el.unmask();

          if (success) {
            const data = me.decodeJSON(response.responseText);
            if (data.success) {
              me.refreshMainGrid(id);
              me.tip("成功完成审核操作", true);
            } else {
              me.showInfo(data.msg);
            }
          } else {
            me.showInfo("网络错误");
          }
        }
      };
      me.ajax(r);
    };
    me.confirm(info, funcConfirm);
  },

  /**
   * @private
   */
  _onCancelConfirm() {
    const me = this;
    const item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择要取消审核的生产订单");
      return;
    }
    const bill = item[0];

    if (bill.get("billStatus") == 0) {
      me.showInfo("当前生产订单还没有审核，无法取消审核");
      return;
    }

    const info = `请确认是否取消审核单号为 <span style='color:red'>${bill.get("ref")}</span> 的生产订单?`;
    const id = bill.get("id");
    const funcConfirm = () => {
      const el = PCL.getBody();
      el.mask("正在提交中...");
      const r = {
        url: me.URL("SLN0001/DMO/cancelConfirmDMOBill"),
        params: {
          id: id
        },
        callback(options, success, response) {
          el.unmask();

          if (success) {
            const data = me.decodeJSON(response.responseText);
            if (data.success) {
              me.refreshMainGrid(id);
              me.tip("成功完成取消审核操作", true);
            } else {
              me.showInfo(data.msg);
            }
          } else {
            me.showInfo("网络错误");
          }
        }
      };
      me.ajax(r);
    };
    me.confirm(info, funcConfirm);
  },

  /**
   * @private
   */
  gotoMainGridRecord(id) {
    const me = this;
    const grid = me.getMainGrid();
    grid.getSelectionModel().deselectAll();
    const store = grid.getStore();
    if (id) {
      const r = store.findExact("id", id);
      if (r != -1) {
        grid.getSelectionModel().select(r);
      } else {
        grid.getSelectionModel().select(0);
      }
    } else {
      grid.getSelectionModel().select(0);
    }
  },

  /**
   * @private
   */
  _onQuery() {
    const me = this;

    me.getMainGrid().getStore().currentPage = 1;
    me.refreshMainGrid();
  },

  /**
   * @private
   */
  _onClearQuery() {
    const me = this;

    me.editQueryBillStatus.setValue(-1);
    me.editQueryRef.setValue(null);
    me.editQueryFromDT.setValue(null);
    me.editQueryToDT.setValue(null);
    me.editQueryFactory.clearIdValue();
    me.editQueryGoods.clearIdValue();

    me._onQuery();
  },

  /**
   * @private
   */
  getQueryParam() {
    const me = this;

    const result = {
      billStatus: me.editQueryBillStatus.getValue()
    };

    const ref = me.editQueryRef.getValue();
    if (ref) {
      result.ref = ref;
    }

    const factoryId = me.editQueryFactory.getIdValue();
    if (factoryId) {
      result.factoryId = factoryId;
    }

    const fromDT = me.editQueryFromDT.getValue();
    if (fromDT) {
      result.fromDT = PCL.Date.format(fromDT, "Y-m-d");
    }

    const toDT = me.editQueryToDT.getValue();
    if (toDT) {
      result.toDT = PCL.Date.format(toDT, "Y-m-d");
    }

    const goodsId = me.editQueryGoods.getIdValue();
    if (goodsId) {
      result.goodsId = goodsId;
    }

    return result;
  },

  /**
   * @private
   */
  _onGenDMWBill() {
    const me = this;
    const item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择要生成入库单的生产订单");
      return;
    }
    const bill = item[0];

    if (bill.get("billStatus") < 1000) {
      me.showInfo("当前生产订单还没有审核，无法生成成品委托生产入库单");
      return;
    }

    if (bill.get("billStatus") >= 4000) {
      me.showInfo("当前生产订单已经关闭，不能再生成成品委托生产入库单");
      return;
    }

    const form = PCL.create("PSI.SLN0001.DMW.DMWEditForm", {
      genBill: true,
      dmobillRef: bill.get("ref"),
      dmoKeyMap: me._keyMapMain,
    });
    form.show();
  },

  /**
   * @private
   */
  _onPDF() {
    const me = this;
    const item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择要生成pdf文件的生产订单");
      return;
    }

    const bill = item[0];

    const url = me.URL("SLN0001/DMO/dmoBillPdf?ref=" + bill.get("ref"));
    window.open(url);

    me.focus();
  },

  /**
   * @private
   */
  getDMWGrid() {
    const me = this;
    if (me._dmwGrid) {
      return me._dmwGrid;
    }

    const modelName = me.buildModelName(me, "DMOBill_DMWBill");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "ref", "bizDate", "factoryName",
        "warehouseName", "inputUserName", "bizUserName",
        "billStatus", "amount", "dateCreated",
        "paymentType"]
    });
    const store = PCL.create("PCL.data.Store", {
      autoLoad: false,
      model: modelName,
      data: []
    });

    me._dmwGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-HL",
      title: "入库详情",
      viewConfig: {
        enableTextSelection: true
      },
      columnLines: true,
      columns: {
        defaults: {
          menuDisabled: true,
          sortable: false
        },
        items: [{
          xtype: "rownumberer",
          width: 50
        }, {
          header: "状态",
          dataIndex: "billStatus",
          width: 60,
          renderer(value) {
            if (value == "待入库") {
              return `<span style='color:red'>${value}</span>`;
            } else if (value == "已退货") {
              // TODO 目前还没有处理退货
              return `<span style='color:blue'>${value}</span>`;
            } else {
              return value;
            }
          }
        }, {
          header: "入库单号",
          dataIndex: "ref",
          width: 130,
          renderer(value, md, record) {
            const href = me.URL("SLN0001/Bill/viewIndex?fid=2036&refType=成品委托生产入库");
            return `<a href='${href}&ref=${encodeURIComponent(record.get("ref"))}' 
              target='_blank'>${value}</a>`;
          }
        }, {
          header: "业务日期",
          dataIndex: "bizDate",
          width: 90,
          align: "center"
        }, {
          header: "工厂",
          dataIndex: "factoryName",
          width: 300
        }, {
          header: "金额",
          dataIndex: "amount",
          align: "right",
          xtype: "numbercolumn",
          width: 90
        }, {
          header: "付款方式",
          dataIndex: "paymentType",
          width: 100,
          renderer(value) {
            if (value == 0) {
              return "记应付账款";
            } else {
              return "";
            }
          }
        }, {
          header: "入库仓库",
          dataIndex: "warehouseName",
          width: 150
        }, {
          header: "业务员",
          dataIndex: "bizUserName"
        }, {
          header: "制单人",
          dataIndex: "inputUserName"
        }, {
          header: "制单时间",
          dataIndex: "dateCreated",
          width: 150
        }]
      },
      store,
    });

    return me._dmwGrid;
  },

  /**
   * @private
   */
  refreshDMWGrid() {
    const me = this;
    const item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      return;
    }

    const bill = item[0];

    const grid = me.getDMWGrid();
    const el = grid.getEl();
    el?.mask(PSI.Const.LOADING);

    const r = {
      url: me.URL("SLN0001/DMO/dmoBillDMWBillList"),
      params: {
        id: bill.get("id")
      },
      callback(options, success, response) {
        const store = grid.getStore();

        store.removeAll();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          store.add(data);
        }

        el?.unmask();
      }
    };
    me.ajax(r);
  },

  /**
   * @private
   */
  _onCloseDMO() {
    const me = this;
    const item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择要关闭的生产订单");
      return;
    }
    const bill = item[0];

    const info = `请确认是否关闭单号为 <span style='color:red'>${bill.get("ref")}</span> 的生产订单?`;
    const id = bill.get("id");

    const funcConfirm = () => {
      const el = PCL.getBody();
      el.mask("正在提交中...");
      const r = {
        url: me.URL("SLN0001/DMO/closeDMOBill"),
        params: {
          id: id
        },
        callback(options, success, response) {
          el.unmask();

          if (success) {
            const data = me.decodeJSON(response.responseText);
            if (data.success) {
              me.refreshMainGrid(id);
              me.tip("成功关闭生产订单", true);
            } else {
              me.showInfo(data.msg);
            }
          } else {
            me.showInfo("网络错误");
          }
        }
      };
      me.ajax(r);
    };
    me.confirm(info, funcConfirm);
  },

  /**
   * @private
   */
  _onCancelClosedDMO() {
    const me = this;
    const item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择要取消关闭状态的生产订单");
      return;
    }
    const bill = item[0];

    const info = `请确认是否取消单号为 <span style='color:red'>${bill.get("ref")}</span> 生产订单的关闭状态?`;
    const id = bill.get("id");

    const funcConfirm = () => {
      const el = PCL.getBody();
      el.mask("正在提交中...");
      const r = {
        url: me.URL("SLN0001/DMO/cancelClosedDMOBill"),
        params: {
          id: id
        },
        callback(options, success, response) {
          el.unmask();

          if (success) {
            const data = me.decodeJSON(response.responseText);
            if (data.success) {
              me.refreshMainGrid(id);
              me.tip("成功取消生产订单关闭状态", true);
            } else {
              me.showInfo(data.msg);
            }
          } else {
            me.showInfo("网络错误");
          }
        }
      };
      me.ajax(r);
    };
    me.confirm(info, funcConfirm);
  },

  /**
   * @private
   */
  _onPrintPreview() {
    const me = this;

    if (PSI.Const.ENABLE_LODOP != "1") {
      me.showInfo("请先在业务设置模块中启用Lodop打印");
      return;
    }

    const lodop = getLodop();
    if (!lodop) {
      me.showInfo("没有安装Lodop控件，无法打印");
      return;
    }

    const item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择要打印的成品委托生产订单");
      return;
    }
    const bill = item[0];

    const el = PCL.getBody();
    el.mask("数据加载中...");
    const r = {
      url: me.URL("SLN0001/DMO/genDMOBillPrintPage"),
      params: {
        id: bill.get("id")
      },
      callback(options, success, response) {
        el.unmask();

        if (success) {
          const data = response.responseText;
          me.previewDMOBill(bill.get("ref"), data);
        }
      }
    };
    me.ajax(r);
  },

  PRINT_PAGE_WIDTH: "200mm",
  PRINT_PAGE_HEIGHT: "95mm",

  /**
   * @private
   */
  previewDMOBill(ref, data) {
    const me = this;

    const lodop = getLodop();
    if (!lodop) {
      me.showInfo("Lodop打印控件没有正确安装");
      return;
    }

    lodop.PRINT_INIT("成品委托生产订单" + ref);
    lodop.SET_PRINT_PAGESIZE(1, me.PRINT_PAGE_WIDTH, me.PRINT_PAGE_HEIGHT,
      "");
    lodop.ADD_PRINT_HTM("0mm", "0mm", "100%", "100%", data);
    lodop.PREVIEW("_blank");
  },

  /**
   * @private
   */
  _onPrint() {
    const me = this;

    if (PSI.Const.ENABLE_LODOP != "1") {
      me.showInfo("请先在业务设置模块中启用Lodop打印");
      return;
    }

    const lodop = getLodop();
    if (!lodop) {
      me.showInfo("没有安装Lodop控件，无法打印");
      return;
    }

    const item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择要打印的成品委托生产订单");
      return;
    }
    const bill = item[0];

    const el = PCL.getBody();
    el.mask("数据加载中...");
    const r = {
      url: me.URL("SLN0001/DMO/genDMOBillPrintPage"),
      params: {
        id: bill.get("id")
      },
      callback(options, success, response) {
        el.unmask();

        if (success) {
          const data = response.responseText;
          me.printDMOBill(bill.get("ref"), data);
        }
      }
    };
    me.ajax(r);
  },

  /**
   * @private
   */
  printDMOBill(ref, data) {
    const me = this;

    const lodop = getLodop();
    if (!lodop) {
      me.showInfo("Lodop打印控件没有正确安装");
      return;
    }

    lodop.PRINT_INIT("成品委托生产订单" + ref);
    lodop.SET_PRINT_PAGESIZE(1, me.PRINT_PAGE_WIDTH, me.PRINT_PAGE_HEIGHT,
      "");
    lodop.ADD_PRINT_HTM("0mm", "0mm", "100%", "100%", data);
    lodop.PRINT();
  }
});
