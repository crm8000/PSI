/**
 * 采购退货出库单-选择采购入库单界面
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.PurchaseRej.PRSelectPWBillForm", {
  extend: "PSI.AFX.Form.EditForm",

  /**
   * @override
   */
  initComponent() {
    const me = this;
    PCL.apply(me, {
      header: {
        title: me.formatTitle("选择要退货的采购入库单"),
        height: 40
      },
      width: 1200,
      height: 600,
      layout: "border",
      items: [{
        region: "center",
        border: 0,
        bodyPadding: 10,
        layout: "border",
        items: [{
          region: "north",
          height: "50%",
          layout: "fit",
          border: 0,
          split: true,
          items: [me.getPWBillGrid()]
        }, {
          region: "center",
          layout: "fit",
          border: 0,
          items: [me.getPWBillDetailGrid()]
        }]
      }, {
        region: "north",
        border: 0,
        layout: {
          type: "table",
          columns: 5
        },
        height: 65,
        bodyPadding: 10,
        items: [{
          id: me.buildId(me, "editPWRef"),
          xtype: "textfield",
          labelAlign: "right",
          labelSeparator: "",
          fieldLabel: "采购入库单单号"
        }, {
          id: me.buildId(me, "editPWSupplier"),
          xtype: "psi_supplierfield",
          showModal: true,
          labelAlign: "right",
          labelSeparator: "",
          fieldLabel: "供应商",
          colspan: 2,
          width: 400,
          labelWidth: 60,
        }, {
          id: me.buildId(me, "editPWFromDT"),
          xtype: "datefield",
          format: "Y-m-d",
          labelAlign: "right",
          labelSeparator: "",
          fieldLabel: "业务日期（起）",
          width: 250,
          labelWidth: 110,
        }, {
          id: me.buildId(me, "editPWToDT"),
          xtype: "datefield",
          format: "Y-m-d",
          labelAlign: "right",
          labelSeparator: "",
          fieldLabel: "业务日期（止）",
          width: 250,
          labelWidth: 110,
        }, {
          id: me.buildId(me, "editPWWarehouse"),
          xtype: "psi_warehousefield",
          showModal: true,
          labelAlign: "right",
          labelSeparator: "",
          fieldLabel: "仓库",
        }, {
          xtype: "container",
          items: [{
            xtype: "button",
            text: "查询",
            ...PSI.Const.BTN_STYLE,
            width: 100,
            margin: "0 0 0 40",
            iconCls: "PSI-button-refresh",
            handler: me._onQuery,
            scope: me
          }, {
            xtype: "button",
            text: "清空查询条件",
            ...PSI.Const.BTN_STYLE,
            width: 100,
            margin: "0, 0, 0, 10",
            handler: me._onClearQuery,
            scope: me
          }]
        }]
      }],
      listeners: {
        show: {
          fn: me._onWndShow,
          scope: me
        },
        close: {
          fn: me._onWndClose,
          scope: me
        }
      },
      buttons: [{
        text: "选择",
        ...PSI.Const.BTN_STYLE,
        iconCls: "PSI-button-ok",
        formBind: true,
        handler: me._onOK,
        scope: me
      }, {
        text: "取消",
        ...PSI.Const.BTN_STYLE,
        handler() {
          me.close();
        },
        scope: me
      }]
    });

    me.callParent(arguments);

    me.editPWRef = PCL.getCmp(me.buildId(me, "editPWRef"));
    me.editPWSupplier = PCL.getCmp(me.buildId(me, "editPWSupplier"));
    me.editPWWarehouse = PCL.getCmp(me.buildId(me, "editPWWarehouse"));
    me.editPWFromDT = PCL.getCmp(me.buildId(me, "editPWFromDT"));
    me.editPWToDT = PCL.getCmp(me.buildId(me, "editPWToDT"));
  },

  /**
   * @private
   */
  _onWndShow() {
    const me = this;

    const parentForm = me.getParentForm();
    if (parentForm) {
      parentForm._keyMap.disable();
    }

    me.editPWRef.focus();
  },

  /**
   * @private
   */
  _onWndClose() {
    const me = this;

    const parentForm = me.getParentForm();
    if (parentForm) {
      parentForm._keyMap.enable();
    }
  },

  /**
   * @private
   */
  _onOK() {
    const me = this;

    const item = me.getPWBillGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("请选择采购入库单");
      return;
    }

    const bill = item[0];
    me.close();
    const parentForm = me.getParentForm();
    if (parentForm) {
      parentForm.getPWBillInfo.apply(parentForm, [bill.get("id")]);
    }
  },

  /**
   * @private
   */
  getPWBillGrid() {
    const me = this;

    if (me._billGrid) {
      return me._billGrid;
    }

    const modelName = me.buildModelName(me, "PRBill_PWSelectForm");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "ref", "bizDate", "supplierName",
        "warehouseName", "inputUserName", "bizUserName",
        "amount", "tax", "moneyWithTax"]
    });
    const store = PCL.create("PCL.data.Store", {
      autoLoad: false,
      model: modelName,
      data: [],
      pageSize: 20,
      proxy: {
        type: "ajax",
        actionMethods: {
          read: "POST"
        },
        url: me.URL("SLN0001/PurchaseRej/selectPWBillList"),
        reader: {
          root: 'dataList',
          totalProperty: 'totalCount'
        }
      }
    });
    store.on("beforeload", () => {
      store.proxy.extraParams = me.getQueryParam();
    });

    me._billGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-FC",
      columnLines: true,
      columns: [PCL.create("PCL.grid.RowNumberer", {
        text: "#",
        width: 50
      }), {
        header: "单号",
        dataIndex: "ref",
        width: 120,
        menuDisabled: true,
        sortable: false
      }, {
        header: "业务日期",
        dataIndex: "bizDate",
        menuDisabled: true,
        sortable: false
      }, {
        header: "供应商",
        dataIndex: "supplierName",
        width: 200,
        menuDisabled: true,
        sortable: false
      }, {
        header: "采购金额(不含税)",
        dataIndex: "amount",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 130
      }, {
        header: "税金",
        dataIndex: "tax",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 80
      }, {
        header: "采购金额(含税)",
        dataIndex: "moneyWithTax",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 130
      }, {
        header: "入库仓库",
        dataIndex: "warehouseName",
        menuDisabled: true,
        sortable: false
      }, {
        header: "业务员",
        dataIndex: "bizUserName",
        menuDisabled: true,
        sortable: false
      }, {
        header: "制单人",
        dataIndex: "inputUserName",
        menuDisabled: true,
        sortable: false
      }],
      listeners: {
        select: {
          fn: me._onPWBillGridSelect,
          scope: me
        },
        itemdblclick: {
          fn: me._onOK,
          scope: me
        }
      },
      store: store,
      tbar: ["->", {
        id: me.buildId(me, "pagingToobar"),
        xtype: "pagingtoolbar",
        border: 0,
        store: store
      }, "-", {
          xtype: "displayfield",
          value: "每页显示"
        }, {
          id: me.buildId(me, "comboCountPerPage"),
          xtype: "combobox",
          editable: false,
          width: 60,
          store: PCL.create("PCL.data.ArrayStore", {
            fields: ["text"],
            data: [["20"], ["50"], ["100"], ["300"],
            ["1000"]]
          }),
          value: 20,
          listeners: {
            change: {
              fn() {
                store.pageSize = PCL.getCmp(me.buildId(me, "comboCountPerPage")).getValue();
                store.currentPage = 1;
                PCL.getCmp(me.buildId(me, "pagingToobar")).doRefresh();
              },
              scope: me
            }
          }
        }, {
          xtype: "displayfield",
          value: "条记录"
        }]
    });

    return me._billGrid;
  },

  /**
   * @private
   */
  getPWBillDetailGrid() {
    const me = this;

    if (me._pwbillDetailGrid) {
      return me._pwbillDetailGrid;
    }

    const modelName = me.buildModelName(me, "PWBillDetail");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "goodsCode", "goodsName", "goodsSpec",
        "unitName", "goodsCount", "goodsMoney",
        "goodsPrice", "memo", "taxRate", "tax", "priceWithTax", "moneyWithTax",
        "rejGoodsCount", "realGoodsCount"]
    });
    const store = PCL.create("PCL.data.Store", {
      autoLoad: false,
      model: modelName,
      data: []
    });

    me._pwbillDetailGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-HL",
      title: "采购入库单明细",
      viewConfig: {
        enableTextSelection: true
      },
      columnLines: true,
      columns: [PCL.create("PCL.grid.RowNumberer", {
        text: "#",
        width: 40
      }), {
        header: "物料编码",
        dataIndex: "goodsCode",
        menuDisabled: true,
        sortable: false,
        width: 120
      }, {
        header: "品名",
        dataIndex: "goodsName",
        menuDisabled: true,
        sortable: false,
        width: 200
      }, {
        header: "规格型号",
        dataIndex: "goodsSpec",
        menuDisabled: true,
        sortable: false,
        width: 200
      }, {
        header: "入库数量",
        width: 120,
        dataIndex: "goodsCount",
        menuDisabled: true,
        sortable: false,
        align: "right"
      }, {
        header: "退货数量",
        width: 120,
        dataIndex: "rejGoodsCount",
        menuDisabled: true,
        sortable: false,
        align: "right"
      }, {
        header: "实际入库数量",
        width: 120,
        dataIndex: "realGoodsCount",
        menuDisabled: true,
        sortable: false,
        align: "right"
      }, {
        header: "单位",
        dataIndex: "unitName",
        menuDisabled: true,
        sortable: false,
        width: 60
      }, {
        header: "采购单价(不含税)",
        dataIndex: "goodsPrice",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 150
      }, {
        header: "采购金额(不含税)",
        dataIndex: "goodsMoney",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 150
      }, {
        header: "税率(%)",
        dataIndex: "taxRate",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 80,
        format: "#"
      }, {
        header: "税金",
        dataIndex: "tax",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 100
      }, {
        header: "采购单价(含税)",
        dataIndex: "priceWithTax",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 150
      }, {
        header: "采购金额(含税)",
        dataIndex: "moneyWithTax",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 150
      }, {
        header: "备注",
        dataIndex: "memo",
        menuDisabled: true,
        sortable: false,
        width: 200
      }],
      store: store
    });

    return me._pwbillDetailGrid;
  },

  /**
   * @private
   */
  _onQuery() {
    const me = this;

    PCL.getCmp(me.buildId(me, "pagingToobar")).doRefresh();

    me.refreshDetailGrid();
  },

  /**
   * @private
   */
  getQueryParam() {
    const me = this;

    const result = {};

    const ref = me.editPWRef.getValue();
    if (ref) {
      result.ref = ref;
    }

    const supplierId = me.editPWSupplier.getIdValue();
    if (supplierId) {
      result.supplierId = supplierId;
    }

    const warehouseId = me.editPWWarehouse.getIdValue();
    if (warehouseId) {
      result.warehouseId = warehouseId;
    }

    const fromDT = me.editPWFromDT.getValue();
    if (fromDT) {
      result.fromDT = PCL.Date.format(fromDT, "Y-m-d");
    }

    const toDT = me.editPWToDT.getValue();
    if (toDT) {
      result.toDT = PCL.Date.format(toDT, "Y-m-d");
    }

    return result;
  },

  /**
   * @private
   */
  _onClearQuery() {
    const me = this;

    me.editPWRef.setValue(null);
    me.editPWSupplier.clearIdValue();
    me.editPWWarehouse.clearIdValue();
    me.editPWFromDT.setValue(null);
    me.editPWToDT.setValue(null);

    me._onQuery();
  },

  /**
   * @private
   */
  _onPWBillGridSelect() {
    const me = this;

    me.getPWBillDetailGrid().setTitle("采购入库单明细");

    me.refreshDetailGrid();
  },

  /**
   * @private
   */
  refreshDetailGrid() {
    const me = this;

    me.getPWBillDetailGrid().setTitle("采购入库单明细");
    me.getPWBillDetailGrid().getStore().removeAll();
    const item = me.getPWBillGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      return;
    }

    const bill = item[0];

    const grid = me.getPWBillDetailGrid();
    grid.setTitle("单号: " + bill.get("ref") + " 供应商: "
      + bill.get("supplierName") + " 入库仓库: "
      + bill.get("warehouseName"));
    const el = grid.getEl();
    el?.mask(PSI.Const.LOADING);

    const r = {
      url: me.URL("SLN0001/PurchaseRej/pwBillDetailList"),
      params: {
        pwBillId: bill.get("id")
      },
      callback(options, success, response) {
        const store = grid.getStore();

        store.removeAll();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          store.add(data);
        }

        el?.unmask();
      }
    };

    me.ajax(r);
  }
});
