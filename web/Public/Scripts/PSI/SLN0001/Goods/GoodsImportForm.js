/**
 * 物料导入
 * 
 * @author 张健
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.Goods.GoodsImportForm", {
  extend: "PSI.AFX.Form.EditForm",

  /**
   * 初始化组件
   * 
   * @override
   */
  initComponent() {
    const me = this;

    const buttons = [];

    buttons.push({
      text: "导入物料",
      ...PSI.Const.BTN_STYLE,
      formBind: true,
      iconCls: "PSI-button-ok",
      handler() {
        me._onOK();
      },
      scope: me
    }, {
      text: "取消",
      ...PSI.Const.BTN_STYLE,
      handler() {
        me.confirm("请确认是否取消操作：导入物料？", () => {
          me.close();
        })
      },
      scope: me
    });

    PCL.apply(me, {
      header: {
        title: me.formatTitle(PSI.Const.PROD_NAME),
        height: 40
      },
      width: 512,
      height: 170,
      layout: "fit",
      items: [{
        id: "importForm",
        xtype: "form",
        border: 0,
        layout: {
          type: "table",
          columns: 1
        },
        height: "100%",
        bodyPadding: 5,
        fieldDefaults: {
          labelWidth: 60,
          labelAlign: "right",
          labelSeparator: "",
          msgTarget: 'side'
        },
        items: [{
          xtype: 'filefield',
          name: 'data_file',
          afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="必需填写">*</span>',
          fieldLabel: '文件',
          labelWidth: 50,
          width: 480,
          msgTarget: 'side',
          allowBlank: false,
          anchor: '100%',
          buttonText: '选择物料文件'
        }, {
          html: `<a href="../Uploads/Goods/goods_template.xlsx"><h4>下载物料导入模板</h4></a>`,
          border: 0
        }],
        buttons,
      }],
      listeners: {
        close: {
          fn: me._onWndClose,
          scope: me
        },
        show: {
          fn: me._onWndShow,
          scope: me
        }
      }
    });

    me.callParent(arguments);
  },

  /**
   * @private
   */
  _onOK() {
    const me = this;
    const f = PCL.getCmp("importForm");
    const el = f.getEl();
    el && el.mask('正在导入...');
    f.submit({
      url: me.URL("SLN0001/Goods/import"),
      method: "POST",
      success(form, action) {
        el && el.unmask();

        me.showInfo("数据导入成功");

        me.close();
        me.getParentForm().freshGoodsGrid();
      },
      failure(form, action) {
        el && el.unmask();
        me.showInfo(action.result.msg);
      }
    });
  },

  /**
   * @private
   */
  _onWndShow() {
    const me = this;

    const parentForm = me.getParentForm();
    if (parentForm) {
      parentForm._keyMapMain.disable();
    }
  },

  /**
   * @private
   */
  _onWndClose() {
    const me = this;

    const parentForm = me.getParentForm();
    if (parentForm) {
      parentForm._keyMapMain.enable();
    }
  },
});
