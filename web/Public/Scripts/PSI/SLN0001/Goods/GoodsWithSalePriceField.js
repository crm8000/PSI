﻿/**
 * 自定义字段 - 商品字段，带销售价格
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.Goods.GoodsWithSalePriceField", {
  extend: "PCL.form.field.Trigger",
  alias: "widget.psi_goods_with_saleprice_field",

  mixins: ["PSI.AFX.Mix.Common"],

  config: {
    parentCmp: null,
    editCustomerName: null,
    editWarehouseName: null,
    showAddButton: false,
    sumInv: "0", // 是否合计当前库存：0 - 不合计；1 - 合计
    goodsInfoCallbackFunc: null,
    goodsInfoCallbackFuncScope: null,
  },

  /**
   * 初始化组件
   * 
   * @override
   */
  initComponent() {
    var me = this;

    me.enableKeyEvents = true;

    me.callParent(arguments);

    me.on("keydown", (field, e) => {
      if (e.getKey() == e.BACKSPACE) {
        field.setValue(null);
        e.preventDefault();
        return false;
      }

      if (e.getKey() != e.ENTER && !e.isSpecialKey(e.getKey())) {
        this.onTriggerClick(e);
      }
    });

    me.on({
      render(p) {
        p.getEl().on("dblclick", () => {
          me.selectText(0, 0);
          me.onTriggerClick();
        });
      },
      single: true
    });
  },

  /**
   * 单击下拉组件
   * 
   * @override
   */
  onTriggerClick(e) {
    var me = this;
    var modelName = "PSIGoodsField";
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "code", "name", "spec", "unitName",
        "salePrice", "memo", "priceSystem", "taxRate", "invCnt"]
    });

    var store = PCL.create("PCL.data.Store", {
      model: modelName,
      autoLoad: false,
      data: []
    });
    var lookupGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-Lookup",
      columnLines: true,
      border: 1,
      store: store,
      viewConfig: {
        enableTextSelection: true
      },
      columns: [{
        header: "编码",
        dataIndex: "code",
        menuDisabled: true,
        width: 100
      }, {
        header: "商品",
        dataIndex: "name",
        menuDisabled: true,
        flex: 1
      }, {
        header: "规格型号",
        dataIndex: "spec",
        menuDisabled: true,
        flex: 1
      }, {
        header: me.getSumInv() == "1" ? "当前库存合计" : "当前库存",
        dataIndex: "invCnt",
        menuDisabled: true,
        align: "right",
        width: 100
      }, {
        header: "单位",
        dataIndex: "unitName",
        menuDisabled: true,
        width: 60
      }, {
        header: "销售价",
        dataIndex: "salePrice",
        menuDisabled: true,
        align: "right",
        xtype: "numbercolumn"
      }, {
        header: "价格体系",
        dataIndex: "priceSystem",
        menuDisabled: true,
        width: 80
      }, {
        header: "备注",
        dataIndex: "memo",
        menuDisabled: true,
        width: 200
      }]
    });
    me.lookupGrid = lookupGrid;
    me.lookupGrid.on("itemdblclick", me.onOK, me);

    var buttons = [{
      xtype: "container",
      html: `
        <div class="PSI-lookup-note">
          输入编码、品名拼音字头、规格型号拼音字头可以过滤查询；
          ↑ ↓ 键改变当前选择项 ；回车键返回
        </div>
        `
    }, "->"];
    if (me.getShowAddButton()) {
      buttons.push({
        text: "新建商品",
        cls: "PSI-Lookup-btn",
        handler: me.onAddGoods,
        scope: me
      });
    }
    buttons.push({
      text: "确定",
      cls: "PSI-Lookup-btn",
      handler: me.onOK,
      scope: me
    }, {
      text: "取消",
      cls: "PSI-Lookup-btn",
      handler() {
        wnd.close();
        me.focus();
      }
    });

    var wnd = PCL.create("PCL.window.Window", {
      title: "选择 - 商品",
      header: false,
      border: 0,
      width: 1050,
      height: 300,
      layout: "border",
      onEsc() {
        me.wnd.hide();
        me.focus();
        me.wnd.close();
      },
      items: [{
        region: "center",
        xtype: "panel",
        layout: "fit",
        border: 0,
        items: [lookupGrid]
      }, {
        xtype: "panel",
        region: "south",
        height: 30,
        layout: "fit",
        border: 0,
        items: [{
          xtype: "form",
          layout: "form",
          bodyPadding: 5,
          bodyCls: "PSI-Field",
          items: [{
            id: "__editGoods",
            xtype: "textfield",
            labelWidth: 0,
            labelAlign: "right",
            labelSeparator: ""
          }]
        }]
      }],
      buttons: buttons
    });

    var customerId = null;
    var editCustomer = PCL.getCmp(me.getEditCustomerName());
    if (editCustomer) {
      customerId = editCustomer.getIdValue();
    }

    var warehouseId = null;
    var editWarehouse = PCL.getCmp(me.getEditWarehouseName());
    if (editWarehouse) {
      warehouseId = editWarehouse.getIdValue();
    }

    wnd.on("close", () => {
      me.focus();
    });
    wnd.on("deactivate", () => {
      wnd.close();
    });

    me.wnd = wnd;

    var editName = PCL.getCmp("__editGoods");
    editName.on("change", () => {
      if (editName._inIME) {
        return;
      }

      var store = me.lookupGrid.getStore();
      PCL.Ajax.request({
        url: PSI.Const.BASE_URL
          + "SLN0001/Goods/queryDataWithSalePrice",
        params: {
          queryKey: editName.getValue(),
          customerId: customerId,
          warehouseId: warehouseId,
          sumInv: me.getSumInv(),
        },
        method: "POST",
        callback(opt, success, response) {
          store.removeAll();
          if (success) {
            var data = PCL.JSON.decode(response.responseText);
            store.add(data);
            if (data.length > 0) {
              me.lookupGrid.getSelectionModel().select(0);
              editName.focus();
            }
          } else {
            PSI.MsgBox.showInfo("网络错误");
          }
        },
        scope: this
      });

    }, me);

    editName.on("specialkey", (field, e) => {
      if (e.getKey() == e.ENTER) {
        me.onOK();
      } else if (e.getKey() == e.UP) {
        var m = me.lookupGrid.getSelectionModel();
        var store = me.lookupGrid.getStore();
        var index = 0;
        for (var i = 0; i < store.getCount(); i++) {
          if (m.isSelected(i)) {
            index = i;
          }
        }
        index--;
        if (index < 0) {
          index = 0;
        }
        m.select(index);
        e.preventDefault();
        editName.focus();
      } else if (e.getKey() == e.DOWN) {
        var m = me.lookupGrid.getSelectionModel();
        var store = me.lookupGrid.getStore();
        var index = 0;
        for (var i = 0; i < store.getCount(); i++) {
          if (m.isSelected(i)) {
            index = i;
          }
        }
        index++;
        if (index > store.getCount() - 1) {
          index = store.getCount() - 1;
        }
        m.select(index);
        e.preventDefault();
        editName.focus();
      }
    }, me);

    me.wnd.on("show", () => {
      me.applyIMEHandler(editName);

      editName.focus();
      editName.fireEvent("change");
    }, me);
    wnd.showBy(me);
  },

  onOK() {
    const me = this;

    const grid = me.lookupGrid;
    const item = grid.getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      return;
    }

    const data = item[0].getData();

    me.wnd.close();
    me.focus();
    me.setValue(data.code);
    me.focus();

    const callbackFunc = me.getGoodsInfoCallbackFunc();
    if (callbackFunc) {
      callbackFunc.apply(me.getGoodsInfoCallbackFuncScope(), [data]);
    }
  },

  onAddGoods() {
    const form = PCL.create("PSI.SLN0001.Goods.GoodsEditForm");

    form.show();
  }
});
