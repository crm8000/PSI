# SLN0002 - 财务总账 - 码表

# ------------------------------------------------------------------------------
# 码表分类
# ------------------------------------------------------------------------------
DELETE FROM t_code_table_category where id = '1BEE661C-5100-11EE-89B4-E86A641ED142';
INSERT INTO t_code_table_category(id, code, name, parent_id, is_system, sln_code)
VALUES ('1BEE661C-5100-11EE-89B4-E86A641ED142', 'SLN0002.01', '银行', NULL, 1, 'SLN0002');

# 凭证
DELETE FROM t_code_table_category where id = '86544F8D-851C-11EF-8872-E86A641ED142';
INSERT INTO t_code_table_category(id, code, name, parent_id, is_system, sln_code)
VALUES ('86544F8D-851C-11EF-8872-E86A641ED142', 'SLN0002.02', '记账凭证', NULL, 1, 'SLN0002');

# ------------------------------------------------------------------------------
# 码表：凭证字
# 元数据
# ------------------------------------------------------------------------------
DELETE FROM `t_code_table_md` where `id` = '551ED336-851F-11EF-8872-E86A641ED142';
INSERT INTO `t_code_table_md` (`id`, `code`, `name`, `table_name`, `category_id`, `memo`, `py`, `fid`, `md_version`, `is_fixed`, `enable_parent_id`, `handler_class_name`, `module_name`, `edit_col_cnt`, `view_paging`, `input_company`, `sln_code`, `module_description`)
VALUES ('551ED336-851F-11EF-8872-E86A641ED142', 'SLN0002.02.01', '凭证字', 't_sln0002_ct_voucher_word', '86544F8D-851C-11EF-8872-E86A641ED142', '', 'PZZ', 'ct20241008104509', 1, 1, 0, '', '凭证字', 1, 2, 2, 'SLN0002', '');

DELETE FROM `t_code_table_cols_md` where `table_id` = '551ED336-851F-11EF-8872-E86A641ED142';
INSERT INTO `t_code_table_cols_md` (`id`, `table_id`, `caption`, `db_field_name`, `db_field_type`, `db_field_length`, `db_field_decimal`, `show_order`, `value_from`, `value_from_table_name`, `value_from_col_name`, `value_from_col_name_display`, `must_input`, `sys_col`, `is_visible`, `width_in_view`, `note`, `show_order_in_view`, `editor_xtype`, `col_span`, `default_value`, `default_value_ext`, `label_width`) VALUES
('551EF11A-851F-11EF-8872-E86A641ED142', '551ED336-851F-11EF-8872-E86A641ED142', 'id', 'id', 'varchar', 255, 0, -1000, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, '', 90),
('551F0370-851F-11EF-8872-E86A641ED142', '551ED336-851F-11EF-8872-E86A641ED142', '编码', 'code', 'varchar', 255, 0, 0, 1, '', '', '', 2, 1, 1, 120, '', 0, 'textfield', 1, 100, '', 40),
('551F1339-851F-11EF-8872-E86A641ED142', '551ED336-851F-11EF-8872-E86A641ED142', '名称', 'name', 'varchar', 255, 0, 1, 1, '', '', '', 2, 1, 1, 200, '', 1, 'textfield', 1, 100, '', 40),
('551F2413-851F-11EF-8872-E86A641ED142', '551ED336-851F-11EF-8872-E86A641ED142', '助记码', 'py', 'varchar', 255, 0, -900, 99, '', '', '', 1, 1, 2, 200, '', 4, 'textfield', 1, 100, '', 90),
('551F337A-851F-11EF-8872-E86A641ED142', '551ED336-851F-11EF-8872-E86A641ED142', '数据域', 'data_org', 'varchar', 255, 0, -800, 99, '', '', '', 1, 1, 2, 150, '', 3, 'textfield', 1, 100, '', 90),
('551F43D8-851F-11EF-8872-E86A641ED142', '551ED336-851F-11EF-8872-E86A641ED142', '公司', 'company_id', 'varchar', 255, 0, -700, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, '', 90),
('551F53F7-851F-11EF-8872-E86A641ED142', '551ED336-851F-11EF-8872-E86A641ED142', '记录创建时间', 'date_created', 'datetime', 0, 0, -699, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, '', 90),
('551F64B4-851F-11EF-8872-E86A641ED142', '551ED336-851F-11EF-8872-E86A641ED142', '记录创建人', 'create_user_id', 'varchar', 255, 0, -698, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, '', 90),
('551F7486-851F-11EF-8872-E86A641ED142', '551ED336-851F-11EF-8872-E86A641ED142', '最后编辑时间', 'update_dt', 'datetime', 0, 0, -697, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, '', 90),
('551F82C2-851F-11EF-8872-E86A641ED142', '551ED336-851F-11EF-8872-E86A641ED142', '最后编辑人', 'update_user_id', 'varchar', 255, 0, -696, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, '', 90),
('551F939C-851F-11EF-8872-E86A641ED142', '551ED336-851F-11EF-8872-E86A641ED142', '状态', 'record_status', 'int', 11, 0, 2, 2, 't_sysdict_sln0000_ct_record_status', 'code_int', 'name', 2, 1, 1, 80, '', 2, 'psi_sysdictfield', 1, 200, '1000', 40);

DELETE FROM `t_code_table_buttons` where `table_id` = '551ED336-851F-11EF-8872-E86A641ED142';
INSERT INTO `t_code_table_buttons` (`id`, `table_id`, `caption`, `fid`, `on_click_frontend`, `on_click_backend`, `show_order`) VALUES
('551FD85B-851F-11EF-8872-E86A641ED142', '551ED336-851F-11EF-8872-E86A641ED142', '新建凭证字', 'ct20241008104509-add', '_onAddCodeTableRecord', '', 1),
('552011FD-851F-11EF-8872-E86A641ED142', '551ED336-851F-11EF-8872-E86A641ED142', '编辑凭证字', 'ct20241008104509-update', '_onEditCodeTableRecord', '', 2),
('55202873-851F-11EF-8872-E86A641ED142', '551ED336-851F-11EF-8872-E86A641ED142', '删除凭证字', 'ct20241008104509-delete', '_onDeleteCodeTableRecord', '', 3),
('5520408D-851F-11EF-8872-E86A641ED142', '551ED336-851F-11EF-8872-E86A641ED142', '-', 'ct20241008104509', '', '', 4),
('55204DCC-851F-11EF-8872-E86A641ED142', '551ED336-851F-11EF-8872-E86A641ED142', '导出Excel', 'ct20241008104509-excel', '_onExcelCodeTableRecord', '', 5),
('552064F9-851F-11EF-8872-E86A641ED142', '551ED336-851F-11EF-8872-E86A641ED142', '-', 'ct20241008104509', '', '', 6),
('552072BC-851F-11EF-8872-E86A641ED142', '551ED336-851F-11EF-8872-E86A641ED142', '刷新', 'ct20241008104509', '_onRefreshCodeTableRecord', '', 7);

DELETE FROM `t_fid_plus` where `fid` = 'ct20241008104509';
INSERT INTO `t_fid_plus` (`fid`, `name`, `py`, `memo`, `sln_code`) VALUES
('ct20241008104509', '凭证字', 'PZZ', '', 'SLN0002');

DELETE FROM `t_permission_plus` where fid = 'ct20241008104509' or parent_fid = 'ct20241008104509';
INSERT INTO `t_permission_plus` (`id`, `fid`, `name`, `note`, `category`, `py`, `show_order`, `parent_fid`) VALUES
('ct20241008104509', 'ct20241008104509', '凭证字 - 模块权限', '通过菜单进入凭证字模块的权限', '凭证字', 'PZZ', 100, null),
('ct20241008104509-add', 'ct20241008104509-add', '凭证字 - 按钮权限 - 新建凭证字', '凭证字模块中按钮[新建凭证字]的启用权限', '凭证字', '', 201, 'ct20241008104509'),
('ct20241008104509-update', 'ct20241008104509-update', '凭证字 - 按钮权限 - 编辑凭证字', '凭证字模块中按钮[编辑凭证字]的启用权限', '凭证字', '', 202, 'ct20241008104509'),
('ct20241008104509-delete', 'ct20241008104509-delete', '凭证字 - 按钮权限 - 删除凭证字', '凭证字模块中按钮[删除凭证字]的启用权限', '凭证字', '', 203, 'ct20241008104509'),
('ct20241008104509-excel', 'ct20241008104509-excel', '凭证字 - 按钮权限 - 导出Excel', '凭证字模块中按钮[导出Excel]的启用权限', '凭证字', '', 205, 'ct20241008104509'),
('ct20241008104509-edit-dataorg', 'ct20241008104509-edit-dataorg', '凭证字 - 按钮权限 - 修改数据域', '凭证字模块中按钮[修改数据域]的启用权限', '凭证字', '', 208, 'ct20241008104509'),
('ct20241008104509-edit-py', 'ct20241008104509-edit-py', '凭证字 - 按钮权限 - 修改助记码', '凭证字模块中按钮[修改助记码]的启用权限', '凭证字', '', 209, 'ct20241008104509'),
('ct20241008104509-dataorg', 'ct20241008104509-dataorg', '凭证字 - 数据域权限 - 凭证字在业务单据中的使用权限', '凭证字在业务单据中的使用权限', '凭证字', '', 300, 'ct20241008104509');

# 凭证字 - 菜单项
DELETE FROM t_menu_item_plus where id = '7BFE527C-851F-11EF-8872-E86A641ED142';
INSERT INTO t_menu_item_plus(id, caption, fid, parent_id, show_order, py, memo, sys_category)
VALUES ('7BFE527C-851F-11EF-8872-E86A641ED142', '凭证字', 'ct20241008104509', '1101', 1000, 'PZZ', '', 1);

# ------------------------------------------------------------------------------
# 码表：凭证字
# CREATE DDL
# ------------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `t_sln0002_ct_voucher_word` (
  `id` varchar(255)  NOT NULL,
  `py` varchar(255)  NOT NULL,
  `data_org` varchar(255)  NOT NULL,
  `company_id` varchar(255)  NOT NULL,
  `date_created` datetime  NOT NULL,
  `create_user_id` varchar(255)  NOT NULL,
  `update_dt` datetime  DEFAULT NULL,
  `update_user_id` varchar(255)  DEFAULT NULL,
  `code` varchar(255)  NOT NULL,
  `name` varchar(255)  NOT NULL,
  `record_status` int(11)  NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# ------------------------------------------------------------------------------
# 码表：银行账户
# 元数据
# ------------------------------------------------------------------------------
DELETE FROM `t_code_table_md` where `id` = 'C56D627F-B768-11EF-81D4-E86A641ED142';
INSERT INTO `t_code_table_md` (`id`, `code`, `name`, `table_name`, `category_id`, `memo`, `py`, `fid`, `md_version`, `is_fixed`, `enable_parent_id`, `handler_class_name`, `module_name`, `edit_col_cnt`, `view_paging`, `input_company`, `sln_code`, `module_description`)
VALUES ('C56D627F-B768-11EF-81D4-E86A641ED142', 'SLN0002.01.01', '银行账户', 't_sln0002_ct_bank', '1BEE661C-5100-11EE-89B4-E86A641ED142', '', 'YHZH', 'ct20241211103648', 3, 1, 0, '\\SLN0002\\Plugin\\Bank\\BankAccountBLL', '银行账户', 2, 2, 2, 'SLN0002', '');

DELETE FROM `t_code_table_cols_md` where `table_id` = 'C56D627F-B768-11EF-81D4-E86A641ED142';
INSERT INTO `t_code_table_cols_md` (`id`, `table_id`, `caption`, `db_field_name`, `db_field_type`, `db_field_length`, `db_field_decimal`, `show_order`, `value_from`, `value_from_table_name`, `value_from_col_name`, `value_from_col_name_display`, `must_input`, `sys_col`, `is_visible`, `width_in_view`, `note`, `show_order_in_view`, `editor_xtype`, `col_span`, `default_value`, `default_value_ext`, `label_width`) VALUES
('431FDD71-B824-11EF-B8E7-E86A641ED142', 'C56D627F-B768-11EF-81D4-E86A641ED142', '开设日期', 'input_dt', 'datetime', 0, 0, 4, 1, '', '', '', 2, 2, 1, 120, '', 5, 'datefield', 1, 100, '', 70),
('6BC72DA7-B824-11EF-B8E7-E86A641ED142', 'C56D627F-B768-11EF-81D4-E86A641ED142', '银行营业机构', 'bank_name', 'varchar', 255, 0, 1, 1, '', '', '', 2, 2, 1, 292, '', 3, 'textfield', 1, 100, '', 100),
('7CC6745E-B823-11EF-B8E7-E86A641ED142', 'C56D627F-B768-11EF-81D4-E86A641ED142', '账户类型', 'category_id', 'varchar', 255, 0, 0, 2, 't_sysdict_sln0002_bank_account_category', 'code', 'name', 2, 2, 1, 120, '', 2, 'textfield', 1, 100, '', 70),
('C56D8814-B768-11EF-81D4-E86A641ED142', 'C56D627F-B768-11EF-81D4-E86A641ED142', 'id', 'id', 'varchar', 255, 0, -1000, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, '', 90),
('C56D9D44-B768-11EF-81D4-E86A641ED142', 'C56D627F-B768-11EF-81D4-E86A641ED142', '编码', 'code', 'varchar', 255, 0, 2, 1, '', '', '', 2, 1, 1, 120, '', 1, 'textfield', 1, 100, '', 70),
('C56DB09C-B768-11EF-81D4-E86A641ED142', 'C56D627F-B768-11EF-81D4-E86A641ED142', '账号', 'name', 'varchar', 255, 0, 3, 1, '', '', '', 2, 1, 1, 259, '', 4, 'textfield', 1, 100, '', 100),
('C56DC407-B768-11EF-81D4-E86A641ED142', 'C56D627F-B768-11EF-81D4-E86A641ED142', '助记码', 'py', 'varchar', 255, 0, -900, 99, '', '', '', 1, 1, 2, 200, '', 8, 'textfield', 1, 100, '', 90),
('C56DD758-B768-11EF-81D4-E86A641ED142', 'C56D627F-B768-11EF-81D4-E86A641ED142', '数据域', 'data_org', 'varchar', 255, 0, -800, 99, '', '', '', 1, 1, 2, 150, '', 7, 'textfield', 1, 100, '', 90),
('C56DEB00-B768-11EF-81D4-E86A641ED142', 'C56D627F-B768-11EF-81D4-E86A641ED142', '公司', 'company_id', 'varchar', 255, 0, -700, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, '', 90),
('C56DFE56-B768-11EF-81D4-E86A641ED142', 'C56D627F-B768-11EF-81D4-E86A641ED142', '记录创建时间', 'date_created', 'datetime', 0, 0, -699, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, '', 90),
('C56E11AF-B768-11EF-81D4-E86A641ED142', 'C56D627F-B768-11EF-81D4-E86A641ED142', '记录创建人', 'create_user_id', 'varchar', 255, 0, -698, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, '', 90),
('C56E254E-B768-11EF-81D4-E86A641ED142', 'C56D627F-B768-11EF-81D4-E86A641ED142', '最后编辑时间', 'update_dt', 'datetime', 0, 0, -697, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, '', 90),
('C56E37EF-B768-11EF-81D4-E86A641ED142', 'C56D627F-B768-11EF-81D4-E86A641ED142', '最后编辑人', 'update_user_id', 'varchar', 255, 0, -696, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, '', 90),
('C56E4932-B768-11EF-81D4-E86A641ED142', 'C56D627F-B768-11EF-81D4-E86A641ED142', '状态', 'record_status', 'int', 11, 0, 5, 2, 't_sysdict_sln0000_ct_record_status', 'code_int', 'name', 2, 1, 1, 80, '', 6, 'psi_sysdictfield', 1, 200, '1000', 100);

DELETE FROM `t_code_table_buttons` where `table_id` = 'C56D627F-B768-11EF-81D4-E86A641ED142';
INSERT INTO `t_code_table_buttons` (`id`, `table_id`, `caption`, `fid`, `on_click_frontend`, `on_click_backend`, `show_order`) VALUES
('C56E692F-B768-11EF-81D4-E86A641ED142', 'C56D627F-B768-11EF-81D4-E86A641ED142', '新建银行账户', 'ct20241211103648-add', '_onAddCodeTableRecord', '', 1),
('C56EA4FD-B768-11EF-81D4-E86A641ED142', 'C56D627F-B768-11EF-81D4-E86A641ED142', '编辑银行账户', 'ct20241211103648-update', '_onEditCodeTableRecord', '', 2),
('C56EBBAB-B768-11EF-81D4-E86A641ED142', 'C56D627F-B768-11EF-81D4-E86A641ED142', '删除银行账户', 'ct20241211103648-delete', '_onDeleteCodeTableRecord', '', 3),
('C56ED26C-B768-11EF-81D4-E86A641ED142', 'C56D627F-B768-11EF-81D4-E86A641ED142', '-', 'ct20241211103648', '', '', 4),
('C56EE0EB-B768-11EF-81D4-E86A641ED142', 'C56D627F-B768-11EF-81D4-E86A641ED142', '导出Excel', 'ct20241211103648-excel', '_onExcelCodeTableRecord', '', 5),
('C56EF774-B768-11EF-81D4-E86A641ED142', 'C56D627F-B768-11EF-81D4-E86A641ED142', '-', 'ct20241211103648', '', '', 6),
('C56F07F9-B768-11EF-81D4-E86A641ED142', 'C56D627F-B768-11EF-81D4-E86A641ED142', '刷新', 'ct20241211103648', '_onRefreshCodeTableRecord', '', 7);

DELETE FROM `t_fid_plus` where `fid` = 'ct20241211103648';
INSERT INTO `t_fid_plus` (`fid`, `name`, `py`, `memo`, `sln_code`) VALUES
('ct20241211103648', '银行账户', 'YHZH', '', 'SLN0002');

DELETE FROM `t_permission_plus` where fid = 'ct20241211103648' or parent_fid = 'ct20241211103648';
INSERT INTO `t_permission_plus` (`id`, `fid`, `name`, `note`, `category`, `py`, `show_order`, `parent_fid`) VALUES
('ct20241211103648', 'ct20241211103648', '银行账户 - 模块权限', '通过菜单进入银行账户模块的权限', '银行账户', 'YHZH', 100, null),
('ct20241211103648-add', 'ct20241211103648-add', '银行账户 - 按钮权限 - 新建银行账户', '银行账户模块中按钮[新建银行账户]的启用权限', '银行账户', '', 201, 'ct20241211103648'),
('ct20241211103648-update', 'ct20241211103648-update', '银行账户 - 按钮权限 - 编辑银行账户', '银行账户模块中按钮[编辑银行账户]的启用权限', '银行账户', '', 202, 'ct20241211103648'),
('ct20241211103648-delete', 'ct20241211103648-delete', '银行账户 - 按钮权限 - 删除银行账户', '银行账户模块中按钮[删除银行账户]的启用权限', '银行账户', '', 203, 'ct20241211103648'),
('ct20241211103648-excel', 'ct20241211103648-excel', '银行账户 - 按钮权限 - 导出Excel', '银行账户模块中按钮[导出Excel]的启用权限', '银行账户', '', 205, 'ct20241211103648'),
('ct20241211103648-edit-dataorg', 'ct20241211103648-edit-dataorg', '银行账户 - 按钮权限 - 修改数据域', '银行账户模块中按钮[修改数据域]的启用权限', '银行账户', '', 208, 'ct20241211103648'),
('ct20241211103648-edit-py', 'ct20241211103648-edit-py', '银行账户 - 按钮权限 - 修改助记码', '银行账户模块中按钮[修改助记码]的启用权限', '银行账户', '', 209, 'ct20241211103648'),
('ct20241211103648-dataorg', 'ct20241211103648-dataorg', '银行账户 - 数据域权限 - 银行账户在业务单据中的使用权限', '银行账户在业务单据中的使用权限', '银行账户', '', 300, 'ct20241211103648');

# ------------------------------------------------------------------------------
# 码表：银行账户
# CREATE DDL
# ------------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `t_sln0002_ct_bank` (
  `id` varchar(255)  NOT NULL,
  `py` varchar(255)  NOT NULL,
  `data_org` varchar(255)  NOT NULL,
  `company_id` varchar(255)  NOT NULL,
  `date_created` datetime  NOT NULL,
  `create_user_id` varchar(255)  NOT NULL,
  `update_dt` datetime  DEFAULT NULL,
  `update_user_id` varchar(255)  DEFAULT NULL,
  `category_id` varchar(255)  DEFAULT NULL,
  `code` varchar(255)  NOT NULL,
  `bank_name` varchar(255)  DEFAULT NULL,
  `name` varchar(255)  NOT NULL,
  `input_dt` datetime  DEFAULT NULL,
  `record_status` int(11)  NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# 银行账户 - 菜单项
DELETE FROM t_menu_item_plus where id = '7D1145E4-B769-11EF-81D4-E86A641ED142';
INSERT INTO t_menu_item_plus(id, caption, fid, parent_id, show_order, py, memo, sys_category)
VALUES ('7D1145E4-B769-11EF-81D4-E86A641ED142', '银行账户', 'ct20241211103648', '1101', 2, 'YHZH', '', 1);

# -----------------------------------------------------------
# 默认角色的权限项
# 这段SQL放在最后执行
# -----------------------------------------------------------
TRUNCATE TABLE `t_role_permission`;
INSERT INTO `t_role_permission` (`role_id`, `permission_id`) 
select 'A83F617E-A153-11E4-A9B8-782BCBD7746B' as role_id, fid as permission_id
from t_permission;
INSERT INTO `t_role_permission` (`role_id`, `permission_id`) 
select 'A83F617E-A153-11E4-A9B8-782BCBD7746B' as role_id, fid as permission_id
from t_permission_plus;
