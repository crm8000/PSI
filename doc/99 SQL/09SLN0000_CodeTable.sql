# ------------------------------------------------------------------------------
# 码表分类
# ------------------------------------------------------------------------------
DELETE FROM t_code_table_category where id = '25D91974-2350-11ED-A956-E86A641ED142';
INSERT INTO t_code_table_category(id, code, name, parent_id, is_system, sln_code)
VALUES ('25D91974-2350-11ED-A956-E86A641ED142', 'SLN0000.01', '组织机构', NULL, 1, 'SLN0000');

# ------------------------------------------------------------------------------
# 码表：组织机构
# 元数据
# ------------------------------------------------------------------------------
DELETE FROM `t_code_table_md` where `id` = 'A9DF7AC4-235A-11ED-A956-E86A641ED142';
INSERT INTO `t_code_table_md` (`id`, `code`, `name`, `table_name`, `category_id`, `memo`, `py`, `fid`, `md_version`, `is_fixed`, `enable_parent_id`, `handler_class_name`, `module_name`, `edit_col_cnt`, `view_paging`, `input_company`, `sln_code`)
VALUES ('A9DF7AC4-235A-11ED-A956-E86A641ED142', 'SLN0000.01.01', '组织机构', 't_org', '25D91974-2350-11ED-A956-E86A641ED142', '', 'ZZJG', '-8999-01', 1, 0, 1, '', '用户管理', 1, 2, 1, 'SLN0000');

DELETE FROM `t_code_table_cols_md` where `table_id` = 'A9DF7AC4-235A-11ED-A956-E86A641ED142';
INSERT INTO `t_code_table_cols_md` (`id`, `table_id`, `caption`, `db_field_name`, `db_field_type`, `db_field_length`, `db_field_decimal`, `show_order`, `value_from`, `value_from_table_name`, `value_from_col_name`, `value_from_col_name_display`, `must_input`, `sys_col`, `is_visible`, `width_in_view`, `note`, `show_order_in_view`, `editor_xtype`, `col_span`, `default_value`, `default_value_ext`) VALUES
('A9DFC3EB-235A-11ED-A956-E86A641ED142', 'A9DF7AC4-235A-11ED-A956-E86A641ED142', 'id', 'id', 'varchar', 255, 0, -1000, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('A9E020C3-235A-11ED-A956-E86A641ED142', 'A9DF7AC4-235A-11ED-A956-E86A641ED142', '编码', 'code', 'varchar', 255, 0, 0, 1, '', '', '', 2, 1, 1, 120, '', 0, 'textfield', 1, 100, ''),
('A9E03641-235A-11ED-A956-E86A641ED142', 'A9DF7AC4-235A-11ED-A956-E86A641ED142', '名称', 'name', 'varchar', 255, 0, 1, 1, '', '', '', 2, 1, 1, 200, '', 1, 'textfield', 1, 100, ''),
('A9E04835-235A-11ED-A956-E86A641ED142', 'A9DF7AC4-235A-11ED-A956-E86A641ED142', '助记码', 'py', 'varchar', 255, 0, -900, 99, '', '', '', 1, 1, 2, 200, '', 4, 'textfield', 1, 100, ''),
('A9E05A13-235A-11ED-A956-E86A641ED142', 'A9DF7AC4-235A-11ED-A956-E86A641ED142', '数据域', 'data_org', 'varchar', 255, 0, -800, 99, '', '', '', 1, 1, 2, 150, '', 3, 'textfield', 1, 100, ''),
('A9E06923-235A-11ED-A956-E86A641ED142', 'A9DF7AC4-235A-11ED-A956-E86A641ED142', '公司', 'company_id', 'varchar', 255, 0, -700, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('A9E07930-235A-11ED-A956-E86A641ED142', 'A9DF7AC4-235A-11ED-A956-E86A641ED142', '记录创建时间', 'date_created', 'datetime', 0, 0, -699, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('A9E08962-235A-11ED-A956-E86A641ED142', 'A9DF7AC4-235A-11ED-A956-E86A641ED142', '记录创建人', 'create_user_id', 'varchar', 255, 0, -698, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('A9E0961C-235A-11ED-A956-E86A641ED142', 'A9DF7AC4-235A-11ED-A956-E86A641ED142', '最后编辑时间', 'update_dt', 'datetime', 0, 0, -697, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('A9E0A27E-235A-11ED-A956-E86A641ED142', 'A9DF7AC4-235A-11ED-A956-E86A641ED142', '最后编辑人', 'update_user_id', 'varchar', 255, 0, -696, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('A9E0AEF1-235A-11ED-A956-E86A641ED142', 'A9DF7AC4-235A-11ED-A956-E86A641ED142', '状态', 'record_status', 'int', 11, 0, 2, 2, 't_sysdict_sln0000_ct_record_status', 'code_int', 'name', 2, 1, 1, 80, '', 2, 'psi_sysdictfield', 1, 200, '1000'),
('A9E0B8E2-235A-11ED-A956-E86A641ED142', 'A9DF7AC4-235A-11ED-A956-E86A641ED142', '上级', 'parent_id', 'varchar', 255, 0, 4, 4, 't_org', 'id', 'full_name', 0, 1, 1, 0, '', -1000, 'psi_codetable_parentidfield', 1, 100, ''),
('A9E0C2F1-235A-11ED-A956-E86A641ED142', 'A9DF7AC4-235A-11ED-A956-E86A641ED142', '全名', 'full_name', 'varchar', 1000, 0, -1000, 99, '', '', '', 0, 1, 2, 300, '', 3, 'textfield', 1, 100, '');

# 因为组织机构不单独作为一个模块使用，所以不需要配置其按钮
DELETE FROM `t_code_table_buttons` where `table_id` = 'A9DF7AC4-235A-11ED-A956-E86A641ED142';

# ------------------------------------------------------------------------------
# 码表：用户
# 元数据
# ------------------------------------------------------------------------------
DELETE FROM `t_code_table_md` where `id` = 'E88224F3-5CC1-11EE-9223-E86A641ED142';
INSERT INTO `t_code_table_md` (`id`, `code`, `name`, `table_name`, `category_id`, `memo`, `py`, `fid`, `md_version`, `is_fixed`, `enable_parent_id`, `handler_class_name`, `module_name`, `edit_col_cnt`, `view_paging`, `input_company`, `sln_code`, `module_description`)
VALUES ('E88224F3-5CC1-11EE-9223-E86A641ED142', 'SLN0000.01.02', '用户', 't_user', '25D91974-2350-11ED-A956-E86A641ED142', '', 'YHGL', '-8999-02', 1, 2, 0, '', '用户管理', 2, 1, 1, 'SLN0000', '');

DELETE FROM `t_code_table_cols_md` where `table_id` = 'E88224F3-5CC1-11EE-9223-E86A641ED142';
INSERT INTO `t_code_table_cols_md` (`id`, `table_id`, `caption`, `db_field_name`, `db_field_type`, `db_field_length`, `db_field_decimal`, `show_order`, `value_from`, `value_from_table_name`, `value_from_col_name`, `value_from_col_name_display`, `must_input`, `sys_col`, `is_visible`, `width_in_view`, `note`, `show_order_in_view`, `editor_xtype`, `col_span`, `default_value`, `default_value_ext`) VALUES
('E88260D1-5CC1-11EE-9223-E86A641ED142', 'E88224F3-5CC1-11EE-9223-E86A641ED142', 'id', 'id', 'varchar', 255, 0, -1000, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('E8827ADE-5CC1-11EE-9223-E86A641ED142', 'E88224F3-5CC1-11EE-9223-E86A641ED142', '编码', 'code', 'varchar', 255, 0, 0, 1, '', '', '', 2, 1, 1, 120, '', 0, 'textfield', 1, 100, ''),
('E8828D9F-5CC1-11EE-9223-E86A641ED142', 'E88224F3-5CC1-11EE-9223-E86A641ED142', '名称', 'name', 'varchar', 255, 0, 1, 1, '', '', '', 2, 1, 1, 200, '', 1, 'textfield', 1, 100, ''),
('E882A0A3-5CC1-11EE-9223-E86A641ED142', 'E88224F3-5CC1-11EE-9223-E86A641ED142', '助记码', 'py', 'varchar', 255, 0, -900, 99, '', '', '', 1, 1, 2, 200, '', 4, 'textfield', 1, 100, ''),
('E882B2FF-5CC1-11EE-9223-E86A641ED142', 'E88224F3-5CC1-11EE-9223-E86A641ED142', '数据域', 'data_org', 'varchar', 255, 0, -800, 99, '', '', '', 1, 1, 2, 150, '', 3, 'textfield', 1, 100, ''),
('E882C6ED-5CC1-11EE-9223-E86A641ED142', 'E88224F3-5CC1-11EE-9223-E86A641ED142', '公司', 'company_id', 'varchar', 255, 0, -700, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('E882D861-5CC1-11EE-9223-E86A641ED142', 'E88224F3-5CC1-11EE-9223-E86A641ED142', '记录创建时间', 'date_created', 'datetime', 0, 0, -699, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('E882E95D-5CC1-11EE-9223-E86A641ED142', 'E88224F3-5CC1-11EE-9223-E86A641ED142', '记录创建人', 'create_user_id', 'varchar', 255, 0, -698, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('E882F9A2-5CC1-11EE-9223-E86A641ED142', 'E88224F3-5CC1-11EE-9223-E86A641ED142', '最后编辑时间', 'update_dt', 'datetime', 0, 0, -697, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('E8830A53-5CC1-11EE-9223-E86A641ED142', 'E88224F3-5CC1-11EE-9223-E86A641ED142', '最后编辑人', 'update_user_id', 'varchar', 255, 0, -696, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('E8831A38-5CC1-11EE-9223-E86A641ED142', 'E88224F3-5CC1-11EE-9223-E86A641ED142', '状态', 'record_status', 'int', 11, 0, 2, 2, 't_sysdict_sln0000_ct_record_status', 'code_int', 'name', 2, 1, 1, 80, '', 2, 'psi_sysdictfield', 1, 200, '1000');

# 因为用户不单独作为一个模块使用，所以不需要配置其按钮
DELETE FROM `t_code_table_buttons` where `table_id` = 'E88224F3-5CC1-11EE-9223-E86A641ED142';
