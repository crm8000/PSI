---
home: true
heroText: PSI应用平台实施指南
tagline: Partner, Solution, Infrastructure
actions:
  - text: 使用手册
    link: /user/
    type: primary
  - text: 系统管理
    link: /admin/
    type: secondary 
  - text: 二次开发
    link: /dev/
    type: secondary
features:
  - title: Partner - 合作伙伴
    details: 商业合作伙伴
  - title: Solution - 解决方案 
    details: 企业管理全面解决方案
  - title: Infrastructure - 基础设施
    details: 100%开源的企业管理软件系统
footer: 版本：PSI 2025 built2024-12-19 08:55，GPL v3 Licensed | Copyright © 2015-present PSI Team
---
