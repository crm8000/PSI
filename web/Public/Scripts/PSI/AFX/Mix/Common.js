/**
 * 常用的公共Mix
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.AFX.Mix.Common", {
  showInfo: function (info, func) {
    PSI.MsgBox.showInfo(info, func);
  },

  /**
   * 显示提示信息
   * 
   * @param {string} info 
   * @param {boolean} showCloseButton - true: 显示关闭按钮，用户可以单机关闭按钮提前关闭提示信息 
   */
  tip(info, showCloseButton) {
    PSI.MsgBox.tip(info, showCloseButton);
  },

  /**
   * 显示需要用户确认的信息
   * @param {string} confirmInfo 提示信息 
   * @param {Function} funcOnYes 用户选择yes的时候的回调函数
   * @param {Object} position 指定窗口显示{x, y}位置  
   */
  confirm(confirmInfo, funcOnYes, position) {
    PSI.MsgBox.confirm(confirmInfo, funcOnYes, position);
  },

  /**
   * 构建最终的URL，供ajax调用
   * 
   * @param {string} url 
   * @returns URL
   */
  URL(url) {
    return PSI.Const.BASE_URL + url;
  },

  /**
   * Ajax调用
   * 
   * @param {object} r 
   */
  ajax(r) {
    if (!r.method) {
      r.method = "POST";
    }
    PCL.Ajax.request(r);
  },

  /**
   * 把字符串解析为JSON
   * 
   * @param {string} str 
   * @returns JSON
   */
  decodeJSON(str) {
    return PCL.JSON.decode(str);
  },

  /**
   * 把对象转换成JSON字符串
   * 
   * @param {object} obj 
   * @returns 字符串 
   */
  encodeJSON(obj) {
    return PCL.JSON.encode(obj);
  },

  /**
   * 关闭当前模块
   */
  closeWindow() {
    if (PSI.Const.MOT == "0") {
      window.location.replace(PSI.Const.BASE_URL);
    } else {
      window.close();

      if (!window.closed) {
        window.location.replace(PSI.Const.BASE_URL);
      }
    }
  },

  /**
   * 把input的光标定位到最后
   * @param {Input} edit 
   */
  setFocusAndCursorPosToLast(edit) {
    if (!edit) {
      return;
    }

    edit.focus();
    const v = `${edit.getValue()}`;
    const dom = edit.inputEl.dom;
    if (dom) {
      dom.selectionStart = v ? v.length : 0;
      dom.selectionEnd = v ? v.length : 0;
    }
  },

  /**
   * 格式化标题文字
   * 
   * 逐步要废弃掉，改用formatTitleLabel
   * 
   * @param {string} title 
   */
  formatTitle(title) {
    return `<span style='font-size:160%'>${title}</span>`;
  },

  /**
   * 格式化标题文字
   * 
   * @param {string} title
   * @param {string} action 
   */
  formatTitleLabel(title, action) {
    return `<strong class="PSI-Bill-title">${title}<span class="PSI-Bill-title-action">${action}</span></strong>`;
  },

  /**
   * 格式化Grid标题
   * 
   * @param {string} title 
   */
  formatGridHeaderTitle(title) {
    return `<span style='font-size:13px'>${title}</sapn>`;
  },

  /**
   * 生成Edit Form这类窗体左上的Logo
   * 
   * @param {object} entity 
   * @param {string} title 
   * @returns 
   */
  genLogoHtml(entity, title) {
    const f = entity == null
      ? "edit-form-create.png"
      : "edit-form-update.png";
    const logoHtml = `
      <img style='float:left;margin:0px 20px 0px 10px;width:48px;height:48px;' 
        src='${PSI.Const.BASE_URL}Public/Images/${f}'></img>
      <div style='margin-left:60px;margin-top:0px;'>
        <h2 style='color:#595959;margin-top:15px;display:inline-block'>${title}</h2>
        &nbsp;&nbsp;<span style='color:#8c8c8c'>标记 <span style='color:red;font-weight:bold'>*</span>的字段需要录入数据</span>
      </div>
      <div style='margin:0px;border-bottom:1px solid #e6f7ff;height:1px' /></div>
      `;

    return logoHtml;
  },

  /**
   * 对字符串做HTML转码处理
   * 
   * @param {string} s 
   * @returns string
   */
  htmlDecode(s) {
    return PCL.String.htmlDecode(s);
  },

  /**
   * 把普通文本转换成备注型HTML
   */
  toFieldNoteText(s) {
    return `<span class='PSI-field-note'>${s}</span>`;
  },

  /**
   * 把普通文本转为Title中强调加粗关键字文本
   */
  toTitleKeyWord(s) {
    return `<span class='PSI-title-keyword'>${s}</span>`;
  },

  /**
   * 把普通文本转为自动换行的文本，用于Grid的renderer
   */
  toAutoWrap(s) {
    return `<div class='PSI-grid-cell-autoWrap'>${s}</div>`;
  },

  /**
   * 应用场景举例：
   * 
   * 当obj是 PSI.A.B的时候，
   * 调用 buildId(obj, "editName")则返回 "PSI.A.B.editName"这样一个全限定id
   * 
   * 
   * @param {Object} obj 
   * @param {string} name 
   * @returns 全限定的id
   */
  buildId(obj, name) {
    return PCL.getClassName(obj) + "." + name;
  },

  /**
   * 构建Model的命名空间
   * 
   * 例如：
   * 当obj是PSI.SLN0001.Warehouse.MainForm的时候，
   * buildModelName(obj, "MName")返回 "PSIModel.PSI.SLN0001.Warehouse.MainForm.MName"
   * 
   * @param {Object} obj 
   * @returns 
   */
  buildModelName(obj, name) {
    return "PSIModel." + PCL.getClassName(obj) + "." + name;
  },

  /**
   * 把字符串由纯文本转换为HTML
   * 
   * @remark 只用于Grid的emptyText属性
   */
  toEmptyText(s) {
    return `<h2 style='color:#874d00;margin-top:0px;margin-left:20px'>${s}</h2>`;
  },

  /**
   * 处理textfield的IME事件
   * 
   * @param {xtype: textfield} edit
   */
  applyIMEHandler(edit) {
    const dom = edit.getEl().dom;
    dom.addEventListener('compositionstart', (event) => {
      edit._inIME = true;
    });

    dom.addEventListener('compositionend', (event) => {
      edit._inIME = false;
    });
  },

  buildTooltip(tip) {
    return `<span style='border-left:4px #e37070 solid; padding-left:4px;margin-top:3px;color:#9e1068'>${tip}</span>`;
  },

  /**
   * 把字符串转换成 YYYY-MM-DD 这样的日期格式
   * @param {string} s 
   * @returns 
   */
  toDT(s) {
    return PCL.Date.format(s, "Y-m-d");
  },

  /**
   * 给掩盖层添加处理事件
   * 
   * @param {PCL.window.Window} wnd 当前窗口
   */
  addMaskClickHandler(wnd) {
    // 当单击掩盖层的时候，关闭当前窗口
    const mask = document.querySelectorAll("div.x-mask");
    mask.forEach(it => {
      it.onclick = () => {
        if (PSI.Const.msgBoxShowing) {
          return;
        }

        wnd?.close();
      }
    });
  },

  /**
   *  格式化金额，负数用红字显示, 0 不显示
   */
  formatMoney(value) {
    if (value == null || value == 0 || isNaN(value)) {
      return "";
    } else {
      const s = PCL.util.Format.number(Math.abs(value), "0,0.00");
      if (value < 0) {
        return `<span style='color:red'>-${s}</span>`;
      } else {
        return s;
      }
    }
  },

  /**
   * 高亮字符串
   */
  htInfo(s) {
    return `<span style='color:#d95350'>${s}</span>`;
  }
});
