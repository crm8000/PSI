/**
 * 菜单快捷访问自定义字段
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.Home.MainMenu.ShortcutField", {
  extend: "PCL.form.field.Trigger",
  alias: "widget.psi_mainmenushortcutfield",

  mixins: ["PSI.AFX.Mix.Common"],

  config: {
    showModal: true
  },

  hideTrigger: true,

  initComponent() {
    const me = this;

    me.enableKeyEvents = true;

    me.callParent(arguments);

    me.on("keydown", (field, e) => {
      if (me.readOnly) {
        return;
      }

      if (e.getKey() == e.BACKSPACE) {
        field.setValue(null);
        me.setIdValue(null);
        e.preventDefault();
        return false;
      }

      if (e.getKey() != e.ENTER && !e.isSpecialKey(e.getKey())) {
        me.onTriggerClick(e);
      }
    });

    me.on({
      render(p) {
        p.getEl().on("click", () => {
          me.onTriggerClick();
        });
      },
      single: true
    });
  },

  /**
   * @override
   */
  onTriggerClick(e) {
    const me = this;

    const modelName = me.buildModelName(me, "MenuShortcutField");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "fid", "caption", "py", "code"]
    });

    const store = PCL.create("PCL.data.Store", {
      model: modelName,
      autoLoad: false,
      data: []
    });
    const lookupGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-Lookup",
      columnLines: true,
      border: 1,
      store: store,
      columns: [{
        header: "助记码",
        dataIndex: "py",
        menuDisabled: true,
        width: 100
      }, {
        header: "编码",
        dataIndex: "code",
        menuDisabled: true,
        width: 100
      }, {
        header: "菜单",
        dataIndex: "caption",
        menuDisabled: true,
        flex: 1
      }]
    });
    me.lookupGrid = lookupGrid;
    me.lookupGrid.on("itemdblclick", me.onOK, me);

    const wnd = PCL.create("PCL.window.Window", {
      title: "选择 - 菜单",
      modal: me.getShowModal(),
      header: false,
      shadow: false,
      border: 0,
      width: 750,
      height: 400,
      layout: "border",
      onEsc() {
        me.wnd.hide();
        me.focus();
        me.wnd.close();
      },
      items: [{
        region: "center",
        xtype: "panel",
        layout: "fit",
        border: 0,
        items: [lookupGrid]
      }, {
        xtype: "panel",
        region: "south",
        height: 32,
        layout: "fit",
        border: 0,
        items: [{
          xtype: "form",
          layout: "form",
          bodyPadding: 5,
          bodyCls: "PSI-Field",
          items: [{
            id: me.buildId(me, "editName"),
            xtype: "textfield",
            labelWidth: 0,
            labelAlign: "right",
            labelSeparator: ""
          }]
        }]
      }],
      buttons: [{
        xtype: "container",
        html: `
          <div class="PSI-lookup-note">
          输入助记码或编码可以过滤查询；
            ↑ ↓ 键改变当前选择项 ；回车键打开模块；ESC键关闭
          </div>
          `
      }, "->", {
        text: "确定",
        cls: "PSI-Lookup-btn",
        handler: me.onOK,
        scope: me
      }, {
        text: "取消",
        cls: "PSI-Lookup-btn",
        handler() {
          wnd.close();
          me.focus();
        }
      }]
    });

    if (!me.getShowModal()) {
      wnd.on("deactivate", () => {
        wnd.close();
      });
    }
    me.wnd = wnd;

    const editName = PCL.getCmp(me.buildId(me, "editName"));
    editName.on("change", () => {
      if (editName._inIME) {
        return;
      }

      const store = me.lookupGrid.getStore();
      me.ajax({
        url: me.URL("Home/MainMenu/queryDataForShortcut"),
        params: {
          queryKey: editName.getValue()
        },
        callback(opt, success, response) {
          store.removeAll();
          if (success) {
            const data = me.decodeJSON(response.responseText);
            store.add(data);
            if (data.length > 0) {
              me.lookupGrid.getSelectionModel().select(0);
              editName.focus();
            }
          } else {
            me.showInfo("网络错误");
          }
        },
        scope: me
      });

    }, me);

    editName.on("specialkey", (field, e) => {
      if (e.getKey() == e.ENTER) {
        me.onOK();
      } else if (e.getKey() == e.UP) {
        const m = me.lookupGrid.getSelectionModel();
        const store = me.lookupGrid.getStore();
        let index = 0;
        for (let i = 0; i < store.getCount(); i++) {
          if (m.isSelected(i)) {
            index = i;
          }
        }
        index--;
        if (index < 0) {
          index = 0;
        }
        m.select(index);
        e.preventDefault();
        editName.focus();
      } else if (e.getKey() == e.DOWN) {
        const m = me.lookupGrid.getSelectionModel();
        const store = me.lookupGrid.getStore();
        let index = 0;
        for (let i = 0; i < store.getCount(); i++) {
          if (m.isSelected(i)) {
            index = i;
          }
        }
        index++;
        if (index > store.getCount() - 1) {
          index = store.getCount() - 1;
        }
        m.select(index);
        e.preventDefault();
        editName.focus();
      }
    }, me);

    me.wnd.on("show", () => {
      me.applyIMEHandler(editName);

      editName.focus();
      editName.fireEvent("change");

      me.addMaskClickHandler(me.wnd);
    }, me);
    wnd.showBy(me);
  },

  // private
  onOK() {
    const me = this;
    const grid = me.lookupGrid;
    const item = grid.getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      return;
    }

    const menuItem = item[0];

    me.wnd.close();

    const fid = menuItem.get("fid");
    let url = PSI.Const.BASE_URL + "Home/MainMenu/navigateTo/fid/" + fid + "/t/2";
    if (fid == "-9995") {
      url = PSI.Const.BASE_URL + "Home/Help/index" + "/t/fromShortcut";
    }

    window.open(url);
  }
});
