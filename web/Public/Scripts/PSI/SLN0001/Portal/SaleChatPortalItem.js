/**
 * 销售看板 - Chat
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.Portal.SaleChatPortalItem", {
  extend: "PSI.Home.Portal.BasePortalItem",

  /**
   * @override
   */
  initComponent() {
    const me = this;

    PCL.apply(me, {
      flex: 1,
      width: "100%",
      height: 270,
      margin: "5",
      header: {
        title: `<span style='font-size:120%;font-weight:normal;'>${me.getPortalItemName()}</span>`,
        iconCls: me.getPortalItemIconClass(),
        height: 40
      },
      layout: "fit",
      items: me.getSaleChart()
    });

    me.callParent(arguments);
  },

  getSaleChart() {
    const me = this;
    if (me._saleChart) {
      return me._saleChart;
    }

    const modelName = me.buildModelName(me, "SaleChart");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["不含税销售额", "毛利", "month"]
    });
    const store = PCL.create("PCL.data.Store", {
      model: modelName,
      data: []
    });

    me._saleChart = PCL.create("PCL.chart.Chart", {
      theme: "Category1",
      animate: true,
      legend: {
        position: "top"
      },
      store: store,
      axes: [{
        title: "金额",
        type: "Numeric",
        position: "left",
        grid: true,
        fields: ["不含税销售额", "毛利"],
        label: {
          renderer: function (v) {
            return me.formatMoney2(v);
          }
        }
      }, {
        type: "Category",
        position: "bottom",
        fields: ["month"]
      }],
      series: [{
        type: "line",
        xField: "month",
        yField: "不含税销售额",
        highlight: {
          size: 7,
          radius: 7
        },
        tips: {
          trackMouse: true,
          width: 120,
          height: 50,
          renderer(storeItem, item) {
            this.setTitle("不含税销售额");
            this.update(me.formatMoney(storeItem.get("不含税销售额")));
          }
        }
      }, {
        type: "line",
        xField: "month",
        yField: "毛利",
        highlight: {
          size: 7,
          radius: 7
        },
        highlight: {
          size: 7,
          radius: 7
        },
        tips: {
          trackMouse: true,
          width: 120,
          height: 50,
          renderer(storeItem, item) {
            this.setTitle("毛利");
            this.update(me.formatMoney(storeItem.get("毛利")));
          }
        }
      }]
    });
    return me._saleChart;
  },

  formatMoney(v) {
    const value = parseFloat(v);
    const format = "0,000.00";
    if (value >= 0) {
      return PCL.util.Format.number(value, format);
    } else {
      return "-" + PCL.util.Format.number(Math.abs(value), format);
    }
  },

  formatMoney2(v) {
    const value = parseFloat(v);
    const format = "0,000";
    if (value >= 0) {
      return PCL.util.Format.number(value, format);
    } else {
      return "-" + PCL.util.Format.number(Math.abs(value), format);
    }
  },

  /**
   * @override
   */
  loadData() {
    const me = this;
    const el = me.getEl();
    el?.mask(PSI.Const.LOADING);
    me.ajax({
      url: me.URL("Home/Portal/salePortal"),
      callback(options, success, response) {
        const store = me.getSaleChart().getStore();
        store.removeAll();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          const len = data.length;
          for (let i = len - 1; i >= 0; i--) {
            const d = data[i];
            store.add({
              month: d.month,
              "不含税销售额": d.saleMoney,
              "毛利": d.profit
            });
          }
        }

        el?.unmask();
      }
    });

  },
});
