/**
 * 工厂自定义字段
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.Factory.FactoryField", {
  extend: "PCL.form.field.Trigger",
  alias: "widget.psi_factoryfield",

  mixins: ["PSI.AFX.Mix.Common"],

  config: {
    callbackFunc: null,
    callbackScope: null,
    showAddButton: false,
    showModal: false
  },

  /**
   * @override
   */
  initComponent() {
    const me = this;

    me._idValue = null;

    me.enableKeyEvents = true;

    me.callParent(arguments);

    me.on("keydown", (field, e) => {
      if (me.readOnly) {
        return;
      }

      if (e.getKey() == e.BACKSPACE) {
        field.setValue(null);
        me.setIdValue(null);
        e.preventDefault();
        return false;
      }

      if (e.getKey() != e.ENTER && !e.isSpecialKey(e.getKey())) {
        me.onTriggerClick(e);
      }
    });

    me.on({
      render(p) {
        p.getEl().on("dblclick", () => {
          me.selectText(0, 0);
          me.onTriggerClick();
        });
      },
      single: true
    });
  },

  /**
   * @override
   */
  onTriggerClick(e) {
    const me = this;
    const modelName = me.buildModelName(me, "FactoryField");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "code", "name", "tel01", "fax",
        "contact01"]
    });

    const store = PCL.create("PCL.data.Store", {
      model: modelName,
      autoLoad: false,
      data: []
    });
    const lookupGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-Lookup",
      columnLines: true,
      border: 1,
      store: store,
      viewConfig: {
        enableTextSelection: true
      },
      columns: [{
        header: "编码",
        dataIndex: "code",
        menuDisabled: true
      }, {
        header: "工厂",
        dataIndex: "name",
        menuDisabled: true,
        flex: 1
      }]
    });
    me.lookupGrid = lookupGrid;
    me.lookupGrid.on("itemdblclick", me._onOK, me);

    const wnd = PCL.create("PCL.window.Window", {
      title: "选择 - 工厂",
      modal: me.getShowModal(),
      header: false,
      border: 0,
      width: 750,
      height: 300,
      layout: "border",
      onEsc() {
        me.wnd.hide();
        me.focus();
        me.wnd.close();
      },
      items: [{
        region: "center",
        xtype: "panel",
        layout: "fit",
        border: 0,
        items: [lookupGrid]
      }, {
        xtype: "panel",
        region: "south",
        height: 32,
        layout: "fit",
        border: 0,
        items: [{
          xtype: "form",
          layout: "form",
          bodyPadding: 5,
          bodyCls: "PSI-Field",
          items: [{
            id: "PSI_Factory_FactoryField_editFactory",
            xtype: "textfield",
            labelWidth: 0,
            labelAlign: "right",
            labelSeparator: ""
          }]
        }]
      }],
      buttons: [{
        xtype: "container",
        html: `
          <div class="PSI-lookup-note">
            输入编码、工厂拼音字头可以过滤查询；
            ↑ ↓ 键改变当前选择项；回车键返回
          </div>
          `
      }, "->", {
        text: "新建工厂",
        hidden: !me.getShowAddButton(),
        cls: "PSI-Lookup-btn",
        handler: me._onAdd,
        scope: me
      }, {
        text: "确定",
        cls: "PSI-Lookup-btn",
        handler: me._onOK,
        scope: me
      }, {
        text: "取消",
        cls: "PSI-Lookup-btn",
        handler() {
          wnd.close();
          me.focus();
        }
      }]
    });

    wnd.on("close", () => {
      me.focus();
    });
    if (!me.getShowModal()) {
      wnd.on("deactivate", () => {
        wnd.close();
      });
    }
    me.wnd = wnd;

    const editName = PCL.getCmp("PSI_Factory_FactoryField_editFactory");
    editName.on("change", () => {
      if (editName._inIME) {
        return;
      }

      const store = me.lookupGrid.getStore();
      PCL.Ajax.request({
        url: PSI.Const.BASE_URL + "SLN0001/Factory/queryData",
        params: {
          queryKey: editName.getValue()
        },
        method: "POST",
        callback(opt, success, response) {
          store.removeAll();
          if (success) {
            const data = PCL.JSON.decode(response.responseText);
            store.add(data);
            if (data.length > 0) {
              me.lookupGrid.getSelectionModel().select(0);
              editName.focus();
            }
          } else {
            me.showInfo("网络错误");
          }
        },
        scope: this
      });

    }, me);

    editName.on("specialkey", (field, e) => {
      if (e.getKey() == e.ENTER) {
        me._onOK();
      } else if (e.getKey() == e.UP) {
        const m = me.lookupGrid.getSelectionModel();
        const store = me.lookupGrid.getStore();
        let index = 0;
        for (let i = 0; i < store.getCount(); i++) {
          if (m.isSelected(i)) {
            index = i;
          }
        }
        index--;
        if (index < 0) {
          index = 0;
        }
        m.select(index);
        e.preventDefault();
        editName.focus();
      } else if (e.getKey() == e.DOWN) {
        const m = me.lookupGrid.getSelectionModel();
        const store = me.lookupGrid.getStore();
        let index = 0;
        for (let i = 0; i < store.getCount(); i++) {
          if (m.isSelected(i)) {
            index = i;
          }
        }
        index++;
        if (index > store.getCount() - 1) {
          index = store.getCount() - 1;
        }
        m.select(index);
        e.preventDefault();
        editName.focus();
      }
    }, me);

    me.wnd.on("show", () => {
      me.applyIMEHandler(editName);

      editName.focus();
      editName.fireEvent("change");

      if (me.getShowModal()) {
        me.addMaskClickHandler(me.wnd);
      }
    }, me);
    wnd.showBy(me);
  },

  /**
   * @private
   */
  _onOK() {
    const me = this;
    const grid = me.lookupGrid;
    const item = grid.getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      return;
    }

    const data = item[0].getData();

    me.wnd.close();
    me.focus();
    me.setValue(data.name);
    me.focus();

    me.setIdValue(data.id);

    const func = me.getCallbackFunc();
    if (func) {
      func.apply(me.getCallbackScope(), [data, me.getCallbackScope()]);
    }
  },

  /**
   * @public
   */
  setIdValue(id) {
    this._idValue = id;
  },

  /**
   * @public
   */
  getIdValue() {
    return this._idValue;
  },

  /**
   * @public
   */
  clearIdValue() {
    this.setValue(null);
    this._idValue = null;
  },

  /**
   * @private
   */
  _onAdd() {
    const form = PCL.create("PSI.SLN0001.Factory.FactoryEditForm");
    form.show();
  }
});
