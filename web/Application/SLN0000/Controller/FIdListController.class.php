<?php

namespace SLN0000\Controller;

use Home\Common\FIdConst;
use Home\Controller\PSIBaseController;
use Home\Service\UserService;
use SLN0000\Service\FIdListService;

/**
 * FId一览Controller
 *
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
class FIdListController extends PSIBaseController
{
  /**
   * FId一览 - 主页面
   * 
   * web\Application\SLN0000\View\FIdList\index.html
   */
  public function index()
  {
    $us = new UserService();

    if ($us->hasPermission(FIdConst::FID_LIST)) {
      $this->initVar();

      $this->assign("title", "FId一览");

      $this->display();
    } else {
      $this->gotoLoginPage("/Home/FIdList");
    }
  }

  /**
   * 查询全部FId数据
   * 
   * JS: web\Public\Scripts\PSI\SLN0000\FIdList\MainForm.js
   */
  public function fidList()
  {
    if (IS_POST) {
      $us = new UserService();
      if (!$us->hasPermission(FIdConst::FID_LIST)) {
        die("没有权限");
      }


      $service = new FIdListService();
      $this->ajaxReturn($service->fidList());
    }
  }

  /**
   * 编辑fid
   * 
   * JS: web\Public\Scripts\PSI\SLN0000\FIdList\EditForm.js
   */
  public function editFId()
  {
    if (IS_POST) {
      $us = new UserService();
      if (!$us->hasPermission(FIdConst::FID_LIST)) {
        die("没有权限");
      }

      $params = [
        "fid" => I("post.fid"),
        "code" => I("post.code"),
        "py" => I("post.py"),
      ];

      $service = new FIdListService();
      $this->ajaxReturn($service->editFId($params));
    }
  }
}
