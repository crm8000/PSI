/**
 * 新增或编辑商品品牌
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.Goods.BrandEditForm", {
  extend: "PSI.AFX.Form.EditForm",

  /**
   * 初始化组件
   * 
   * @override
   */
  initComponent() {
    const me = this;
    const entity = me.getEntity();

    const t = entity == null ? "新建物料品牌" : "编辑物料品牌";
    const logoHtml = me.genLogoHtml(entity, t);

    const width1 = 600;
    const width2 = 295;
    PCL.apply(me, {
      header: {
        title: me.formatTitle(PSI.Const.PROD_NAME),
        height: 40
      },
      width: 650,
      height: 300,
      layout: "border",
      items: [{
        region: "north",
        border: 0,
        height: 70,
        html: logoHtml
      }, {
        region: "center",
        border: 0,
        id: me.buildId(me, "editForm"),
        xtype: "form",
        layout: {
          type: "table",
          columns: 2,
          tableAttrs: PSI.Const.TABLE_LAYOUT,
        },
        height: "100%",
        bodyPadding: 5,
        defaultType: 'textfield',
        fieldDefaults: {
          labelWidth: 50,
          labelAlign: "right",
          labelSeparator: "",
          msgTarget: 'side',
        },
        items: [{
          xtype: "hidden",
          name: "id",
          value: entity === null ? null : entity.get("id")
        }, {
          id: me.buildId(me, "editName"),
          fieldLabel: "品牌",
          labelWidth: 60,
          allowBlank: false,
          blankText: "没有输入品牌",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          name: "name",
          value: entity === null ? null : entity.get("text"),
          listeners: {
            specialkey: {
              fn: me._onEditNameSpecialKey,
              scope: me
            }
          },
          colspan: 2,
          width: width1,
        }, {
          id: me.buildId(me, "editParentBrand"),
          xtype: "PSI_parent_brand_editor",
          parentItem: me,
          parentBrandFunc: me._parentBrandCallbackFunc,
          fieldLabel: "上级品牌",
          labelWidth: 60,
          listeners: {
            specialkey: {
              fn: me._onEditParentBrandSpecialKey,
              scope: me
            }
          },
          colspan: 2,
          width: width1,
        }, {
          id: me.buildId(me, "editParentBrandId"),
          xtype: "hidden",
          name: "parentId",
          value: entity === null ? null : entity.get("parentId")
        }, {
          xtype: "combo",
          queryMode: "local",
          editable: false,
          valueField: "id",
          fieldLabel: "状态",
          allowBlank: false,
          blankText: "没有输入状态",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          name: "recordStatus",
          store: PCL.create("PCL.data.ArrayStore", {
            fields: ["id", "text"],
            data: [[1, "启用"], [2, "停用"]]
          }),
          value: entity == null
            ? 1
            : parseInt(entity.get("recordStatus")),
          width: width2,
        }],
        buttons: [{
          text: "确定",
          ...PSI.Const.BTN_STYLE,
          formBind: true,
          iconCls: "PSI-button-ok",
          handler: me._onOK,
          scope: me
        }, {
          text: "取消",
          ...PSI.Const.BTN_STYLE,
          handler() {
            me.confirm("请确认是否取消操作?",
              () => {
                me.close();
              });
          },
          scope: me
        }]
      }],
      listeners: {
        show: {
          fn: me._onWndShow,
          scope: me
        },
        close: {
          fn: me._onWndClose,
          scope: me
        }
      }
    });

    me.callParent(arguments);

    me.editForm = PCL.getCmp(me.buildId(me, "editForm"));

    me.editName = PCL.getCmp(me.buildId(me, "editName"));
    me.editParentBrand = PCL.getCmp(me.buildId(me, "editParentBrand"));
    me.editParentBrandId = PCL.getCmp(me.buildId(me, "editParentBrandId"));
  },

  /**
   * @private
   */
  _onWndClose() {
    const me = this;

    PCL.get(window).un('beforeunload', me.__onWindowBeforeUnload);
  },

  /**
   * @private
   */
  _onWndShow() {
    const me = this;

    PCL.get(window).on('beforeunload', me.__onWindowBeforeUnload);

    me.setFocusAndCursorPosToLast(me.editName);

    const entity = me.getEntity();
    if (entity === null) {
      return;
    }

    me.getEl().mask("数据加载中...");
    me.ajax({
      url: me.URL("SLN0001/Goods/brandParentName"),
      params: {
        id: entity.get("id")
      },
      callback(options, success, response) {
        me.getEl().unmask();
        if (success) {
          const data = me.decodeJSON(response.responseText);
          me.editParentBrand.setValue(me.htmlDecode(data.parentBrandName));
          me.editParentBrandId.setValue(data.parentBrandId);
          me.editName.setValue(me.htmlDecode(data.name));
        }
      }
    });
  },

  /**
   * @private
   */
  _parentBrandCallbackFunc(data) {
    const me = this;

    me.editParentBrand.setValue(me.htmlDecode(data.fullName));
    me.editParentBrandId.setValue(data.id);
  },

  /**
   * @private
   */
  _onOK() {
    const me = this;
    const f = me.editForm;
    const el = f.getEl();
    el?.mask("数据保存中...");
    f.submit({
      url: me.URL("SLN0001/Goods/editBrand"),
      method: "POST",
      success(form, action) {
        el.unmask();
        me.close();
        if (me.getParentForm()) {
          me.getParentForm().refreshGrid();
        }
      },
      failure(form, action) {
        el?.unmask();
        me.showInfo(action.result.msg, () => {
          me.editName.focus();
        });
      }
    });
  },

  /**
   * @private
   */
  _onEditNameSpecialKey(field, e) {
    const me = this;

    if (e.getKey() == e.ENTER) {
      me.setFocusAndCursorPosToLast(me.editParentBrand);
    }
  },

  /**
   * @private
   */
  _onEditParentBrandSpecialKey(field, e) {
    const me = this;
    if (e.getKey() == e.ENTER) {
      if (me.editForm.getForm().isValid()) {
        me._onOK();
      }
    }
  }
});
