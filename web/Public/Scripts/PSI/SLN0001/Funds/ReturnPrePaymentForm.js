/**
 * 预付款管理 - 供应商退回采购预付款
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.Funds.ReturnPrePaymentForm", {
  extend: "PSI.AFX.Form.EditForm",

  /**
   * @override
   */
  initComponent() {
    var me = this;

    var t = "供应商退回采购预付款";
    var f = "edit-form-money.png";
    const logoHtml = `
      <img style='float:left;margin:0px 20px 0px 10px;width:48px;height:48px;' 
        src='${PSI.Const.BASE_URL}Public/Images/${f}'></img>
      <div style='margin-left:60px;margin-top:0px;'>
        <h2 style='color:#595959;margin-top:0px;'>${t}</h2>
        <p style='color:#8c8c8c'>标记 <span style='color:red;font-weight:bold'>*</span>的是必须录入数据的字段</p>
      </div>
      <div style='margin:0px;border-bottom:1px solid #e6f7ff;height:1px' /></div>
      `;

    const width1 = 600;
    const width2 = 290;
    PCL.apply(me, {
      header: {
        title: me.formatTitle(PSI.Const.PROD_NAME),
        height: 40
      },
      width: 650,
      height: 300,
      layout: "border",
      defaultFocus: "editSupplier",
      listeners: {
        show: {
          fn: me.onWndShow,
          scope: me
        },
        close: {
          fn: me.onWndClose,
          scope: me
        }
      },
      items: [{
        region: "north",
        border: 0,
        height: 70,
        html: logoHtml
      }, {
        region: "center",
        border: 0,
        id: "editForm",
        xtype: "form",
        layout: {
          type: "table",
          columns: 2,
          tableAttrs: PSI.Const.TABLE_LAYOUT,
        },
        height: "100%",
        bodyPadding: 5,
        defaultType: 'textfield',
        fieldDefaults: {
          labelWidth: 65,
          labelAlign: "right",
          labelSeparator: "",
          msgTarget: 'side',
          width: width2,
          margin: "5"
        },
        items: [{
          id: "editSupplierId",
          xtype: "hidden",
          name: "supplierId"
        }, {
          id: "editSupplier",
          fieldLabel: "供应商",
          xtype: "psi_supplierfield",
          allowBlank: false,
          blankText: "没有输入供应商",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          listeners: {
            specialkey: {
              fn: me.onEditSupplierSpecialKey,
              scope: me
            }
          }
        }, {
          id: "editBizDT",
          fieldLabel: "退款日期",
          allowBlank: false,
          blankText: "没有输入退款日期",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          xtype: "datefield",
          format: "Y-m-d",
          value: new Date(),
          name: "bizDT",
          listeners: {
            specialkey: {
              fn: me.onEditBizDTSpecialKey,
              scope: me
            }
          }
        }, {
          fieldLabel: "退款金额",
          allowBlank: false,
          blankText: "没有输入退款金额",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          xtype: "numberfield",
          hideTrigger: true,
          name: "inMoney",
          id: "editInMoney",
          listeners: {
            specialkey: {
              fn: me.onEditInMoneySpecialKey,
              scope: me
            }
          }
        }, {
          id: "editBizUserId",
          xtype: "hidden",
          name: "bizUserId"
        }, {
          id: "editBizUser",
          fieldLabel: "收款人",
          xtype: "psi_userfield",
          allowBlank: false,
          blankText: "没有输入收款人",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          listeners: {
            specialkey: {
              fn: me.onEditBizUserSpecialKey,
              scope: me
            }
          }
        }, {
          fieldLabel: "备注",
          name: "memo",
          id: "editMemo",
          listeners: {
            specialkey: {
              fn: me.onEditMemoSpecialKey,
              scope: me
            }
          },
          colspan: 2,
          width: width1,
        }],
        buttons: [{
          text: "保存",
          ...PSI.Const.BTN_STYLE,
          iconCls: "PSI-button-ok",
          formBind: true,
          handler: me.onOK,
          scope: me
        }, {
          text: "取消",
          ...PSI.Const.BTN_STYLE,
          handler() {
            me.close();
          },
          scope: me
        }]
      }]
    });

    me.callParent(arguments);
  },

  onWindowBeforeUnload(e) {
    return (window.event.returnValue = e.returnValue = '确认离开当前页面？');
  },

  onWndClose() {
    var me = this;

    PCL.get(window).un('beforeunload', me.onWindowBeforeUnload);
  },

  onWndShow() {
    var me = this;

    PCL.get(window).on('beforeunload', me.onWindowBeforeUnload);

    var f = PCL.getCmp("editForm");
    var el = f.getEl();
    el.mask(PSI.Const.LOADING);
    PCL.Ajax.request({
      url: PSI.Const.BASE_URL
        + "SLN0001/Funds/returnPrePaymentInfo",
      params: {},
      method: "POST",
      callback(options, success, response) {
        el.unmask();

        if (success) {
          var data = PCL.JSON.decode(response.responseText);

          PCL.getCmp("editBizUserId").setValue(data.bizUserId);
          PCL.getCmp("editBizUser").setValue(data.bizUserName);
          PCL.getCmp("editBizUser").setIdValue(data.bizUserId);
        } else {
          me.showInfo("网络错误")
        }
      }
    });
  },

  // private
  onOK() {
    var me = this;
    PCL.getCmp("editBizUserId").setValue(PCL.getCmp("editBizUser").getIdValue());
    PCL.getCmp("editSupplierId").setValue(PCL.getCmp("editSupplier").getIdValue());

    var f = PCL.getCmp("editForm");
    var el = f.getEl();
    el.mask(PSI.Const.SAVING);
    f.submit({
      url: PSI.Const.BASE_URL + "SLN0001/Funds/returnPrePayment",
      method: "POST",
      success(form, action) {
        el.unmask();

        me.close();

        me.getParentForm().onQuery();
      },
      failure(form, action) {
        el.unmask();
        me.showInfo(action.result.msg, () => {
          PCL.getCmp("editBizDT").focus();
        });
      }
    });
  },

  onEditSupplierSpecialKey(field, e) {
    if (e.getKey() == e.ENTER) {
      PCL.getCmp("editBizDT").focus();
    }
  },

  onEditBizDTSpecialKey(field, e) {
    if (e.getKey() == e.ENTER) {
      PCL.getCmp("editInMoney").focus();
    }
  },

  onEditInMoneySpecialKey(field, e) {
    if (e.getKey() == e.ENTER) {
      PCL.getCmp("editBizUser").focus();
    }
  },

  onEditBizUserSpecialKey(field, e) {
    if (e.getKey() == e.ENTER) {
      PCL.getCmp("editMemo").focus();
    }
  },

  onEditMemoSpecialKey(field, e) {
    if (e.getKey() == e.ENTER) {
      var f = PCL.getCmp("editForm");
      if (f.getForm().isValid()) {
        var me = this;
        me.confirm("请确认是否录入退款记录?", () => {
          me.onOK();
        });
      }
    }
  }
});
