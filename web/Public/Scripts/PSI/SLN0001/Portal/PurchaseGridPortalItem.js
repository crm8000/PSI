/**
 * 采购看板 - 列表
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.Portal.PurchaseGridPortalItem", {
  extend: "PSI.Home.Portal.BasePortalItem",

  /**
   * @override
   */
  initComponent() {
    const me = this;

    PCL.apply(me, {
      flex: 1,
      width: "100%",
      height: 270,
      margin: "5",
      header: {
        title: `<span style='font-size:120%;font-weight:normal;'>${me.getPortalItemName()}</span>`,
        iconCls: me.getPortalItemIconClass(),
        height: 40
      },
      layout: "fit",
      items: me.getPurchaseGrid()
    });

    me.callParent(arguments);
  },

  getPurchaseGrid() {
    const me = this;
    if (me._purchaseGrid) {
      return me._purchaseGrid;
    }

    const modelName = me.buildModelName(me, "PortalPurchase");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["month", "purchaseMoney"]
    });

    me._purchaseGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-KB",
      viewConfig: {
        enableTextSelection: true
      },
      columnLines: true,
      border: 0,
      columns: [{
        header: "月份",
        dataIndex: "month",
        width: 80,
        menuDisabled: true,
        sortable: false
      }, {
        header: "采购额",
        dataIndex: "purchaseMoney",
        width: 160,
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn"
      }],
      store: PCL.create("PCL.data.Store", {
        model: modelName,
        autoLoad: false,
        data: []
      })
    });

    return me._purchaseGrid;
  },

  /**
   * @override
   */
  loadData() {
    const me = this;
    const el = me.getEl();
    el?.mask(PSI.Const.LOADING);
    me.ajax({
      url: me.URL("Home/Portal/purchasePortal"),
      callback(options, success, response) {
        const store = me.getPurchaseGrid().getStore();
        store.removeAll();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          store.add(data);
        }

        el?.unmask();
      }
    });
  },
});
