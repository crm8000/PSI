/**
 * 销售订单 - 订单变更界面
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.SaleOrder.ChangeOrderEditForm", {
  extend: "PSI.AFX.Form.EditForm",

  /**
   * @override
   */
  initComponent() {
    const me = this;

    const entity = me.getEntity();

    const buttons = [];

    let btn = {
      text: "保存",
      ...PSI.Const.BTN_STYLE,
      formBind: true,
      iconCls: "PSI-button-ok",
      handler() {
        me._onOK();
      },
      scope: me
    };
    buttons.push(btn);

    btn = {
      text: "取消",
      ...PSI.Const.BTN_STYLE,
      handler() {
        me.close();
      },
      scope: me
    };
    buttons.push(btn);

    const t = "订单变更";
    const f = "edit-form-update.png";
    const logoHtml = 
      `<img style='float:left;margin:10px 20px 0px 10px;width:48px;height:48px;' 
        src='${PSI.Const.BASE_URL}Public/Images/${f}'></img>
      <h2 style='color:#196d83'>
        ${t}
      </h2>
      <p style='color:#196d83'>标记 <span style='color:red;font-weight:bold'>*</span>的是必须录入数据的字段</p>`;
    PCL.apply(me, {
      header: {
        title: me.formatTitle(PSI.Const.PROD_NAME),
        height: 40
      },
      width: 400,
      height: 380,
      layout: "border",
      listeners: {
        show: {
          fn: me._onWndShow,
          scope: me
        },
        close: {
          fn: me._onWndClose,
          scope: me
        }
      },
      items: [{
        region: "north",
        height: 90,
        border: 0,
        html: logoHtml
      }, {
        region: "center",
        border: 0,
        id: me.buildId(me, "editForm"),
        xtype: "form",
        layout: {
          type: "table",
          columns: 2
        },
        height: "100%",
        bodyPadding: 5,
        defaultType: 'textfield',
        fieldDefaults: {
          labelWidth: 70,
          labelAlign: "right",
          labelSeparator: "",
          msgTarget: 'side',
          width: 370,
          margin: "5"
        },
        items: [{
          xtype: "hidden",
          name: "id",
          value: entity.get("id")
        }, {
          fieldLabel: "商品编码",
          readOnly: true,
          value: entity.get("goodsCode"),
          colspan: 2
        }, {
          fieldLabel: "商品名称",
          readOnly: true,
          value: entity.get("goodsName"),
          colspan: 2
        }, {
          fieldLabel: "规格型号",
          readOnly: true,
          value: entity.get("goodsSpec"),
          colspan: 2
        }, {
          id: me.buildId(me, "editGoodsCount"),
          fieldLabel: "销售数量",
          xtype: "numberfield",
          hideTrigger: true,
          name: "goodsCount",
          value: entity.get("goodsCount"),
          allowDecimals: PSI.Const.GC_DEC_NUMBER > 0,
          decimalPrecision: PSI.Const.GC_DEC_NUMBER,
          minValue: 0,
          allowBlank: false,
          blankText: "没有输入销售数量",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          width: 180,
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            },
            change: {
              fn: me._onEditGoodsCountChange,
              scope: me
            }
          }
        }, {
          fieldLabel: "单位",
          readOnly: true,
          value: entity.get("unitName"),
          width: 180
        }, {
          id: me.buildId(me, "editGoodsPrice"),
          fieldLabel: "销售单价",
          hideTrigger: true,
          xtype: "numberfield",
          name: "goodsPrice",
          value: entity.get("goodsPrice"),
          allowDecimals: true,
          decimalPrecision: 2,
          minValue: 0,
          allowBlank: false,
          blankText: "没有输入销售单价",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          width: 180,
          listeners: {
            specialkey: {
              fn: me._onEditGoodsPriceSpecialKey,
              scope: me
            },
            change: {
              fn: me._onEditGoodsPriceChange,
              scope: me
            }
          }
        }, {
          id: me.buildId(me, "editGoodsMoney"),
          fieldLabel: "销售金额",
          readOnly: true,
          value: entity.get("goodsMoney"),
          width: 180
        }, {
          id: me.buildId(me, "editGoodsWSCount"),
          fieldLabel: "已出库数量",
          readOnly: true,
          value: entity.get("wsCount"),
          width: 180
        }, {
          id: me.buildId(me, "editGoodsLeftCount"),
          fieldLabel: "未出库数量",
          readOnly: true,
          value: entity.get("leftCount"),
          width: 180
        }],
        buttons: buttons
      }]
    });

    me.callParent(arguments);

    me.editForm = PCL.getCmp(me.buildId(me, "editForm"));
    me.editGoodsCount = PCL.getCmp(me.buildId(me, "editGoodsCount"));
    me.editGoodsPrice = PCL.getCmp(me.buildId(me, "editGoodsPrice"));
    me.editGoodsMoney = PCL.getCmp(me.buildId(me, "editGoodsMoney"));
    me.editWSCount = PCL.getCmp(me.buildId(me, "editGoodsWSCount"));
    me.editLeftCount = PCL.getCmp(me.buildId(me, "editGoodsLeftCount"));

    // AFX
    me.__editorList = [
      me.editGoodsCount,
      me.editGoodsPrice,
    ];
  },

  /**
   * @private
   */
  _onOK(thenAdd) {
    const me = this;

    const confirmFunc = () => {
      const f = me.editForm;
      const el = f.getEl();
      el.mask(PSI.Const.SAVING);
      const sf = {
        url: me.URL("SLN0001/SaleOrder/changeSaleOrder"),
        method: "POST",
        success(form, action) {
          me._lastId = action.result.id;

          el.unmask();

          me.tip("数据保存成功", true);
          me.focus();
          me.close();
        },
        failure(form, action) {
          el.unmask();
          me.showInfo(action.result.msg, () => {
            me.setFocusAndCursorPosToLast(me.editGoodsCount);
          });
        }
      };
      f.submit(sf);
    };

    me.confirm("请确认是否变更销售订单?", confirmFunc);
  },

  /**
   * @private
   */
  _onWndClose() {
    const me = this;

    // AFX
    PCL.get(window).un('beforeunload', me.__onWindowBeforeUnload);

    if (me._lastId) {
      const parentForm = me.getParentForm();
      if (parentForm) {
        parentForm.refreshAterChangeOrder.apply(parentForm, [me._lastId]);
      }
    }

    const parentForm = me.getParentForm();
    if (parentForm) {
      parentForm._keyMapMain.enable();
    }
  },

  /**
   * @private
   */
  _onWndShow() {
    const me = this;

    // AFX
    PCL.get(window).on('beforeunload', me.__onWindowBeforeUnload);

    me.setFocusAndCursorPosToLast(me.editGoodsCount);

    const parentForm = me.getParentForm();
    if (parentForm) {
      parentForm._keyMapMain.disable();
    }
  },

  /**
   * @private
   */
  _onEditGoodsPriceSpecialKey(field, e) {
    const me = this;

    if (e.getKey() == e.ENTER) {
      me._onOK();
    }
  },

  /**
   * @private
   */
  _onEditGoodsCountChange() {
    const me = this;

    const cnt = me.editGoodsCount.getValue();
    const price = me.editGoodsPrice.getValue();
    me.editGoodsMoney.setValue(cnt * price);

    const wsCount = me.editWSCount.getValue();
    me.editLeftCount.setValue(cnt - wsCount);
  },

  /**
   * @private
   */
  _onEditGoodsPriceChange() {
    const me = this;

    const cnt = me.editGoodsCount.getValue();
    const price = me.editGoodsPrice.getValue();
    me.editGoodsMoney.setValue(cnt * price);
  }
});
