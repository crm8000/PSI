<?php

namespace SLN0002\Service;

use Home\DAO\OrgDAO;
use Home\Service\BizlogService;
use Home\Service\PSIBaseExService;
use SLN0002\DAO\InitDAO;

/**
 * 总账建账 Service
 *
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
class InitService extends PSIBaseExService
{
  private $LOG_CATEGORY_GL = "总分类账建账";
  private $LOG_CATEGORY_DETAIL = "明细分类账建账";

  /**
   * 返回所有的公司列表
   *
   * @return array
   */
  public function companyList($fid)
  {
    if ($this->isNotOnline()) {
      return $this->emptyResult();
    }

    $params = [
      "loginUserId" => $this->getLoginUserId(),
      "fid" => $fid
    ];

    $dao = new OrgDAO($this->db());
    return $dao->getCompanyExList($params);
  }

  /**
   * 查询明细分类账建账数据
   */
  public function queryAccDetailData($params)
  {
    if ($this->isNotOnline()) {
      return $this->emptyResult();
    }

    $dao = new InitDAO($this->db());
    return $dao->queryAccDetailData($params);
  }

  /**
   * 录入明细分类账期初数据
   * 
   */
  public function editAccDetailInitRecord($json)
  {
    if ($this->isNotOnline()) {
      return $this->notOnlineError();
    }

    $params = json_decode(html_entity_decode($json), true);
    if ($params == null) {
      return $this->bad("传入的参数错误，不是正确的JSON格式");
    }

    $db = $this->db();
    $db->startTrans();

    $dao = new InitDAO($db);
    $rc = $dao->editAccDetailInitRecord($params);
    if ($rc) {
      $db->rollback();
      return $rc;
    }

    $id = $params["id"];
    // 记录业务日志
    $log = $params["log"];
    $bs = new BizlogService($db);
    $bs->insertBizlog($log, $this->LOG_CATEGORY_DETAIL);

    $db->commit();

    return $this->ok($id);
  }

  /**
   * 查询账样的扩展项
   */
  function queryFmtEx($params)
  {
    if ($this->isNotOnline()) {
      return $this->emptyResult();
    }

    $dao = new InitDAO($this->db());
    return $dao->queryFmtEx($params);
  }

  /**
   * 查询账样列
   */
  function queryFmtColsForDetailAcc($params)
  {
    if ($this->isNotOnline()) {
      return $this->emptyResult();
    }

    $dao = new InitDAO($this->db());
    return $dao->queryFmtColsForDetailAcc($params);
  }

  /**
   * 查询明细分类账建账数据
   */
  public function queryDataForDetailAcc($params)
  {
    if ($this->isNotOnline()) {
      return $this->emptyResult();
    }

    $dao = new InitDAO($this->db());
    return $dao->queryDataForDetailAcc($params);
  }

  /**
   * 查询总账建账期间
   */
  public function queryAccInitYearAndMonth($params)
  {
    if ($this->isNotOnline()) {
      return $this->emptyResult();
    }

    $dao = new InitDAO($this->db());
    return $dao->queryAccInitYearAndMonth($params);
  }

  /**
   * 完成明细分类账建账
   */
  public function commitAccDetailInit($params)
  {
    if ($this->isNotOnline()) {
      return $this->notOnlineError();
    }

    $db = $this->db();
    $db->startTrans();

    $dao = new InitDAO($db);
    $rc = $dao->commitAccDetailInit($params);
    if ($rc) {
      $db->rollback();
      return $rc;
    }

    // 记录业务日志
    $log = $params["log"];
    $bs = new BizlogService($db);
    $bs->insertBizlog($log, $this->LOG_CATEGORY_DETAIL);

    $db->commit();

    return $this->ok();
  }

  /**
   * 取消明细分类账建账
   */
  public function cancelAccDetailInit($params)
  {
    if ($this->isNotOnline()) {
      return $this->notOnlineError();
    }

    $db = $this->db();
    $db->startTrans();

    $dao = new InitDAO($db);
    $rc = $dao->cancelAccDetailInit($params);
    if ($rc) {
      $db->rollback();
      return $rc;
    }

    // 记录业务日志
    $log = $params["log"];
    $bs = new BizlogService($db);
    $bs->insertBizlog($log, $this->LOG_CATEGORY_DETAIL);

    $db->commit();

    return $this->ok();
  }

  /**
   * 从明细分类账生成总账建账数据
   */
  public function genInitDataFromAccDetail($params)
  {
    if ($this->isNotOnline()) {
      return $this->notOnlineError();
    }

    $db = $this->db();
    $db->startTrans();

    $dao = new InitDAO($db);
    $rc = $dao->genInitDataFromAccDetail($params);
    if ($rc) {
      $db->rollback();
      return $rc;
    }

    // 记录业务日志
    $log = $params["log"];
    $bs = new BizlogService($db);
    $bs->insertBizlog($log, $this->LOG_CATEGORY_GL);

    $db->commit();

    return $this->ok();
  }

  /**
   * 查询建账完成情况
   * 
   */
  public function queryInitStatus($params)
  {
    if ($this->isNotOnline()) {
      return $this->emptyResult();
    }

    $dao = new InitDAO($this->db());
    return $dao->queryInitStatus($params);
  }

  /**
   * 查询总分类账建账数据
   */
  public function queryAccGlData($params)
  {
    if ($this->isNotOnline()) {
      return $this->emptyResult();
    }

    $dao = new InitDAO($this->db());
    return $dao->queryAccGlData($params);
  }

  /**
   * 查询总分类账建账数据
   */
  public function queryDataForGlAcc($params)
  {
    if ($this->isNotOnline()) {
      return $this->emptyResult();
    }

    $dao = new InitDAO($this->db());
    return $dao->queryDataForGlAcc($params);
  }

  /**
   * 查询账样列
   */
  function queryFmtColsForGlAcc($params)
  {
    if ($this->isNotOnline()) {
      return $this->emptyResult();
    }

    $dao = new InitDAO($this->db());
    return $dao->queryFmtColsForGlAcc($params);
  }

  /**
   * 完成总分类账建账
   */
  public function commitAccGlInit($params)
  {
    if ($this->isNotOnline()) {
      return $this->notOnlineError();
    }

    $db = $this->db();
    $db->startTrans();

    $dao = new InitDAO($db);
    $rc = $dao->commitAccGlInit($params);
    if ($rc) {
      $db->rollback();
      return $rc;
    }

    // 记录业务日志
    $log = $params["log"];
    $bs = new BizlogService($db);
    $bs->insertBizlog($log, $this->LOG_CATEGORY_GL);

    $db->commit();

    return $this->ok();
  }

  /**
   * 取消总分类账建账
   */
  public function cancelAccGlInit($params)
  {
    if ($this->isNotOnline()) {
      return $this->notOnlineError();
    }

    $db = $this->db();
    $db->startTrans();

    $dao = new InitDAO($db);
    $rc = $dao->cancelAccGlInit($params);
    if ($rc) {
      $db->rollback();
      return $rc;
    }

    // 记录业务日志
    $log = $params["log"];
    $bs = new BizlogService($db);
    $bs->insertBizlog($log, $this->LOG_CATEGORY_GL);

    $db->commit();

    return $this->ok();
  }
}
