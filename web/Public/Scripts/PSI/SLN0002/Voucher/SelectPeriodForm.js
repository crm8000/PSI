/**
 * 选择会计期间界面
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0002.Voucher.SelectPeriodForm", {
  extend: "PSI.AFX.Form.EditForm",

  config: {
    caption: "",
    companyName: "",
    voucherYear: null,
    voucherMonth: null,

    okCallbackFunc: null,
  },

  /**
   * 初始化组件
   * 
   * @override
   */
  initComponent() {
    const me = this;

    const buttons = [{
      text: "确定",
      ...PSI.Const.BTN_STYLE,
      formBind: true,
      iconCls: "PSI-button-ok",
      handler() {
        me._onOK();
      },
      scope: me
    }, {
      text: "取消",
      ...PSI.Const.BTN_STYLE,
      handler() {
        me.close();
      },
      scope: me
    }];

    const logoHtml = me.genLogoHtml(null, me.getCaption());

    const width1 = 600;

    PCL.apply(me, {
      header: {
        title: me.formatTitle(PSI.Const.PROD_NAME),
        height: 40
      },
      width: 650,
      height: 300,
      layout: "border",
      listeners: {
        show: {
          fn: me._onWndShow,
          scope: me
        },
        close: {
          fn: me._onWndClose,
          scope: me
        }
      },
      items: [{
        region: "north",
        height: 70,
        border: 0,
        html: logoHtml
      }, {
        region: "center",
        border: 0,
        xtype: "form",
        layout: {
          type: "table",
          columns: 1,
          tableAttrs: PSI.Const.TABLE_LAYOUT,
        },
        height: "100%",
        bodyPadding: 5,
        defaultType: 'textfield',
        fieldDefaults: {
          labelWidth: 85,
          labelAlign: "right",
          labelSeparator: "",
          msgTarget: 'side',
          width: width1,
        },
        items: [{
          xtype: "displayfield",
          fieldLabel: "组织机构",
          value: me.toFieldNoteText(me.getCompanyName()),
        }, {
          id: me.buildId(me, "editQueryPeriod"),
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          fieldLabel: "会计期间",
          margin: 0,
          xtype: "fieldcontainer",
          layout: "hbox",
          items: [
            {
              xtype: "numberfield",
              hideTrigger: true,
              allowDecimals: false,
              width: 40,
              value: me.getVoucherYear(),
              id: me.buildId(me, "editVoucherYear"),
            }, { xtype: "displayfield", value: "年", width: 15, }, {
              xtype: "combobox",
              width: 70,
              store: PCL.create("PCL.data.ArrayStore", {
                fields: ["id", "text"],
                data: [[1, "一月"], [2, "二月"],
                [3, "三月"], [4, "四月"],
                [5, "五月"], [6, "六月"],
                [7, "七月"], [8, "八月"],
                [9, "九月"], [10, "十月"],
                [11, "十一月"], [12, "十二月"]]
              }),
              valueField: "id",
              displayFIeld: "text",
              queryMode: "local",
              editable: false,
              value: me.getVoucherMonth(),
              id: me.buildId(me, "editVoucherMonth"),
            }
          ],
        },],
        buttons,
      }]
    });

    me.callParent(arguments);

    me.editVoucherYear = PCL.getCmp(me.buildId(me, "editVoucherYear"));
    me.editVoucherMonth = PCL.getCmp(me.buildId(me, "editVoucherMonth"));
  },

  /**
   * @private
   */
  _onOK() {
    const me = this;

    const year = me.editVoucherYear.getValue();
    const month = me.editVoucherMonth.getValue();

    const info = `请确认是否执行 ${me.htInfo(me.getCaption())} 操作?`;

    const funcConfirm = () => {
      const func = me.getOkCallbackFunc();
      if (func) {
        func.apply(me.getParentForm(), [year, month]);
      }

      me.close();
    };

    me.confirm(info, funcConfirm);

  },

  /**
   * @private
   */
  _onWndClose() {
    const me = this;
    PSI.Const.msgBoxShowing = false;
  },

  /**
   * @private
   */
  _onWndShow() {
    const me = this;

    PSI.Const.msgBoxShowing = true;

  },
});
