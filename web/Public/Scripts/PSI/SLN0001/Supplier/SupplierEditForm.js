/**
 * 供应商档案 - 新建或编辑界面
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.Supplier.SupplierEditForm", {
  extend: "PSI.AFX.Form.EditForm",

  /**
   * @override
   */
  initComponent() {
    const me = this;
    const entity = me.getEntity();
    me.adding = entity == null;

    const buttons = [];
    if (!entity) {
      buttons.push({
        text: "保存并继续新建",
        ...PSI.Const.BTN_STYLE,
        formBind: true,
        handler() {
          me._onOK(true);
        },
        scope: me
      });
    }

    buttons.push({
      text: "保存",
      ...PSI.Const.BTN_STYLE,
      formBind: true,
      iconCls: "PSI-button-ok",
      handler() {
        me._onOK(false);
      },
      scope: me
    }, {
      text: entity == null ? "关闭" : "取消",
      ...PSI.Const.BTN_STYLE,
      handler() {
        me.close();
      },
      scope: me
    });

    const t = entity == null ? "新建供应商档案" : "编辑供应商档案";
    const logoHtml = me.genLogoHtml(entity, t);

    PCL.apply(me, {
      header: {
        title: me.formatTitle(PSI.Const.PROD_NAME),
        height: 40
      },
      width: 850,
      height: 520,
      layout: "border",
      items: [{
        region: "north",
        border: 0,
        height: 50,
        html: logoHtml
      }, {
        region: "center",
        border: 0,
        layout: "fit",
        items: {
          xtype: "tabpanel",
          id: me.buildId(me, "tabPanel"),
          border: 0,
          bodyStyle: { borderWidth: 0 },
          items: [{
            title: "通用",
            id: me.buildId(me, "tabGeneral"),
            border: 0,
            layout: "fit",
            items: {
              border: 0,
              xtype: "form",
              id: me.buildId(me, "form1"),
              bodyStyle: "margin-top:10px",
              layout: {
                type: "table",
                columns: 2,
                tableAttrs: PSI.Const.TABLE_LAYOUT,
              },
              defaultType: 'textfield',
              fieldDefaults: {
                labelWidth: 70,
                labelAlign: "right",
                labelSeparator: "",
                msgTarget: 'side'
              },
              items: me.getGeneralInputs()
            }
          }, {
            title: "联系人",
            id: me.buildId(me, "tabContact"),
            border: 0,
            layout: "fit",
            items: {
              border: 0,
              xtype: "form",
              id: me.buildId(me, "form2"),
              bodyStyle: "margin-top:10px",
              layout: {
                type: "table",
                columns: 2,
                tableAttrs: PSI.Const.TABLE_LAYOUT,
              },
              defaultType: 'textfield',
              fieldDefaults: {
                labelWidth: 70,
                labelAlign: "right",
                labelSeparator: "",
                msgTarget: 'side'
              },
              items: me.getContactInputs(),
            }
          }, {
            title: "应付账款",
            id: me.buildId(me, "tabPayables"),
            border: 0,
            layout: "fit",
            items: {
              border: 0,
              xtype: "form",
              id: me.buildId(me, "form3"),
              bodyStyle: "margin-top:10px",
              layout: {
                type: "table",
                columns: 2,
                tableAttrs: PSI.Const.TABLE_LAYOUT,
              },
              defaultType: 'textfield',
              fieldDefaults: {
                labelWidth: 90,
                labelAlign: "right",
                labelSeparator: "",
                msgTarget: 'side'
              },
              items: me.getPayablesInputs(),
            }
          }, {
            title: "物料",
            id: me.buildId(me, "tabMaterial"),
            border: 0,
            layout: "fit",
            items: {
              border: 0,
              xtype: "form",
              id: me.buildId(me, "form4"),
              bodyStyle: "margin-top:10px",
              layout: {
                type: "table",
                columns: 2,
                tableAttrs: PSI.Const.TABLE_LAYOUT,
              },
              defaultType: 'textfield',
              fieldDefaults: {
                labelWidth: 70,
                labelAlign: "right",
                labelSeparator: "",
                msgTarget: 'side'
              },
              items: me.getMaterialInputs(),
            }
          },],
          listeners: {
            tabchange: {
              fn: me._onTabChange,
              scope: me
            },
          }
        }
      }],
      buttons,
      listeners: {
        show: {
          fn: me._onWndShow,
          scope: me
        },
        close: {
          fn: me._onWndClose,
          scope: me
        }
      }
    });

    me.callParent(arguments);

    me.editCategory = PCL.getCmp(me.buildId(me, "editCategory"));
    me.editCode = PCL.getCmp(me.buildId(me, "editCode"));
    me.editName = PCL.getCmp(me.buildId(me, "editName"));
    me.editAddress = PCL.getCmp(me.buildId(me, "editAddress"));
    me.editContact01 = PCL.getCmp(me.buildId(me, "editContact01"));
    me.editMobile01 = PCL.getCmp(me.buildId(me, "editMobile01"));
    me.editTel01 = PCL.getCmp(me.buildId(me, "editTel01"));
    me.editQQ01 = PCL.getCmp(me.buildId(me, "editQQ01"));
    me.editContact02 = PCL.getCmp(me.buildId(me, "editContact02"));
    me.editMobile02 = PCL.getCmp(me.buildId(me, "editMobile02"));
    me.editTel02 = PCL.getCmp(me.buildId(me, "editTel02"));
    me.editQQ02 = PCL.getCmp(me.buildId(me, "editQQ02"));
    me.editAddressShipping = PCL.getCmp(me.buildId(me, "editAddressShipping"));
    me.editBankName = PCL.getCmp(me.buildId(me, "editBankName"));
    me.editBankAccount = PCL.getCmp(me.buildId(me, "editBankAccount"));
    me.editTax = PCL.getCmp(me.buildId(me, "editTax"));
    me.editFax = PCL.getCmp(me.buildId(me, "editFax"));
    me.editInitPayables = PCL.getCmp(me.buildId(me, "editInitPayables"));
    me.editInitPayablesDT = PCL.getCmp(me.buildId(me, "editInitPayablesDT"));
    me.editNote = PCL.getCmp(me.buildId(me, "editNote"));

    me.editInitPayablesInfo = PCL.getCmp(me.buildId(me, "editInitPayablesInfo"));

    me.editTaxRate = PCL.getCmp(me.buildId(me, "editTaxRate"));

    me.editRecordStatus = PCL.getCmp(me.buildId(me, "editRecordStatus"));
    me.editGoodsRange = PCL.getCmp(me.buildId(me, "editGoodsRange"));

    // AFX - 启用tab页多Input跳转
    me.__useTabPanel = true;
    me.__tabPanelId = me.buildId(me, "tabPanel");

    me.__editorList = [
      [me.editCategory, me.editCode, me.editName, me.editAddress,
      me.editAddressShipping, me.editBankName, me.editBankAccount, me.editFax,
      me.editTax, me.editTaxRate, me.editNote],
      [me.editContact01, me.editMobile01,
      me.editTel01, me.editQQ01, me.editContact02, me.editMobile02,
      me.editTel02, me.editQQ02],
      [me.editInitPayables, me.editInitPayablesDT],
      [me.editGoodsRange]
    ];

    me.form1 = PCL.getCmp(me.buildId(me, "form1"));
    me.form2 = PCL.getCmp(me.buildId(me, "form2"));
    me.form3 = PCL.getCmp(me.buildId(me, "form3"));
    me.form4 = PCL.getCmp(me.buildId(me, "form4"));
    me.tabPanelMain = PCL.getCmp(me.buildId(me, "tabPanel"));
  },

  /**
   * @private
   */
  getGeneralInputs() {
    const me = this;

    const entity = me.getEntity();

    let categoryStore = null;
    if (me.getParentForm()) {
      categoryStore = me.getParentForm().getCategoryGrid().getStore();
    }

    const width1 = 800;
    const width2 = 395;

    return [{
      xtype: "hidden",
      name: "id",
      value: entity == null ? null : entity.get("id")
    }, {
      id: me.buildId(me, "editCategory"),
      xtype: "combo",
      fieldLabel: "分类",
      allowBlank: false,
      blankText: "没有输入供应商分类",
      beforeLabelTextTpl: PSI.Const.REQUIRED,
      valueField: "id",
      displayField: "name",
      store: categoryStore,
      queryMode: "local",
      editable: false,
      value: categoryStore != null
        ? categoryStore.getAt(0).get("id")
        : null,
      name: "categoryId",
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      },
      width: width2,
    }, {
      id: me.buildId(me, "editCode"),
      fieldLabel: "编码",
      allowBlank: false,
      blankText: "没有输入供应商编码",
      beforeLabelTextTpl: PSI.Const.REQUIRED,
      name: "code",
      value: entity == null ? null : entity.get("code"),
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      },
      width: width2,
    }, {
      id: me.buildId(me, "editName"),
      fieldLabel: "供应商名称",
      labelWidth: 80,
      allowBlank: false,
      blankText: "没有输入供应商名称",
      beforeLabelTextTpl: PSI.Const.REQUIRED,
      name: "name",
      value: entity == null ? null : entity.get("name"),
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      },
      colspan: 2,
      width: width1
    }, {
      id: me.buildId(me, "editAddress"),
      fieldLabel: "地址",
      name: "address",
      value: entity == null ? null : entity.get("address"),
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      },
      colspan: 2,
      width: width1
    }, {
      id: me.buildId(me, "editAddressShipping"),
      fieldLabel: "发货地址",
      name: "addressShipping",
      value: entity == null ? null : entity.get("addressShipping"),
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      },
      colspan: 2,
      width: width1
    }, {
      id: me.buildId(me, "editBankName"),
      fieldLabel: "开户行",
      name: "bankName",
      value: entity == null ? null : entity.get("bankName"),
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      },
      width: width2,
    }, {
      id: me.buildId(me, "editBankAccount"),
      fieldLabel: "开户行账号",
      name: "bankAccount",
      value: entity == null ? null : entity.get("bankAccount"),
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      },
      width: width2,
    }, {
      id: me.buildId(me, "editFax"),
      fieldLabel: "传真",
      name: "fax",
      value: entity == null ? null : entity
        .get("fax"),
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      },
      width: width2,
    }, {
      id: me.buildId(me, "editTax"),
      fieldLabel: "社会统一信用代码",
      labelWidth: 115,
      name: "tax",
      value: entity == null ? null : entity.get("tax"),
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      },
      width: width2,
    }, {
      id: me.buildId(me, "editTaxRate"),
      fieldLabel: "税率",
      name: "taxRate",
      xtype: "numberfield",
      hideTrigger: true,
      allowDecimals: false,
      value: entity == null ? null : entity.get("taxRate"),
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      },
      width: width2,
    }, {
      xtype: "displayfield",
      value: "%"
    }, {
      id: me.buildId(me, "editNote"),
      fieldLabel: "备注",
      name: "note",
      value: entity == null ? null : entity.get("note"),
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      },
      width: width1,
      colspan: 2
    }, {
      id: me.buildId(me, "editRecordStatus"),
      xtype: "combo",
      queryMode: "local",
      editable: false,
      valueField: "id",
      fieldLabel: "状态",
      name: "recordStatus",
      store: PCL.create("PCL.data.ArrayStore", {
        fields: ["id", "text"],
        data: [[1000, "启用"], [0, "停用"]]
      }),
      value: 1000,
      width: width2,
    }];
  },

  /**
   * @private
   */
  getContactInputs() {
    const me = this;

    const entity = me.getEntity();

    const width1 = 800;
    const width2 = 395;

    return [{
      id: me.buildId(me, "editContact01"),
      fieldLabel: "联系人",
      name: "contact01",
      value: entity == null ? null : entity.get("contact01"),
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      },
      width: width2,
    }, {
      id: me.buildId(me, "editMobile01"),
      fieldLabel: "手机",
      name: "mobile01",
      value: entity == null ? null : entity.get("mobile01"),
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      },
      width: width2,
    }, {
      id: me.buildId(me, "editTel01"),
      fieldLabel: "固话",
      name: "tel01",
      value: entity == null ? null : entity.get("tel01"),
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      },
      width: width2,
    }, {
      id: me.buildId(me, "editQQ01"),
      fieldLabel: "QQ",
      name: "qq01",
      value: entity == null ? null : entity.get("qq01"),
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      },
      width: width2,
    }, {
      id: me.buildId(me, "editContact02"),
      fieldLabel: "备用联系人",
      name: "contact02",
      value: entity == null ? null : entity.get("contact02"),
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      },
      width: width2,
    }, {
      id: me.buildId(me, "editMobile02"),
      fieldLabel: "备用联系人手机",
      name: "mobile02",
      value: entity == null ? null : entity.get("mobile02"),
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      },
      width: width2,
    }, {
      id: me.buildId(me, "editTel02"),
      fieldLabel: "备用联系人固话",
      name: "tel02",
      value: entity == null ? null : entity.get("tel02"),
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      },
      width: width2,
    }, {
      id: me.buildId(me, "editQQ02"),
      fieldLabel: "备用联系人QQ",
      name: "qq02",
      value: entity == null ? null : entity.get("qq02"),
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      },
      width: width2,
    }];
  },

  /**
   * @private
   */
  getPayablesInputs() {
    const me = this;

    const entity = me.getEntity();

    const width1 = 800;
    const width2 = 395;

    return [{
      id: me.buildId(me, "editInitPayables"),
      fieldLabel: "应付期初余额",
      name: "initPayables",
      xtype: "numberfield",
      hideTrigger: true,
      value: entity == null ? null : entity.get("initPayables"),
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      },
      width: width2,
    }, {
      id: me.buildId(me, "editInitPayablesDT"),
      fieldLabel: "余额日期",
      name: "initPayablesDT",
      xtype: "datefield",
      format: "Y-m-d",
      value: entity == null ? null : entity.get("initPayablesDT"),
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      },
      width: width2,
    }, {
      id: me.buildId(me, "editInitPayablesInfo"),
      xtype: "displayfield",
      fieldLabel: "说明",
      value: me.toFieldNoteText("当前供应商已经产生了应付账款业务，所以就不能再修改其期初数据"),
      hidden: true,
      width: width1,
      colspan: 2,
    }];
  },

  /**
   * @private
   */
  getMaterialInputs() {
    const me = this;

    const width1 = 800;
    const width2 = 395;

    return [{
      id: me.buildId(me, "editGoodsRange"),
      xtype: "combo",
      queryMode: "local",
      editable: false,
      valueField: "id",
      fieldLabel: "关联物料",
      name: "goodsRange",
      store: PCL.create("PCL.data.ArrayStore", {
        fields: ["id", "text"],
        data: [[1, "全部物料"],
        [2, "部分设置的物料"]]
      }),
      value: 1,
      width: width2,
      listeners: {
        specialkey: {
          fn: me._onEditLastSpecialKey,
          scope: me
        }
      },
    }];
  },

  /**
   * @private
   */
  _onWndShow() {
    const me = this;

    const parentForm = me.getParentForm();
    if (parentForm) {
      parentForm._keyMapMain.disable();
    }

    // AFX
    PCL.get(window).on('beforeunload', me.__onWindowBeforeUnload);

    if (me.adding) {
      // 新建
      if (me.getParentForm()) {
        const grid = me.getParentForm().getCategoryGrid();
        const item = grid.getSelectionModel().getSelection();
        if (item == null || item.length != 1) {
          return;
        }

        me.editCategory.setValue(item[0].get("id"));
      } else {
        // 从其他界面调用本窗口
        const modelName = me.buildModelName(me, "SupplierCategory");
        PCL.define(modelName, {
          extend: "PCL.data.Model",
          fields: ["id", "code", "name", {
            name: "cnt",
            type: "int"
          }]
        });
        const store = PCL.create("PCL.data.Store", {
          model: modelName,
          autoLoad: false,
          data: []
        });
        me.editCategory.bindStore(store);
        const el = PCL.getBody();
        el.mask(PSI.Const.LOADING);
        const r = {
          url: me.URL("SLN0001/Supplier/categoryList"),
          params: {
            recordStatus: -1
          },
          callback(options, success, response) {
            store.removeAll();

            if (success) {
              const data = me.decodeJSON(response.responseText);
              store.add(data);
              if (store.getCount() > 0) {
                const id = store.getAt(0).get("id");
                me.editCategory.setValue(id);
              }
            }

            el.unmask();
          }
        };
        me.ajax(r);
      }
    } else {
      // 编辑
      const el = me.getEl();
      el.mask(PSI.Const.LOADING);
      me.ajax({
        url: me.URL("SLN0001/Supplier/supplierInfo"),
        params: {
          id: me.getEntity().get("id")
        },
        callback(options, success, response) {
          if (success) {
            const data = PCL.JSON.decode(response.responseText);
            me.editCategory.setValue(data.categoryId);
            me.editCode.setValue(data.code);
            me.editName.setValue(data.name);
            me.editAddress.setValue(data.address);
            me.editContact01.setValue(data.contact01);
            me.editMobile01.setValue(data.mobile01);
            me.editTel01.setValue(data.tel01);
            me.editQQ01.setValue(data.qq01);
            me.editContact02.setValue(data.contact02);
            me.editMobile02.setValue(data.mobile02);
            me.editTel02.setValue(data.tel02);
            me.editQQ02.setValue(data.qq02);
            me.editAddressShipping.setValue(data.addressShipping);
            me.editInitPayables.setValue(data.initPayables);
            me.editInitPayablesDT.setValue(data.initPayablesDT);
            me.editBankName.setValue(data.bankName);
            me.editBankAccount.setValue(data.bankAccount);
            me.editTax.setValue(data.tax);
            me.editFax.setValue(data.fax);
            me.editNote.setValue(data.note);
            me.editTaxRate.setValue(data.taxRate);
            me.editRecordStatus.setValue(parseInt(data.recordStatus));
            me.editGoodsRange.setValue(parseInt(data.goodsRange));

            const initPayablesFlag = data.initPayablesFlag;
            if (initPayablesFlag) {
              // 已经发生过应付账款业务了，就不能再编辑期初数据
              me.editInitPayables.setReadOnly(true);
              me.editInitPayablesDT.setReadOnly(true);
              me.editInitPayablesInfo.setVisible(true);
            }
          }

          el.unmask();
        }
      });
    }

    me.setFocusAndCursorPosToLast(me.editCode);
  },

  /**
   * @private
   */
  _onOK(thenAdd) {
    const me = this;

    // 检查数据是否录入完整
    let f = me.form1;
    if (!f.getForm().isValid()) {
      me.showInfo("数据没有录入完整", () => {
        me.tabPanelMain.setActiveTab(0);
      });

      return;
    }

    f = me.form2;
    if (!f.getForm().isValid()) {
      me.showInfo("数据没有录入完整", () => {
        me.tabPanelMain.setActiveTab(1);
      });

      return;
    }

    f = me.form3;
    if (!f.getForm().isValid()) {
      me.showInfo("数据没有录入完整", () => {
        me.tabPanelMain.setActiveTab(2);
      });

      return;
    }

    f = me.form4;
    if (!f.getForm().isValid()) {
      me.showInfo("数据没有录入完整", () => {
        me.tabPanelMain.setActiveTab(3);
      });

      return;
    }

    const taxRate = me.editTaxRate.getValue();
    if (taxRate) {
      if (taxRate < 0 || taxRate > 17) {
        me.tabPanelMain.setActiveTab(0);
        me.showInfo("税率在0到17之间");
        return;
      }
    }

    const entity = me.getEntity();
    const params = {
      id: entity == null ? null : entity.get("id"),
      code: me.editCode.getValue(),
      name: me.editName.getValue(),
      address: me.editAddress.getValue(),
      addressShipping: me.editAddressShipping.getValue(),
      contact01: me.editContact01.getValue(),
      mobile01: me.editMobile01.getValue(),
      tel01: me.editTel01.getValue(),
      qq01: me.editQQ01.getValue(),
      contact02: me.editContact02.getValue(),
      mobile02: me.editMobile02.getValue(),
      tel02: me.editTel02.getValue(),
      qq02: me.editQQ02.getValue(),
      bankName: me.editBankName.getValue(),
      bankAccount: me.editBankAccount.getValue(),
      tax: me.editTax.getValue(),
      fax: me.editFax.getValue(),
      taxRate: me.editTaxRate.getValue(),
      note: me.editNote.getValue(),
      categoryId: me.editCategory.getValue(),
      initPayables: me.editInitPayables.getValue(),
      initPayablesDT: me.editInitPayablesDT.getValue(),
      recordStatus: me.editRecordStatus.getValue(),
      goodsRange: me.editGoodsRange.getValue(),
    };
    const el = me.getEl();
    el && el.mask(PSI.Const.SAVING);

    const r = {
      url: me.URL("SLN0001/Supplier/editSupplier"),
      params,
      callback(options, success, response) {
        el && el.unmask();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          if (data.success) {
            me._lastId = data.id;
            me.tip("数据保存成功", !thenAdd);
            me.focus();
            if (thenAdd) {
              me.clearEdit();
            } else {
              me.close();
            }
          } else {
            me.showInfo(data.msg);
          }
        } else {
          me.showInfo("网络错误");
        }
      }
    };

    me.ajax(r);
  },

  /**
   * @private
   */
  _onEditLastSpecialKey(field, e) {
    const me = this;

    if (e.getKey() === e.ENTER) {
      me._onOK(me.adding);
    }
  },

  /**
   * @private
   */
  clearEdit() {
    const me = this;

    me.editCode.focus();

    const editors = [me.editCode, me.editName, me.editAddress,
    me.editAddressShipping, me.editContact01, me.editMobile01,
    me.editTel01, me.editQQ01, me.editContact02, me.editMobile02,
    me.editTel02, me.editQQ02, me.editInitPayables,
    me.editInitPayablesDT, me.editBankName, me.editBankAccount,
    me.editTax, me.editTaxRate, me.editFax, me.editNote];
    for (let i = 0; i < editors.length; i++) {
      const edit = editors[i];
      edit.setValue(null);
      edit.clearInvalid();
    }

    me.tabPanelMain.setActiveTab(0);
  },

  /**
   * @private
   */
  _onWndClose() {
    const me = this;

    const parentForm = me.getParentForm();
    if (parentForm) {
      parentForm._keyMapMain.enable();
    }

    // AFX
    PCL.get(window).un('beforeunload', me.__onWindowBeforeUnload);

    if (me._lastId) {
      const form = me.getParentForm();
      if (form) {
        form.freshSupplierGrid.apply(form, [me._lastId]);
      }
    }
  },

  /**
   * TabPanel选中的Tab发生改变的时候的事件处理函数
   * @private
   */
  _onTabChange(tabPanel, newCard, oldCard, eOpts) {
    const me = this;

    const id = newCard.getId();

    // 延迟0.1秒后设置input焦点
    // 这是一个奇怪的写法，不这样处理，就不能正确设置焦点
    // 原因目前不明
    PCL.Function.defer(() => {
      if (id == me.buildId(me, "tabGeneral")) {
        me.setFocusAndCursorPosToLast(me.editCode);
      } else if (id == me.buildId(me, "tabContact")) {
        me.setFocusAndCursorPosToLast(me.editContact01);
      } else if (id == me.buildId(me, "tabPayables")) {
        me.setFocusAndCursorPosToLast(me.editInitPayables);
      } else if (id == me.buildId(me, "tabMaterial")) {
        me.setFocusAndCursorPosToLast(me.editGoodsRange);
      }
    }, 100);
  },
});
