/**
 * 信息提示框
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.MsgBox", {
  statics: {
    /**
     * 显示提示信息
     * @param {string} info 提示信息
     * @param {Function} func 单击确定按钮后的回调函数
     */
    showInfo(info, func) {

      const form = PCL.create("PSI.AFX.MessageBox.ShowInfoForm", {
        msg: info,
        fn: func,
        renderTo: PSI.Const.RENDER_TO(),
      });
      form.show();
    },

    /**
     * 显示确认信息
     * 
     * @param {string} confirmInfo 显示给用户的确认信息
     * @param {Function} funcOnYes 选择YES按钮后的回调函数
     * @param {Object} position 指定窗口显示{x, y}位置  
     */
    confirm(confirmInfo, funcOnYes, position) {
      const form = PCL.create("PSI.AFX.MessageBox.ConfirmForm", {
        msg: confirmInfo,
        fn: funcOnYes,
        renderTo: PSI.Const.RENDER_TO(),
      });

      if (position) {
        const { x, y } = position;
        form.showAt(x, y);
      } else {
        form.show();
      }
    },

    /**
     * 显示提示信息，提示信息会在3.5秒后自动关闭
     * 
     * 备注：本方法之前有个参数showCloseButton（是否显示关闭按钮）
     *      在2024-11-15把这个参数去掉了，改为始终显示关闭按钮
     * 
     * @param {string} info 提示信息
     */
    tip(info) {
      const wnd = PCL.create("PCL.window.Window", {
        modal: false,
        onEsc: PCL.emptyFn,
        width: 400,
        height: 100,
        header: false,
        laytout: "fit",
        border: 0,
        bodyCls: "PSI-Msgbx-tip",
        items: [
          {
            xtype: "container",
            html: `
            <img style='float:left;margin:25px 20px 0px 10px;width:28px;height:28px;' 
              src='${PSI.Const.BASE_URL}Public/Images/icons/info.png'></img>
            <h2 style='color:#820014;margin-top:25px;margin-left:10px;'>
                ${info}
            </h2>
            `
          }
        ],
        buttons: [{
          cls: "PSI-MsgBox-tip",
          text: "关闭",
          ...PSI.Const.BTN_STYLE,
          handler() {
            wnd.close();
          }
        }],
      });

      PCL.WindowManager.register(wnd);
      wnd.showAt(document.body.clientWidth - 420, 20);
      PCL.WindowManager.bringToFront(wnd);
      PCL.WindowManager.unregister(wnd);

      PCL.Function.defer(() => {
        wnd.hide();
        wnd.close();
      }, 3500);
    }
  }
});
