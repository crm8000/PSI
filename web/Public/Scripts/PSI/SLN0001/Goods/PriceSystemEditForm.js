/**
 * 价格体系 - 新增或编辑界面
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.Goods.PriceSystemEditForm", {
  extend: "PSI.AFX.Form.EditForm",

  /**
   * 初始化组件
   * 
   * @overrride
   */
  initComponent() {
    const me = this;
    const entity = me.getEntity();
    me.adding = entity == null;

    const buttons = [];
    if (!entity) {
      buttons.push({
        text: "保存并继续新建",
        ...PSI.Const.BTN_STYLE,
        formBind: true,
        handler() {
          me._onOK(true);
        },
        scope: me
      });
    }

    buttons.push({
      text: "保存",
      ...PSI.Const.BTN_STYLE,
      formBind: true,
      iconCls: "PSI-button-ok",
      handler() {
        me._onOK(false);
      },
      scope: me
    }, {
      text: entity == null ? "关闭" : "取消",
      ...PSI.Const.BTN_STYLE,
      handler() {
        me.close();
      },
      scope: me
    });

    const t = entity == null ? "新建价格" : "编辑价格";
    const logoHtml = me.genLogoHtml(entity, t);

    const width1 = 600;
    const width2 = 295;
    PCL.apply(me, {
      header: {
        title: me.formatTitle(PSI.Const.PROD_NAME),
        height: 40
      },
      width: 650,
      height: 250,
      layout: "border",
      items: [{
        region: "north",
        border: 0,
        height: 70,
        html: logoHtml
      }, {
        region: "center",
        border: 0,
        id: me.buildId(me, "editForm"),
        xtype: "form",
        layout: {
          type: "table",
          columns: 2,
          tableAttrs: PSI.Const.TABLE_LAYOUT,
        },
        height: "100%",
        bodyPadding: 5,
        defaultType: 'textfield',
        fieldDefaults: {
          labelAlign: "right",
          labelSeparator: "",
          msgTarget: 'side',
          width: width2,
          margin: "5"
        },
        items: [{
          xtype: "hidden",
          name: "id",
          value: entity == null ? null : entity.get("id")
        }, {
          id: me.buildId(me, "editName"),
          labelWidth: 65,
          fieldLabel: "价格名称",
          allowBlank: false,
          blankText: "没有输入价格名称",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          name: "name",
          value: entity == null ? null : entity.get("name"),
          listeners: {
            specialkey: {
              fn: me._onEditNameSpecialKey,
              scope: me
            }
          }
        }, {
          id: me.buildId(me, "editFactor"),
          xtype: "numberfield",
          hideTrigger: true,
          fieldLabel: "销售基准价倍数",
          labelWidth: 115,
          allowBlank: false,
          blankText: "没有输入销售基准价倍数",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          name: "factor",
          value: entity == null ? 1 : entity.get("factor"),
          listeners: {
            specialkey: {
              fn: me._onEditFactorSpecialKey,
              scope: me
            }
          }
        }],
        buttons,
      }],
      listeners: {
        show: {
          fn: me._onWndShow,
          scope: me
        },
        close: {
          fn: me._onWndClose,
          scope: me
        }
      }
    });

    me.callParent(arguments);

    me.editForm = PCL.getCmp(me.buildId(me, "editForm"));
    me.editName = PCL.getCmp(me.buildId(me, "editName"));
    me.editFactor = PCL.getCmp(me.buildId(me, "editFactor"));
  },

  /**
   * @private
   */
  _onOK(thenAdd) {
    const me = this;
    const f = me.editForm;
    const el = f.getEl();
    el?.mask(PSI.Const.SAVING);
    f.submit({
      url: me.URL("SLN0001/Goods/editPriceSystem"),
      method: "POST",
      success(form, action) {
        el?.unmask();
        me._lastId = action.result.id;
        me.tip("数据保存成功", !thenAdd);
        me.focus();
        if (thenAdd) {
          const editName = me.editName;
          editName.focus();
          editName.setValue(null);
          editName.clearInvalid();
        } else {
          me.close();
        }
      },
      failure(form, action) {
        el.unmask();
        me.showInfo(action.result.msg, () => {
          me.editName.focus();
        });
      }
    });
  },

  /**
   * @private
   */
  _onEditNameSpecialKey(field, e) {
    const me = this;

    if (e.getKey() == e.ENTER) {
      me.setFocusAndCursorPosToLast(me.editFactor);
    }
  },

  /**
   * @private
   */
  _onEditFactorSpecialKey(field, e) {
    const me = this;

    if (e.getKey() == e.ENTER) {
      const f = me.editForm;
      if (f.getForm().isValid()) {
        me._onOK(this.adding);
      }
    }
  },

  /**
   * @private
   */
  _onWndClose() {
    const me = this;

    // AFX
    PCL.get(window).un('beforeunload', me.__onWindowBeforeUnload);

    if (me._lastId) {
      me.getParentForm().freshGrid(me._lastId);
    }
  },

  /**
   * @private
   */
  _onWndShow() {
    const me = this;

    PCL.get(window).on('beforeunload', me.__onWindowBeforeUnload);

    me.setFocusAndCursorPosToLast(me.editName);
  }
});
