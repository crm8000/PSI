/**
 * 资金看板 - 列表
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.Portal.MoneyPortalItem", {
  extend: "PSI.Home.Portal.BasePortalItem",

  /**
   * @override
   */
  initComponent() {
    const me = this;

    PCL.apply(me, {
      flex: 1,
      width: "100%",
      height: 170,
      margin: "5",
      header: {
        title: `<span style='font-size:120%;font-weight:normal;'>${me.getPortalItemName()}</span>`,
        iconCls: me.getPortalItemIconClass(),
        height: 40
      },
      layout: "fit",
      items: me.getMainGrid()
    });

    me.callParent(arguments);
  },

  getMainGrid() {
    const me = this;
    if (me._moneyGrid) {
      return me._moneyGrid;
    }

    const modelName = me.buildModelName(me, "PortalMoney");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["item", "balanceMoney", "money30", "money30to60",
        "money60to90", "money90"]
    });

    me._moneyGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-KB",
      viewConfig: {
        enableTextSelection: true
      },
      columnLines: true,
      border: 0,
      columns: [{
        header: "款项",
        dataIndex: "item",
        width: 80,
        menuDisabled: true,
        sortable: false
      }, {
        header: "当期余额",
        dataIndex: "balanceMoney",
        width: 160,
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn"
      }, {
        header: "账龄30天内",
        dataIndex: "money30",
        width: 160,
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn"
      }, {
        header: "账龄30-60天",
        dataIndex: "money30to60",
        width: 160,
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn"
      }, {
        header: "账龄60-90天",
        dataIndex: "money60to90",
        width: 160,
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn"
      }, {
        header: "账龄大于90天",
        dataIndex: "money90",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 160
      }],
      store: PCL.create("PCL.data.Store", {
        model: modelName,
        autoLoad: false,
        data: []
      })
    });

    return me._moneyGrid;
  },

  /**
   * @override
   */
  loadData() {
    const me = this;
    const el = me.getEl();
    el?.mask(PSI.Const.LOADING);
    me.ajax({
      url: me.URL("Home/Portal/moneyPortal"),
      callback(options, success, response) {
        const store = me.getMainGrid().getStore();
        store.removeAll();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          store.add(data);
        }

        el?.unmask();
      }
    });
  },
});
