/**
 * 库存看板 - 列表
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.Portal.InventoryPortalItem", {
  extend: "PSI.Home.Portal.BasePortalItem",

  /**
   * @override
   */
  initComponent() {
    const me = this;

    PCL.apply(me, {
      flex: 1,
      width: "100%",
      height: 370,
      margin: "5",
      header: {
        title: `<span style='font-size:120%;font-weight:normal;'>${me.getPortalItemName()}</span>`,
        iconCls: me.getPortalItemIconClass(),
        height: 40
      },
      layout: "fit",
      items: me.getMainGrid()
    });

    me.callParent(arguments);
  },

  getMainGrid() {
    const me = this;
    if (me._inventoryGrid) {
      return me._inventoryGrid;
    }

    const modelName = me.buildModelName(me, "PortalInventory");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["warehouseName", { name: "inventoryMoney", type: "float" },
        { name: "siCount", type: "float" },
        { name: "iuCount", type: "float" }]
    });

    me._inventoryGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-KB",
      viewConfig: {
        enableTextSelection: true
      },
      features: [{
        ftype: "summary",
        dock: "bottom"
      }],
      columnLines: true,
      border: 0,
      columns: [{
        header: "仓库",
        dataIndex: "warehouseName",
        width: 200,
        menuDisabled: true,
        sortable: false,
        summaryRenderer() {
          return "合计";
        }
      }, {
        header: "存货金额",
        dataIndex: "inventoryMoney",
        width: 160,
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        summaryType: "sum"
      }, {
        header: "低于安全库存物料种类数",
        dataIndex: "siCount",
        width: 200,
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        format: "0",
        renderer(value) {
          return value > 0
            ? "<span style='color:red'>"
            + value + "</span>"
            : value;
        },
        summaryType: "sum"
      }, {
        header: "超过库存上限的物料种类数",
        dataIndex: "iuCount",
        width: 200,
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        format: "0",
        renderer(value) {
          return value > 0
            ? "<span style='color:red'>"
            + value + "</span>"
            : value;
        },
        summaryType: "sum"
      }],
      store: PCL.create("PCL.data.Store", {
        model: modelName,
        autoLoad: false,
        data: []
      })
    });

    return me._inventoryGrid;
  },

  /**
   * @override
   */
  loadData() {
    const me = this;
    const el = me.getEl();
    el?.mask(PSI.Const.LOADING);
    me.ajax({
      url: me.URL("Home/Portal/inventoryPortal"),
      callback(options, success, response) {
        const store = me.getMainGrid().getStore();
        store.removeAll();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          store.add(data);
        }

        el?.unmask();
      }
    });
  },
});
