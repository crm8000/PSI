/**
 * 商品价格体系 - 设置界面
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.Goods.GoodsPriceSystemEditForm", {
  extend: "PSI.AFX.Form.EditForm",

  /**
   * 初始化组件
   * 
   * @override
   */
  initComponent() {
    const me = this;

    const entity = me.getEntity();

    PCL.apply(me, {
      header: {
        title: me.formatTitle(PSI.Const.PROD_NAME),
        height: 40,
      },
      width: 580,
      height: 500,
      layout: "border",
      items: [{
        region: "center",
        border: 0,
        bodyPadding: 10,
        layout: "fit",
        items: [me.getGoodsGrid()]
      }, {
        region: "north",
        border: 0,
        layout: {
          type: "table",
          columns: 2
        },
        height: 90,
        bodyPadding: 10,
        items: [{
          border: 0,
          html: `<div style='margin-bottom:30px'>
                    <img style='float:left;margin:0px 20px 0px 10px;width:48px;height:48px;' 
                    src='${PSI.Const.BASE_URL}Public/Images/edit-form-update.png'></img>
                    <div style='margin-left:60px;margin-top:0px;'>
                      <h2 style='color:#196d83;margin-top:0px;width:100%'>设置商品价格体系</h2>
                    </div>
                  </div>`,
          colspan: 2
        }, {
          xtype: "hidden",
          id: me.buildId(me, "hiddenId"),
          name: "id",
          value: entity.get("id")
        }, {
          fieldLabel: "商品",
          labelWidth: 40,
          labelAlign: "right",
          labelSeparator: "",
          xtype: "displayfield",
          value: `<span class='PSI-field-note'> ${entity.get("code")} - ${entity.get("name")} ${entity.get("spec")}</span>`
        }]
      }],
      buttons: [{
        text: "保存",
        ...PSI.Const.BTN_STYLE,
        iconCls: "PSI-button-ok",
        handler: me._onOK,
        scope: me,
        id: "buttonSave"
      }, {
        text: "取消",
        ...PSI.Const.BTN_STYLE,
        handler() {
          me.confirm("请确认是否取消当前操作？",
            () => {
              me.close();
            });
        },
        scope: me,
        id: "buttonCancel"
      }],
      listeners: {
        show: {
          fn: me._onWndShow,
          scope: me
        },
        close: {
          fn: me._onWndClose,
          scope: me
        }
      }
    });

    me.callParent(arguments);

    me.hiddenId = PCL.getCmp(me.buildId(me, "hiddenId"));
    me.editBaseSalePrice = PCL.getCmp(me.buildId(me, "editBaseSalePrice"));
  },

  /**
   * @private
   */
  _onWndClose() {
    const me = this;

    const parentForm = me.getParentForm();
    if (parentForm) {
      parentForm._keyMapMain.enable();
    }

    // AFX
    PCL.get(window).un('beforeunload', me.__onWindowBeforeUnload);
  },

  /**
   * @private
   */
  _onWndShow() {
    const me = this;

    const parentForm = me.getParentForm();
    if (parentForm) {
      parentForm._keyMapMain.disable();
    }

    // AFX
    PCL.get(window).on('beforeunload', me.__onWindowBeforeUnload);

    const el = me.getEl() || PCL.getBody();
    el.mask(PSI.Const.LOADING);
    me.ajax({
      url: me.URL("SLN0001/Goods/goodsPriceSystemInfo"),
      params: {
        id: me.hiddenId.getValue()
      },
      callback(options, success, response) {
        el.unmask();

        if (success) {
          const data = me.decodeJSON(response.responseText);

          const store = me.getGoodsGrid().getStore();
          store.removeAll();
          store.add(data.priceList);

          me.editBaseSalePrice.setValue(data.baseSalePrice);
        } else {
          me.showInfo("网络错误")
        }
      }
    });
  },

  /**
   * @private
   */
  _onOK() {
    const me = this;

    PCL.getBody().mask("正在保存中...");
    me.ajax({
      url: me.URL("SLN0001/Goods/editGoodsPriceSystem"),
      params: {
        jsonStr: me.getSaveData()
      },
      callback(options, success, response) {
        PCL.getBody().unmask();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          if (data.success) {
            me.close();
            const parentForm = me.getParentForm();
            if (parentForm) {
              parentForm._onGoodsSelect.apply(parentForm, []);
            }
            me.tip("成功保存数据", true);
          } else {
            me.showInfo(data.msg);
          }
        }
      }
    });
  },

  /**
   * @private
   */
  getGoodsGrid() {
    const me = this;

    if (me._goodsGrid) {
      return me._goodsGrid;
    }

    const modelName = me.buildModelName(me, "GoodsPriceSystem");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "name", "factor", "price"]
    });
    const store = PCL.create("PCL.data.Store", {
      autoLoad: false,
      model: modelName,
      data: []
    });

    me._cellEditing = PCL.create("PSI.UX.CellEditing", {
      clicksToEdit: 1,
      listeners: {
        edit: {
          fn: me.cellEditingAfterEdit,
          scope: me
        }
      }
    });

    me._goodsGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-EF",
      viewConfig: {
        enableTextSelection: true
      },
      plugins: [me._cellEditing],
      columnLines: true,
      tbar: [{
        fieldLabel: "销售基准价",
        labelWidth: 70,
        labelAlign: "right",
        id: me.buildId(me, "editBaseSalePrice"),
        xtype: "numberfield",
        hideTrigger: true
      }, " ", {
        xtype: "button",
        text: "根据销售基准价自动计算其他价格",
        iconCls: "PSI-button-ok",
        handler: me._onCalPrice,
        scope: me
      }],
      columns: [{
        header: "名称",
        dataIndex: "name",
        width: 120,
        menuDisabled: true,
        sortable: false
      }, {
        header: "销售基准价倍数",
        dataIndex: "factor",
        width: 120,
        menuDisabled: true,
        sortable: false
      }, {
        header: "价格",
        dataIndex: "price",
        width: 120,
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        editor: {
          xtype: "numberfield",
          hideTrigger: true
        }
      }],
      store: store
    });

    return me._goodsGrid;
  },

  /**
   * @private
   */
  cellEditingAfterEdit(editor, e) {
    // do nothing
  },

  /**
   * @private
   */
  getSaveData() {
    const me = this;

    const result = {
      id: me.hiddenId.getValue(),
      basePrice: me.editBaseSalePrice.getValue(),
      items: []
    };

    const store = me.getGoodsGrid().getStore();
    for (let i = 0; i < store.getCount(); i++) {
      const item = store.getAt(i);
      result.items.push({
        id: item.get("id"),
        price: item.get("price")
      });
    }

    return me.encodeJSON(result);
  },

  /**
   * @private
   */
  _onCalPrice() {
    const me = this;

    const basePrice = me.editBaseSalePrice.getValue();

    if (!basePrice) {
      me.showInfo("请设置基准价格", () => {
        me.editBaseSalePrice.focus();
      });
      return;
    }

    const store = me.getGoodsGrid().getStore();
    for (let i = 0; i < store.getCount(); i++) {
      const item = store.getAt(i);
      item.set("price", item.get("factor") * basePrice);
    }
  }
});
