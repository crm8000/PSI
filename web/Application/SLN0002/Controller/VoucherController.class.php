<?php

namespace SLN0002\Controller;

use Home\Common\FIdConst;
use Home\Controller\PSIBaseController;
use Home\Service\UserService;
use SLN0002\Service\VoucherService;

/**
 * 会计凭证Controller
 *
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
class VoucherController extends PSIBaseController
{

  /**
   * 凭证 - 主页面
   * 
   * 模板页面：web\Application\SLN0002\View\Voucher\index.html
   */
  public function index()
  {
    $us = new UserService();

    if ($us->hasPermission(FIdConst::VOUCHER)) {
      $this->initVar();

      $this->assign("title", "记账凭证");

      // 按钮权限：新建凭证
      $this->assign("pAdd", $us->hasPermission(FIdConst::VOUCHER_ADD) ? 1 : 0);
      // 按钮权限：编辑凭证
      $this->assign("pEdit", $us->hasPermission(FIdConst::VOUCHER_EDIT) ? 1 : 0);
      // 按钮权限：删除凭证
      $this->assign("pDelete", $us->hasPermission(FIdConst::VOUCHER_DELETE) ? 1 : 0);
      // 按钮权限：复核凭证
      $this->assign("pCommit", $us->hasPermission(FIdConst::VOUCHER_COMMIT) ? 1 : 0);
      // 按钮权限：打印凭证
      $this->assign("pPrint", $us->hasPermission(FIdConst::VOUCHER_PRINT) ? 1 : 0);
      // 按钮权限：登记总分类账
      $this->assign("pAccGL", $us->hasPermission(FIdConst::VOUCHER_ACC_GL) ? 1 : 0);
      // 按钮权限：登记明细分类账
      $this->assign("pAccDetail", $us->hasPermission(FIdConst::VOUCHER_ACC_DETAIL) ? 1 : 0);
      // 按钮权限：凭证断号重排
      $this->assign("pRefReorder", $us->hasPermission(FIdConst::VOUCHER_REF_REORDER) ? 1 : 0);

      $this->display();
    } else {
      $this->gotoLoginPage("/SLN0002/Voucher/index");
    }
  }

  /**
   * 返回所有的公司列表
   * 
   * JS: web\Public\Scripts\PSI\SLN0002\Voucher\MainForm.js
   *     web\Public\Scripts\PSI\SLN0002\Voucher\EditForm.js
   *     web\Public\Scripts\PSI\SLN0002\Voucher\SelectOrgAndWordForm.js
   */
  public function companyList()
  {
    if (IS_POST) {
      $us = new UserService();

      if (!$us->hasPermission(FIdConst::VOUCHER)) {
        die("没有权限");
      }

      $service = new VoucherService();
      $this->ajaxReturn($service->companyList());
    }
  }

  /**
   * 凭证主表列表
   * 
   * JS: web\Public\Scripts\PSI\SLN0002\Voucher\MainForm.js
   */
  public function voucherList()
  {
    if (IS_POST) {
      $us = new UserService();

      if (!$us->hasPermission(FIdConst::VOUCHER)) {
        die("没有权限");
      }

      $params = [
        "orgId" => I("post.orgId"),
        "year" => I("post.year"),
        "month" => I("post.month"),
        "ref" => I("post.ref"),
        "inputUserId" => I("post.inputUserId"),
        "commitUserId" => I("post.commitUserId"),
        "status" => I("post.status"),
        "start" => I("post.start"),
        "limit" => I("post.limit")
      ];

      $service = new VoucherService();
      $this->ajaxReturn($service->voucherList($params));
    }
  }

  /**
   * 查询凭证字列表
   * 
   * JS: web\Public\Scripts\PSI\SLN0002\Voucher\EditForm.js
   *     web\Public\Scripts\PSI\SLN0002\Voucher\SelectOrgAndWordForm.js
   */
  public function queryVoucherWord()
  {
    if (IS_POST) {
      $us = new UserService();

      $hasPermission = $us->hasPermission(FIdConst::VOUCHER_ADD) ||
        $us->hasPermission(FIdConst::VOUCHER_EDIT);
      if (!$hasPermission) {
        die("没有权限");
      }

      $params = [
        "orgId" => I("post.orgId"),
      ];

      $service = new VoucherService();
      $this->ajaxReturn($service->queryVoucherWord($params));
    }
  }

  /**
   * 查询账样的扩展项
   * 
   * 账样的扩展项是在实施中由用户自定义的，这些扩展项的数据录入就对应着凭证的分录附加项
   * 
   * JS: web\Public\Scripts\PSI\SLN0002\Voucher\EditForm.js
   *     web\Public\Scripts\PSI\SLN0002\Voucher\MainForm.js
   */
  public function queryFmtEx()
  {
    if (IS_POST) {
      $us = new UserService();

      $hasPermission = $us->hasPermission(FIdConst::VOUCHER_ADD) ||
        $us->hasPermission(FIdConst::VOUCHER_EDIT);
      if (!$hasPermission) {
        die("没有权限");
      }

      $params = [
        "companyId" => I("post.companyId"),
        "subjectCode" => I("post.subjectCode"),
      ];

      $service = new VoucherService();
      $this->ajaxReturn($service->queryFmtEx($params));
    }
  }

  /**
   * 新建或编辑凭证
   * 
   * JS：web\Public\Scripts\PSI\SLN0002\Voucher\EditForm.js
   */
  public function editVoucher()
  {
    if (IS_POST) {
      $adding = I("post.adding");
      $us = new UserService();
      if ($adding == "1") {
        // 新建凭证
        if (!$us->hasPermission(FIdConst::VOUCHER_ADD)) {
          die("没有权限");
        }
      } else {
        // 编辑凭证
        if (!$us->hasPermission(FIdConst::VOUCHER_EDIT)) {
          die("没有权限");
        }
      }

      $json = I("post.jsonStr");
      $ps = new VoucherService();
      $this->ajaxReturn($ps->editVoucher($json));
    }
  }

  /**
   * 凭证分录
   * 
   * JS：web\Public\Scripts\PSI\SLN0002\Voucher\MainForm.js
   */
  public function voucherDetailList()
  {
    if (IS_POST) {
      $us = new UserService();

      if (!$us->hasPermission(FIdConst::VOUCHER)) {
        die("没有权限");
      }

      $params = [
        "id" => I("post.id")
      ];

      $ws = new VoucherService();
      $this->ajaxReturn($ws->voucherDetailList($params));
    }
  }

  /**
   * 删除凭证
   * 
   * JS: web\Public\Scripts\PSI\SLN0002\Voucher\MainForm.js
   */
  public function deleteVoucher()
  {
    if (IS_POST) {
      $us = new UserService();
      if (!$us->hasPermission(FIdConst::VOUCHER_DELETE)) {
        die("没有权限");
      }

      $id = I("post.id");
      $ps = new VoucherService();
      $this->ajaxReturn($ps->deleteVoucher($id));
    }
  }

  /**
   * 凭证详情
   * 
   * JS: web\Public\Scripts\PSI\SLN0002\Voucher\EditForm.js
   */
  public function voucherInfo()
  {
    if (IS_POST) {
      $us = new UserService();

      if (!$us->hasPermission(FIdConst::VOUCHER_EDIT)) {
        die("没有权限");
      }

      $params = [
        "id" => I("post.id")
      ];

      $service = new VoucherService();
      $this->ajaxReturn($service->voucherInfo($params));
    }
  }

  /**
   * 复核凭证
   * 
   * JS: web\Public\Scripts\PSI\SLN0002\Voucher\MainForm.js
   */
  public function commitVoucher()
  {
    if (IS_POST) {
      $us = new UserService();
      if (!$us->hasPermission(FIdConst::VOUCHER_COMMIT)) {
        die("没有权限");
      }

      $id = I("post.id");
      $ps = new VoucherService();
      $this->ajaxReturn($ps->commitVoucher($id));
    }
  }

  /**
   * 取消复核凭证
   * 
   * JS: web\Public\Scripts\PSI\SLN0002\Voucher\MainForm.js
   */
  public function cancelCommitVoucher()
  {
    if (IS_POST) {
      $us = new UserService();
      if (!$us->hasPermission(FIdConst::VOUCHER_COMMIT)) {
        die("没有权限");
      }

      $id = I("post.id");
      $ps = new VoucherService();
      $this->ajaxReturn($ps->cancelCommitVoucher($id));
    }
  }

  /**
   * 登记明细分类账
   * 
   * JS: web\Public\Scripts\PSI\SLN0002\Voucher\MainForm.js
   */
  public function voucherToAccDetail()
  {
    if (IS_POST) {
      $us = new UserService();
      if (!$us->hasPermission(FIdConst::VOUCHER_ACC_DETAIL)) {
        die("没有权限");
      }

      $params = [
        "companyId" => I("post.companyId"),
        "year" => I("post.year"),
        "month" => I("post.month"),
      ];
      $ps = new VoucherService();
      $this->ajaxReturn($ps->voucherToAccDetail($params));
    }
  }

  /**
   * 凭证断号重排
   * 
   * JS：web\Public\Scripts\PSI\SLN0002\Voucher\MainForm.js
   */
  public function refReorder()
  {
    if (IS_POST) {
      $us = new UserService();
      if (!$us->hasPermission(FIdConst::VOUCHER_REF_REORDER)) {
        die("没有权限");
      }

      $params = [
        "companyId" => I("post.companyId"),
        "year" => I("post.year"),
        "month" => I("post.month"),
      ];

      $ps = new VoucherService();
      $this->ajaxReturn($ps->refReorder($params));
    }
  }
}
