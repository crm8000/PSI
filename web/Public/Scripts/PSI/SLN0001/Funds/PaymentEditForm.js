/**
 * 应付账款 - 付款记录
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.Funds.PaymentEditForm", {
  extend: "PSI.AFX.Form.EditForm",

  config: {
    payDetail: null
  },

  /**
   * @override
   */
  initComponent() {
    var me = this;

    var t = "录入付款记录";
    var f = "edit-form-money.png";
    var logoHtml = `
      <img style='float:left;margin:0px 20px 0px 10px;width:48px;height:48px;' 
        src='${PSI.Const.BASE_URL}Public/Images/${f}'></img>
      <div style='margin-left:60px;margin-top:0px;'>
        <h2 style='color:#595959;margin-top:0px;'>${t}</h2>
        <p style='color:#8c8c8c'>标记 <span style='color:red;font-weight:bold'>*</span>的是必须录入数据的字段</p>
      </div>
      <div style='margin:0px;border-bottom:1px solid #e6f7ff;height:1px' /></div>
      `;

    const width1 = 600;
    const width2 = 290;
    PCL.apply(me, {
      header: {
        title: me.formatTitle(PSI.Const.PROD_NAME),
        height: 40
      },
      width: 650,
      height: 340,
      layout: "border",
      defaultFocus: "editActMoney",
      listeners: {
        show: {
          fn: me.onWndShow,
          scope: me
        },
        close: {
          fn: me.onWndClose,
          scope: me
        }
      },
      items: [{
        region: "north",
        border: 0,
        height: 70,
        html: logoHtml
      }, {
        region: "center",
        border: 0,
        id: "editForm",
        xtype: "form",
        layout: {
          type: "table",
          columns: 2,
          tableAttrs: PSI.Const.TABLE_LAYOUT,
        },
        height: "100%",
        bodyPadding: 5,
        defaultType: 'textfield',
        fieldDefaults: {
          labelWidth: 65,
          labelAlign: "right",
          labelSeparator: "",
          msgTarget: 'side',
          width: width2,
          margin: "5"
        },
        items: [{
          xtype: "hidden",
          name: "refNumber",
          value: me.getPayDetail().get("refNumber")
        }, {
          xtype: "hidden",
          name: "refType",
          value: me.getPayDetail().get("refType")
        }, {
          fieldLabel: "单号",
          xtype: "displayfield",
          value: me.toFieldNoteText(me.getPayDetail().get("refNumber"))
        }, {
          id: "editBizDT",
          fieldLabel: "付款日期",
          allowBlank: false,
          blankText: "没有输入付款日期",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          xtype: "datefield",
          format: "Y-m-d",
          value: new Date(),
          name: "bizDT",
          listeners: {
            specialkey: {
              fn: me.onEditBizDTSpecialKey,
              scope: me
            }
          }
        }, {
          fieldLabel: "付款金额",
          allowBlank: false,
          blankText: "没有输入付款金额",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          xtype: "numberfield",
          hideTrigger: true,
          name: "actMoney",
          id: "editActMoney",
          listeners: {
            specialkey: {
              fn: me.onEditActMoneySpecialKey,
              scope: me
            }
          }
        }, {
          id: "editBizUserId",
          xtype: "hidden",
          name: "bizUserId"
        }, {
          id: "editBizUser",
          fieldLabel: "付款人",
          xtype: "psi_userfield",
          allowBlank: false,
          blankText: "没有输入付款人",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          listeners: {
            specialkey: {
              fn: me.onEditBizUserSpecialKey,
              scope: me
            }
          }
        }, {
          fieldLabel: "备注",
          name: "remark",
          id: "editRemark",
          listeners: {
            specialkey: {
              fn: me.onEditRemarkSpecialKey,
              scope: me
            }
          },
          colspan: 2,
          width: width1,
        }],
        buttons: [{
          text: "保存",
          ...PSI.Const.BTN_STYLE,
          iconCls: "PSI-button-ok",
          formBind: true,
          handler: me.onOK,
          scope: me
        }, {
          text: "取消",
          ...PSI.Const.BTN_STYLE,
          handler() {
            me.close();
          },
          scope: me
        }]
      }]
    });

    me.callParent(arguments);
  },

  onWindowBeforeUnload(e) {
    return (window.event.returnValue = e.returnValue = '确认离开当前页面？');
  },

  onWndClose() {
    var me = this;

    PCL.get(window).un('beforeunload', me.onWindowBeforeUnload);
  },

  onWndShow() {
    var me = this;

    PCL.get(window).on('beforeunload', me.onWindowBeforeUnload);

    var f = PCL.getCmp("editForm");
    var el = f.getEl();
    el.mask(PSI.Const.LOADING);
    PCL.Ajax.request({
      url: PSI.Const.BASE_URL + "SLN0001/Funds/payRecInfo",
      params: {},
      method: "POST",
      callback(options, success, response) {
        el.unmask();

        if (success) {
          var data = PCL.JSON.decode(response.responseText);

          PCL.getCmp("editBizUserId").setValue(data.bizUserId);
          PCL.getCmp("editBizUser").setValue(data.bizUserName);
          PCL.getCmp("editBizUser").setIdValue(data.bizUserId);
        } else {
          me.showInfo("网络错误")
        }
      }
    });
  },

  onOK() {
    var me = this;
    PCL.getCmp("editBizUserId").setValue(PCL.getCmp("editBizUser").getIdValue());

    var f = PCL.getCmp("editForm");
    var el = f.getEl();
    el.mask(PSI.Const.SAVING);
    f.submit({
      url: PSI.Const.BASE_URL + "SLN0001/Funds/addPayment",
      method: "POST",
      success(form, action) {
        el.unmask();

        me.close();
        var pf = me.getParentForm();
        pf.refreshPayInfo();
        pf.refreshPayDetailInfo();
        pf.getPayRecordGrid().getStore().loadPage(1);
      },
      failure(form, action) {
        el.unmask();
        me.showInfo(action.result.msg, () => {
          PCL.getCmp("editBizDT").focus();
        });
      }
    });
  },

  onEditBizDTSpecialKey(field, e) {
    if (e.getKey() == e.ENTER) {
      PCL.getCmp("editActMoney").focus();
    }
  },

  onEditActMoneySpecialKey(field, e) {
    if (e.getKey() == e.ENTER) {
      PCL.getCmp("editBizUser").focus();
    }
  },

  onEditBizUserSpecialKey(field, e) {
    if (e.getKey() == e.ENTER) {
      PCL.getCmp("editRemark").focus();
    }
  },

  onEditRemarkSpecialKey(field, e) {
    if (e.getKey() == e.ENTER) {
      var f = PCL.getCmp("editForm");
      if (f.getForm().isValid()) {
        var me = this;
        me.confirm("请确认是否录入收款记录?", () => {
          me.onOK();
        });
      }
    }
  },
});
