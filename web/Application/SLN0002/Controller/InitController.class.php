<?php

namespace SLN0002\Controller;

use Home\Common\FIdConst;
use Home\Controller\PSIBaseController;
use Home\Service\UserService;
use SLN0002\Service\InitService;

/**
 * 总账建账 Controller
 *
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
class InitController extends PSIBaseController
{

  /**
   * 总分类账建账 - 主页面
   * 
   * 模板页面：web\Application\SLN0002\View\Init\indexGL.html
   */
  public function indexGL()
  {
    $us = new UserService();

    if ($us->hasPermission(FIdConst::GL_INIT_GL)) {
      $this->initVar();

      $this->assign("title", "总分类账建账");

      $this->display();
    } else {
      $this->gotoLoginPage("/SLN0002/Init/indexGL");
    }
  }

  /**
   * 明细分类账建账 - 主页面
   * 
   * 模板页面：web\Application\SLN0002\View\Init\indexDetail.html
   */
  public function indexDetail()
  {
    $us = new UserService();

    if ($us->hasPermission(FIdConst::GL_INIT_DETAIL)) {
      $this->initVar();

      $this->assign("title", "明细分类账建账");

      $this->display();
    } else {
      $this->gotoLoginPage("/SLN0002/Init/indexDetail");
    }
  }

  /**
   * 返回所有的公司列表
   * 
   * JS: web\Public\Scripts\PSI\SLN0002\Init\InitDetailMainForm.js
   *     web\Public\Scripts\PSI\SLN0002\Init\InitGlMainForm.js
   */
  public function companyList()
  {
    if (IS_POST) {
      $us = new UserService();

      $hasPermission = $us->hasPermission(FIdConst::GL_INIT_DETAIL) || $us->hasPermission(FIdConst::GL_INIT_GL);
      if (!$hasPermission) {
        die("没有权限");
      }

      $service = new InitService();
      $this->ajaxReturn($service->companyList(FIdConst::GL_INIT_DETAIL));
    }
  }

  /**
   * 查询明细分类账建账数据
   * 
   * JS：web\Public\Scripts\PSI\SLN0002\Init\InitDetailMainForm.js
   */
  public function queryAccDetailData()
  {
    if (IS_POST) {
      $us = new UserService();

      if (!$us->hasPermission(FIdConst::GL_INIT_DETAIL)) {
        die("没有权限");
      }

      $params = [
        "companyId" => I("post.companyId"),
      ];

      $service = new InitService();
      $this->ajaxReturn($service->queryAccDetailData($params));
    }
  }

  /**
   * 录入明细分类账期初数据
   * 
   * JS: web\Public\Scripts\PSI\SLN0002\Init\DetailEditForm.js
   */
  public function editAccDetailInitRecord()
  {
    if (IS_POST) {
      $us = new UserService();

      if (!$us->hasPermission(FIdConst::GL_INIT_DETAIL)) {
        die("没有权限");
      }

      $json = I("post.jsonStr");

      $service = new InitService();
      $this->ajaxReturn($service->editAccDetailInitRecord($json));
    }
  }

  /**
   * 查询账样的扩展项
   * 
   * JS: web\Public\Scripts\PSI\SLN0002\Init\DetailEditForm.js
   */
  public function queryFmtEx()
  {
    if (IS_POST) {
      $us = new UserService();

      $hasPermission = $us->hasPermission(FIdConst::GL_INIT_DETAIL);
      if (!$hasPermission) {
        die("没有权限");
      }

      $params = [
        "companyId" => I("post.companyId"),
        "subjectCode" => I("post.subjectCode"),
      ];

      $service = new InitService();
      $this->ajaxReturn($service->queryFmtEx($params));
    }
  }

  /**
   * 查询账样列
   * 
   * JS： web\Public\Scripts\PSI\SLN0002\Init\InitDetailMainForm.js
   */
  public function queryFmtColsForDetailAcc()
  {
    if (IS_POST) {
      $us = new UserService();

      $hasPermission = $us->hasPermission(FIdConst::GL_INIT_DETAIL);
      if (!$hasPermission) {
        die("没有权限");
      }

      $params = [
        "companyId" => I("post.companyId"),
        "subjectCode" => I("post.subjectCode"),
      ];

      $service = new InitService();
      $this->ajaxReturn($service->queryFmtColsForDetailAcc($params));
    }
  }

  /**
   * 查询明细分类账建账数据
   * 
   * JS： web\Public\Scripts\PSI\SLN0002\Init\InitDetailMainForm.js
   */
  public function queryDataForDetailAcc()
  {
    if (IS_POST) {
      $us = new UserService();

      $hasPermission = $us->hasPermission(FIdConst::GL_INIT_DETAIL);
      if (!$hasPermission) {
        die("没有权限");
      }

      $params = [
        "companyId" => I("post.companyId"),
        "subjectCode" => I("post.subjectCode"),
      ];

      $service = new InitService();
      $this->ajaxReturn($service->queryDataForDetailAcc($params));
    }
  }

  /**
   * 查询总账建账期间
   * 
   * JS: web\Public\Scripts\PSI\SLN0002\Init\DetailEditForm.js
   */
  public function queryAccInitYearAndMonth()
  {
    if (IS_POST) {
      $us = new UserService();

      $hasPermission = $us->hasPermission(FIdConst::GL_INIT_DETAIL);
      if (!$hasPermission) {
        die("没有权限");
      }

      $params = [
        "companyId" => I("post.companyId"),
      ];

      $service = new InitService();
      $this->ajaxReturn($service->queryAccInitYearAndMonth($params));
    }
  }

  /**
   * 完成明细分类账建账
   * 
   * JS: web\Public\Scripts\PSI\SLN0002\Init\InitDetailMainForm.js
   */
  public function commitAccDetailInit()
  {

    if (IS_POST) {
      $us = new UserService();

      $hasPermission = $us->hasPermission(FIdConst::GL_INIT_DETAIL);
      if (!$hasPermission) {
        die("没有权限");
      }

      $params = [
        "companyId" => I("post.companyId"),
      ];

      $service = new InitService();
      $this->ajaxReturn($service->commitAccDetailInit($params));
    }
  }

  /**
   * 取消明细分类账建账
   * 
   * JS: web\Public\Scripts\PSI\SLN0002\Init\InitDetailMainForm.js
   */
  public function cancelAccDetailInit()
  {
    if (IS_POST) {
      $us = new UserService();

      $hasPermission = $us->hasPermission(FIdConst::GL_INIT_DETAIL);
      if (!$hasPermission) {
        die("没有权限");
      }

      $params = [
        "companyId" => I("post.companyId"),
      ];

      $service = new InitService();
      $this->ajaxReturn($service->cancelAccDetailInit($params));
    }
  }

  /**
   * 查询明建账完成情况
   * 
   * JS: web\Public\Scripts\PSI\SLN0002\Init\InitDetailMainForm.js
   */
  public function queryInitStatus()
  {
    if (IS_POST) {
      $us = new UserService();

      if (!$us->hasPermission(FIdConst::GL_INIT_DETAIL)) {
        die("没有权限");
      }

      $params = [
        "companyId" => I("post.companyId"),
      ];

      $service = new InitService();
      $this->ajaxReturn($service->queryInitStatus($params));
    }
  }

  /**
   * 从明细分类账生成总账建账数据
   * 
   * JS: web\Public\Scripts\PSI\SLN0002\Init\InitGlMainForm.js
   */
  public function genInitDataFromAccDetail()
  {
    if (IS_POST) {
      $us = new UserService();

      $hasPermission = $us->hasPermission(FIdConst::GL_INIT_GL);
      if (!$hasPermission) {
        die("没有权限");
      }

      $params = [
        "companyId" => I("post.companyId"),
      ];

      $service = new InitService();
      $this->ajaxReturn($service->genInitDataFromAccDetail($params));
    }
  }

  /**
   * 查询总分类账建账数据
   * 
   * JS: web\Public\Scripts\PSI\SLN0002\Init\InitGlMainForm.js
   */
  public function queryAccGlData()
  {
    if (IS_POST) {
      $us = new UserService();

      if (!$us->hasPermission(FIdConst::GL_INIT_GL)) {
        die("没有权限");
      }

      $params = [
        "companyId" => I("post.companyId"),
      ];

      $service = new InitService();
      $this->ajaxReturn($service->queryAccGlData($params));
    }
  }

  /**
   * 查询总分类账建账数据
   * 
   * JS: web\Public\Scripts\PSI\SLN0002\Init\InitGlMainForm.js
   */
  public function queryDataForGlAcc()
  {
    if (IS_POST) {
      $us = new UserService();

      $hasPermission = $us->hasPermission(FIdConst::GL_INIT_GL);
      if (!$hasPermission) {
        die("没有权限");
      }

      $params = [
        "companyId" => I("post.companyId"),
        "subjectCode" => I("post.subjectCode"),
      ];

      $service = new InitService();
      $this->ajaxReturn($service->queryDataForGlAcc($params));
    }
  }

  /**
   * 查询账样列
   * 
   * JS: web\Public\Scripts\PSI\SLN0002\Init\InitGlMainForm.js
   */
  public function queryFmtColsForGlAcc()
  {
    if (IS_POST) {
      $us = new UserService();

      $hasPermission = $us->hasPermission(FIdConst::GL_INIT_GL);
      if (!$hasPermission) {
        die("没有权限");
      }

      $params = [
        "companyId" => I("post.companyId"),
        "subjectCode" => I("post.subjectCode"),
      ];

      $service = new InitService();
      $this->ajaxReturn($service->queryFmtColsForGlAcc($params));
    }
  }

  /**
   * 完成总分类账建账
   * 
   * JS: web\Public\Scripts\PSI\SLN0002\Init\InitGlMainForm.js
   */
  public function commitAccGlInit()
  {

    if (IS_POST) {
      $us = new UserService();

      $hasPermission = $us->hasPermission(FIdConst::GL_INIT_GL);
      if (!$hasPermission) {
        die("没有权限");
      }

      $params = [
        "companyId" => I("post.companyId"),
      ];

      $service = new InitService();
      $this->ajaxReturn($service->commitAccGlInit($params));
    }
  }

  /**
   * 取消总分类账建账
   * 
   * JS：web\Public\Scripts\PSI\SLN0002\Init\InitGlMainForm.js
   */
  public function cancelAccGlInit()
  {
    if (IS_POST) {
      $us = new UserService();

      $hasPermission = $us->hasPermission(FIdConst::GL_INIT_GL);
      if (!$hasPermission) {
        die("没有权限");
      }

      $params = [
        "companyId" => I("post.companyId"),
      ];

      $service = new InitService();
      $this->ajaxReturn($service->cancelAccGlInit($params));
    }
  }
}
