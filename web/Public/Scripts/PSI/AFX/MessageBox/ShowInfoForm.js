/**
 * showInfo窗体
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.AFX.MessageBox.ShowInfoForm", {
  extend: "PCL.window.Window",
  config: {
    fn: null,
    msg: "",
  },

  modal: true,
  closable: false,
  width: 600,
  layout: "fit",

  /**
   * @override
   */
  initComponent() {
    const me = this;

    PCL.apply(me, {
      header: {
        title: `<span style='font-size:160%'>${PSI.Const.PROD_NAME}</span>`,
        height: 40
      },
      height: 190,
      items: [{
        border: 0,
        xtype: "container",
        margin: "0 0 0 10",
        autoScroll: true,
        html: `
              <img style='float:left;margin:5px 20px 0px 0px;width:32px;height:32px;' 
                src='${PSI.Const.BASE_URL}Public/Images/msgbox_show_info.png?v=2023061401'></img>
              <h2 style='margin-left:50px;margin-top:10px;color:#820014;padding-right:10px'>${me.getMsg()}</h2>
              `
      }],
      buttons: [{
        id: "PSI_AFX_MessageBox_ShowInfoForm_buttonOK",
        text: "确定",
        ...PSI.Const.BTN_STYLE,
        handler: me._onOK,
        scope: me,
        iconCls: "PSI-button-ok"
      }],
      listeners: {
        show: {
          fn: me._onWndShow,
          scope: me
        },
        close: {
          fn: me._onWndClose,
          scope: me
        }
      }
    });

    me.callParent(arguments);
  },

  /**
   * @private
   */
  _onWndShow() {
    PSI.Const.msgBoxShowing = true;

    PCL.getCmp("PSI_AFX_MessageBox_ShowInfoForm_buttonOK").focus();
  },

  /**
   * @private
   */
  _onWndClose() {
    PSI.Const.msgBoxShowing = false;

    PCL.getCmp("PSI_AFX_MessageBox_ShowInfoForm_buttonOK").focus();
  },

  /**
   * @private
   */
  _onOK() {
    const me = this;

    me.close();

    const fn = me.getFn();
    if (fn) {
      fn();
    }
  }
});
