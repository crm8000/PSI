/**
 * 工厂 - 主界面
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.Factory.MainForm", {
  extend: "PSI.AFX.Form.MainForm",

  config: {
    permission: null
  },

  /**
   * @override
   */
  initComponent() {
    const me = this;

    PCL.apply(me, {
      items: [{
        tbar: me.getToolbarCmp(),
        id: me.buildId(me, "panelQueryCmp"),
        region: "north",
        height: 95,
        border: 0,
        collapsible: true,
        collapseMode: "mini",
        header: false,
        layout: {
          type: "table",
          columns: 5
        },
        bodyCls: "PSI-Query-Panel",
        items: me.getQueryCmp()
      }, {
        region: "center",
        layout: "border",
        border: 0,
        ...PSI.Const.BODY_PADDING,
        items: [{
          region: "center",
          xtype: "panel",
          layout: "border",
          border: 0,
          items: [{
            region: "north",
            border: 1,
            height: 30,
            /**
             * 这个panel只是为了使用其tbar展示信息
             * 这么做的原因是：MainGrid的column中用了lock属性后，
             * 导致页面在1920*1080分辨率（125%的放大率）下显示不正常。
             * 这应该是PCL的bug，但是目前尚未找到如何修正，所以采用这种方式迂回处理一下。
             */
            xtype: "panel",
            margin: 0,
            cls: "PSI-LC",
            tbar: ["", {
              xtype: "displayfield",
              id: me.buildId(me, "dfFactoryInfo"),
              value: "工厂列表",
            }, "->", {
                id: "pagingToolbar",
                border: 0,
                xtype: "pagingtoolbar",
                store: me.getMainGrid().getStore(),
              }, "-", {
                xtype: "displayfield",
                fieldStyle: "font-size:13px",
                value: "每页显示"
              }, {
                id: "comboCountPerPage",
                xtype: "combobox",
                editable: false,
                width: 60,
                store: PCL.create("PCL.data.ArrayStore", {
                  fields: ["text"],
                  data: [["20"], ["50"], ["100"], ["300"],
                  ["1000"]]
                }),
                value: 20,
                listeners: {
                  change: {
                    fn() {
                      const store = me.getMainGrid().getStore();
                      store.pageSize = PCL.getCmp("comboCountPerPage").getValue();
                      store.currentPage = 1;
                      PCL.getCmp("pagingToolbar").doRefresh();
                    },
                    scope: me
                  }
                }
              }, {
                xtype: "displayfield",
                fieldStyle: "font-size:13px",
                value: "条记录"
              }],
          }, {
            region: "center",
            border: 0,
            layout: "fit",
            items: me.getMainGrid()
          }]
        }, {
          id: me.buildId(me, "panelCategory"),
          xtype: "panel",
          region: "west",
          layout: "fit",
          width: 370,
          split: true,
          collapsible: true,
          header: false,
          border: 0,
          items: [me.getCategoryGrid()]
        }]
      }]
    });

    me.callParent(arguments);

    me.dfFactoryInfo = PCL.getCmp(me.buildId(me, "dfFactoryInfo"));

    me.editQueryCode = PCL.getCmp(me.buildId(me, "editQueryCode"));
    me.editQueryName = PCL.getCmp(me.buildId(me, "editQueryName"));
    me.editQueryAddress = PCL.getCmp(me.buildId(me, "editQueryAddress"));
    me.editQueryContact = PCL.getCmp(me.buildId(me, "editQueryContact"));
    me.editQueryMobile = PCL.getCmp(me.buildId(me, "editQueryMobile"));
    me.editQueryTel = PCL.getCmp(me.buildId(me, "editQueryTel"));
    me.editQueryRecordStatus = PCL.getCmp(me.buildId(me, "editQueryRecordStatus"));

    // AFX
    me.__editorList = [
      me.editQueryCode, me.editQueryName, me.editQueryAddress,
      me.editQueryContact, me.editQueryMobile, me.editQueryTel, me.editQueryRecordStatus];

    me._keyMapMain = PCL.create("PCL.util.KeyMap", PCL.getBody(), [{
      key: "N",
      ctrl: false,
      shift: false,
      alt: true,
      fn() {
        if (PSI.Const.msgBoxShowing) {
          return;
        }

        // 判断权限
        if (me.getPermission().add != "1") {
          return;
        }

        me._onAddFactory.apply(me, []);
      },
      scope: me
    }, {
      key: "Q",
      ctrl: false,
      shift: false,
      alt: true,
      fn() {
        if (PSI.Const.msgBoxShowing) {
          return;
        }
        me.editQueryCode.focus();
      },
      scope: me
    }, {
      key: "H",
      ctrl: false,
      shift: false,
      alt: true,
      fn() {
        if (PSI.Const.msgBoxShowing) {
          return;
        }

        const panel = PCL.getCmp(me.buildId(me, "panelQueryCmp"));
        if (panel.getCollapsed()) {
          panel.expand();
        } else {
          panel.collapse();
        };
      },
      scope: me
    },
    ]);

    me.freshCategoryGrid();
  },

  /**
   * @private
   */
  getToolbarCmp() {
    const me = this;

    const result = [];
    const list = [];
    if (me.getPermission().addCategory == "1") {
      list.push({
        text: "新建工厂分类",
        ...PSI.Const.BTN_STYLE,
        handler: me._onAddCategory,
        scope: me
      });
    }
    if (me.getPermission().editCategory == "1") {
      list.push({
        text: "编辑工厂分类",
        ...PSI.Const.BTN_STYLE,
        handler: me._onEditCategory,
        scope: me
      });
    }
    if (me.getPermission().deleteCategory == "1") {
      list.push({
        text: "删除工厂分类",
        ...PSI.Const.BTN_STYLE,
        handler: me._onDeleteCategory,
        scope: me
      });
    }

    if (list.length > 0) {
      result.push({
        text: "工厂分类",
        iconCls: "PSI-tb-new",
        ...PSI.Const.BTN_STYLE,
        menu: list
      });
    }

    let sep = list.length > 0;
    if (me.getPermission().add == "1") {
      if (sep) {
        result.push("-");
        sep = false;
      }

      result.push({
        iconCls: "PSI-tb-new-entity",
        text: "新建工厂 <span class='PSI-shortcut-DS'>Alt + N</span>",
        tooltip: me.buildTooltip("快捷键：Alt + N"),
        ...PSI.Const.BTN_STYLE,
        handler: me._onAddFactory,
        scope: me
      });
    }

    if (me.getPermission().edit == "1") {
      if (sep) {
        result.push("-");
        sep = false;
      }

      result.push({
        text: "编辑工厂",
        ...PSI.Const.BTN_STYLE,
        handler: me._onEditFactory,
        scope: me
      });
    }

    if (me.getPermission().del == "1") {
      if (sep) {
        result.push("-");
        sep = false;
      }

      result.push({
        text: "删除工厂",
        ...PSI.Const.BTN_STYLE,
        handler: me._onDeleteFactory,
        scope: me
      });
    }

    sep = result.length > 0;
    if (sep) {
      result.push("-");
      sep = false;
    }

    result.push({
      iconCls: "PSI-tb-help",
      text: "指南",
      ...PSI.Const.BTN_STYLE,
      handler() {
        me.focus();
        window.open(me.URL("Home/Help/index?t=factory"));
      }
    }, "-", {
      iconCls: "PSI-tb-close",
      text: "关闭",
      ...PSI.Const.BTN_STYLE,
      handler() {
        me.focus();
        me.closeWindow();
      }
    }, {
      // 空容器，只是为了撑高工具栏
      xtype: "container", height: 28,
      items: []
    });

    return result.concat(me.getShortcutCmp());
  },

  /**
   * 快捷访问
   */
  getShortcutCmp() {
    return ["->",
      {
        cls: "PSI-Shortcut-Cmp",
        labelWidth: 0,
        emptyText: "快捷访问",
        xtype: "psi_mainmenushortcutfield",
        width: 90
      }
    ];
  },

  /**
   * @private
   */
  getQueryCmp() {
    const me = this;

    return [{
      xtype: "container",
      height: 26,
      html: `<h2 style='margin-left:20px;margin-top:18px;color:#595959;display:inline-block'>工厂</h2>
              &nbsp;&nbsp;<span style='color:#8c8c8c'>主数据</span>
              <div style='float:right;display:inline-block;margin:10px 0px 0px 20px;border-left:1px solid #e5e6e8;height:40px'>&nbsp;</div>
              `
    }, {
      id: me.buildId(me, "editQueryCode"),
      labelWidth: 105,
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "工厂编码 <span class='PSI-shortcut-DS'>Alt + Q</span>",
      margin: "5, 0, 0, 0",
      xtype: "textfield",
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      }
    }, {
      id: me.buildId(me, "editQueryName"),
      labelWidth: 70,
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "工厂名称",
      margin: "5, 0, 0, 0",
      xtype: "textfield",
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      }
    }, {
      id: me.buildId(me, "editQueryAddress"),
      labelWidth: 70,
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "地址",
      margin: "5, 0, 0, 0",
      xtype: "textfield",
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      }
    }, {
      id: me.buildId(me, "editQueryContact"),
      labelWidth: 70,
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "联系人",
      margin: "5, 0, 0, 0",
      xtype: "textfield",
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      }
    }, { xtype: "container" }, {
      id: me.buildId(me, "editQueryMobile"),
      labelWidth: 70,
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "手机",
      margin: "5, 0, 0, 0",
      xtype: "textfield",
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      }
    }, {
      id: me.buildId(me, "editQueryTel"),
      labelWidth: 70,
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "固话",
      margin: "5, 0, 0, 0",
      xtype: "textfield",
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      }
    }, {
      id: me.buildId(me, "editQueryRecordStatus"),
      xtype: "combo",
      queryMode: "local",
      editable: false,
      valueField: "id",
      labelWidth: 70,
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "状态",
      margin: "5, 0, 0, 0",
      store: PCL.create("PCL.data.ArrayStore", {
        fields: ["id", "text"],
        data: [[-1, "全部"], [1000, "启用"], [0, "停用"]]
      }),
      value: -1,
      listeners: {
        specialkey: {
          fn: me.__onLastEditSpecialKey,
          scope: me
        }
      }
    }, {
      xtype: "container",
      items: [{
        xtype: "button",
        text: "查询",
        cls: "PSI-Query-btn1",
        width: 100,
        height: 26,
        margin: "5, 0, 0, 20",
        handler: me._onQuery,
        scope: me
      }, {
        xtype: "button",
        text: "清空查询条件",
        cls: "PSI-Query-btn2",
        width: 100,
        height: 26,
        margin: "5, 0, 0, 15",
        handler: me._onClearQuery,
        scope: me
      }, {
        xtype: "button",
        text: "隐藏工具栏 <span class='PSI-shortcut-DS'>Alt + H</span>",
        cls: "PSI-Query-btn3",
        width: 140,
        height: 26,
        iconCls: "PSI-button-hide",
        margin: "5 0 0 20",
        handler() {
          PCL.getCmp(me.buildId(me, "panelQueryCmp")).collapse();
        },
        scope: me
      }]
    }];
  },

  /**
   * @private
   */
  getCategoryGrid() {
    const me = this;
    if (me._categoryGrid) {
      return me._categoryGrid;
    }

    const modelName = me.buildModelName(me, "FactoryCategory");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "code", "name", {
        name: "cnt",
        type: "int"
      }]
    });

    me._categoryGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-FC",
      viewConfig: {
        enableTextSelection: true
      },
      header: {
        height: 30,
        title: me.formatGridHeaderTitle(me.toTitleKeyWord("工厂分类"))
      },
      tools: [{
        type: "close",
        handler() {
          PCL.getCmp(me.buildId(me, "panelCategory")).collapse();
        }
      }],
      features: [{
        ftype: "summary"
      }],
      columnLines: true,
      columns: [{
        header: "分类编码",
        dataIndex: "code",
        width: 80,
        menuDisabled: true,
        sortable: false
      }, {
        header: "工厂分类",
        dataIndex: "name",
        width: 160,
        menuDisabled: true,
        sortable: false,
        summaryRenderer() {
          return "工厂个数合计";
        }
      }, {
        header: "工厂个数",
        dataIndex: "cnt",
        width: 100,
        menuDisabled: true,
        sortable: false,
        summaryType: "sum",
        align: "right"
      }],
      store: PCL.create("PCL.data.Store", {
        model: modelName,
        autoLoad: false,
        data: []
      }),
      listeners: {
        select: {
          fn: me._onCategoryGridSelect,
          scope: me
        },
        itemdblclick: {
          fn: me._onEditCategory,
          scope: me
        }
      }
    });

    return me._categoryGrid;
  },

  /**
   * @private
   */
  getMainGrid() {
    const me = this;
    if (me._mainGrid) {
      return me._mainGrid;
    }

    const modelName = me.buildModelName(me, "Factory");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "code", "name", "contact01", "tel01",
        "mobile01", "contact02", "tel02", "mobile02",
        "categoryId", "initPayables", "initPayablesDT",
        "address", "bankName", "bankAccount", "tax", "fax",
        "note", "dataOrg", "recordStatus"]
    });

    const store = PCL.create("PCL.data.Store", {
      autoLoad: false,
      model: modelName,
      data: [],
      pageSize: 20,
      proxy: {
        type: "ajax",
        actionMethods: {
          read: "POST"
        },
        url: me.URL("SLN0001/Factory/factoryList"),
        reader: {
          root: 'dataList',
          totalProperty: 'totalCount'
        }
      },
      listeners: {
        beforeload: {
          fn() {
            store.proxy.extraParams = me.getQueryParam();
          },
          scope: me
        },
        load: {
          fn(e, records, successful) {
            if (successful) {
              me.refreshCategoryCount();
              me.gotoMainGridRecord(me._lastId);
            }
          },
          scope: me
        }
      }
    });

    me._mainGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-LC",
      viewConfig: {
        enableTextSelection: true
      },
      columnLines: true,
      columns: {
        defaults: {
          menuDisabled: true,
          sortable: false
        },
        items: [PCL.create("PCL.grid.RowNumberer", {
          text: "#",
          width: 40
        }), {
          header: "工厂编码",
          locked: true,
          dataIndex: "code",
          renderer(value, metaData, record) {
            if (parseInt(record.get("recordStatus")) == 1000) {
              return value;
            } else {
              return `<span class="PSI-record-disabled">${value}</span>`;
            }
          }
        }, {
          header: "工厂名称",
          locked: true,
          dataIndex: "name",
          width: 300,
          renderer(value, metaData, record) {
            if (parseInt(record.get("recordStatus")) == 1000) {
              return value;
            } else {
              return `<span class="PSI-record-disabled">${value}</span>`;
            }
          }
        }, {
          header: "地址",
          dataIndex: "address",
          width: 300
        }, {
          header: "联系人",
          dataIndex: "contact01"
        }, {
          header: "手机",
          dataIndex: "mobile01"
        }, {
          header: "固话",
          dataIndex: "tel01"
        }, {
          header: "备用联系人",
          dataIndex: "contact02"
        }, {
          header: "备用联系人手机",
          dataIndex: "mobile02",
          width: 150
        }, {
          header: "备用联系人固话",
          dataIndex: "tel02",
          width: 150
        }, {
          header: "开户行",
          dataIndex: "bankName"
        }, {
          header: "开户行账号",
          dataIndex: "bankAccount"
        }, {
          header: "社会统一信用代码",
          dataIndex: "tax",
          width: 150,
        }, {
          header: "传真",
          dataIndex: "fax"
        }, {
          header: "应付期初余额",
          dataIndex: "initPayables",
          align: "right",
          xtype: "numbercolumn",
          width: 150
        }, {
          header: "应付期初余额日期",
          dataIndex: "initPayablesDT",
          width: 150
        }, {
          header: "备注",
          dataIndex: "note",
          width: 400
        }, {
          header: "数据域",
          dataIndex: "dataOrg"
        }, {
          header: "状态",
          dataIndex: "recordStatus",
          renderer(value) {
            if (parseInt(value) == 1000) {
              return "启用";
            } else {
              return "<span style='color:red'>停用</span>";
            }
          }
        }]
      },
      store,
      listeners: {
        itemdblclick: {
          fn: me._onEditFactory,
          scope: me
        }
      }
    });

    return me._mainGrid;
  },

  /**
   * @private
   */
  _onAddCategory() {
    const me = this;

    const form = PCL.create("PSI.SLN0001.Factory.CategoryEditForm", {
      parentForm: me,
      renderTo: PSI.Const.RENDER_TO(),
    });

    form.show();
  },

  /**
   * @private
   */
  _onEditCategory() {
    const me = this;
    if (me.getPermission().editCategory == "0") {
      return;
    }

    const item = me.getCategoryGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("请选择要编辑的工厂分类");
      return;
    }

    const category = item[0];

    const form = PCL.create("PSI.SLN0001.Factory.CategoryEditForm", {
      parentForm: me,
      entity: category,
      renderTo: PSI.Const.RENDER_TO(),
    });

    form.show();
  },

  /**
   * @private
   */
  _onDeleteCategory() {
    const me = this;

    const item = me.getCategoryGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      PSI.MsgBox.showInfo("请选择要删除的工厂分类");
      return;
    }

    const category = item[0];
    const info = "请确认是否删除工厂分类: <span style='color:red'>"
      + category.get("name") + "</span>";

    const store = me.getCategoryGrid().getStore();
    let index = store.findExact("id", category.get("id"));
    index--;
    let preIndex = null;
    const preItem = store.getAt(index);
    if (preItem) {
      preIndex = preItem.get("id");
    }

    me.confirm(info, () => {
      const el = PCL.getBody();
      el && el.mask("正在删除中...");
      me.ajax({
        url: me.URL("SLN0001/Factory/deleteCategory"),
        params: {
          id: category.get("id")
        },
        callback(options, success, response) {
          el && el.unmask();

          if (success) {
            const data = me.decodeJSON(response.responseText);
            if (data.success) {
              me.tip("成功完成删除操作", true);
              me.freshCategoryGrid(preIndex);
            } else {
              me.showInfo(data.msg);
            }
          }
        }
      });
    });
  },

  /**
   * @private
   */
  freshCategoryGrid(id) {
    const me = this;
    const grid = me.getCategoryGrid();
    const el = grid.getEl() || PCL.getBody();
    el.mask(PSI.Const.LOADING);
    me.ajax({
      url: me.URL("SLN0001/Factory/categoryList"),
      params: me.getQueryParam(),
      callback(options, success, response) {
        const store = grid.getStore();

        store.removeAll();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          store.add(data);

          if (id) {
            const r = store.findExact("id", id);
            if (r != -1) {
              grid.getSelectionModel().select(r);
            }
          } else {
            grid.getSelectionModel().select(0);
          }
        }

        el.unmask();
      }
    });
  },

  /**
   * @private
   */
  freshMainGrid(id) {
    const me = this;

    const item = me.getCategoryGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.dfFactoryInfo.setValue("工厂列表");
      return;
    }

    const category = item[0];

    const info = `<span class='PSI-title-keyword'>${category.get("name")}</span> - 工厂列表`;
    me.dfFactoryInfo.setValue(info);

    me._lastId = id;
    PCL.getCmp("pagingToolbar").doRefresh()
  },

  /**
   * @private
   */
  _onCategoryGridSelect() {
    const me = this;
    me.getMainGrid().getStore().currentPage = 1;
    me.freshMainGrid();
  },

  /**
   * @private
   */
  _onAddFactory() {
    const me = this;

    if (me.getCategoryGrid().getStore().getCount() == 0) {
      me.showInfo("没有工厂分类，请先新建工厂分类");
      return;
    }

    const form = PCL.create("PSI.SLN0001.Factory.FactoryEditForm", {
      parentForm: me,
      renderTo: PSI.Const.RENDER_TO(),
    });

    form.show();
  },

  /**
   * @private
   */
  _onEditFactory() {
    const me = this;
    if (me.getPermission().edit == "0") {
      return;
    }

    let item = me.getCategoryGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择工厂分类");
      return;
    }
    const category = item[0];

    item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("请选择要编辑的工厂");
      return;
    }

    const factory = item[0];
    factory.set("categoryId", category.get("id"));
    const form = PCL.create("PSI.SLN0001.Factory.FactoryEditForm", {
      parentForm: me,
      entity: factory,
      renderTo: PSI.Const.RENDER_TO(),
    });

    form.show();
  },

  /**
   * @private
   */
  _onDeleteFactory() {
    const me = this;
    const item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("请选择要删除的工厂");
      return;
    }

    const factory = item[0];

    const store = me.getMainGrid().getStore();
    let index = store.findExact("id", factory.get("id"));
    index--;
    let preIndex = null;
    const preItem = store.getAt(index);
    if (preItem) {
      preIndex = preItem.get("id");
    }

    const info = `请确认是否删除工厂: <span style='color:red'>${factory.get("name")}</span>`;
    me.confirm(info, () => {
      const el = PCL.getBody();
      el.mask("正在删除中...");
      me.ajax({
        url: me.URL("SLN0001/Factory/deleteFactory"),
        params: {
          id: factory.get("id")
        },
        callback(options, success, response) {
          el.unmask();

          if (success) {
            const data = me.decodeJSON(response.responseText);
            if (data.success) {
              me.tip("成功完成删除操作", true);
              me.freshMainGrid(preIndex);
            } else {
              me.showInfo(data.msg);
            }
          }
        }

      });
    });
  },

  /**
   * @private
   */
  gotoMainGridRecord(id) {
    const me = this;
    const grid = me.getMainGrid();
    const store = grid.getStore();
    if (id) {
      const r = store.findExact("id", id);
      if (r != -1) {
        grid.getSelectionModel().select(r);
        grid.getSelectionModel().setLastFocused(store.getAt(r));
      } else {
        grid.getSelectionModel().select(0);
      }
    } else {
      grid.getSelectionModel().select(0);
    }
  },

  /**
   * @private
   */
  refreshCategoryCount() {
    const me = this;
    const item = me.getCategoryGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      return;
    }

    const category = item[0];
    category.set("cnt", me.getMainGrid().getStore().getTotalCount());
    me.getCategoryGrid().getStore().commitChanges();
  },

  /**
   * @private
   */
  getQueryParam() {
    const me = this;
    const item = me.getCategoryGrid().getSelectionModel().getSelection();
    let categoryId;
    if (item == null || item.length != 1) {
      categoryId = null;
    } else {
      categoryId = item[0].get("id");
    }

    const result = {
      categoryId: categoryId
    };

    const code = PCL.getCmp(me.buildId(me, "editQueryCode")).getValue();
    if (code) {
      result.code = code;
    }

    const address = PCL.getCmp(me.buildId(me, "editQueryAddress")).getValue();
    if (address) {
      result.address = address;
    }

    const name = PCL.getCmp(me.buildId(me, "editQueryName")).getValue();
    if (name) {
      result.name = name;
    }

    const contact = PCL.getCmp(me.buildId(me, "editQueryContact")).getValue();
    if (contact) {
      result.contact = contact;
    }

    const mobile = PCL.getCmp(me.buildId(me, "editQueryMobile")).getValue();
    if (mobile) {
      result.mobile = mobile;
    }

    const tel = PCL.getCmp(me.buildId(me, "editQueryTel")).getValue();
    if (tel) {
      result.tel = tel;
    }

    result.recordStatus = PCL.getCmp(me.buildId(me, "editQueryRecordStatus")).getValue();

    return result;
  },

  /**
   * @private
   */
  _onQuery() {
    const me = this;

    me.getMainGrid().getStore().removeAll();
    me.freshCategoryGrid();
  },

  /**
   * @private
   */
  _onClearQuery() {
    const me = this;

    me.editQueryCode.setValue("");
    me.editQueryName.setValue("");
    me.editQueryAddress.setValue("");
    me.editQueryContact.setValue("");
    me.editQueryMobile.setValue("");
    me.editQueryTel.setValue("");
    me.editQueryRecordStatus.setValue(-1);

    me._onQuery();
  }
});
