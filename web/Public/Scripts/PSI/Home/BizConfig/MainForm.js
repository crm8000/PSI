/**
 * 业务设置 - 主窗体
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.Home.BizConfig.MainForm", {
  extend: "PSI.AFX.Form.MainForm",

  /**
   * @override
   */
  initComponent() {
    const me = this;

    PCL.apply(me, {
      border: 0,
      layout: "border",
      tbar: me.getToolbarCmp(),
      items: [{
        id: me.buildId(me, "panelQueryCmp"),
        region: "north",
        height: 40,
        margin:"5 0 0 0",
        border: 0,
        header: false,
        collapsible: true,
        collapseMode: "mini",
        layout: {
          type: "table",
          columns: 5
        },
        bodyCls: "PSI-Query-Panel",
        items: me.getQueryCmp()
      }, {
        region: "center",
        xtype: "panel",
        layout: "border",
        border: 0,
        ...PSI.Const.BODY_PADDING,
        items: [{
          region: "center",
          border: 1,
          items: me.getMainGrid()
        }]
      }]
    });

    me.callParent();

    me.comboCompany = PCL.getCmp(me.buildId(me, "comboCompany"));

    me.queryCompany();
  },

  /**
   * @private
   */
  getQueryCmp() {
    const me = this;

    const modelName = me.buildModelName(me, "Company");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "name"]
    });

    return [{
      xtype: "container",
      height: 26,
      html: `<h2 style='margin-left:20px;margin-top:3px;color:#595959;display:inline-block'>业务设置</h2>
              &nbsp;&nbsp;<span style='color:#8c8c8c'>设置各公司的全局业务配置项</span>
              <div style='float:right;display:inline-block;margin:0px 0px 0px 20px;border-left:1px solid #e5e6e8;height:32px'>&nbsp;</div>
              `
    }, {
      xtype: "combobox",
      labelWidth: 65,
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "组织机构",
      margin: "5, 0, 0, 0",
      id: me.buildId(me, "comboCompany"),
      queryMode: "local",
      editable: false,
      valueField: "id",
      displayField: "name",
      store: PCL.create("PCL.data.Store", {
        model: modelName,
        autoLoad: false,
        data: []
      }),
      width: 500,
      listeners: {
        select: {
          fn: me._onComboCompanySelect,
          scope: me
        }
      }
    }, {
      xtype: "container",
      items: [{
        xtype: "button",
        text: "设置",
        cls: "PSI-Query-btn1",
        width: 100,
        height: 26,
        margin: "5 0 0 20",
        handler: me._onEdit,
        scope: me
      }]
    }];
  },

  /**
   * @private
   */
  getMainGrid() {
    const me = this;
    if (me._mainGrid) {
      return me._mainGrid;
    }

    const modelName = me.buildModelName(me, "BizConfig");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "name", "value", "displayValue",
        "note"],
      idProperty: "id"
    });
    const store = PCL.create("PCL.data.Store", {
      model: modelName,
      data: [],
      autoLoad: false
    });

    me._mainGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-FC",
      viewConfig: {
        enableTextSelection: true
      },
      loadMask: true,
      border: 0,
      columnLines: true,
      columns: [PCL.create("PCL.grid.RowNumberer", {
        text: "#",
        width: 40
      }), {
        text: "设置项",
        dataIndex: "name",
        width: 224,
        menuDisabled: true
      }, {
        text: "值",
        dataIndex: "displayValue",
        width: 500,
        menuDisabled: true
      }, {
        text: "备注",
        dataIndex: "note",
        width: 500,
        menuDisabled: true
      }],
      store: store,
      listeners: {
        itemdblclick: {
          fn: me._onEdit,
          scope: me
        }
      }
    });

    return me._mainGrid;
  },

  /**
   * @private
   */
  getToolbarCmp() {
    const me = this;

    return [{
      iconCls: "PSI-tb-new",
      text: "设置",
      ...PSI.Const.BTN_STYLE,
      handler: me._onEdit,
      scope: me
    }, "-", {
      iconCls: "PSI-tb-help",
      text: "指南",
      ...PSI.Const.BTN_STYLE,
      handler() {
        me.focus();
        window.open(me.URL("/Home/Help/index?t=bizconfig"));
      }
    }, "-", {
      iconCls: "PSI-tb-close",
      text: "关闭",
      ...PSI.Const.BTN_STYLE,
      handler() {
        me.focus();
        me.closeWindow();
      }
    }, {
      // 空容器，只是为了撑高工具栏
      xtype: "container", height: 28,
      items: []
    }, "->", {
      cls: "PSI-Shortcut-Cmp",
      labelWidth: 0,
      emptyText: "快捷访问",
      xtype: "psi_mainmenushortcutfield",
      width: 90
    }];
  },

  /**
   * 设置按钮被单击
   */
  _onEdit() {
    const me = this;

    const companyId = me.comboCompany.getValue();
    if (!companyId) {
      me.showInfo("没有选择要设置的公司");
      return;
    }

    const form = PCL.create("PSI.Home.BizConfig.EditForm", {
      renderTo: PSI.Const.RENDER_TO(),
      parentForm: me,
      companyId: companyId
    });
    form.show();
  },

  /**
   * 查询公司信息
   */
  queryCompany() {
    const me = this;
    const el = PCL.getBody();
    const comboCompany = me.comboCompany;
    const store = comboCompany.getStore();
    el.mask(PSI.Const.LOADING);
    const r = {
      url: me.URL("Home/BizConfig/getCompany"),
      callback(options, success, response) {
        store.removeAll();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          store.add(data);
          if (data.length > 0) {
            comboCompany.setValue(data[0]["id"]);
            me.refreshGrid();
          }
        }

        el.unmask();
      }
    };
    me.ajax(r);
  },

  _onComboCompanySelect() {
    const me = this;

    me.refreshGrid();
  },

  /**
   * @private
   */
  refreshGrid() {
    const me = this;
    const grid = me.getMainGrid();
    const el = grid.getEl() || PCL.getBody();
    el.mask(PSI.Const.LOADING);
    const r = {
      url: me.URL("Home/BizConfig/allConfigs"),
      params: {
        companyId: me.comboCompany.getValue()
      },
      callback(options, success, response) {
        const store = grid.getStore();

        store.removeAll();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          store.add(data);
        }

        el.unmask();
      }
    };
    me.ajax(r);
  },
});
