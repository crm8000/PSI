/**
 * 供应商自定义字段
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.Supplier.SupplierField", {
  extend: "PCL.form.field.Trigger",
  alias: "widget.psi_supplierfield",

  mixins: ["PSI.AFX.Mix.Common"],

  config: {
    callbackFunc: null,
    callbackScope: null,
    showAddButton: false,
    showModal: false
  },

  /**
   * @override
   */
  initComponent() {
    const me = this;

    me._idValue = null;

    me.enableKeyEvents = true;

    me.callParent(arguments);

    me.on("keydown", (field, e) => {
      if (me.readOnly) {
        return;
      }

      if (e.getKey() == e.BACKSPACE) {
        field.setValue(null);
        me.setIdValue(null);
        e.preventDefault();
        return false;
      }

      if (e.altKey) {
        return;
      }

      if (e.getKey() != e.ENTER && !e.isSpecialKey(e.getKey())) {
        me.onTriggerClick(e);
      }
    });

    me.on({
      render(p) {
        p.getEl().on("dblclick", (e) => {
          me.selectText(0, 0);
          me.onTriggerClick();
        });
      },
      single: true
    });
  },

  /**
   * @override
   */
  onTriggerClick(e) {
    const me = this;

    if (me.readOnly) {
      return;
    }

    const modelName = me.buildModelName(me, "SupplierField");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "code", "name", "tel01", "fax",
        "address_shipping", "contact01", "taxRate"]
    });

    const store = PCL.create("PCL.data.Store", {
      model: modelName,
      autoLoad: false,
      data: []
    });
    const lookupGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-Lookup",
      columnLines: true,
      border: 1,
      store: store,
      viewConfig: {
        enableTextSelection: true
      },
      columns: [{
        header: "编码",
        dataIndex: "code",
        menuDisabled: true
      }, {
        header: "供应商",
        dataIndex: "name",
        menuDisabled: true,
        flex: 1
      }]
    });
    me._lookupGrid = lookupGrid;
    me._lookupGrid.on("itemdblclick", me._onOK, me);

    const wnd = PCL.create("PCL.window.Window", {
      title: "选择 - 供应商",
      modal: me.getShowModal(),
      header: false,
      border: 0,
      width: 750,
      height: 300,
      layout: "border",
      onEsc() {
        me._wnd.hide();
        me.focus();
        me._wnd.close();
      },
      items: [{
        region: "center",
        xtype: "panel",
        layout: "fit",
        border: 0,
        items: [lookupGrid]
      }, {
        xtype: "panel",
        region: "south",
        height: 32,
        layout: "fit",
        border: 0,
        items: [{
          xtype: "form",
          layout: "form",
          bodyPadding: 5,
          bodyCls: "PSI-Field",
          items: [{
            id: me.buildId(me, "editSupplier"),
            xtype: "textfield",
            labelWidth: 0,
            labelAlign: "right",
            labelSeparator: ""
          }]
        }]
      }],
      buttons: [{
        xtype: "container",
        html: `
          <div class="PSI-lookup-note">
            输入编码、供应商拼音字头可以过滤查询；
            ↑ ↓ 键改变当前选择项 ；回车键返回
          </div>
        `
      }, "->", {
        text: "新建供应商档案",
        cls: "PSI-Lookup-btn",
        hidden: !me.getShowAddButton(),
        handler: me._onAdd,
        scope: me
      }, {
        text: "确定",
        cls: "PSI-Lookup-btn",
        handler: me._onOK,
        scope: me
      }, {
        text: "取消",
        cls: "PSI-Lookup-btn",
        handler() {
          wnd.close();
          me.focus();
        }
      }]
    });

    wnd.on("close", () => {
      me.focus();
    });
    if (!me.getShowModal()) {
      wnd.on("deactivate", () => {
        wnd.close();
      });
    }
    me._wnd = wnd;

    const editName = PCL.getCmp(me.buildId(me, "editSupplier"));
    editName.on("change", () => {
      if (editName._inIME) {
        return;
      }

      const store = me._lookupGrid.getStore();
      me.ajax({
        url: me.URL("SLN0001/Supplier/queryData"),
        params: {
          queryKey: editName.getValue()
        },
        callback(opt, success, response) {
          store.removeAll();
          if (success) {
            const data = me.decodeJSON(response.responseText);
            store.add(data);
            if (data.length > 0) {
              me._lookupGrid.getSelectionModel().select(0);
              editName.focus();
            }
          } else {
            me.showInfo("网络错误");
          }
        },
        scope: me
      });

    }, me);

    editName.on("specialkey", (field, e) => {
      if (e.getKey() == e.ENTER) {
        me._onOK();
      } else if (e.getKey() == e.UP) {
        const m = me._lookupGrid.getSelectionModel();
        const store = me._lookupGrid.getStore();
        let index = 0;
        for (let i = 0; i < store.getCount(); i++) {
          if (m.isSelected(i)) {
            index = i;
          }
        }
        index--;
        if (index < 0) {
          index = 0;
        }
        m.select(index);
        e.preventDefault();
        editName.focus();
      } else if (e.getKey() == e.DOWN) {
        const m = me._lookupGrid.getSelectionModel();
        const store = me._lookupGrid.getStore();
        let index = 0;
        for (let i = 0; i < store.getCount(); i++) {
          if (m.isSelected(i)) {
            index = i;
          }
        }
        index++;
        if (index > store.getCount() - 1) {
          index = store.getCount() - 1;
        }
        m.select(index);
        e.preventDefault();
        editName.focus();
      }
    }, me);

    me._wnd.on("show", () => {
      me.applyIMEHandler(editName);

      editName.focus();
      editName.fireEvent("change");

      if (me.getShowModal()) {
        me.addMaskClickHandler(me._wnd);
      }
    }, me);
    wnd.showBy(me);
  },

  /**
   * @private
   */
  _onOK() {
    const me = this;

    const grid = me._lookupGrid;
    const item = grid.getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      return;
    }

    const data = item[0].getData();

    me._wnd.close();

    me.setValue(data.name);
    me.setFocusAndCursorPosToLast(me);

    me.setIdValue(data.id);

    const func = me.getCallbackFunc();
    if (func) {
      func.apply(me.getCallbackScope(), [data]);
    }
  },

  /**
   * @public
   */
  setIdValue(id) {
    this._idValue = id;
  },

  /**
   * @public
   */
  getIdValue() {
    return this._idValue;
  },

  /**
   * @public
   */
  clearIdValue() {
    this.setValue(null);
    this._idValue = null;
  },

  /**
   * @private
   */
  _onAdd() {
    const form = PCL.create("PSI.SLN0001.Supplier.SupplierEditForm");
    form.show();
  }
});
