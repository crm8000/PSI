/**
 * FId一览 - 主界面
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0000.FIdList.MainForm", {
  extend: "PSI.AFX.Form.MainForm",

  /**
   * @override
   */
  initComponent() {
    const me = this;

    PCL.apply(me, {
      tbar: me.getToolbarCmp(),
      items: [{
        region: "north",
        height: 55,
        border: 0,
        bodyPadding: "0 20 0 20",
        html: "<h2 style='color:#595959;display:inline-block'>FId一览</h2>&nbsp;&nbsp;<span style='color:#8c8c8c'>fid用来表示一个功能的全局唯一标识符</span>",
      }, {
        region: "center",
        layout: "fit",
        border: 0,
        bodyPadding: "0 20 5 15",
        items: me.getMainGrid()
      }]
    });

    me.callParent(arguments);

    me.refreshMainGrid();
  },

  /**
   * @private
   */
  getToolbarCmp() {
    const me = this;

    return [{
      iconCls: "PSI-tb-new",
      text: "编辑fid的编码和助记码",
      ...PSI.Const.BTN_STYLE,
      handler: me._onEdit,
      scope: me,
    }, "-", {
      iconCls: "PSI-tb-help",
      ...PSI.Const.BTN_STYLE,
      text: "指南",
      handler() {
        me.focus();
        window.open(me.URL("/Home/Help/index?t=fidList"));
      }
    }, "-", {
      iconCls: "PSI-tb-close",
      ...PSI.Const.BTN_STYLE,
      text: "关闭",
      handler() {
        me.focus();
        me.closeWindow();
      }
    }, {
      // 空容器，只是为了撑高工具栏
      xtype: "container", height: 28,
      items: []
    }].concat(me.getShortcutCmp());
  },

  /**
   * 快捷访问
   * 
   * @private
   */
  getShortcutCmp() {
    return ["->",
      {
        cls: "PSI-Shortcut-Cmp",
        labelWidth: 0,
        emptyText: "快捷访问",
        xtype: "psi_mainmenushortcutfield",
        width: 90
      }
    ];
  },

  /**
   * @private
   */
  getMainGrid() {
    const me = this;

    if (me._mainGrid) {
      return me._mainGrid;
    }

    const modelName = me.buildModelName(me, "FIdModel");

    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "fid", "name", "code", "category", "sln", "py"]
    });

    me._mainGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-FC",
      viewConfig: {
        enableTextSelection: true
      },
      columnLines: true,
      columns: {
        defaults: {
          menuDisabled: true,
          sortable: false
        },
        items: [PCL.create("PCL.grid.RowNumberer", {
          text: "#",
          width: 60
        }), {
          header: "fid",
          dataIndex: "fid",
          width: 140,
        }, {
          header: "编码",
          dataIndex: "code",
          width: 140,
        }, {
          header: "助记码",
          dataIndex: "py",
          width: 140,
        }, {
          header: "名称",
          dataIndex: "name",
          width: 400,
        }, {
          header: "性质",
          dataIndex: "category",
          width: 90,
        }, {
          header: "解决方案",
          dataIndex: "sln",
          width: 320,
        }]
      },
      store: PCL.create("PCL.data.Store", {
        model: modelName,
        autoLoad: false,
        data: []
      }),
      listeners: {
        itemdblclick: {
          fn: me._onEdit,
          scope: me
        }
      }
    });

    return me._mainGrid;
  },

  /**
   * @private
   */
  refreshMainGrid(fid) {
    const me = this;

    const grid = me.getMainGrid();
    const el = grid.getEl() || PCL.getBody();
    el.mask(PSI.Const.LOADING);
    const r = {
      url: me.URL("SLN0000/FIdList/fidList"),
      callback(options, success, response) {
        const store = grid.getStore();

        store.removeAll();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          store.add(data);

          if (store.getCount() > 0) {
            if (fid) {
              const r = store.findExact("fid", fid);
              if (r != -1) {
                grid.getSelectionModel().select(r);
              }
            } else {
              grid.getSelectionModel().select(0);
            }
          }
        }

        el.unmask();
      }
    };

    me.ajax(r);
  },

  /**
   * 编辑FId的编码和拼音字头
   */
  _onEdit() {
    const me = this;
    const item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("请选择要编辑的FId");
      return;
    }

    const entity = item[0];

    const form = PCL.create("PSI.SLN0000.FIdList.EditForm", {
      entity,
      parentForm: me,
      renderTo: PSI.Const.RENDER_TO(),
    });
    form.show();
  },
});
