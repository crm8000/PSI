/**
 * 自定义字段 - 码表用于凭证录入的自定义字段
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0000.CodeTable.Runtime.CodeTableVoucherField", {
  extend: "PCL.form.field.Trigger",
  alias: "widget.psi_codetable_voucherfield",

  mixins: ["PSI.AFX.Mix.Common"],

  config: {
    fid: null,
    showModal: false,
    initIdValue: null,

    // 用于动态获取companyId
    // 当码表记录需要优先按compnayId过滤的时候，就需要设置本回调函数
    companyIdFuncCallback: null,
    companyIdFuncCallbackScope: null,
  },

  // config
  applyInitIdValue(idValue) {
    const me = this;
    me.setIdValue(idValue);
  },

  /**
   * @override
   * 
   * 初始化组件
   */
  initComponent() {
    const me = this;
    me._idValue = null;
    me._code = null;
    me._name = null;

    me.enableKeyEvents = true;

    me.callParent(arguments);

    me.on("keydown", (field, e) => {
      if (me.readOnly) {
        return;
      }

      if (e.getKey() == e.BACKSPACE) {
        field.setValue(null);
        me.setIdValue(null);
        e.preventDefault();
        return false;
      }

      if (e.getKey() != e.ENTER && !e.isSpecialKey(e.getKey())) {
        me.onTriggerClick(e);
      }
    });

    me.on({
      render(p) {
        p.getEl().on("dblclick", () => {
          me.selectText(0, 0);
          me.onTriggerClick();
        });
      },
      single: true
    });
  },

  /**
   * @override
   * 
   * 单击下拉按钮
   */
  onTriggerClick(e) {
    const me = this;

    if (me.readOnly) {
      return;
    }

    const modelName = me.buildModelName(me, "PSICodeTableRecordRefField");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "code", "name"]
    });

    const store = PCL.create("PCL.data.Store", {
      model: modelName,
      autoLoad: false,
      data: []
    });
    const lookupGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-Lookup",
      columnLines: true,
      border: 1,
      store: store,
      columns: [{
        header: "编码",
        dataIndex: "code",
        menuDisabled: true
      }, {
        header: "名称",
        dataIndex: "name",
        menuDisabled: true,
        flex: 1
      }]
    });
    me.lookupGrid = lookupGrid;
    me.lookupGrid.on("itemdblclick", me._onOK, me);

    const wnd = PCL.create("PCL.window.Window", {
      modal: me.getShowModal(),
      width: 700,
      height: 320,
      header: false,
      border: 0,
      layout: "border",
      onEsc() {
        me.wnd.hide();
        me.focus();
        me.wnd.close();
      },
      items: [{
        region: "center",
        xtype: "panel",
        layout: "fit",
        border: 0,
        items: [lookupGrid]
      }, {
        xtype: "panel",
        region: "south",
        height: 32,
        layout: "fit",
        border: 0,
        items: [{
          xtype: "form",
          layout: "form",
          bodyPadding: 5,
          bodyCls: "PSI-Field",
          items: [{
            id: me.buildId(me, "editName"),
            xtype: "textfield",
            labelWidth: 0,
            labelAlign: "right",
            labelSeparator: ""
          }]
        }]
      }],
      buttons: [{
        xtype: "container",
        html: `
          <div class="PSI-lookup-note">
            输入编码、名称拼音字头可以过滤查询；
            ↑ ↓ 键改变当前选择项 ；回车键返回；ESC键取消
          </div>
          `
      }, "->", {
        text: "确定",
        handler: me._onOK,
        scope: me
      }, {
        text: "取消",
        handler() {
          wnd.close();
          me.focus();
        }
      }]
    });

    wnd.on("close", function () {
      me.focus();
    });
    if (!me.getShowModal()) {
      wnd.on("deactivate", function () {
        wnd.close();
      });
    }
    me.wnd = wnd;

    const editName = PCL.getCmp(me.buildId(me, "editName"));
    editName.on("change", function () {
      let companyId = null;
      const fc = me.getCompanyIdFuncCallback();
      if (fc) {
        companyId = fc.call(me.getCompanyIdFuncCallbackScope());
      }
      const store = me.lookupGrid.getStore();
      me.ajax({
        url: me.URL("SLN0000/CodeTableRuntime/queryDataForRecordRef"),
        params: {
          queryKey: editName.getValue(),
          fid: me.getFid(),
          companyId,
        },
        callback(opt, success, response) {
          store.removeAll();
          if (success) {
            const data = me.decodeJSON(response.responseText);
            store.add(data);
            if (data.length > 0) {
              me.lookupGrid.getSelectionModel().select(0);
              editName.focus();
            }
          } else {
            me.showInfo("网络错误");
          }
        },
        scope: me
      });

    }, me);

    editName.on("specialkey", (field, e) => {
      if (e.getKey() == e.ENTER) {
        me._onOK();
      } else if (e.getKey() == e.UP) {
        const m = me.lookupGrid.getSelectionModel();
        const store = me.lookupGrid.getStore();
        let index = 0;
        for (let i = 0; i < store.getCount(); i++) {
          if (m.isSelected(i)) {
            index = i;
          }
        }
        index--;
        if (index < 0) {
          index = 0;
        }
        m.select(index);
        e.preventDefault();
        editName.focus();
      } else if (e.getKey() == e.DOWN) {
        const m = me.lookupGrid.getSelectionModel();
        const store = me.lookupGrid.getStore();
        let index = 0;
        for (let i = 0; i < store.getCount(); i++) {
          if (m.isSelected(i)) {
            index = i;
          }
        }
        index++;
        if (index > store.getCount() - 1) {
          index = store.getCount() - 1;
        }
        m.select(index);
        e.preventDefault();
        editName.focus();
      }
    }, me);

    me.wnd.on("show", () => {
      editName.focus();
      editName.fireEvent("change");
    }, me);
    wnd.showBy(me);
  },

  /**
   * @private
   */
  _onOK() {
    const me = this;
    const grid = me.lookupGrid;
    const item = grid.getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      return;
    }

    const data = item[0];

    me.wnd.close();
    const code = data.get("code");
    const name = data.get("name");

    me.setValue(`${code} - ${name}`);
    me.focus();

    me.setIdValue(data.get("id"));
    me.setCodeValue(code);
    me.setNameValue(name);
  },

  /**
   * @public
   */
  setIdValue(id) {
    this._idValue = id;
  },

  /**
   * @public
   */
  getIdValue() {
    return this._idValue;
  },

  /**
   * @public
   */
  clearIdValue() {
    const me = this;

    me.setValue(null);
    me._idValue = null;
    me._code = null;
    me._name = null;
  },

  /**
   * @public
   */
  setCodeValue(code) {
    this._code = code;
  },

  /**
   * @public
   */
  getCodeValue() {
    return this._code;
  },

  /**
   * @public
   */
  setNameValue(name) {
    this._name = name;
  },

  /**
   * @public
   */
  getNameValue() {
    return this._name;
  },
});
