<?php

namespace SLN0002\Controller;

use Home\Common\FIdConst;
use Home\Controller\PSIBaseController;
use Home\Service\UserService;
use SLN0002\Service\SubjectService;

/**
 * 会计科目Controller
 *
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
class SubjectController extends PSIBaseController
{

  /**
   * 会计科目 - 主页面
   * 
   * 模板页面：web\Application\SLN0002\View\Subject\index.html
   */
  public function index()
  {
    $us = new UserService();

    if ($us->hasPermission(FIdConst::GL_SUBJECT)) {
      $this->initVar();

      $this->assign("title", "会计科目");

      $this->display();
    } else {
      $this->gotoLoginPage("/SLN0002/Subject/index");
    }
  }

  /**
   * 返回所有的公司列表
   * 
   * JS: web\Public\Scripts\PSI\SLN0002\Subject\MainForm.js
   */
  public function companyList()
  {
    if (IS_POST) {
      $us = new UserService();

      if (!$us->hasPermission(FIdConst::GL_SUBJECT)) {
        die("没有权限");
      }

      $service = new SubjectService();
      $this->ajaxReturn($service->companyList());
    }
  }

  /**
   * 某个公司的科目码列表
   * 
   * JS: web\Public\Scripts\PSI\SLN0002\Subject\MainForm.js
   */
  public function subjectList()
  {
    if (IS_POST) {
      $us = new UserService();

      if (!$us->hasPermission(FIdConst::GL_SUBJECT)) {
        die("没有权限");
      }

      $params = [
        "companyId" => I("post.companyId")
      ];

      $service = new SubjectService();
      $this->ajaxReturn($service->subjectList($params));
    }
  }

  /**
   * 初始国家标准科目
   * 
   * JS: web\Public\Scripts\PSI\SLN0002\Subject\MainForm.js
   */
  public function initStandardSubject()
  {
    if (IS_POST) {
      $us = new UserService();

      if (!$us->hasPermission(FIdConst::GL_SUBJECT)) {
        die("没有权限");
      }

      $params = [
        "id" => I("post.id")
      ];

      $service = new SubjectService();
      $this->ajaxReturn($service->initStandardSubject($params));
    }
  }

  /**
   * 新增或编辑科目
   * 
   * JS: web\Public\Scripts\PSI\SLN0002\Subject\EditForm.js
   */
  public function editSubject()
  {
    if (IS_POST) {
      $us = new UserService();

      if (!$us->hasPermission(FIdConst::GL_SUBJECT)) {
        die("没有权限");
      }

      $params = [
        "companyId" => I("post.companyId"),
        "id" => I("post.id"),
        "parentCode" => I("post.parentCode"),
        "code" => I("post.code"),
        "name" => I("post.name"),
        "isLeaf" => I("post.isLeaf"),
        "balanceDir" => I("post.balanceDir"),
      ];

      $service = new SubjectService();
      $this->ajaxReturn($service->editSubject($params));
    }
  }

  /**
   * 上级科目字段 - 查询数据
   * 
   * JS: web\Public\Scripts\PSI\SLN0002\Subject\ParentSubjectField.js
   */
  public function queryDataForParentSubject()
  {
    if (IS_POST) {
      $us = new UserService();

      if (!$us->hasPermission(FIdConst::GL_SUBJECT)) {
        die("没有权限");
      }

      $queryKey = I("post.queryKey");
      $companyId = I("post.companyId");

      $service = new SubjectService();
      $this->ajaxReturn($service->queryDataForParentSubject($queryKey, $companyId));
    }
  }

  /**
   * 某个科目的详情
   * 
   * JS: web\Public\Scripts\PSI\SLN0002\Subject\EditForm.js
   */
  public function subjectInfo()
  {
    if (IS_POST) {
      $us = new UserService();

      if (!$us->hasPermission(FIdConst::GL_SUBJECT)) {
        die("没有权限");
      }

      $params = [
        "id" => I("post.id")
      ];

      $service = new SubjectService();
      $this->ajaxReturn($service->subjectInfo($params));
    }
  }

  /**
   * 删除科目
   * 
   * JS: web\Public\Scripts\PSI\SLN0002\Subject\MainForm.js
   */
  public function deleteSubject()
  {
    if (IS_POST) {
      $us = new UserService();

      if (!$us->hasPermission(FIdConst::GL_SUBJECT)) {
        die("没有权限");
      }

      $params = [
        "id" => I("post.id")
      ];

      $service = new SubjectService();
      $this->ajaxReturn($service->deleteSubject($params));
    }
  }

  /**
   * 初始化科目的标准账样
   * 
   * JS: web\Public\Scripts\PSI\SLN0002\Subject\MainForm.js
   */
  public function initFmt()
  {
    if (IS_POST) {
      $us = new UserService();

      if (!$us->hasPermission(FIdConst::GL_SUBJECT)) {
        die("没有权限");
      }

      $params = [
        // id: 科目id
        "id" => I("post.id"),
        "companyId" => I("post.companyId")
      ];

      $service = new SubjectService();
      $this->ajaxReturn($service->initFmt($params));
    }
  }

  /**
   * 某个科目账样的属性列表
   * 
   * JS: web\Public\Scripts\PSI\SLN0002\Subject\MainForm.js
   */
  public function fmtPropList()
  {
    if (IS_POST) {
      $us = new UserService();

      if (!$us->hasPermission(FIdConst::GL_SUBJECT)) {
        die("没有权限");
      }

      $params = [
        "id" => I("post.id"),
        "companyId" => I("post.companyId")
      ];

      $service = new SubjectService();
      $this->ajaxReturn($service->fmtPropList($params));
    }
  }

  /**
   * 某个科目账样的字段列表
   * 
   * JS: web\Public\Scripts\PSI\SLN0002\Subject\MainForm.js
   */
  public function fmtColsList()
  {
    if (IS_POST) {
      $us = new UserService();

      if (!$us->hasPermission(FIdConst::GL_SUBJECT)) {
        die("没有权限");
      }

      $params = [
        "id" => I("post.id"),
        "companyId" => I("post.companyId")
      ];

      $service = new SubjectService();
      $this->ajaxReturn($service->fmtColsList($params));
    }
  }

  /**
   * 新增或编辑账样字段
   * 
   * JS: web\Public\Scripts\PSI\SLN0002\Subject\FmtColEditForm.js
   */
  public function editFmtCol()
  {
    if (IS_POST) {
      $us = new UserService();

      if (!$us->hasPermission(FIdConst::GL_SUBJECT)) {
        die("没有权限");
      }

      $params = [
        "id" => I("post.id"),
        "companyId" => I("post.companyId"),
        "subjectCode" => I("post.subjectCode"),
        "fieldName" => I("post.fieldName"),
        "fieldCaption" => I("post.fieldCaption"),
        "fieldType" => I("post.fieldType"),
        "voucherInputShowOrder" => I("post.voucherInputShowOrder"),
        "voucherInput" => I("post.voucherInput"),
        "voucherInputXtype" => I("post.voucherInputXtype"),
        "voucherInputColspan" => I("post.voucherInputColspan"),
        "voucherInputWidth" => I("post.voucherInputWidth"),
        "codeTableName" => I("post.codeTableName"),
        "subAccLevel" => I("post.subAccLevel"),
        "colWidth" => I("post.colWidth"),
      ];

      $service = new SubjectService();
      $this->ajaxReturn($service->editFmtCol($params));
    }
  }

  /**
   * 获得某个账样字段的详情
   * 
   * JS: web\Public\Scripts\PSI\SLN0002\Subject\FmtColEditForm.js
   */
  public function fmtColInfo()
  {
    if (IS_POST) {
      $us = new UserService();

      if (!$us->hasPermission(FIdConst::GL_SUBJECT)) {
        die("没有权限");
      }

      $params = [
        "id" => I("post.id")
      ];

      $service = new SubjectService();
      $this->ajaxReturn($service->fmtColInfo($params));
    }
  }

  /**
   * 删除某个账样字段
   * 
   * JS: web\Public\Scripts\PSI\SLN0002\Subject\MainForm.js
   */
  public function deleteFmtCol()
  {
    if (IS_POST) {
      $us = new UserService();

      if (!$us->hasPermission(FIdConst::GL_SUBJECT)) {
        die("没有权限");
      }

      $params = [
        "id" => I("post.id")
      ];

      $service = new SubjectService();
      $this->ajaxReturn($service->deleteFmtCol($params));
    }
  }

  /**
   * 某个账样所有字段 - 设置字段显示次序用
   * 
   * JS: web\Public\Scripts\PSI\SLN0002\Subject\FmtColShowOrderEditForm.js
   */
  public function fmtGridColsList()
  {
    if (IS_POST) {
      $us = new UserService();

      if (!$us->hasPermission(FIdConst::GL_SUBJECT)) {
        die("没有权限");
      }

      $params = [
        "id" => I("post.id")
      ];

      $service = new SubjectService();
      $this->ajaxReturn($service->fmtGridColsList($params));
    }
  }

  /**
   * 编辑账样字段的显示次序
   * 
   * 注意：开始这个方法是用来修改显示次序，但是后来又得加上同时修改列宽
   * 
   * JS: web\Public\Scripts\PSI\SLN0002\Subject\FmtColShowOrderEditForm.js
   */
  public function editFmtColShowOrder()
  {
    if (IS_POST) {
      $us = new UserService();

      if (!$us->hasPermission(FIdConst::GL_SUBJECT)) {
        die("没有权限");
      }

      $params = [
        "id" => I("post.id"), // 科目id
        "idList" => I("post.idList"),
        "widthList" => I("post.widthList"),
      ];

      $service = new SubjectService();
      $this->ajaxReturn($service->editFmtColShowOrder($params));
    }
  }

  /**
   * 选择值来源的引用列 - 查询表
   * 
   * JS：web\Public\Scripts\PSI\SLN0002\Subject\SelectColRefForm.js
   */
  public function queryTablesForColRef()
  {
    if (IS_POST) {
      $us = new UserService();
      if (!$us->hasPermission(FIdConst::GL_SUBJECT)) {
        die("没有权限");
      }

      $params = [
        // 过滤查询键值
        "searchKey" => I("post.searchKey"),
      ];

      $service = new SubjectService();
      $this->ajaxReturn($service->queryTablesForColRef($params));
    }
  }

  /**
   * 科目自定义字段查询数据
   * 
   * JS：web\Public\Scripts\PSI\SLN0002\Subject\SubjectField.js
   */
  public function queryDataForSubjectField()
  {
    if (IS_POST) {
      $us = new UserService();
      // 科目自定义字段是用于凭证录入，所以这里的权限是凭证的有关权限

      if (!$us->hasPermission(FIdConst::VOUCHER_ADD)) {
        die("没有权限");
      }
      if (!$us->hasPermission(FIdConst::VOUCHER_EDIT)) {
        die("没有权限");
      }

      $params = [
        "companyId" => I("post.companyId"),
        "queryKey" => I("post.queryKey"),
      ];

      $ss = new SubjectService();
      $this->ajaxReturn($ss->queryDataForSubjectField($params));
    }
  }
}
