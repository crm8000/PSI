# @author PSI
# @copyright 2015 - present
# @license GPL v3

TRUNCATE TABLE `t_solution`;

insert into t_solution (id, code, name) values
('D5B0A2DE-71DE-11EC-97BD-E86A641ED142', 'SLN0000', '实施配置'),
('E0D4BBE8-71DE-11EC-97BD-E86A641ED142', 'SLN0001', '供应链'),
('ECA855E8-71DE-11EC-97BD-E86A641ED142', 'SLN0002', '财务总账'),
('7F245CE1-7443-11EC-9702-E86A641ED142', 'SLN0003', '人力资源'),
('4A57675E-1E0B-11ED-AF37-E86A641ED142', 'SLN0004', '知识管理'); 
