/**
 * 自定义字段 - 物料字段
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.Goods.GoodsField", {
  extend: "PCL.form.field.Trigger",
  alias: "widget.psi_goodsfield",

  mixins: ["PSI.AFX.Mix.Common"],

  config: {
    parentCmp: null,
    showAddButton: false,
    showModal: false,
    // 显示库存量（与warehouseEdit一起使用）
    showInvCnt: false,
    // 获得关联的仓库，当设置了仓库后，showInvCnt也设置为true的时候，查询Wnd中显示当前仓库的物料库存量
    warehouseEditName: null,

    callbackFunc: null,
    callbackScope: null,
  },

  /**
   * 初始化组件
   * 
   * @override
   */
  initComponent() {
    const me = this;

    me._idValue = null;

    me.enableKeyEvents = true;

    me.callParent(arguments);

    me.on("keydown", (field, e) => {
      if (e.getKey() == e.BACKSPACE) {
        field.setValue(null);
        me.clearIdValue();
        e.preventDefault();
        return false;
      }

      if (e.getKey() != e.ENTER && !e.isSpecialKey(e.getKey())) {
        me.onTriggerClick(e);
      }
    });

    me.on({
      render(p) {
        p.getEl().on("dblclick", () => {
          me.selectText(0, 0);
          me.onTriggerClick();
        });
      },
      single: true
    });
  },

  /**
   * @override
   */
  onTriggerClick(e) {
    const me = this;

    const editWarehouse = PCL.getCmp(me.getWarehouseEditName());
    let warehouseId = null;
    if (editWarehouse) {
      warehouseId = editWarehouse.getIdValue();
    }

    const modelName = me.buildModelName(me, "GoodsField");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "code", "name", "spec", "unitName",
        "taxRate", "invCnt"]
    });

    const store = PCL.create("PCL.data.Store", {
      model: modelName,
      autoLoad: false,
      data: []
    });
    const lookupGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-Lookup",
      columnLines: true,
      border: 1,
      store: store,
      viewConfig: {
        enableTextSelection: true
      },
      columns: [{
        header: "物料编码",
        dataIndex: "code",
        menuDisabled: true,
        width: 100
      }, {
        header: "品名",
        dataIndex: "name",
        menuDisabled: true,
        flex: 1
      }, {
        header: "规格型号",
        dataIndex: "spec",
        menuDisabled: true,
        flex: 1
      }, {
        header: "当前库存",
        dataIndex: "invCnt",
        menuDisabled: true,
        align: "right",
        hidden: !me.getShowInvCnt() || warehouseId == null,
        width: 80
      }, {
        header: "单位",
        dataIndex: "unitName",
        menuDisabled: true,
        width: 60
      }]
    });
    me.lookupGrid = lookupGrid;
    me.lookupGrid.on("itemdblclick", me._onOK, me);

    const buttons = [{
      xtype: "container",
      html: `
        <div class="PSI-lookup-note">
          输入编码、品名拼音字头、规格型号拼音字头可以过滤查询；
          ↑ ↓ 键改变当前选择项 ；回车键返回
        </div>
        `
    }, "->"];
    if (me.getShowAddButton()) {
      buttons.push({
        text: "新建物料",
        cls: "PSI-Lookup-btn",
        handler: me._onAddGoods,
        scope: me
      });
    }

    buttons.push({
      text: "确定",
      cls: "PSI-Lookup-btn",
      handler: me._onOK,
      scope: me
    }, {
      text: "取消",
      cls: "PSI-Lookup-btn",
      handler() {
        wnd.close();
      }
    });

    const wnd = PCL.create("PCL.window.Window", {
      title: "选择 - 物料",
      header: false,
      modal: me.getShowModal(),
      border: 0,
      width: 850,
      height: 300,
      layout: "border",
      onEsc() {
        me.wnd.hide();
        me.focus();
        me.wnd.close();
      },
      items: [{
        region: "center",
        xtype: "panel",
        layout: "fit",
        border: 0,
        items: [lookupGrid]
      }, {
        xtype: "panel",
        region: "south",
        height: 32,
        layout: "fit",
        border: 0,
        items: [{
          xtype: "form",
          layout: "form",
          bodyPadding: 5,
          bodyCls: "PSI-Field",
          items: [{
            id: me.buildId(me, "editGoods"),
            xtype: "textfield",
            labelWidth: 0,
            labelAlign: "right",
            labelSeparator: ""
          }]
        }]
      }],
      buttons: buttons
    });

    wnd.on("close", () => {
      me.focus();
    });
    wnd.on("deactivate", () => {
      wnd.close();
    });

    me.wnd = wnd;

    const editName = PCL.getCmp(me.buildId(me, "editGoods"));
    editName.on("change", () => {
      if (editName._inIME) {
        return;
      }

      const store = me.lookupGrid.getStore();
      me.ajax({
        url: me.URL("SLN0001/Goods/queryData"),
        params: {
          queryKey: editName.getValue(),
          warehouseId: warehouseId
        },
        callback(opt, success, response) {
          store.removeAll();
          if (success) {
            const data = me.decodeJSON(response.responseText);
            store.add(data);
            if (data.length > 0) {
              me.lookupGrid.getSelectionModel().select(0);
              editName.focus();
            }
          } else {
            me.showInfo("网络错误");
          }
        },
        scope: me
      });

    }, me);

    editName.on("specialkey", (field, e) => {
      if (e.getKey() == e.ENTER) {
        me._onOK();
      } else if (e.getKey() == e.UP) {
        const m = me.lookupGrid.getSelectionModel();
        const store = me.lookupGrid.getStore();
        let index = 0;
        for (let i = 0; i < store.getCount(); i++) {
          if (m.isSelected(i)) {
            index = i;
          }
        }
        index--;
        if (index < 0) {
          index = 0;
        }
        m.select(index);
        e.preventDefault();
        editName.focus();
      } else if (e.getKey() == e.DOWN) {
        const m = me.lookupGrid.getSelectionModel();
        const store = me.lookupGrid.getStore();
        let index = 0;
        for (let i = 0; i < store.getCount(); i++) {
          if (m.isSelected(i)) {
            index = i;
          }
        }
        index++;
        if (index > store.getCount() - 1) {
          index = store.getCount() - 1;
        }
        m.select(index);
        e.preventDefault();
        editName.focus();
      }
    }, me);

    me.wnd.on("show", () => {
      me.applyIMEHandler(editName);

      editName.focus();
      editName.fireEvent("change");

      if (me.getShowModal()) {
        me.addMaskClickHandler(me.wnd);
      }
    }, me);
    wnd.showBy(me);
  },

  /**
   * @private
   */
  _onOK() {
    const me = this;
    const grid = me.lookupGrid;
    const item = grid.getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      return;
    }

    const data = item[0].getData();

    me.wnd.close();
    me.focus();
    me.setValue(data.code);
    me.focus();
    me.setIdValue(data.id);

    const parentForm = me.getParentCmp();
    if (parentForm) {
      callbackFunc = me.getCallbackFunc();
      if (callbackFunc) {
        callbackFunc.apply(me.getCallbackScope(), [data]);
      }
    }
  },

  /**
   * @public
   */
  setIdValue(id) {
    this._idValue = id;
  },

  /**
   * @public
   */
  getIdValue() {
    return this._idValue;
  },

  /**
   * @public
   */
  clearIdValue() {
    this.setValue(null);
    this._idValue = null;
  },

  /**
   * @private
   */
  _onAddGoods() {
    const form = PCL.create("PSI.SLN0001.Goods.GoodsEditForm");

    form.show();
  }
});
