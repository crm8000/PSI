/**
 * 主界面基类
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.AFX.Form.MainForm", {
  extend: "PCL.panel.Panel",

  mixins: ["PSI.AFX.Mix.Common"],

  border: 0,

  layout: "border",

  /**
   * 保存查询条件里面的input列表，用于在回车的时候跳转
   * 
   * @protected
   */
  __editorList: [],

  /**
   * @protected
   */
  __onEditSpecialKey(field, e) {
    const me = this;

    if (e.getKey() === e.ENTER) {
      const id = field.getId();
      for (let i = 0; i < me.__editorList.length; i++) {
        const editor = me.__editorList[i];
        if (id === editor.getId()) {
          const edit = me.__editorList[i + 1];
          me.setFocusAndCursorPosToLast(edit);
        }
      }
    }
  },

  /**
   * 最后一个查询input回车后，触发 _onQuery 方法查询数据
   * 
   * @protected
   */
  __onLastEditSpecialKey(field, e) {
    const me = this;

    if (e.getKey() === e.ENTER) {
      // ComboBox的下拉框出现的时候，回车是选中下拉框中相应的条码
      // 这里ComboBox的默认键盘操作行为，所以不触发查询
      if (field.isExpanded) {
        return;
      }

      if (me._onQuery) {
        me._onQuery.apply(me);
      }
    }
  },

  /**
   * 查询Grid控件指定id之前的一个记录的id
   * 
   * 应用场景：删除当前记录后，需要把Grid的当前选中项设置为前一条记录
   * 
   * @param {*} grid Grid控件
   * @param {*} id 当前id
   * @returns 业务对象的前一个id | null
   */
  getPreIndexInGrid(grid, id) {
    const store = grid.getStore();
    const index = store.findExact("id", id) - 1;

    let result = null;
    const preEntity = store.getAt(index);
    if (preEntity) {
      result = preEntity.get("id");
    }

    return result;
  },

  /**
   * 选中Grid中id为指定值的记录
   * 
   * 应用场景举例：Grid数据刷新后，仍需要在界面中保持之前选中的记录仍然是选中状态
   * 
   * @param {*} grid
   * @param {*} id 
   */
  gotoGridRecord(grid, id) {
    const store = grid.getStore();
    if (id) {
      const r = store.findExact("id", id);
      if (r != -1) {
        grid.getSelectionModel().select(r);
      } else {
        grid.getSelectionModel().select(0);
      }
    }
  },
});
