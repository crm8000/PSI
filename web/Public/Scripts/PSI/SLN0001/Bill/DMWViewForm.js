/**
 * 成品委托生产入库单 - 查看界面
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.Bill.DMWViewForm", {
  extend: "PSI.AFX.Form.EditForm",

  config: {
    ref: null
  },

  /**
   * @override
   */
  initComponent() {
    const me = this;

    const fieldProps = {
      xtype: "textfield",
      readOnly: true,
      fieldCls: "PSI-viewBill-field",
      labelSeparator: "",
      labelAlign: "right",
    };

    PCL.apply(me, {
      header: {
        title: "<span style='font-size:160%'>查看成品委托生产入库单</span>",
        height: 40
      },
      maximized: true,
      layout: "border",
      items: [{
        region: "center",
        layout: "fit",
        border: 0,
        bodyPadding: 10,
        items: [me.getGoodsGrid()]
      }, {
        region: "north",
        id: "editForm",
        layout: {
          type: "table",
          columns: 4
        },
        height: 60,
        bodyPadding: 10,
        border: 0,
        items: [{
          labelWidth: 60,
          fieldLabel: "单号",
          value: me.getRef(),
          ...fieldProps,
        }, {
          id: me.buildId(me, "editBizDT"),
          fieldLabel: "业务日期",
          labelWidth: 60,
          ...fieldProps,
        }, {
          id: me.buildId(me, "editFactory"),
          colspan: 2,
          width: 430,
          labelWidth: 60,
          fieldLabel: "工厂",
          ...fieldProps,
        }, {
          id: me.buildId(me, "editWarehouse"),
          labelWidth: 60,
          fieldLabel: "入库仓库",
          ...fieldProps,
        }, {
          id: me.buildId(me, "editBizUser"),
          labelWidth: 60,
          fieldLabel: "业务员",
          ...fieldProps,
        }]
      }],
      listeners: {
        show: {
          fn: me._onWndShow,
          scope: me
        }
      }
    });

    me.callParent(arguments);

    me.editFactory = PCL.getCmp(me.buildId(me, "editFactory"));
    me.editWarehouse = PCL.getCmp(me.buildId(me, "editWarehouse"));
    me.editBizUser = PCL.getCmp(me.buildId(me, "editBizUser"));
    me.editBizDT = PCL.getCmp(me.buildId(me, "editBizDT"));
  },

  /**
   * @private
   */
  _onWndShow: function () {
    const me = this;

    const el = me.getEl() || PCL.getBody();
    el.mask(PSI.Const.LOADING);
    me.ajax({
      url: me.URL("SLN0001/Bill/dmwBillInfo"),
      params: {
        ref: me.getRef()
      },
      callback: function (options, success, response) {
        el.unmask();

        if (success) {
          const data = me.decodeJSON(response.responseText);

          me.editFactory.setValue(data.factoryName);
          me.editWarehouse.setValue(data.warehouseName);
          me.editBizUser.setValue(data.bizUserName);
          me.editBizDT.setValue(data.bizDT);

          const store = me.getGoodsGrid().getStore();
          store.removeAll();
          if (data.items) {
            store.add(data.items);
          }
        }
      }
    });
  },

  /**
   * @private
   */
  getGoodsGrid() {
    const me = this;
    if (me._goodsGrid) {
      return me._goodsGrid;
    }

    const modelName = me.buildModelName(me, "DMWBillDetail_ViewForm");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "goodsId", "goodsCode",
        "goodsName", "goodsSpec", "unitName",
        "goodsCount", "goodsMoney", "goodsPrice",
        "memo"]
    });
    const store = PCL.create("PCL.data.Store", {
      autoLoad: false,
      model: modelName,
      data: []
    });

    me._goodsGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-HL",
      viewConfig: {
        enableTextSelection: true
      },
      columnLines: true,
      columns: [{
        xtype: "rownumberer",
        width: 40,
        text: "序号"
      }, {
        header: "物料编码",
        dataIndex: "goodsCode",
        menuDisabled: true,
        sortable: false
      }, {
        header: "品名",
        dataIndex: "goodsName",
        menuDisabled: true,
        sortable: false,
        width: 200
      }, {
        header: "规格型号",
        dataIndex: "goodsSpec",
        menuDisabled: true,
        sortable: false,
        width: 300
      }, {
        header: "生产入库数量",
        dataIndex: "goodsCount",
        menuDisabled: true,
        sortable: false,
        align: "right",
        width: 100
      }, {
        header: "单位",
        dataIndex: "unitName",
        menuDisabled: true,
        sortable: false,
        width: 60
      }, {
        header: "单价",
        dataIndex: "goodsPrice",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 100
      }, {
        header: "金额",
        dataIndex: "goodsMoney",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 120
      }, {
        header: "备注",
        dataIndex: "memo",
        menuDisabled: true,
        sortable: false,
        width: 200
      }],
      store,
    });

    return me._goodsGrid;
  }
});
