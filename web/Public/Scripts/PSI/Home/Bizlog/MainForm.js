/**
 * 业务日志 - 主界面
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.Home.Bizlog.MainForm", {
  extend: "PSI.AFX.Form.MainForm",

  config: {
    // 是否显示单元测试按钮，0 - 不启用； 1 - 启用
    unitTest: "0"
  },

  /**
   * @override
   */
  initComponent() {
    const me = this;

    PCL.apply(me, {
      tbar: me.getToolbarCmp(),
      items: [{
        id: me.buildId(me, "panelQueryCmp"),
        region: "north",
        height: 65,
        layout: "fit",
        border: 0,
        header: false,
        collapsible: true,
        collapseMode: "mini",
        layout: {
          type: "table",
          columns: 5
        },
        bodyCls: "PSI-Query-Panel",
        items: me.getQueryCmp()
      }, {
        region: "center",
        layout: "border",
        ...PSI.Const.BODY_PADDING,
        border: 0,
        items: [{
          region: "center",
          border: 1,
          items: me.getMainGrid()
        }]
      }]
    });

    me.callParent();

    // AFX
    me.__editorList =
      [PCL.getCmp(me.buildId(me, "editQueryLoginName")),
      PCL.getCmp(me.buildId(me, "editQueryUser")),
      PCL.getCmp(me.buildId(me, "editQueryFromDT")),
      PCL.getCmp(me.buildId(me, "editQueryToDT")),
      PCL.getCmp(me.buildId(me, "editQueryIP")),
      PCL.getCmp(me.buildId(me, "comboCategory")),];

    me._keyMapMain = PCL.create("PCL.util.KeyMap", PCL.getBody(), [{
      key: "Q",
      ctrl: false,
      shift: false,
      alt: true,
      fn() {
        if (PSI.Const.msgBoxShowing) {
          return;
        }
        PCL.getCmp(me.buildId(me, "editQueryLoginName"))?.focus();
      },
      scope: me
    }, {
      key: "H",
      ctrl: false,
      shift: false,
      alt: true,
      fn() {
        if (PSI.Const.msgBoxShowing) {
          return;
        }

        const panel = PCL.getCmp(me.buildId(me, "panelQueryCmp"));
        if (panel.getCollapsed()) {
          panel.expand();
        } else {
          panel.collapse();
        };
      },
      scope: me
    },
    ]);


    me.fetchLogCategory();

    me._onQuery();
  },

  /**
   * @private
   */
  fetchLogCategory() {
    const me = this;

    const r = {
      url: me.URL("Home/Bizlog/getLogCategoryList"),
      callback(options, success, response) {
        const combo = PCL.getCmp(me.buildId(me, "comboCategory"));
        const store = combo.getStore();

        store.removeAll();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          store.add(data);

          if (store.getCount() > 0) {
            combo.setValue(store.getAt(0).get("id"))
          }
        }
      }
    };
    me.ajax(r);
  },

  /**
   * @private
   */
  getToolbarCmp() {
    const me = this;

    const store = me.getMainGrid().getStore();

    const buttons = [{
      iconCls: "PSI-tb-new",
      text: "一键升级数据库",
      ...PSI.Const.BTN_STYLE,
      scope: me,
      handler: me._onUpdateDatabase,
      hidden: true,
    }, {
      iconCls: "PSI-tb-help",
      text: "指南",
      ...PSI.Const.BTN_STYLE,
      handler() {
        me.focus();
        window.open(me.URL("Home/Help/index?t=bizlog"));
      }
    }, "-", {
      iconCls: "PSI-tb-close",
      text: "关闭",
      ...PSI.Const.BTN_STYLE,
      handler() {
        me.focus();
        me.closeWindow();
      }
    }, "->", {
      cls: "PSI-Pagination",
      id: me.buildId(me, "pagingToobar"),
      xtype: "pagingtoolbar",
      border: 0,
      store: store
    }, "-", {
      xtype: "displayfield",
      fieldStyle: "font-size:13px",
      value: "每页显示"
    }, {
      cls: "PSI-Pagination",
      id: "comboCountPerPage",
      xtype: "combobox",
      editable: false,
      width: 60,
      store: PCL.create("PCL.data.ArrayStore", {
        fields: ["text"],
        data: [["20"], ["50"], ["100"], ["300"],
        ["1000"]]
      }),
      value: 20,
      listeners: {
        change: {
          fn() {
            store.pageSize = PCL.getCmp("comboCountPerPage").getValue();
            store.currentPage = 1;
            PCL.getCmp(me.buildId(me, "pagingToobar")).doRefresh();
          },
          scope: me
        }
      }
    }, {
      xtype: "displayfield",
      fieldStyle: "font-size:13px",
      value: "条记录"
    }];

    if (me.getUnitTest() == "1") {
      buttons.push("-", {
        text: "单元测试",
        handler: me._onUnitTest,
        scope: me
      });
    }

    buttons.push("-", " ", {
      cls: "PSI-Shortcut-Cmp",
      labelWidth: 0,
      emptyText: "快捷访问",
      xtype: "psi_mainmenushortcutfield",
      width: 90
    });

    return buttons;
  },

  /**
   * @private
   */
  getQueryCmp() {
    const me = this;

    const modelName = me.buildModelName(me, "LogCategory");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "name"]
    });

    return [{
      xtype: "container",
      height: 26,
      html: `<h2 style='margin-left:20px;margin-top:18px;color:#595959;display:inline-block'>业务日志</h2>
              &nbsp;&nbsp;<span style='color:#8c8c8c'>查询式报表</span>
              <div style='float:right;display:inline-block;margin:10px 0px 0px 20px;border-left:1px solid #e5e6e8;height:40px'>&nbsp;</div>`
    }, {
      id: me.buildId(me, "editQueryLoginName"),
      labelWidth: 100,
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "登录名 <span class='PSI-shortcut-DS'>Alt + Q</span>",
      margin: "5, 0, 0, 0",
      xtype: "textfield",
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      }
    }, {
      id: me.buildId(me, "editQueryUser"),
      labelWidth: 60,
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "姓名",
      margin: "5, 0, 0, 0",
      xtype: "psi_userfield",
      showModal: true,
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      }
    }, {
      id: me.buildId(me, "editQueryFromDT"),
      xtype: "datefield",
      margin: "5, 0, 0, 0",
      format: "Y-m-d",
      labelAlign: "right",
      labelSeparator: "",
      labelWidth: 110,
      fieldLabel: "日志日期（起）",
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      }
    }, {
      id: me.buildId(me, "editQueryToDT"),
      xtype: "datefield",
      margin: "5, 0, 0, 0",
      format: "Y-m-d",
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "日志日期（止）",
      labelWidth: 110,
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      }
    }, { xtype: "container" }, {
      id: me.buildId(me, "editQueryIP"),
      labelWidth: 60,
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "IP",
      margin: "5, 0, 0, 0",
      xtype: "textfield",
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      }
    }, {
      xtype: "combobox",
      id: me.buildId(me, "comboCategory"),
      queryMode: "local",
      editable: false,
      matchFieldWidth: false,
      labelWidth: 60,
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "日志分类",
      margin: "5, 0, 0, 0",
      valueField: "id",
      displayField: "name",
      store: PCL.create("PCL.data.Store", {
        model: modelName,
        autoLoad: false,
        data: []
      }),
      listeners: {
        specialkey: {
          fn: me.__onLastEditSpecialKey,
          scope: me
        }
      }
    }, {
      xtype: "container",
      items: [{
        xtype: "button",
        text: "查询",
        cls: "PSI-Query-btn1",
        width: 100,
        height: 26,
        margin: "5 0 0 10",
        handler: me._onQuery,
        scope: me
      }, {
        xtype: "button",
        text: "清空查询条件",
        cls: "PSI-Query-btn2",
        width: 100,
        height: 26,
        margin: "5, 0, 0, 10",
        handler: me._onClearQuery,
        scope: me
      }]
    }, {
      xtype: "container",
      items: [{
        xtype: "button",
        iconCls: "PSI-button-hide",
        text: "隐藏查询条件栏 <span class='PSI-shortcut-DS'>Alt + H</span>",
        cls: "PSI-Query-btn3",
        width: 170,
        height: 26,
        margin: "5 0 0 10",
        handler() {
          PCL.getCmp(me.buildId(me, "panelQueryCmp")).collapse();
        },
        scope: me
      }]
    }];
  },

  /**
   * @private
   */
  getMainGrid() {
    const me = this;
    if (me._mainGrid) {
      return me._mainGrid;
    }

    const modelName = me.buildModelName(me, "LogModel");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "loginName", "userName", "ip", "ipFrom",
        "content", "dt", "logCategory"],
      idProperty: "id"
    });
    const store = PCL.create("PCL.data.Store", {
      model: modelName,
      pageSize: 20,
      proxy: {
        type: "ajax",
        actionMethods: {
          read: "POST"
        },
        url: me.URL("Home/Bizlog/logList"),
        reader: {
          root: 'logs',
          totalProperty: 'totalCount'
        }
      },
      autoLoad: true
    });
    store.on("beforeload", () => {
      store.proxy.extraParams = me.getQueryParam();
    });
    store.on("load", (e, records, successful) => {
      if (successful) {
        me.gotoMainGridRecord(null);
      }
    });

    me._mainGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-FC",
      viewConfig: {
        enableTextSelection: true
      },
      border: 0,
      loadMask: true,
      columnLines: true,
      columns: {
        defaults: {
          menuDisabled: true,
          sortable: false
        },
        items: [PCL.create("PCL.grid.RowNumberer", {
          text: "#",
          width: 60
        }), {
          text: "登录名",
          dataIndex: "loginName",
          width: 160
        }, {
          text: "姓名",
          dataIndex: "userName",
          width: 100 // 保证类似系统管理员五个字长的用户名可以全部显示
        }, {
          text: "IP",
          dataIndex: "ip",
          width: 120,
          renderer(value) {
            return `<a href='http://www.baidu.com/s?wd=${encodeURIComponent(value)}' target='_blank'>${value}</a>`;
          }
        }, {
          text: "IP所属地",
          dataIndex: "ipFrom",
          width: 200
        }, {
          text: "日志分类",
          dataIndex: "logCategory",
          width: 150
        }, {
          text: "日志内容",
          dataIndex: "content",
          flex: 1,
          renderer(value) {
            return me.toAutoWrap(value);
          }
        }, {
          text: "日志记录时间",
          dataIndex: "dt",
          width: 150
        }]
      },
      store: store,
      listeners: {
        celldblclick: {
          fn: me._onCellDbclick,
          scope: me
        }
      }
    });

    return me._mainGrid;
  },

  /**
   * @private
   */
  gotoMainGridRecord(id) {
    const me = this;
    const grid = me.getMainGrid();
    grid.getSelectionModel().deselectAll();
    const store = grid.getStore();
    if (id) {
      const r = store.findExact("id", id);
      if (r != -1) {
        grid.getSelectionModel().select(r);
      } else {
        grid.getSelectionModel().select(0);
      }
    } else {
      grid.getSelectionModel().select(0);
    }
  },

  /**
   * @private
   */
  _onCellDbclick(ths, td, cellIndex, record, tr, rowIndex, e, eOpts) {
    const me = this;
    if (cellIndex == 1) {
      PCL.getCmp(me.buildId(me, "editQueryLoginName")).setValue(record.get("loginName"));
      me._onQuery();
    } else if (cellIndex == 3) {
      PCL.getCmp(me.buildId(me, "editQueryIP")).setValue(record.get("ip"));
      me._onQuery();
    }
  },

  /**
   * 刷新
   * 
   * @private
   */
  _onQuery() {
    const me = this;

    me.getMainGrid().getStore().currentPage = 1;
    PCL.getCmp(me.buildId(me, "pagingToobar")).doRefresh();
  },

  /**
   * 升级数据库
   * 
   * 本功能已经从前台界面中移除，但是在二次开发中依然可以使用
   * 
   * @private
   */
  _onUpdateDatabase() {
    const me = this;

    me.confirm("请确认是否升级数据库？", () => {
      const el = PCL.getBody();
      el.mask("正在升级数据库，请稍等......");

      me.ajax({
        url: me.URL("Home/Bizlog/updateDatabase"),
        callback(options, success, response) {
          el.unmask();

          if (success) {
            const data = me.decodeJSON(response.responseText);
            if (data.success) {
              me.tip("成功升级数据库", true);
              me._onQuery();
            } else {
              me.showInfo(data.msg);
            }
          } else {
            me.showInfo("网络错误");
          }
        }
      });
    });
  },

  /**
   * @private
   */
  _onUnitTest() {
    const url = PSI.Const.BASE_URL + "UnitTest";
    window.open(url);
  },

  /**
   * @private
   */
  getQueryParam() {
    const me = this;

    const result = {
      loginName: PCL.getCmp(me.buildId(me, "editQueryLoginName")).getValue(),
      userId: PCL.getCmp(me.buildId(me, "editQueryUser")).getIdValue(),
      ip: PCL.getCmp(me.buildId(me, "editQueryIP")).getValue(),
      logCategory: PCL.getCmp(me.buildId(me, "comboCategory")).getValue()
    };

    const fromDT = PCL.getCmp(me.buildId(me, "editQueryFromDT")).getValue();
    if (fromDT) {
      result.fromDT = PCL.Date.format(fromDT, "Y-m-d");
    }

    const toDT = PCL.getCmp(me.buildId(me, "editQueryToDT")).getValue();
    if (toDT) {
      result.toDT = PCL.Date.format(toDT, "Y-m-d");
    }

    return result;
  },

  /**
   * @private
   */
  _onClearQuery() {
    const me = this;

    PCL.getCmp(me.buildId(me, "editQueryLoginName")).setValue(null);
    PCL.getCmp(me.buildId(me, "editQueryUser")).clearIdValue();
    PCL.getCmp(me.buildId(me, "editQueryIP")).setValue(null);
    PCL.getCmp(me.buildId(me, "editQueryFromDT")).setValue(null);
    PCL.getCmp(me.buildId(me, "editQueryToDT")).setValue(null);
    const combo = PCL.getCmp(me.buildId(me, "comboCategory"));
    const store = combo.getStore();
    if (store.getCount() > 0) {
      combo.setValue(store.getAt(0).get("id"))
    }

    me.getMainGrid().getStore().currentPage = 1;

    me._onQuery();
  }
});
