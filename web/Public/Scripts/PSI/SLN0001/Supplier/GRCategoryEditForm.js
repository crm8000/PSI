/**
 * 关联物料 - 添加物料分类界面
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.Supplier.GRCategoryEditForm", {
  extend: "PSI.AFX.Form.EditForm",

  /**
   * 初始化组件
   * 
   * @override
   */
  initComponent() {
    const me = this;

    const buttons = [];
    buttons.push({
      text: "保存并继续新增",
      ...PSI.Const.BTN_STYLE,
      formBind: true,
      handler() {
        me._onOK(true);
      },
      scope: me
    });

    buttons.push({
      text: "保存",
      ...PSI.Const.BTN_STYLE,
      formBind: true,
      iconCls: "PSI-button-ok",
      handler() {
        me._onOK(false);
      },
      scope: me
    }, {
      text: "取消",
      ...PSI.Const.BTN_STYLE,
      handler() {
        me.close();
      },
      scope: me
    });

    const t = "添加物料分类";
    const f = "edit-form-create.png";
    const logoHtml = `
      <img style='float:left;margin:0px 20px 0px 10px;width:48px;height:48px;' 
        src='${PSI.Const.BASE_URL}Public/Images/${f}'></img>
      <h2 style='color:#196d83;margin-top:0px;'>
        ${t}
      </h2>
      <p style='color:#196d83'>标记 <span style='color:red;font-weight:bold'>*</span>的是必须录入数据的字段</p>`;

    PCL.apply(me, {
      header: {
        title: me.formatTitle(PSI.Const.PROD_NAME),
        height: 40
      },
      width: 460,
      height: 220,
      layout: "border",
      items: [{
        region: "north",
        border: 0,
        height: 70,
        html: logoHtml
      }, {
        region: "center",
        border: 0,
        id: me.buildId(me, "editForm"),
        xtype: "form",
        layout: {
          type: "table",
          columns: 1
        },
        height: "100%",
        bodyPadding: 5,
        defaultType: 'textfield',
        fieldDefaults: {
          labelWidth: 70,
          labelAlign: "right",
          labelSeparator: "",
          msgTarget: 'side'
        },
        items: [{
          id: me.buildId(me, "editCategory"),
          xtype: "psi_goodscategoryfield",
          fieldLabel: "物料分类",
          allowBlank: false,
          blankText: "没有输入物料分类",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          width: 410,
          listeners: {
            specialkey: {
              fn: me._onLastEditSpecialKey,
              scope: me
            }
          }
        }, {
          id: me.buildId(me, "editCategoryId"),
          name: "categoryId",
          xtype: "hidden"
        }, {
          name: "id",
          xtype: "hidden",
          value: me.getEntity().get("id")
        }],
        buttons: buttons
      }],
      listeners: {
        show: {
          fn: me._onWndShow,
          scope: me
        },
        close: {
          fn: me._onWndClose,
          scope: me
        }
      }
    });

    me.callParent(arguments);

    me.editForm = PCL.getCmp(me.buildId(me, "editForm"));
    me.editCategory = PCL.getCmp(me.buildId(me, "editCategory"));
    me.editCategoryId = PCL.getCmp(me.buildId(me, "editCategoryId"));
  },

  /**
   * @private
   */
  _onWndShow() {
    const me = this;

    const parentForm = me.getParentForm();
    if (parentForm) {
      parentForm._keyMapMain.disable();
    }

    // AFX
    PCL.get(window).on('beforeunload', me.__onWindowBeforeUnload);

    me.editCategory.focus();
  },

  /**
   * @private
   */
  _onOK(thenAdd) {
    const me = this;

    const categoryId = me.editCategory.getIdValue();
    me.editCategoryId.setValue(categoryId);

    const f = me.editForm;
    const el = f.getEl();
    el?.mask(PSI.Const.SAVING);
    f.submit({
      url: me.URL("SLN0001/Supplier/addGRCategory"),
      method: "POST",
      success(form, action) {
        el?.unmask();

        me.tip("数据保存成功", !thenAdd);
        me.focus();

        if (thenAdd) {
          me.clearEdit();
        } else {
          me.close();
        }
      },
      failure(form, action) {
        el?.unmask();
        me.showInfo(action.result.msg);
      }
    });
  },

  /**
   * @private
   */
  _onLastEditSpecialKey(field, e) {
    const me = this;

    if (e.getKey() == e.ENTER) {
      const f = me.editForm;
      if (f.getForm().isValid()) {
        me._onOK();
      }
    }
  },

  /**
   * @private
   */
  clearEdit() {
    const me = this;

    me.editCategory.setIdValue(null);
    me.editCategory.setValue(null);

    me.editCatgory.focus();
  },

  /**
   * @private
   */
  _onWndClose() {
    const me = this;

    const parentForm = me.getParentForm();
    if (parentForm) {
      parentForm._keyMapMain.enable();
    }

    // AFX
    PCL.get(window).un('beforeunload', me.__onWindowBeforeUnload);

    if (me.getParentForm()) {
      me.getParentForm().refreshGRCategoryGrid.apply(me.getParentForm(), []);
    }
  }
});
