/**
 * 物料计量单位 - 新增或编辑界面
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.Goods.UnitEditForm", {
  extend: "PSI.AFX.Form.EditForm",

  /**
   * 初始化组件
   * 
   * @override
   */
  initComponent() {
    const me = this;
    const entity = me.getEntity();
    me.adding = entity == null;

    const buttons = [];
    if (!entity) {
      buttons.push({
        text: "保存并继续新建",
        ...PSI.Const.BTN_STYLE,
        formBind: true,
        handler() {
          me._onOK(true);
        },
        scope: me
      });
    }

    buttons.push({
      text: "保存",
      ...PSI.Const.BTN_STYLE,
      formBind: true,
      iconCls: "PSI-button-ok",
      handler() {
        me._onOK(false);
      },
      scope: me
    }, {
      text: entity == null ? "关闭" : "取消",
      ...PSI.Const.BTN_STYLE,
      handler() {
        me.close();
      },
      scope: me
    });

    const t = entity == null ? "新建物料计量单位" : "编辑物料计量单位";
    const logoHtml = me.genLogoHtml(entity, t);

    PCL.apply(me, {
      header: {
        title: me.formatTitle(PSI.Const.PROD_NAME),
        height: 40
      },
      width: 650,
      height: 290,
      layout: "border",
      items: [{
        region: "north",
        border: 0,
        height: 70,
        html: logoHtml
      }, {
        region: "center",
        border: 0,
        id: "PSI_Goods_UnitEditForm_editForm",
        xtype: "form",
        layout: {
          type: "table",
          columns: 2,
          tableAttrs: PSI.Const.TABLE_LAYOUT,
        },
        height: "100%",
        bodyPadding: 5,
        defaultType: 'textfield',
        fieldDefaults: {
          labelWidth: 65,
          labelAlign: "right",
          labelSeparator: "",
          msgTarget: 'side',
          width: 295,
          margin: "5"
        },
        items: [{
          xtype: "hidden",
          name: "id",
          value: entity == null ? null : entity.get("id")
        }, {
          id: "PSI_Goods_UnitEditForm_editName",
          fieldLabel: "计量单位",
          allowBlank: false,
          blankText: "没有输入计量单位",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          name: "name",
          value: entity == null ? null : entity.get("name"),
          listeners: {
            specialkey: {
              fn: me._onEditNameSpecialKey,
              scope: me
            }
          }
        }, {
          id: "PSI_Goods_UnitEditForm_editCode",
          fieldLabel: "编码",
          allowBlank: false,
          blankText: "没有输入编码",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          name: "code",
          value: entity == null ? null : entity.get("code"),
          listeners: {
            specialkey: {
              fn: me._onEditCodeSpecialKey,
              scope: me
            }
          }
        }, {
          xtype: "combo",
          queryMode: "local",
          editable: false,
          valueField: "id",
          fieldLabel: "状态",
          allowBlank: false,
          blankText: "没有输入状态",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          name: "recordStatus",
          store: PCL.create("PCL.data.ArrayStore", {
            fields: ["id", "text"],
            data: [[1, "启用"], [2, "停用"]]
          }),
          value: entity == null
            ? 1
            : parseInt(entity.get("recordStatus"))
        }],
        buttons,
      }],
      listeners: {
        show: {
          fn: me._onWndShow,
          scope: me
        },
        close: {
          fn: me._onWndClose,
          scope: me
        }
      }
    });

    me.callParent(arguments);

    me.editForm = PCL.getCmp("PSI_Goods_UnitEditForm_editForm");
    me.editName = PCL.getCmp("PSI_Goods_UnitEditForm_editName");
    me.editCode = PCL.getCmp("PSI_Goods_UnitEditForm_editCode");
  },

  /**
   * @private
   */
  _onOK(thenAdd) {
    const me = this;
    const f = me.editForm;
    const el = f.getEl();
    el?.mask(PSI.Const.SAVING);
    f.submit({
      url: me.URL("SLN0001/Goods/editUnit"),
      method: "POST",
      success(form, action) {
        el?.unmask();
        me._lastId = action.result.id;
        me.tip("数据保存成功", !thenAdd);
        me.focus();
        if (thenAdd) {
          me.clearEditor();
          me.refreshParentFormMainGrid();
        } else {
          me.close();
        }
      },
      failure(form, action) {
        el?.unmask();
        me.showInfo(action.result.msg, () => {
          me.editName.focus();
        });
      }
    });
  },

  /**
   * @private
   */
  clearEditor() {
    const me = this;

    const editName = me.editName;
    editName.focus();
    editName.setValue(null);
    editName.clearInvalid();

    me.editCode.setValue(null);
  },

  /**
   * @private
   */
  _onEditNameSpecialKey(field, e) {
    const me = this;

    if (e.getKey() == e.ENTER) {
      me.setFocusAndCursorPosToLast(me.editCode);
    }
  },

  /**
   * @private
   */
  _onEditCodeSpecialKey(field, e) {
    const me = this;

    if (e.getKey() == e.ENTER) {
      const f = me.editForm;
      if (f.getForm().isValid()) {
        me._onOK(me.adding);
      }
    }
  },

  /**
   * @private
   */
  _onWndClose() {
    const me = this;

    PCL.get(window).un('beforeunload', me.__onWindowBeforeUnload);

    if (me._lastId) {
      const parentForm = me.getParentForm();
      if (parentForm) {
        parentForm.refreshGrid.apply(parentForm, [me._lastId]);
      }
    }
  },

  /**
   * @private
   */
  _onWndShow() {
    const me = this;

    PCL.get(window).on('beforeunload', me.__onWindowBeforeUnload);

    me.setFocusAndCursorPosToLast(me.editName);
  },

  /**
   * 刷新父窗体上Grid的数据
   * 
   * 连续新增计量单位后，这样不用关闭当前窗体就能看到数据
   * 
   * @private
   */
  refreshParentFormMainGrid() {
    const me = this;
    const form = me.getParentForm();
    if (form) {
      form.refreshGrid.apply(form, []);
    }
  }
});
