TRUNCATE TABLE `t_portal_item`;
TRUNCATE TABLE `t_portal_item_category`;

# 看板分类
INSERT INTO `t_portal_item_category` (`id`, `code`, `name`, `show_order`) VALUES
('4F6D4994-1AE0-11EE-8E8E-E86A641ED142', '01', '销售看板', 1),
('86DBDBC2-1AE0-11EE-8E8E-E86A641ED142', '02', '采购看板', 2),
('9041C60A-1AE0-11EE-8E8E-E86A641ED142', '03', '库存看板', 3),
('99ADC0DA-1AE0-11EE-8E8E-E86A641ED142', '04', '资金看板', 4);


# 销售看板
INSERT INTO `t_portal_item` (`id`, `category_id`, `code`, `name`, `show_order`, `data_url`, `icon_class`, `fid`, `js_class_name`, `col_span`) VALUES
('04BDB454-1AE2-11EE-8E8E-E86A641ED142', '4F6D4994-1AE0-11EE-8E8E-E86A641ED142', '0101', '近六个月销售额图表展示', 1, '', 'PSI-portal-sale', '2011-01', 'PSI.SLN0001.Portal.SaleChatPortalItem', 1),
('0E802C87-1AE2-11EE-8E8E-E86A641ED142', '4F6D4994-1AE0-11EE-8E8E-E86A641ED142', '0102', '近六个月销售额列表展示', 2, '', 'PSI-portal-sale', '2011-01', 'PSI.SLN0001.Portal.SaleGridPortalItem', 1);

# 采购看板
INSERT INTO `t_portal_item` (`id`, `category_id`, `code`, `name`, `show_order`, `data_url`, `icon_class`, `fid`, `js_class_name`, `col_span`) VALUES
('5EDB0C1F-1B9E-11EE-8942-E86A641ED142', '86DBDBC2-1AE0-11EE-8E8E-E86A641ED142', '0201', '近六个月采购额图表展示', 1, '', 'PSI-portal-purchase', '2011-03', 'PSI.SLN0001.Portal.PurchaseChatPortalItem', 1),
('6B00AD5F-1B9E-11EE-8942-E86A641ED142', '86DBDBC2-1AE0-11EE-8E8E-E86A641ED142', '0202', '近六个月采购额列表展示', 2, '', 'PSI-portal-purchase', '2011-03', 'PSI.SLN0001.Portal.PurchaseGridPortalItem', 1);

# 库存看板
INSERT INTO `t_portal_item` (`id`, `category_id`, `code`, `name`, `show_order`, `data_url`, `icon_class`, `fid`, `js_class_name`, `col_span`) VALUES
('331B5083-1B9F-11EE-8942-E86A641ED142', '9041C60A-1AE0-11EE-8E8E-E86A641ED142', '0301', '库存预警', 1, '', 'PSI-portal-inventory', '2011-02', 'PSI.SLN0001.Portal.InventoryPortalItem', 2);

# 资金看板
INSERT INTO `t_portal_item` (`id`, `category_id`, `code`, `name`, `show_order`, `data_url`, `icon_class`, `fid`, `js_class_name`, `col_span`) VALUES
('DC897B41-1B9F-11EE-8942-E86A641ED142', '99ADC0DA-1AE0-11EE-8E8E-E86A641ED142', '0401', '账龄分析', 1, '', 'PSI-portal-money', '2011-04', 'PSI.SLN0001.Portal.MoneyPortalItem', 2);
