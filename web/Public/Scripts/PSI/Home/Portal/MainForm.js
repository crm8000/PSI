/**
 * Portal 主界面
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.Home.Portal.MainForm", {
  extend: "PSI.AFX.Form.MainForm",

  config: {
  },

  initComponent() {
    const me = this;

    PCL.apply(me, {
      layout: "fit",
      margin: "0 10 10 10",
      items: [{
        xtype: "tabpanel",
        cls: "PSI-Portal",
        id: me.buildId(me, "tabPanel"),
        border: 0,
        bodyStyle: { borderWidth: 0 },
        items: []
      }],
    });

    me.callParent(arguments);

    me._initUI();
  },

  /**
   * @private
   * 
   * 初始化界面
   */
  _initUI() {
    const me = this;

    const r = {
      url: me.URL("Home/Portal/queryPortalCategory"),
      params: {
      },
      callback(options, success, response) {
        if (success) {
          const data = me.decodeJSON(response.responseText);
          me._createPortalCategory(data);
        }
      }
    };

    me.ajax(r);
  },

  /**
   * @private
   * 
   * 创建Portal分类
   * 
   * @param {Array} data portal分类构成的数组
   */
  _createPortalCategory(data) {
    const me = this;
    const tabPanel = PCL.getCmp(me.buildId(me, "tabPanel"));

    tabPanel.add({
      title: "欢迎",
      border: 0,
      iconCls: "PSI-fid-9997",
      html: `
            <h1 style="margin-left:20px;margin-top: 40px;color:#768fa0;padding-left: 5px;border-left:6px solid #e6e6e6;">
              管理就是界定企业的使命，并激励和组织人力资源去实现这个使命。
              <br/>
              界定使命是企业家的任务，而激励与组织人力资源是领导力的范畴，二者的结合就是管理。
            </h1>
            <div style="text-align: right;margin-right: 30px;color:#768fa0;">彼得·德鲁克</div>
            `
    });
    if (data.length > 0) {
      data.forEach(it => {
        const items = [];

        const itemCnt = it.portalItemList.length;
        let currentIndex = 0;
        for (let i = 0; i < itemCnt; i++) {
          if (i < currentIndex) {
            continue;
          }

          let item = it.portalItemList[currentIndex];
          let colSpan = parseInt(item.colSpan);
          if (isNaN(colSpan)) {
            colSpan = 1;
          }


          if (colSpan == 1) {
            const itemList = [];

            let className = "PSI.Home.Portal.ErrorInfoPortalItem";
            if (PCL.isCreated(item.jsClassName)) {
              className = item.jsClassName;
            }

            itemList.push(PCL.create(className, {
              portalItemName: item.name,
              portalItemIconClass: item.iconClass,
            }));

            item = it.portalItemList[currentIndex + 1];
            className = "PSI.Home.Portal.ErrorInfoPortalItem";
            if (PCL.isCreated(item.jsClassName)) {
              className = item.jsClassName;
            }

            itemList.push(PCL.create(className, {
              portalItemName: item.name,
              portalItemIconClass: item.iconClass,
            }));

            items.push({
              width: "100%",
              layout: "hbox",
              border: 0,
              items: itemList,
            })

            currentIndex += 2;
          } else {
            // colSpan == 2

            let className = "PSI.Home.Portal.ErrorInfoPortalItem";
            if (PCL.isCreated(item.jsClassName)) {
              className = item.jsClassName;
            }

            items.push({
              width: "100%",
              layout: "hbox",
              border: 0,
              items: PCL.create(className, {
                portalItemName: item.name,
                portalItemIconClass: item.iconClass,
              })
            })

            currentIndex++;
          }
        } // end of for

        tabPanel.add({
          title: it.caption,
          border: 0,
          layout: "border",
          items: [
            {
              region: "north",
              height: 40,
              border: 0,
              html: `<div style='display:inline-block;border-left:6px solid #e6e6e6;width:6px;height:22px;margin:10px;float:left'></div>
                      <h2 style='display:inline-block;color:#768fa0;margin:9px 0px 0px 0px;'>
                      ${it.caption}
                    </h2>`
            },
            {
              region: "center",
              layout: "vbox",
              border: 0,
              autoScroll: true,
              items
            },
          ],
        });
      });

      // 把默认页设置到第一个Portal分类上
      tabPanel.setActiveTab(1);
    } else {
      // 把默认页设置到欢迎页
      tabPanel.setActiveTab(0);
    }
  }
});
