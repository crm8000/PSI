# 独立模块

---

在<strong>{{$theme.productName}}</strong>开发完成之前，二次开发的主要形式是手工编写代码。

手工编码有两种方式和<strong>{{$theme.productName}}</strong>集成：一种是在现有的代码中增加新的模块；另一种就是通过数据库集成，即另外编写一个程序（可以使用最适合的技术），共同操作同一个数据库。

遇到应用平台不能完成的特殊需求，我们更推荐使用数据库集成的方式来做二次开发。

- [如何新增一个模块](100.md)
