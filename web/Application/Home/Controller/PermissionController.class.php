<?php

namespace Home\Controller;

use Home\Common\FIdConst;
use Home\Service\PermissionService;
use Home\Service\UserService;

/**
 * 权限Controller
 *
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
class PermissionController extends PSIBaseController
{

  /**
   * 权限管理 - 主页面
   * 
   * web\Application\Home\View\Permission\index.html
   */
  public function index()
  {
    $us = new UserService();

    if ($us->hasPermission(FIdConst::PERMISSION_MANAGEMENT)) {
      $this->initVar();

      $this->assign("title", "权限管理");

      // 按钮权限：新增角色
      $this->assign("pAdd", $us->hasPermission(FIdConst::PERMISSION_MANAGEMENT_ADD) ? 1 : 0);
      // 按钮权限：编辑角色
      $this->assign("pEdit", $us->hasPermission(FIdConst::PERMISSION_MANAGEMENT_EDIT) ? 1 : 0);
      // 按钮权限：删除角色
      $this->assign("pDelete", $us->hasPermission(FIdConst::PERMISSION_MANAGEMENT_DELETE) ? 1 : 0);

      $this->display();
    } else {
      $this->gotoLoginPage("/Home/Permission/index");
    }
  }

  /**
   * 获得所有的角色列表
   * 
   * JS: web\Public\Scripts\PSI\Home\Permission\MainForm.js
   */
  public function roleList()
  {
    if (IS_POST) {
      $us = new UserService();
      if (!$us->hasPermission(FIdConst::PERMISSION_MANAGEMENT)) {
        die("没有权限");
      }

      $params = [
        "loginName" => I("post.queryLoginName"),
        "name" => I("post.queryName")
      ];
      $ps = new PermissionService();

      $this->ajaxReturn($ps->roleList($params));
    }
  }

  /**
   * 获得某个角色的所有权限
   * 
   * JS: web\Public\Scripts\PSI\Home\Permission\MainForm.js
   */
  public function permissionList()
  {
    if (IS_POST) {
      $us = new UserService();
      if (!$us->hasPermission(FIdConst::PERMISSION_MANAGEMENT)) {
        die("没有权限");
      }

      $ps = new PermissionService();
      $roleId = I("post.roleId");

      $data = $ps->permissionList($roleId);

      $this->ajaxReturn($data);
    }
  }

  /**
   * 获得某个角色的所有用户
   * 
   * JS: web\Public\Scripts\PSI\Home\Permission\MainForm.js
   */
  public function userList()
  {
    if (IS_POST) {
      $us = new UserService();
      if (!$us->hasPermission(FIdConst::PERMISSION_MANAGEMENT)) {
        die("没有权限");
      }

      $ps = new PermissionService();
      $roleId = I("post.roleId");

      $data = $ps->userList($roleId);

      $this->ajaxReturn($data);
    }
  }

  /**
   * 新增或编辑角色
   * 
   * JS: web\Public\Scripts\PSI\Home\Permission\EditForm.js
   */
  public function editRole()
  {
    if (IS_POST) {
      // 检查权限
      $us = new UserService();
      if (I("post.id")) {
        // 编辑角色
        if (!$us->hasPermission(FIdConst::PERMISSION_MANAGEMENT_EDIT)) {
          die("没有权限");
        }
      } else {
        // 新增角色
        if (!$us->hasPermission(FIdConst::PERMISSION_MANAGEMENT_ADD)) {
          die("没有权限");
        }
      }

      $ps = new PermissionService();
      $params = [
        "id" => I("post.id"),
        "name" => I("post.name"),
        "code" => strtoupper(I("post.code")),
        "permissionIdList" => I("post.permissionIdList"),
        "dataOrgList" => I("post.dataOrgList"),
        "userIdList" => I("post.userIdList")
      ];

      $this->ajaxReturn($ps->editRole($params));
    }
  }

  /**
   * 选择权限
   * 
   * TODO: 本方法不再使用，需要从代码中清除掉
   */
  public function selectPermission()
  {
    if (IS_POST) {
      $us = new UserService();
      if (!$us->hasPermission(FIdConst::PERMISSION_MANAGEMENT)) {
        die("没有权限");
      }

      $idList = I("post.idList");

      $ps = new PermissionService();
      $data = $ps->selectPermission($idList);

      $this->ajaxReturn($data);
    }
  }

  /**
   * 选择用户
   * 
   * JS: web\Public\Scripts\PSI\Home\Permission\SelectUserForm.js
   */
  public function selectUsers()
  {
    if (IS_POST) {
      $us = new UserService();
      if (!$us->hasPermission(FIdConst::PERMISSION_MANAGEMENT)) {
        die("没有权限");
      }

      $idList = I("post.idList");
      $name = I("post.name");

      $this->ajaxReturn((new PermissionService())->selectUsers($idList, $name));
    }
  }

  /**
   * 删除角色
   * 
   * JS: web\Public\Scripts\PSI\Home\Permission\MainForm.js
   */
  public function deleteRole()
  {
    if (IS_POST) {
      // 检查权限
      $us = new UserService();
      if (!$us->hasPermission(FIdConst::PERMISSION_MANAGEMENT_DELETE)) {
        die("没有权限");
      }

      $id = I("post.id");

      $ps = new PermissionService();
      $result = $ps->deleteRole($id);

      $this->ajaxReturn($result);
    }
  }

  /**
   * 获得角色的某个权限的数据域列表
   * 
   * TODO: 本方法不再使用，需要从代码中清除掉
   */
  public function dataOrgList()
  {
    if (IS_POST) {
      $us = new UserService();
      if (!$us->hasPermission(FIdConst::PERMISSION_MANAGEMENT)) {
        die("没有权限");
      }

      $ps = new PermissionService();
      $params = [
        "roleId" => I("post.roleId"),
        "permissionId" => I("post.permissionId")
      ];

      $this->ajaxReturn($ps->dataOrgList($params));
    }
  }

  /**
   * 选择数据域
   * 
   * JS: web\Public\Scripts\PSI\Home\Permission\SelectDataOrgForm.js
   */
  public function selectDataOrg()
  {
    if (IS_POST) {
      $us = new UserService();
      if (!$us->hasPermission(FIdConst::PERMISSION_MANAGEMENT)) {
        die("没有权限");
      }

      $ps = new PermissionService();

      $this->ajaxReturn($ps->selectDataOrg());
    }
  }

  /**
   * 获得权限分类
   * 
   * JS: web\Public\Scripts\PSI\Home\Permission\SelectPermissionForm.js
   */
  public function permissionCategory()
  {
    if (IS_POST) {
      $us = new UserService();
      if (!$us->hasPermission(FIdConst::PERMISSION_MANAGEMENT)) {
        die("没有权限");
      }

      $ps = new PermissionService();

      $params = [
        "queryKey" => I("post.queryKey"),
      ];

      $this->ajaxReturn($ps->permissionCategory($params));
    }
  }

  /**
   * 按权限分类查询权限项
   * 
   * JS: web\Public\Scripts\PSI\Home\Permission\SelectPermissionForm.js
   */
  public function permissionByCategory()
  {
    if (IS_POST) {
      $us = new UserService();
      if (!$us->hasPermission(FIdConst::PERMISSION_MANAGEMENT)) {
        die("没有权限");
      }

      $params = [
        "category" => I("post.category")
      ];

      $ps = new PermissionService();
      $this->ajaxReturn($ps->permissionByCategory($params));
    }
  }

  /**
   * 把【用户管理-模块权限】的数据域设置为全域
   * 
   * JS: web\Public\Scripts\PSI\Home\Permission\MainForm.js
   */
  public function applyUserManagementFullDataOrg()
  {
    if (IS_POST) {
      $us = new UserService();
      if (!$us->hasPermission(FIdConst::PERMISSION_MANAGEMENT_ADD)) {
        die("没有权限");
      }

      $params = [
        // 角色id
        "id" => I("post.id"),
      ];

      $ps = new PermissionService();
      $this->ajaxReturn($ps->applyUserManagementFullDataOrg($params));
    }
  }
}
