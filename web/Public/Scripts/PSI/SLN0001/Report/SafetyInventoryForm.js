/**
 * 安全库存明细表
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.Report.SafetyInventoryForm", {
  extend: "PSI.AFX.Form.MainForm",

  initComponent: function () {
    var me = this;

    var store = me.getMainGrid().getStore();

    PCL.apply(me, {
      tbar: [{
        text: "查询",
        ...PSI.Const.BTN_STYLE,
        iconCls: "PSI-tb-query",
        handler: me.onQuery,
        scope: me
      }, "-", {
        text: "打印",
        ...PSI.Const.BTN_STYLE,
        menu: [{
          text: "打印预览",
          iconCls: "PSI-button-print-preview",
          scope: me,
          handler: me.onPrintPreview
        }, "-", {
          text: "直接打印",
          iconCls: "PSI-button-print",
          scope: me,
          handler: me.onPrint
        }]
      }, {
        text: "导出",
        ...PSI.Const.BTN_STYLE,
        menu: [{
          text: "导出PDF",
          iconCls: "PSI-button-pdf",
          scope: me,
          handler: me.onPDF
        }, "-", {
          text: "导出Excel",
          iconCls: "PSI-button-excel",
          scope: me,
          handler: me.onExcel
        }]
      }, "-", {
        iconCls: "PSI-tb-close",
        text: "关闭",
        ...PSI.Const.BTN_STYLE,
        handler: function () {
          me.closeWindow();
        }
      }, "->", {
        id: "pagingToobar",
        cls: "PSI-toolbox",
        xtype: "pagingtoolbar",
        border: 0,
        store: store
      }, "-", {
        xtype: "displayfield",
        value: "每页显示"
      }, {
        id: "comboCountPerPage",
        cls: "PSI-toolbox",
        xtype: "combobox",
        editable: false,
        width: 60,
        store: PCL.create("PCL.data.ArrayStore", {
          fields: ["text"],
          data: [["20"], ["50"], ["100"],
          ["300"], ["1000"]]
        }),
        value: 100,
        listeners: {
          change: {
            fn: function () {
              store.pageSize = PCL.getCmp("comboCountPerPage").getValue();
              store.currentPage = 1;
              PCL.getCmp("pagingToobar").doRefresh();
            },
            scope: me
          }
        }
      }, {
        xtype: "displayfield",
        value: "条记录"
      }].concat(me.getShortcutCmp()),
      items: [{
        region: "north",
        height: 55,
        border: 0,
        margin: 0,
        bodyPadding: "0 20 0 15",
        html: `<h2 style='color:#595959;display:inline-block'>安全库存明细表</h2>
                &nbsp;&nbsp;<span style='color:#8c8c8c'>查询式报表</span>`,
      }, {
        region: "center",
        layout: "fit",
        border: 0,
        bodyPadding: "0 20 5 15",
        items: [me.getMainGrid()]
      }]
    });

    me.callParent(arguments);
  },

  /**
   * 快捷访问
   * 
   * @private
   */
  getShortcutCmp() {
    return ["|", " ",
      {
        cls: "PSI-Shortcut-Cmp",
        labelWidth: 0,
        emptyText: "快捷访问",
        xtype: "psi_mainmenushortcutfield",
        width: 90
      }
    ];
  },

  getMainGrid: function () {
    var me = this;
    if (me.__mainGrid) {
      return me.__mainGrid;
    }

    var modelName = "PSIReportSafetyInventory";
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["warehouseCode", "warehouseName", "siCount",
        "invCount", "goodsCode", "goodsName", "goodsSpec",
        "unitName", "delta"]
    });
    var store = PCL.create("PCL.data.Store", {
      autoLoad: false,
      model: modelName,
      data: [],
      pageSize: 20,
      proxy: {
        type: "ajax",
        actionMethods: {
          read: "POST"
        },
        url: PSI.Const.BASE_URL
          + "SLN0001/Report/safetyInventoryQueryData",
        reader: {
          root: 'dataList',
          totalProperty: 'totalCount'
        }
      }
    });

    me.__mainGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-FC",
      viewConfig: {
        enableTextSelection: true
      },
      border: 1,
      columnLines: true,
      columns: [{
        xtype: "rownumberer"
      }, {
        header: "仓库编码",
        dataIndex: "warehouseCode",
        menuDisabled: true,
        sortable: false
      }, {
        header: "仓库",
        dataIndex: "warehouseName",
        menuDisabled: true,
        sortable: false,
        width: 200
      }, {
        header: "物料编码",
        dataIndex: "goodsCode",
        menuDisabled: true,
        sortable: false
      }, {
        header: "品名",
        dataIndex: "goodsName",
        menuDisabled: true,
        sortable: false,
        width: 200
      }, {
        header: "规格型号",
        dataIndex: "goodsSpec",
        menuDisabled: true,
        sortable: false,
        width: 160
      }, {
        header: "安全库存",
        dataIndex: "siCount",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        format: "0"
      }, {
        header: "当前库存",
        dataIndex: "invCount",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        format: "0"
      }, {
        header: "存货缺口",
        dataIndex: "delta",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        format: "0",
        renderer: function (value) {
          return "<span style='color:red'>" + value
            + "</span>";
        }
      }, {
        header: "计量单位",
        dataIndex: "unitName",
        menuDisabled: true,
        sortable: false
      }],
      store: store
    });

    return me.__mainGrid;
  },

  onQuery: function () {
    const me = this;

    me.focus();
    me.refreshMainGrid();
  },

  refreshMainGrid: function (id) {
    PCL.getCmp("pagingToobar").doRefresh();
  },

  onPrintPreview: function () {
    if (PSI.Const.ENABLE_LODOP != "1") {
      PSI.MsgBox.showInfo("请先在业务设置模块中启用Lodop打印");
      return;
    }

    var lodop = getLodop();
    if (!lodop) {
      PSI.MsgBox.showInfo("没有安装Lodop控件，无法打印");
      return;
    }

    var me = this;

    var el = PCL.getBody();
    el.mask("数据加载中...");
    var r = {
      url: PSI.Const.BASE_URL
        + "SLN0001/Report/genSafetyInventoryPrintPage",
      params: {
        limit: -1
      },
      callback: function (options, success, response) {
        el.unmask();

        if (success) {
          var data = response.responseText;
          me.previewReport("安全库存明细表", data);
        }
      }
    };
    me.ajax(r);
  },

  PRINT_PAGE_WIDTH: "200mm",
  PRINT_PAGE_HEIGHT: "95mm",

  previewReport: function (ref, data) {
    var me = this;

    var lodop = getLodop();
    if (!lodop) {
      PSI.MsgBox.showInfo("Lodop打印控件没有正确安装");
      return;
    }

    lodop.PRINT_INIT(ref);
    lodop.SET_PRINT_PAGESIZE(1, me.PRINT_PAGE_WIDTH, me.PRINT_PAGE_HEIGHT,
      "");
    lodop.ADD_PRINT_HTM("0mm", "0mm", "100%", "100%", data);
    var result = lodop.PREVIEW("_blank");
  },

  onPrint: function () {
    if (PSI.Const.ENABLE_LODOP != "1") {
      PSI.MsgBox.showInfo("请先在业务设置模块中启用Lodop打印");
      return;
    }

    var lodop = getLodop();
    if (!lodop) {
      PSI.MsgBox.showInfo("没有安装Lodop控件，无法打印");
      return;
    }

    var me = this;

    var el = PCL.getBody();
    el.mask("数据加载中...");
    var r = {
      url: PSI.Const.BASE_URL
        + "SLN0001/Report/genSafetyInventoryPrintPage",
      params: {
        limit: -1
      },
      callback: function (options, success, response) {
        el.unmask();

        if (success) {
          var data = response.responseText;
          me.printReport("安全库存明细表", data);
        }
      }
    };
    me.ajax(r);
  },

  printReport: function (ref, data) {
    var me = this;

    var lodop = getLodop();
    if (!lodop) {
      PSI.MsgBox.showInfo("Lodop打印控件没有正确安装");
      return;
    }

    lodop.PRINT_INIT(ref);
    lodop.SET_PRINT_PAGESIZE(1, me.PRINT_PAGE_WIDTH, me.PRINT_PAGE_HEIGHT,
      "");
    lodop.ADD_PRINT_HTM("0mm", "0mm", "100%", "100%", data);
    var result = lodop.PRINT();
  },

  onPDF: function () {
    var me = this;

    var url = "SLN0001/Report/SafetyInventoryPdf?limit=-1";
    window.open(me.URL(url));
  },

  onExcel: function () {
    var me = this;

    var url = "SLN0001/Report/SafetyInventoryExcel?limit=-1";
    window.open(me.URL(url));
  }
});
