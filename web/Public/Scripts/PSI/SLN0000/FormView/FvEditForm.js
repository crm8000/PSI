/**
 * 视图 - 新建或编辑界面
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0000.FormView.FvEditForm", {
  extend: "PSI.AFX.Form.EditForm",

  config: {
    category: null,
    slnCode: "",
    slnName: "",
  },

  /**
   * @override
   */
  initComponent() {
    const me = this;
    const entity = me.getEntity();
    me.adding = entity == null;

    const buttons = [];

    buttons.push({
      text: "保存",
      ...PSI.Const.BTN_STYLE,
      formBind: true,
      iconCls: "PSI-button-ok",
      handler: function () {
        me._onOK(false);
      },
      scope: me
    }, {
      text: "取消",
      ...PSI.Const.BTN_STYLE,
      handler: function () {
        const info = entity == null ? "新建视图" : "编辑视图";

        me.confirm(`请确认是否取消：${info}?`, () => {
          me.close();
        });
      },
      scope: me
    });

    const t = entity == null ? "新建视图" : "编辑视图";
    const logoHtml = me.genLogoHtml(entity, t);

    const width1 = 600;
    const width2 = 300;
    PCL.apply(me, {
      header: {
        title: me.formatTitle(PSI.Const.PROD_NAME),
        height: 40
      },
      width: 650,
      height: 490,
      layout: "border",
      items: [{
        region: "north",
        border: 0,
        height: 55,
        html: logoHtml
      }, {
        region: "center",
        border: 0,
        id: me.buildId(me, "editForm"),
        xtype: "form",
        layout: {
          type: "table",
          columns: 2,
          tableAttrs: PSI.Const.TABLE_LAYOUT,
        },
        height: "100%",
        bodyPadding: 5,
        defaultType: 'textfield',
        fieldDefaults: {
          labelWidth: 90,
          labelAlign: "right",
          labelSeparator: "",
          msgTarget: 'side',
          width: width2
        },
        items: [{
          xtype: "hidden",
          name: "id",
          value: entity == null ? null : entity.get("id")
        }, {
          xtype: "hidden",
          name: "slnCode",
          value: me.getSlnCode()
        }, {
          id: me.buildId(me, "editCategoryId"),
          xtype: "hidden",
          name: "categoryId"
        }, {
          xtype: "displayfield",
          colspan: 2,
          width: width1,
          fieldLabel: "解决方案",
          value: `<span class='PSI-field-note'>${me.getSlnName()}</span>`
        }, {
          id: me.buildId(me, "editCategory"),
          xtype: "psi_fvcategoryfield",
          slnCode: me.getSlnCode(),
          fieldLabel: "分类",
          allowBlank: false,
          blankText: "没有输入视图分类",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          valueField: "id",
          displayField: "name",
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          }
        }, {
          id: me.buildId(me, "editCode"),
          fieldLabel: "编码",
          name: "code",
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          }
        }, {
          id: me.buildId(me, "editName"),
          fieldLabel: "视图名称",
          allowBlank: false,
          blankText: "没有输入中文名称",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          name: "name",
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          }
        }, {
          id: me.buildId(me, "editModuleName"),
          fieldLabel: "模块名称",
          allowBlank: false,
          blankText: "没有输入模块名称",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          name: "moduleName",
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          }
        }, {
          id: me.buildId(me, "editXtype"),
          xtype: "combo",
          queryMode: "local",
          editable: false,
          valueField: "id",
          fieldLabel: "xtype",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          store: PCL.create("PCL.data.ArrayStore", {
            fields: ["id", "text"],
            data: []
          }),
          name: "xtype",
          width: width1,
          colspan: 2
        }, {
          id: me.buildId(me, "editRegion"),
          xtype: "combo",
          queryMode: "local",
          editable: false,
          valueField: "id",
          fieldLabel: "位置",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          store: PCL.create("PCL.data.ArrayStore", {
            fields: ["id", "text"],
            data: [["center", "主体"], ["west", "左"], ["south", "下"]]
          }),
          name: "region",
          value: "center"
        }, {
          id: me.buildId(me, "editWidthOrHeight"),
          fieldLabel: "宽度/高度",
          allowBlank: false,
          blankText: "没有输入宽度/高度",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          name: "widthOrHeight",
          value: "100%",
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          }
        }, {
          id: me.buildId(me, "editLayout"),
          xtype: "combo",
          queryMode: "local",
          editable: false,
          valueField: "id",
          fieldLabel: "布局",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          store: PCL.create("PCL.data.ArrayStore", {
            fields: ["id", "text"],
            data: [[1, "填满整个区域"], [2, "左右布局"], [3, "上下布局"]]
          }),
          name: "layout",
          value: 2
        }, {
          id: me.buildId(me, "editDataSourceType"),
          xtype: "combo",
          queryMode: "local",
          editable: false,
          valueField: "id",
          fieldLabel: "数据源",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          store: PCL.create("PCL.data.ArrayStore", {
            fields: ["id", "text"],
            data: [[1, "码表"], [2, "自定义表单"], [0, "[无]"]]
          }),
          name: "dataSourceType",
          value: 1
        }, {
          id: me.buildId(me, "editDataSouceTableName"),
          fieldLabel: "数据源表名",
          name: "dataSourceTableName",
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          },
          width: width1,
          colspan: 2
        }, {
          id: me.buildId(me, "editHandlerClassName"),
          fieldLabel: "业务逻辑类名",
          name: "handlerClassName",
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          },
          width: width1,
          colspan: 2
        }, {
          id: me.buildId(me, "editMemo"),
          fieldLabel: "备注",
          name: "memo",
          value: entity == null ? null : entity.get("note"),
          listeners: {
            specialkey: {
              fn: me._onEditLastSpecialKey,
              scope: me
            }
          },
          width: width1,
          colspan: 2
        }],
        buttons
      }],
      listeners: {
        show: {
          fn: me._onWndShow,
          scope: me
        },
        close: {
          fn: me._onWndClose,
          scope: me
        }
      }
    });

    me.callParent(arguments);

    me.editForm = PCL.getCmp(me.buildId(me, "editForm"));

    me.editCategoryId = PCL.getCmp(me.buildId(me, "editCategoryId"));
    me.editCategory = PCL.getCmp(me.buildId(me, "editCategory"));
    me.editCode = PCL.getCmp(me.buildId(me, "editCode"));
    me.editName = PCL.getCmp(me.buildId(me, "editName"));
    me.editModuleName = PCL.getCmp(me.buildId(me, "editModuleName"));
    me.editXtype = PCL.getCmp(me.buildId(me, "editXtype"));
    me.editRegion = PCL.getCmp(me.buildId(me, "editRegion"));
    me.editWidthOrHeight = PCL.getCmp(me.buildId(me, "editWidthOrHeight"));
    me.editLayout = PCL.getCmp(me.buildId(me, "editLayout"));
    me.editDataSourceType = PCL.getCmp(me.buildId(me, "editDataSourceType"));
    me.editDataSourceTableName = PCL.getCmp(me.buildId(me, "editDataSouceTableName"));
    me.editHandlerClassName = PCL.getCmp(me.buildId(me, "editHandlerClassName"));
    me.editMemo = PCL.getCmp(me.buildId(me, "editMemo"));

    // AFX
    me.__editorList = [
      me.editCategory, me.editCode, me.editName, me.editModuleName,
      me.editWidthOrHeight, me.editDataSourceTableName, me.editHandlerClassName, me.editMemo];

    const c = me.getCategory();
    if (c) {
      me.editCategory.setIdValue(c.get("id"));
      me.editCategory.setValue(c.get("name"));
    }
  },

  _onWndShow() {
    const me = this;

    // AFX
    PCL.get(window).on('beforeunload', me.__onWindowBeforeUnload);

    const el = me.getEl();
    el && el.mask(PSI.Const.LOADING);
    me.ajax({
      url: me.URL("SLN0000/FormView/fvInfo"),
      params: {
        id: me.adding ? null : me.getEntity().get("id")
      },
      callback(options, success, response) {
        if (success) {
          const data = me.decodeJSON(response.responseText);
          const store = me.editXtype.getStore();
          store.removeAll();
          store.add(data.allXtype);

          if (me.adding) {
            me.editXtype.setValue(store.getAt(0));
          } else {
            // 编辑
            me.editCategory.setIdValue(data.categoryId);
            me.editCategory.setValue(data.categoryName);
            me.editCode.setValue(data.code);
            me.editName.setValue(data.name);
            me.editModuleName.setValue(data.moduleName);
            me.editXtype.setValue(data.xtype);
            me.editRegion.setValue(data.region);
            me.editWidthOrHeight.setValue(data.widthOrHeight);
            me.editLayout.setValue(parseInt(data.layout));
            me.editDataSourceType.setValue(parseInt(data.dataSourceType));
            me.editDataSourceTableName.setValue(data.dataSourceTableName);
            me.editHandlerClassName.setValue(data.handlerClassName);
            me.editMemo.setValue(data.memo);

            me.editLayout.setReadOnly(true);
            me.editRegion.setReadOnly(true);

            if (data.parentId) {
              // 子视图
              me.editCategory.setReadOnly(true);
              me.editCode.setReadOnly(true);
              me.editModuleName.setReadOnly(true);

              me.editWidthOrHeight.focus();
            }
          }
        }

        el && el.unmask();
      }
    });

    me.setFocusAndCursorPosToLast(me.editCode);
  },

  _onOK() {
    const me = this;

    me.editCategoryId.setValue(me.editCategory.getIdValue());

    const f = me.editForm;
    const el = f.getEl();
    el && el.mask(PSI.Const.SAVING);
    f.submit({
      url: me.URL("SLN0000/FormView/editFv"),
      method: "POST",
      success: function (form, action) {
        el && el.unmask();
        me.tip("数据保存成功", true);
        me.focus();
        me._lastId = action.result.id;
        me.close();
      },
      failure: function (form, action) {
        el && el.unmask();
        me.showInfo(action.result.msg, () => {
          me.editCode.focus();
        });
      }
    });
  },

  _onEditLastSpecialKey(field, e) {
    const me = this;

    if (e.getKey() === e.ENTER) {
      const f = me.editForm;
      if (f.getForm().isValid()) {
        me._onOK();
      }
    }
  },

  _onWndClose: function () {
    const me = this;

    // AFX
    PCL.get(window).un('beforeunload', me.__onWindowBeforeUnload);

    if (me._lastId) {
      const form = me.getParentForm();
      if (form && form.refreshMainGrid) {
        form.refreshMainGrid.apply(form, [me._lastId]);
      }
    }
  }
});
