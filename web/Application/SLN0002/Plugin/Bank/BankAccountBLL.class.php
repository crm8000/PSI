<?php

namespace SLN0002\Plugin\Bank;

/**
 * 银行账户 - 自定义业务逻辑处理class
 *
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
class BankAccountBLL
{
  /**
   * 在一个码表记录删除之前做的自定义业务逻辑判断
   * @param $db 数据库对象
   * @param $fid fid
   * @param $id 码表记录id
   * @return null|string;
   *    null : 表示可以执行删除操作; 返回字符串则会作为提示信息返回给用户，同时不执行删除操作（数据库事务会回滚）
   */
  public function beforeDelete($db, $fid, $id)
  {
    return "删除银行账户功能尚未开发<br/>本调用来自\SLN0002\Plugin\Bank\BankAccountBLL";
  }

  /**
   * 在一个码表记录新增之后做的自定义业务逻辑处理
   * @param $db 数据库对象
   * @param $fid fid
   * @param $id 码表记录id
   * @return null|string
   *    null : 表示可以继续执行新增操作; 返回字符串则会作为提示信息返回给用户，同时不执行新增操作（数据库事务会回滚）
   */
  public function afterAdd($db, $fid, $id)
  {
    // ------------------------------------------------------------------
    // TODO 
    //    为了更方便地实现这些业务逻辑，需要提供更多的 helper class
    //    目前就直接硬编码来实现
    // ------------------------------------------------------------------

    // 业务逻辑1：一个企业只能选择一家营业机构开立一个基本存款账户，不得在多家银行同时开立基本存款账户；

    $sql = "select company_id, category_id
            from t_sln0002_ct_bank 
            where id = '%s' ";
    $data = $db->query($sql, $id);
    if (!$data) {
      return "银行账户不存在";
    }
    $v = $data[0];
    $compnayId = $v["company_id"];
    $categoryId = $v["category_id"];

    if ($categoryId == 1) {
      // 基本存款账户
      $sql = "select count(*) as cnt
              from t_sln0002_ct_bank
              where company_id = '%s' and category_id = 1 and id <> '%s' ";
      $data = $db->query($sql, $compnayId, $id);
      $cnt = $data[0]["cnt"];
      if ($cnt > 0) {
        return "已经存在基本存款账户了</br>一个企业只能选择一家营业机构开立一个基本存款账户，不得在多家银行同时开立基本存款账户";
      }
    }

    // 业务逻辑2：不得在同一家银行的几个分支机构同时开立一般存款账户
    // TODO 因为目前并没设计分支机构，所以这个逻辑判断待开发

    return null;
  }

  /**
   * 在一个码表记录编辑之后做的自定义业务逻辑处理
   * @param $db 数据库对象
   * @param $fid fid
   * @param $id 码表记录id
   * @return null|string; 
   *    null : 表示可以继续执行编辑操作; 返回字符串则会作为提示信息返回给用户，同时不执行编辑操作（数据库事务会回滚）
   */
  public function afterUpdate($db, $fid, $id)
  {
    // 业务逻辑1：一个企业只能选择一家营业机构开立一个基本存款账户，不得在多家银行同时开立基本存款账户；

    $sql = "select company_id, category_id
            from t_sln0002_ct_bank 
            where id = '%s' ";
    $data = $db->query($sql, $id);
    if (!$data) {
      return "银行账户不存在";
    }
    $v = $data[0];
    $compnayId = $v["company_id"];
    $categoryId = $v["category_id"];

    if ($categoryId == 1) {
      // 基本存款账户
      $sql = "select count(*) as cnt
              from t_sln0002_ct_bank
              where company_id = '%s' and category_id = 1 and id <> '%s'";
      $data = $db->query($sql, $compnayId, $id);
      $cnt = $data[0]["cnt"];
      if ($cnt > 0) {
        return "已经存在基本存款账户了</br>一个企业只能选择一家营业机构开立一个基本存款账户，不得在多家银行同时开立基本存款账户";
      }
    }

    // 业务逻辑2：不得在同一家银行的几个分支机构同时开立一般存款账户
    // TODO 因为目前并没设计分支机构，所以这个逻辑判断待开发

    return null;
  }

  /**
   * 获得码表的默认值，其触发时机是新建一个码表记录的时候
   * 
   * @param $db 数据库对象
   * @param $fid fid
   * @return null|array; 
   *    null : 表示码表的默认值均不需要后台PHP代码处理
   */
  public function getDefaultValues($db, $fid)
  {
    return null;
  }
}
