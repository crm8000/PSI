/**
 * 用户管理 - 主界面
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */

PCL.define("PSI.Home.User.MainForm", {
  extend: "PSI.AFX.Form.MainForm",

  config: {
    pAddOrg: null,
    pEditOrg: null,
    pDeleteOrg: null,
    pAddUser: null,
    pEditUser: null,
    pDeleteUser: null,
    pChangePassword: null
  },

  /**
   * 初始化组件
   * 
   * @override
   */
  initComponent() {
    const me = this;

    let tbar = [];
    const list = [];
    if (me.getPAddOrg() == "1") {
      list.push({
        text: "新建组织机构",
        ...PSI.Const.BTN_STYLE,
        handler: me._onAddOrg,
        scope: me
      });
    }
    if (me.getPEditOrg() == "1") {
      list.push({
        text: "编辑组织机构",
        ...PSI.Const.BTN_STYLE,
        handler: me._onEditOrg,
        scope: me
      });
    }
    if (me.getPDeleteOrg() == "1") {
      list.push({
        text: "删除组织机构",
        ...PSI.Const.BTN_STYLE,
        handler: me._onDeleteOrg,
        scope: me
      });
    }
    if (list.length > 0) {
      tbar.push({
        text: "组织机构",
        iconCls: "PSI-tb-new",
        ...PSI.Const.BTN_STYLE,
        menu: list
      });
    }

    let sep = tbar.length > 0;
    if (me.getPAddUser() == "1") {
      if (sep) {
        tbar.push("-");
        sep = false;
      }
      tbar.push({
        iconCls: "PSI-tb-new-entity",
        text: "新建用户 <span class='PSI-shortcut-DS'>Alt + N</span>",
        tooltip: me.buildTooltip("快捷键：Alt + N"),
        ...PSI.Const.BTN_STYLE,
        handler: me._onAddUser,
        scope: me
      });
    }
    if (me.getPEditUser() == "1") {
      if (sep) {
        tbar.push("-");
        sep = false;
      }
      tbar.push({
        text: "编辑用户",
        ...PSI.Const.BTN_STYLE,
        handler: me._onEditUser,
        scope: me
      });
    }
    if (me.getPDeleteUser() == "1") {
      if (sep) {
        tbar.push("-");
        sep = false;
      }
      tbar.push({
        text: "删除用户",
        ...PSI.Const.BTN_STYLE,
        handler: me._onDeleteUser,
        scope: me
      });
    }
    sep = tbar.length > 0;
    if (me.getPChangePassword() == "1") {
      if (sep) {
        tbar.push("-");
        sep = false;
      }
      tbar.push({
        text: "修改用户密码",
        ...PSI.Const.BTN_STYLE,
        handler: me._onEditUserPassword,
        scope: me
      });
    }
    sep = tbar.length > 0;
    if (sep) {
      tbar.push("-");
      sep = false;
    }
    tbar.push({
      iconCls: "PSI-tb-help",
      text: "指南",
      ...PSI.Const.BTN_STYLE,
      handler() {
        me.focus();
        window.open(me.URL("/Home/Help/index?t=user"));
      }
    }, "-", {
      iconCls: "PSI-tb-close",
      text: "关闭",
      ...PSI.Const.BTN_STYLE,
      handler() {
        me.focus();
        me.closeWindow();
      }
    });

    tbar = tbar.concat(me.getShortcutCmp());

    PCL.apply(me, {
      items: [{
        tbar,
        id: me.buildId(me, "panelQueryCmp"),
        region: "north",
        border: 0,
        height: 80,
        bodyPadding: "10 0 0 0",
        header: false,
        collapsible: true,
        collapseMode: "mini",
        layout: {
          type: "table",
          columns: 5
        },
        bodyCls: "PSI-Query-Panel",
        items: me.getQueryCmp()
      }, {
        region: "center",
        layout: "border",
        ...PSI.Const.BODY_PADDING,
        border: 0,
        items: [{
          region: "center",
          layout: "border",
          border: 0,
          items: [{
            region: "north",
            border: 1,
            height: 30,
            /**
             * 这个panel只是为了使用其tbar展示信息
             * 这么做的原因是：UserGrid的column中用了lock属性后，
             * 导致页面在1920*1080分辨率（125%的放大率）下显示不正常。
             * 这应该是PCL的bug，但是目前尚未找到如何修正，所以采用这种方式迂回处理一下。
             */
            xtype: "panel",
            margin: 0,
            cls: "PSI-LC",
            tbar: [" ", {
              xtype: "displayfield",
              id: me.buildId(me, "dfUserInfo"),
              value: "人员列表",
            }, {
                // 空容器，撑大工具栏的高度
                xtype: "container",
                height: 26
              }]
          }, {
            region: "center",
            layout: "fit",
            border: 0,
            items: me.getUserGrid()
          }]
        }, {
          id: me.buildId(me, "panelOrg"),
          region: "west",
          layout: "fit",
          width: 510,
          split: true,
          collapsible: true,
          header: false,
          border: 0,
          items: [me.getOrgGrid()]
        }],
      },]
    });

    me.callParent(arguments);

    me._dfUserInfo = PCL.getCmp(me.buildId(me, "dfUserInfo"));

    // AFX: 查询控件input List
    me.__editorList = [
      PCL.getCmp(me.buildId(me, "editQueryLoginName")),
      PCL.getCmp(me.buildId(me, "editQueryName")),
      PCL.getCmp(me.buildId(me, "editQueryEnabled"))];

    me._keyMapMain = PCL.create("PCL.util.KeyMap", PCL.getBody(), [{
      key: "N",
      ctrl: false,
      shift: false,
      alt: true,
      fn() {
        if (PSI.Const.msgBoxShowing) {
          return;
        }

        // 判断权限
        if (me.getPAddUser() != "1") {
          return;
        }

        me._onAddUser.apply(me, []);
      },
      scope: me
    }, {
      key: "Q",
      ctrl: false,
      shift: false,
      alt: true,
      fn() {
        if (PSI.Const.msgBoxShowing) {
          return;
        }
        PCL.getCmp(me.buildId(me, "editQueryLoginName"))?.focus();
      },
      scope: me
    }, {
      key: "H",
      ctrl: false,
      shift: false,
      alt: true,
      fn() {
        if (PSI.Const.msgBoxShowing) {
          return;
        }

        const panel = PCL.getCmp(me.buildId(me, "panelQueryCmp"));
        if (panel.getCollapsed()) {
          panel.expand();
        } else {
          panel.collapse();
        };
      },
      scope: me
    },
    ]);
  },

  /**
   * @private
   */
  getQueryCmp() {
    const me = this;
    return [{
      xtype: "container",
      height: 26,
      html: `<h2 style='margin-left:20px;margin-top:0px;color:#595959;display:inline-block'>用户管理</h2>
              &nbsp;&nbsp;<span style='color:#8c8c8c'>集中管理组织机构和用户</span>
              <div style='float:right;display:inline-block;margin:0px 0px 0px 20px;border-left:1px solid #e5e6e8;height:26px'>&nbsp;</div>
              `
    }, {
      id: me.buildId(me, "editQueryLoginName"),
      labelWidth: 100,
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "登录名 <span class='PSI-shortcut-DS'>Alt + Q</span>",
      margin: "5 0 0 0",
      xtype: "textfield",
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      }
    }, {
      id: me.buildId(me, "editQueryName"),
      labelWidth: 60,
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "姓名",
      margin: "5 0 0 0",
      xtype: "textfield",
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      }
    }, {
      id: me.buildId(me, "editQueryEnabled"),
      xtype: "combo",
      queryMode: "local",
      editable: false,
      valueField: "id",
      labelWidth: 60,
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "状态",
      margin: "5 0 0 0",
      store: PCL.create("PCL.data.ArrayStore", {
        fields: ["id", "text"],
        data: [[-1, "全部"], [1, "允许登录"], [0, "禁止登录"]]
      }),
      value: -1,
      listeners: {
        specialkey: {
          fn: me.__onLastEditSpecialKey,
          scope: me
        }
      }
    }, {
      xtype: "container",
      items: [{
        xtype: "button",
        text: "查询",
        cls: "PSI-Query-btn1",
        width: 100,
        height: 26,
        margin: "5 0 0 20",
        handler: me._onQuery,
        scope: me
      }, {
        xtype: "button",
        text: "清空查询条件",
        cls: "PSI-Query-btn2",
        width: 100,
        height: 26,
        margin: "5 0 0 5",
        handler: me._onClearQuery,
        scope: me
      }, {
        xtype: "button",
        text: "隐藏工具栏 <span class='PSI-shortcut-DS'>Alt + H</span>",
        cls: "PSI-Query-btn3",
        width: 140,
        height: 26,
        iconCls: "PSI-button-hide",
        margin: "5 0 0 20",
        handler() {
          PCL.getCmp(me.buildId(me, "panelQueryCmp")).collapse();
        },
        scope: me
      }]
    }];
  },

  /**
   * 快捷访问
   * 
   * @private
   */
  getShortcutCmp() {
    const me = this;

    return ["->",
      {
        id: me.buildId(me, "pagingToolbar"),
        border: 0,
        cls: "PSI-Pagination",
        xtype: "pagingtoolbar",
        store: me.getUserGrid().getStore(),
      }, "-", {
        xtype: "displayfield",
        fieldStyle: "font-size:13px",
        value: "每页显示"
      }, {
        id: me.buildId(me, "comboCountPerPage"),
        xtype: "combobox",
        cls: "PSI-Pagination",
        editable: false,
        width: 60,
        store: PCL.create("PCL.data.ArrayStore", {
          fields: ["text"],
          data: [["20"], ["50"], ["100"], ["300"],
          ["1000"]]
        }),
        value: 20,
        listeners: {
          change: {
            fn() {
              const storeGrid = me.getUserGrid().getStore();
              storeGrid.pageSize = PCL.getCmp(me.buildId(me, "comboCountPerPage")).getValue();
              storeGrid.currentPage = 1;
              PCL.getCmp(me.buildId(me, "pagingToolbar")).doRefresh();
            },
            scope: me
          }
        }
      }, {
        xtype: "displayfield",
        fieldStyle: "font-size:13px",
        value: "条人员记录"
      }, "-", " ",
      {
        cls: "PSI-Shortcut-Cmp",
        labelWidth: 0,
        emptyText: "快捷访问",
        xtype: "psi_mainmenushortcutfield",
        width: 90
      }
    ];
  },

  /**
   * @private
   */
  getOrgGrid() {
    const me = this;
    if (me._orgGrid) {
      return me._orgGrid;
    }

    const modelName = me.buildModelName(me, "Org");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "text", "fullName", "orgCode", "dataOrg",
        "leaf", "children", "userCount", "orgType"]
    });

    const orgStore = PCL.create("PCL.data.TreeStore", {
      model: modelName,
      proxy: {
        type: "ajax",
        actionMethods: {
          read: "POST"
        },
        url: me.URL("Home/User/allOrgs")
      },
      listeners: {
        beforeload: {
          fn() {
            orgStore.proxy.extraParams = me.getQueryParamForCategory();
          },
          scope: me
        }
      }
    });

    orgStore.on("load", me._onOrgStoreLoad, me);

    const orgTree = PCL.create("PCL.tree.Panel", {
      cls: "PSI",
      header: {
        height: 30,
        title: me.formatGridHeaderTitle(me.toTitleKeyWord("组织机构"))
      },
      store: orgStore,
      rootVisible: false,
      useArrows: true,
      viewConfig: {
        enableTextSelection: true,
      },
      tools: [{
        type: "close",
        handler() {
          PCL.getCmp(me.buildId(me, "panelOrg")).collapse();
        }
      }],
      columns: {
        defaults: {
          sortable: false,
          menuDisabled: true,
          draggable: false
        },
        items: [{
          xtype: "treecolumn",
          text: "名称",
          dataIndex: "text",
          width: 300
        }, {
          text: "编码",
          dataIndex: "orgCode",
          width: 100
        }, {
          text: "数据域",
          dataIndex: "dataOrg",
          width: 100
        }, {
          text: "用户数",
          dataIndex: "userCount",
          width: 80,
          align: "right"
        }, {
          text: "组织机构性质",
          dataIndex: "orgType",
          width: 200
        }]
      }
    });

    orgTree.on("select", (rowModel, record) => {
      me._onOrgTreeNodeSelect(record);
    }, me);

    orgTree.on("beforeitemdblclick", () => { me._onEditOrg(); return false; }, me);

    me._orgGrid = orgTree;

    return me._orgGrid;
  },

  /**
   * @private
   */
  getUserGrid() {
    const me = this;

    if (me._userGrid) {
      return me._userGrid;
    }

    const modelName = me.buildModelName(me, "User");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "loginName", "name", "enabled", "orgCode",
        "gender", "birthday", "idCardNumber", "tel",
        "tel02", "address", "dataOrg", "roleName"]
    });
    const storeGrid = PCL.create("PCL.data.Store", {
      autoLoad: false,
      model: modelName,
      data: [],
      pageSize: 20,
      proxy: {
        type: "ajax",
        actionMethods: {
          read: "POST"
        },
        url: me.URL("Home/User/users"),
        reader: {
          root: 'dataList',
          totalProperty: 'totalCount'
        }
      }
    });
    storeGrid.on("beforeload", () => {
      storeGrid.proxy.extraParams = me.getUserParam();
    });

    me._userGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-LC",
      viewConfig: {
        enableTextSelection: true
      },
      columnLines: true,
      columns: {
        defaults: {
          menuDisabled: true,
          sortable: false,
        },
        items: [PCL.create("PCL.grid.RowNumberer", {
          text: "#",
          width: 40
        }), {
          header: "登录名",
          dataIndex: "loginName",
          locked: true,
          width: 250, // 登录名用这么宽，是因为常用手机号作为登录名
          renderer(value, metaData, record) {
            if (parseInt(record.get("enabled")) == 1) {
              return value;
            } else {
              return `<span class="PSI-record-disabled">${value}</span>`;
            }
          }
        }, {
          header: "姓名",
          dataIndex: "name",
          locked: true,
          renderer(value, metaData, record) {
            if (parseInt(record.get("enabled")) == 1) {
              return value;
            } else {
              return `<span class="PSI-record-disabled">${value}</span>`;
            }
          }
        }, {
          header: "权限角色",
          dataIndex: "roleName",
          width: 200,
          renderer(value) {
            return me.toAutoWrap(value);
          },
        }, {
          header: "编码",
          dataIndex: "orgCode",
        }, {
          header: "是否允许登录",
          dataIndex: "enabled",
          renderer(value) {
            return value == 1
              ? "允许登录"
              : "<span style='color:red'>禁止登录</span>";
          }
        }, {
          header: "性别",
          dataIndex: "gender",
          width: 70
        }, {
          header: "生日",
          dataIndex: "birthday",
        }, {
          header: "身份证号",
          dataIndex: "idCardNumber",
          width: 200
        }, {
          header: "联系电话",
          dataIndex: "tel",
        }, {
          header: "备用联系电话",
          dataIndex: "tel02",
        }, {
          header: "家庭住址",
          dataIndex: "address",
          width: 200
        }, {
          header: "数据域",
          dataIndex: "dataOrg",
          width: 100
        }]
      },
      store: storeGrid,
      listeners: {
        itemdblclick: {
          fn: me._onEditUser,
          scope: me
        }
      },
    });

    return me._userGrid;
  },

  /**
   * 新增组织机构
   * 
   * @private
   */
  _onAddOrg() {
    const me = this;

    const form = PCL.create("PSI.Home.User.OrgEditForm", {
      parentForm: me,
      renderTo: PSI.Const.RENDER_TO(),
    });
    form.show();
  },

  /**
   * 编辑组织机构
   * 
   * @private
   */
  _onEditOrg() {
    const me = this;
    if (me.getPEditOrg() == "0") {
      return;
    }

    const tree = me.getOrgGrid();
    const item = tree.getSelectionModel().getSelection();
    if (item === null || item.length !== 1) {
      me.showInfo("请选择要编辑的组织机构");
      return;
    }

    const org = item[0];

    const form = PCL.create("PSI.Home.User.OrgEditForm", {
      parentForm: me,
      renderTo: PSI.Const.RENDER_TO(),
      entity: org
    });
    form.show();
  },

  /**
   * 删除组织机构
   * 
   * @private
   */
  _onDeleteOrg() {
    const me = this;
    const tree = me.getOrgGrid();
    const item = tree.getSelectionModel().getSelection();
    if (item === null || item.length !== 1) {
      me.showInfo("请选择要删除的组织机构");
      return;
    }

    const org = item[0];

    const funcConfirm = () => {
      PCL.getBody().mask("正在删除中...");
      const r = {
        url: me.URL("Home/User/deleteOrg"),
        params: {
          id: org.get("id")
        },
        callback(options, success, response) {
          PCL.getBody().unmask();

          if (success) {
            const data = me.decodeJSON(response.responseText);
            if (data.success) {
              me.tip("成功完成删除操作", true);
              me.freshOrgGrid();
            } else {
              me.showInfo(data.msg);
            }
          }
        }
      };

      me.ajax(r);
    };

    const info = `请确认是否删除组织机构 <span style='color:red'>${org.get("fullName")}</span> ?`;
    me.confirm(info, funcConfirm);
  },

  /**
   * @public
   * 在web\Public\Scripts\PSI\Home\User\OrgEditForm.js中也会调用本方法
   */
  freshOrgGrid(id) {
    const me = this;

    me._lastOrgId = id;

    me.getOrgGrid().getStore().reload();
  },

  /**
   * @public
   * 在web\Public\Scripts\PSI\Home\User\UserEditForm.js中也会调用本方法
   */
  freshUserGrid() {
    const me = this;

    const tree = me.getOrgGrid();
    const item = tree.getSelectionModel().getSelection();
    if (item === null || item.length !== 1) {
      return;
    }

    me._onOrgTreeNodeSelect(item[0]);
  },

  /**
   * 新增用户
   * 
   * @private
   */
  _onAddUser() {
    const me = this;

    const tree = me.getOrgGrid();
    const item = tree.getSelectionModel().getSelection();
    let org = null;
    if (item != null && item.length > 0) {
      org = item[0];
    }

    const form = PCL.create("PSI.Home.User.UserEditForm", {
      parentForm: me,
      renderTo: PSI.Const.RENDER_TO(),
      defaultOrg: org
    });
    form.show();
  },

  /**
   * 编辑用户
   * 
   * @private
   */
  _onEditUser() {
    const me = this;
    if (me.getPEditUser() == "0") {
      return;
    }

    const item = me.getUserGrid().getSelectionModel().getSelection();
    if (item === null || item.length !== 1) {
      me.showInfo("请选择要编辑的用户");
      return;
    }

    const user = item[0];

    const form = PCL.create("PSI.Home.User.UserEditForm", {
      parentForm: me,
      renderTo: PSI.Const.RENDER_TO(),
      entity: user
    });
    form.show();
  },

  /**
   * 修改用户密码
   * 
   * @private
   */
  _onEditUserPassword() {
    const me = this;

    const item = me.getUserGrid().getSelectionModel().getSelection();
    if (item === null || item.length !== 1) {
      me.showInfo("请选择要修改密码的用户");
      return;
    }

    const user = item[0].getData();
    const form = PCL.create("PSI.Home.User.ChangeUserPasswordForm", {
      parentForm: me,
      renderTo: PSI.Const.RENDER_TO(),
      entity: user
    });
    form.show();
  },

  /**
   * 删除用户
   * 
   * @private
   */
  _onDeleteUser() {
    const me = this;
    const item = me.getUserGrid().getSelectionModel().getSelection();
    if (item === null || item.length !== 1) {
      me.showInfo("请选择要删除的用户");
      return;
    }

    const user = item[0];

    const funcConfirm = () => {
      PCL.getBody().mask("正在删除中...");
      const r = {
        url: me.URL("Home/User/deleteUser"),
        params: {
          id: user.get("id")
        },
        callback(options, success, response) {
          PCL.getBody().unmask();

          if (success) {
            const data = me.decodeJSON(response.responseText);
            if (data.success) {
              me.tip("成功完成删除操作", true);
              me.freshUserGrid();
            } else {
              me.showInfo(data.msg);
            }
          }
        }
      };
      me.ajax(r);
    };

    const info = `请确认是否删除用户 <span style='color:red'>${user.get("name")}</span> ?`;
    me.confirm(info, funcConfirm);
  },

  /**
   * @private
   */
  _onOrgTreeNodeSelect(rec) {
    if (!rec) {
      return;
    }

    const org = rec.data;
    if (!org) {
      return;
    }

    const me = this;

    const info = me.toTitleKeyWord(org.fullName) + " - 人员列表";
    me._dfUserInfo.setValue(info);

    PCL.getCmp(me.buildId(me, "pagingToolbar")).doRefresh();
  },

  /**
   * @private
   */
  _onOrgStoreLoad() {
    const me = this;

    const tree = me.getOrgGrid();
    tree.getSelectionModel().select(tree.getRootNode());
    if (me._lastOrgId) {
      const r = tree.getStore().getNodeById(me._lastOrgId);
      if (r) {
        tree.getSelectionModel().select(r);
      }
    } else {
      const root = tree.getRootNode();
      if (root) {
        const node = root.firstChild;
        if (node) {
          tree.getSelectionModel().select(node);
        }
      }
    }
  },

  /**
   * @private
   */
  getUserParam() {
    const me = this;
    const item = me.getOrgGrid().getSelectionModel().getSelection();
    if (item == null || item.length == 0) {
      return {};
    }

    const org = item[0];

    let queryLoginName = null;
    const editLoginName = PCL.getCmp(me.buildId(me, "editQueryLoginName"));
    if (editLoginName) {
      queryLoginName = editLoginName.getValue();
    }

    let queryName = null;
    const editQueryName = PCL.getCmp(me.buildId(me, "editQueryName"));
    if (editQueryName) {
      queryName = editQueryName.getValue();
    }

    let enabled = -1;
    const edit = PCL.getCmp(me.buildId(me, "editQueryEnabled"));
    if (edit) {
      enabled = edit.getValue();
    }

    const orgId = org.get("id");

    return {
      orgId,
      queryLoginName,
      queryName,
      enabled,
    }
  },

  /**
   * @private
   */
  _onClearQuery() {
    const me = this;

    PCL.getCmp(me.buildId(me, "editQueryLoginName")).setValue(null);
    PCL.getCmp(me.buildId(me, "editQueryName")).setValue(null);
    PCL.getCmp(me.buildId(me, "editQueryEnabled")).setValue(-1);

    me._onQuery();
  },

  /**
   * @private
   */
  _onQuery() {
    const me = this;

    me.getUserGrid().getStore().removeAll();

    me.freshOrgGrid();
  },

  /**
   * @private
   */
  getQueryParamForCategory() {
    const me = this;

    let queryLoginName = null;
    const editLoginName = PCL.getCmp(me.buildId(me, "editQueryLoginName"));
    if (editLoginName) {
      queryLoginName = editLoginName.getValue();
    }

    let queryName = null;
    const editQueryName = PCL.getCmp(me.buildId(me, "editQueryName"));
    if (editQueryName) {
      queryName = editQueryName.getValue();
    }

    let enabled = -1;
    const edit = PCL.getCmp(me.buildId(me, "editQueryEnabled"));
    if (edit) {
      enabled = edit.getValue();
    }

    return {
      queryLoginName,
      queryName,
      enabled
    };
  }
});
