/**
 * 关联物料 - 添加个别物料界面
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.Supplier.GRGoodsEditForm", {
  extend: "PSI.AFX.Form.EditForm",

  /**
   * @override
   */
  initComponent() {
    const me = this;

    const buttons = [];
    let btn = {
      text: "保存并继续新增",
      ...PSI.Const.BTN_STYLE,
      formBind: true,
      handler() {
        me._onOK(true);
      },
      scope: me
    };

    buttons.push(btn);

    btn = {
      text: "保存",
      ...PSI.Const.BTN_STYLE,
      formBind: true,
      iconCls: "PSI-button-ok",
      handler() {
        me._onOK(false);
      },
      scope: me
    };
    buttons.push(btn);

    btn = {
      text: "取消",
      ...PSI.Const.BTN_STYLE,
      handler() {
        me.close();
      },
      scope: me
    };
    buttons.push(btn);

    const t = "添加个别物料";
    const f = "edit-form-create.png";
    const logoHtml = "<img style='float:left;margin:0px 20px 0px 10px;width:48px;height:48px;' src='"
      + PSI.Const.BASE_URL
      + "Public/Images/"
      + f
      + "'></img>"
      + "<h2 style='color:#196d83;margin-top:0px;'>"
      + t
      + "</h2>"
      + "<p style='color:#196d83'>标记 <span style='color:red;font-weight:bold'>*</span>的是必须录入数据的字段</p>";;

    PCL.apply(me, {
      header: {
        title: me.formatTitle(PSI.Const.PROD_NAME),
        height: 40
      },
      width: 520,
      height: 300,
      layout: "border",
      listeners: {
        show: {
          fn: me._onWndShow,
          scope: me
        },
        close: {
          fn: me._onWndClose,
          scope: me
        }
      },
      items: [{
        region: "north",
        border: 0,
        height: 70,
        html: logoHtml
      }, {
        region: "center",
        border: 0,
        id: me.buildId(me, "editForm"),
        xtype: "form",
        layout: {
          type: "table",
          columns: 2
        },
        height: "100%",
        bodyPadding: 5,
        defaultType: 'textfield',
        fieldDefaults: {
          labelAlign: "right",
          labelSeparator: "",
          msgTarget: 'side',
          margin: "5"
        },
        items: [{
          xtype: "hidden",
          name: "id",
          value: me.getEntity().get("id")
        }, {
          xtype: "hidden",
          id: me.buildId(me, "editGoodsId"),
          name: "goodsId"
        }, {
          id: me.buildId(me, "editGoodsCode"),
          fieldLabel: "物料编码",
          width: 470,
          colspan: 2,
          allowBlank: false,
          blankText: "没有输入物料",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          xtype: "psi_goodsfield",
          callbackFunc: me._setGoodsInfo,
          callbackScope: me,
          parentCmp: me,
          listeners: {
            specialkey: {
              fn: me._onEditCodeSpecialKey,
              scope: me
            }
          }
        }, {
          fieldLabel: "品名",
          width: 470,
          readOnly: true,
          colspan: 2,
          id: me.buildId(me, "editGoodsName")
        }, {
          fieldLabel: "规格型号",
          readOnly: true,
          width: 470,
          colspan: 2,
          id: me.buildId(me, "editGoodsSpec")
        }],
        buttons: buttons
      }]
    });

    me.callParent(arguments);

    me.editForm = PCL.getCmp(me.buildId(me, "editForm"));

    me.editGoodsId = PCL.getCmp(me.buildId(me, "editGoodsId"));
    me.editGoodsCode = PCL.getCmp(me.buildId(me, "editGoodsCode"));
    me.editGoodsName = PCL.getCmp(me.buildId(me, "editGoodsName"));
    me.editGoodsSpec = PCL.getCmp(me.buildId(me, "editGoodsSpec"));
  },

  /**
   * 保存
   * 
   * @private
   */
  _onOK(thenAdd) {
    const me = this;
    const f = me.editForm;
    const el = f.getEl();
    el?.mask(PSI.Const.SAVING);
    const sf = {
      url: me.URL("SLN0001/Supplier/addGRGoods"),
      method: "POST",
      success(form, action) {
        el?.unmask();

        me.tip("数据保存成功", !thenAdd);
        me.focus();
        if (thenAdd) {
          me.clearEdit();
        } else {
          me.close();
        }
      },
      failure(form, action) {
        el?.unmask();
        me.showInfo(action.result.msg, () => {
          me.editGoodsCode.focus();
        });
      }
    };
    f.submit(sf);
  },

  /**
   * @private
   */
  _onEditCodeSpecialKey(field, e) {
    const me = this;

    if (e.getKey() == e.ENTER) {
      const f = me.editForm;
      if (f.getForm().isValid()) {
        me._onOK(true);
      }
    }
  },

  /**
   * @private
   */
  clearEdit() {
    const me = this;
    me.editGoodsCode.focus();

    const editors = [me.editGoodsCode, me.editGoodsName, me.editGoodsSpec];
    for (let i = 0; i < editors.length; i++) {
      const edit = editors[i];
      edit.setValue(null);
      edit.clearInvalid();
    }
  },

  /**
   * @private
   */
  _onWndClose() {
    const me = this;

    const parentForm = me.getParentForm();
    if (parentForm) {
      parentForm._keyMapMain.enable();
    }

    // AFX
    PCL.get(window).un('beforeunload', me.__onWindowBeforeUnload);

    if (me.getParentForm()) {
      me.getParentForm().refreshGRGoodsGrid();
    }
  },

  /**
   * @private
   */
  _onWndShow() {
    const me = this;

    const parentForm = me.getParentForm();
    if (parentForm) {
      parentForm._keyMapMain.disable();
    }

    // AFX
    PCL.get(window).on('beforeunload', me.__onWindowBeforeUnload);

    const editCode = me.editGoodsCode;
    editCode.focus();
  },

  /**
   * @private
   */
  _setGoodsInfo(goods) {
    const me = this;

    if (goods) {
      me.editGoodsId.setValue(goods.id);
      me.editGoodsName.setValue(goods.name);
      me.editGoodsSpec.setValue(goods.spec);
    } else {
      me.editGoodsId.setValue(null);
      me.editGoodsName.setValue(null);
      me.editGoodsSpec.setValue(null);
    }
  }
});
