/**
 * 库存建账 - 编辑界面
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.Inventory.InitInventoryEditForm", {
  extend: "PSI.AFX.Form.EditForm",
  config: {
    warehouse: null,
  },

  /**
   * @override
   */
  initComponent() {
    const me = this;
    const warehouse = me.getWarehouse();
    PCL.define("PSIGoodsCategory", {
      extend: "PCL.data.Model",
      fields: ["id", "name"]
    });
    const storeGoodsCategory = PCL.create("PCL.data.Store", {
      autoLoad: false,
      model: "PSIGoodsCategory",
      data: []
    });
    me.storeGoodsCategory = storeGoodsCategory;

    const logoHtml = "<img style='float:left;margin:0px 10px 0px 10px;width:48px;height:48px;' src='"
      + PSI.Const.BASE_URL
      + "Public/Images/edit-form-update.png'></img>"
      + "<h2 style='margin:10px'>建账仓库：<span style='color:#cf1322'>"
      + warehouse.get("name") + "</span></h2>";

    PCL.apply(me, {
      header: {
        title: "<span style='font-size:160%'>录入建账数据</span>",
        height: 40
      },
      modal: true,
      onEsc: PCL.emptyFn,
      width: 1000,
      height: 600,
      maximized: true,
      layout: "fit",
      items: [{
        id: "editForm",
        layout: "border",
        border: 0,
        bodyPadding: 5,
        items: [{
          xtype: "panel",
          region: "north",
          height: 50,
          border: 0,
          html: logoHtml
        }, {
          xtype: "panel",
          region: "center",
          layout: "border",
          border: 0,
          items: [{
            xtype: "panel",
            region: "center",
            layout: "border",
            border: 0,
            items: [{
              xtype: "panel",
              region: "north",
              height: 40,
              layout: "hbox",
              border: 0,
              items: [{
                xtype: "displayfield",
                margin: 5,
                value: "物料分类"
              }, {
                id: "comboboxGoodsCategory",
                margin: 5,
                xtype: "combobox",
                flex: 1,
                store: storeGoodsCategory,
                editable: false,
                displayField: "name",
                valueField: "id",
                queryMode: "local",
                listeners: {
                  change() {
                    me.getGoods();
                  }
                }
              }]
            }, {
              xtype: "panel",
              region: "center",
              layout: "fit",
              items: [me.getGoodsGrid()]
            }]
          }, {
            xtype: "panel",
            region: "east",
            width: 400,
            split: true,
            items: [{
              xtype: "form",
              layout: "form",
              border: 0,
              fieldDefaults: {
                labelWidth: 65,
                labelAlign: "right",
                labelSeparator: "",
                msgTarget: 'side'
              },
              bodyPadding: 5,
              defaultType: 'textfield',
              items: [{
                id: "editGoodsCode",
                fieldLabel: "物料编码",
                xtype: "displayfield"
              }, {
                id: "editGoodsName",
                fieldLabel: "品名",
                xtype: "displayfield"
              }, {
                id: "editGoodsSpec",
                fieldLabel: "规格型号",
                xtype: "displayfield"
              }, {
                id: "editGoodsCount",
                fieldLabel: "期初数量",
                beforeLabelTextTpl: PSI.Const.REQUIRED,
                xtype: "numberfield",
                allowDecimals: PSI.Const.GC_DEC_NUMBER > 0,
                decimalPrecision: PSI.Const.GC_DEC_NUMBER,
                minValue: 0,
                hideTrigger: true
              }, {
                id: "editUnit",
                xtype: "displayfield",
                fieldLabel: "计量单位",
                value: ""
              }, {
                id: "editGoodsMoney",
                fieldLabel: "期初金额",
                xtype: "numberfield",
                allowDecimals: true,
                hideTrigger: true,
                beforeLabelTextTpl: PSI.Const.REQUIRED
              }, {
                id: "editGoodsPrice",
                fieldLabel: "期初单价",
                xtype: "displayfield"
              }]
            }, {
              xtype: "container",
              layout: "hbox",
              items: [{
                xtype: "container",
                flex: 1
              }, {
                id: "buttonSubmit",
                xtype: "button",
                height: 36,
                text: "保存当前物料的建账信息",
                iconCls: "PSI-button-ok",
                flex: 2,
                handler: me._onSave,
                scope: me
              }, {
                xtype: "container",
                flex: 1
              }]
            }, {
              xtype: "container",
              layout: "hbox",
              margin: 10,
              items: [{
                xtype: "container",
                flex: 1
              }, {
                xtype: "checkbox",
                id: "checkboxGotoNext",
                checked: true,
                fieldLabel: "保存后自动跳转到下一条记录",
                labelWidth: 200,
                labelSeparator: ""
              }, {
                xtype: "container",
                flex: 1
              }]
            }, {
              fieldLabel: "&nbsp;&nbsp;",
              xtype: "displayfield",
              labelWidth: 60,
              labelAlign: "right",
              labelSeparator: "",
              value: "<span class='PSI-field-note'>如果期初数量设置为0，就会清除该物料的建账记录</span>"
            }]
          }]
        }]
      }],
      buttons: [{
        text: "关闭",
        ...PSI.Const.BTN_STYLE,
        handler() {
          me.close();
        }
      }],
      listeners: {
        show: {
          fn: me._onWndShow,
          scope: me
        },
        close: {
          fn: me._onWndClose,
          scope: me
        }
      }
    });
    me.callParent(arguments);
    PCL.getCmp("editGoodsCount").on("specialkey", (field, e) => {
      if (e.getKey() == e.ENTER) {
        PCL.getCmp("editGoodsMoney").focus();
      }
    });
    PCL.getCmp("editGoodsMoney").on("specialkey", (field, e) => {
      if (e.getKey() == e.ENTER) {
        PCL.getCmp("buttonSubmit").focus();
      }
    });
    me.getGoodsCategories();
  },

  /**
   * @private
   */
  _onWndClose() {
    const me = this;

    PCL.get(window).un('beforeunload', me.__onWindowBeforeUnload);

    const parentForm = me.getParentForm();
    parentForm?.refreshInvGrid?.apply(parentForm);

  },

  /**
   * @private
   */
  _onWndShow() {
    const me = this;

    PCL.get(window).on('beforeunload', me.__onWindowBeforeUnload);
  },

  /**
   * @private
   */
  getGoodsGrid() {
    const me = this;
    if (me._gridGoods) {
      return me._gridGoods;
    }

    const modelName = me.buildModelName(me, "InitInvGoods");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "goodsCode", "goodsName", "goodsSpec",
        "goodsCount", "unitName", "goodsMoney",
        "goodsPrice", "initDate"]
    });
    const store = PCL.create("PCL.data.Store", {
      autoLoad: false,
      model: modelName,
      data: [],
      pageSize: 20,
      proxy: {
        type: "ajax",
        actionMethods: {
          read: "POST"
        },
        url: me.URL("SLN0001/InitInventory/goodsList"),
        reader: {
          root: 'goodsList',
          totalProperty: 'totalCount'
        }
      },
      listeners: {
        beforeload: {
          fn() {
            const comboboxGoodsCategory = PCL.getCmp("comboboxGoodsCategory");
            const categoryId = comboboxGoodsCategory.getValue();
            const warehouseId = me.getWarehouse().get("id");
            PCL.apply(store.proxy.extraParams, {
              categoryId,
              warehouseId,
            });
          },
          scope: me
        },
        load: {
          fn(e, records, successful) {
            if (successful) {
              me.getGoodsGrid().getSelectionModel().select(0);
              PCL.getCmp("editGoodsCount").focus();
            }
          },
          scope: me
        }
      }
    });
    me._gridGoods = PCL.create("PCL.grid.Panel", {
      cls: "PSI-FC",
      border: 0,
      columnLines: true,
      columns: [PCL.create("PCL.grid.RowNumberer", {
        text: "序号",
        width: 50
      }), {
        header: "物料编码",
        dataIndex: "goodsCode",
        menuDisabled: true,
        sortable: false,
        width: 80
      }, {
        header: "品名",
        dataIndex: "goodsName",
        menuDisabled: true,
        sortable: false,
        width: 200
      }, {
        header: "规格型号",
        dataIndex: "goodsSpec",
        menuDisabled: true,
        sortable: false,
        width: 200
      }, {
        header: "期初数量",
        dataIndex: "goodsCount",
        menuDisabled: true,
        sortable: false,
        align: "right"
      }, {
        header: "单位",
        dataIndex: "unitName",
        menuDisabled: true,
        sortable: false,
        width: 50
      }, {
        header: "期初金额",
        dataIndex: "goodsMoney",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn"
      }, {
        header: "期初单价",
        dataIndex: "goodsPrice",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn"
      }],
      bbar: [{
        id: "_pagingToolbar",
        border: 0,
        xtype: "pagingtoolbar",
        store: store
      }, "-", {
        xtype: "displayfield",
        fieldStyle: "font-size:13px",
        value: "每页显示"
      }, {
        id: "_comboCountPerPage",
        xtype: "combobox",
        editable: false,
        width: 60,
        store: PCL.create("PCL.data.ArrayStore", {
          fields: ["text"],
          data: [["20"], ["50"], ["100"],
          ["300"], ["1000"]]
        }),
        value: 20,
        listeners: {
          change: {
            fn() {
              store.pageSize = PCL.getCmp("_comboCountPerPage").getValue();
              store.currentPage = 1;
              PCL.getCmp("_pagingToolbar").doRefresh();
            },
            scope: me
          }
        }
      }, {
        xtype: "displayfield",
        fieldStyle: "font-size:13px",
        value: "条记录"
      }],
      store: store,
      listeners: {
        select: {
          fn: me._onGoodsGridSelect,
          scope: me
        }
      }
    });
    return me._gridGoods;
  },

  getGoodsCategories() {
    var store = this.storeGoodsCategory;
    var el = PCL.getBody();
    el.mask(PSI.Const.LOADING);
    PCL.Ajax.request({
      url: PSI.Const.BASE_URL
        + "SLN0001/InitInventory/goodsCategoryList",
      method: "POST",
      callback(options, success, response) {
        store.removeAll();
        if (success) {
          var data = PCL.JSON.decode(response.responseText);
          store.add(data);
        }

        el.unmask();
      }
    });
  },

  /**
   * @private
   */
  getGoods() {
    const me = this;
    me.getGoodsGrid().getStore().currentPage = 1;
    PCL.getCmp("_pagingToolbar").doRefresh();
  },

  /**
   * @private
   */
  _onGoodsGridSelect() {
    const me = this;
    const grid = me.getGoodsGrid();
    const item = grid.getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      return;
    }

    const goods = item[0];
    PCL.getCmp("editGoodsCode").setValue(goods.get("goodsCode"));
    PCL.getCmp("editGoodsName").setValue(goods.get("goodsName"));
    PCL.getCmp("editGoodsSpec").setValue(goods.get("goodsSpec"));
    PCL.getCmp("editUnit").setValue("<strong>" + goods.get("unitName")
      + "</strong>");
    const goodsCount = goods.get("goodsCount");
    if (goodsCount == "0") {
      PCL.getCmp("editGoodsCount").setValue(null);
      PCL.getCmp("editGoodsMoney").setValue(null);
      PCL.getCmp("editGoodsPrice").setValue(null);
    } else {
      PCL.getCmp("editGoodsCount").setValue(goods.get("goodsCount"));
      PCL.getCmp("editGoodsMoney").setValue(goods.get("goodsMoney"));
      PCL.getCmp("editGoodsPrice").setValue(goods.get("goodsPrice"));
    }
  },

  /**
   * @private
   */
  updateAfterSave(goods) {
    const me = this;

    goods.set("goodsCount", PCL.getCmp("editGoodsCount").getValue());
    goods.set("goodsMoney", PCL.getCmp("editGoodsMoney").getValue());
    const cnt = PCL.getCmp("editGoodsCount").getValue();
    if (cnt == 0) {
      goods.set("goodsPrice", null);
    } else {
      let p = PCL.getCmp("editGoodsMoney").getValue()
        / PCL.getCmp("editGoodsCount").getValue();
      p = PCL.Number.toFixed(p, 2);
      goods.set("goodsPrice", p);
    }

    me.getGoodsGrid().getStore().commitChanges();
    PCL.getCmp("editGoodsPrice").setValue(goods.get("goodsPrice"));
  },

  /**
   * @private
   */
  _onSave() {
    const me = this;
    const grid = me.getGoodsGrid();
    const item = grid.getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("请选择一个建账物料");
      return;
    }

    const goods = item[0];
    const goodsCount = PCL.getCmp("editGoodsCount").getValue();
    const goodsMoney = PCL.getCmp("editGoodsMoney").getValue();
    const el = PCL.getBody();
    el.mask(PSI.Const.SAVING);
    me.ajax({
      url: me.URL("SLN0001/InitInventory/commitInitInventoryGoods"),
      params: {
        goodsId: goods.get("id"),
        goodsCount: goodsCount,
        goodsMoney: goodsMoney,
        warehouseId: me.getWarehouse().get("id")
      },
      callback(options, success, response) {
        el.unmask();
        if (success) {
          const result = me.decodeJSON(response.responseText);
          if (result.success) {
            me.updateAfterSave(goods);
            if (!PCL.getCmp("checkboxGotoNext").getValue()) {
              me.showInfo("数据成功保存");
            } else {
              me.gotoNext();
            }
          } else {
            me.showInfo(result.msg, () => {
              PCL.getCmp("editGoodsCount").focus();
            });
          }
        }
      }
    });
  },

  /**
   * @private
   */
  gotoNext() {
    const me = this;
    if (!PCL.getCmp("checkboxGotoNext").getValue()) {
      return;
    }

    const grid = me.getGoodsGrid();
    const hasNext = grid.getSelectionModel().selectNext();
    if (!hasNext) {
      PCL.getCmp("_pagingToolbar").moveNext();
    }
    const editCount = PCL.getCmp("editGoodsCount");
    editCount.focus();
  }
});
