/**
 * 权限管理 - 主界面
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.Home.Permission.MainForm", {
  extend: "PSI.AFX.Form.MainForm",

  config: {
    pAdd: "",
    pEdit: "",
    pDelete: ""
  },

  /**
   * 初始化组件
   * 
   * @override
   */
  initComponent() {
    const me = this;

    PCL.apply(me, {
      items: [{
        tbar: me.getToolbarCmp(),
        id: me.buildId(me, "panelQueryCmp"),
        region: "north",
        border: 0,
        height: 80,
        bodyPadding: "10 0 0 0",
        header: false,
        collapsible: true,
        collapseMode: "mini",
        layout: {
          type: "table",
          columns: 5
        },
        bodyCls: "PSI-Query-Panel",
        items: me.getQueryCmp()
      }, {
        region: "center",
        layout: "border",
        ...PSI.Const.BODY_PADDING,
        border: 0,
        items: [{
          region: "center",
          layout: "border",
          border: 0,
          items: [{
            region: "north",
            layout: "fit",
            height: 30,
            xtype: "panel",
            margin: 0,
            cls: "PSI-LC",
            tbar: [" ", {
              xtype: "displayfield",
              id: me.buildId(me, "dfRole"),
              value: "当前角色",
            }, {
                // 空容器，撑大工具栏的高度
                xtype: "container",
                height: 26
              }]
          }, {
            region: "center",
            layout: "fit",
            xtype: "tabpanel",
            bodyStyle: { borderWidth: 0 },
            border: 0,
            items: [me.getPermissionGrid()
              , me.getUserGrid()
            ]
          }]
        }, {
          id: "panelRole",
          region: "west",
          layout: "fit",
          width: 250,
          split: true,
          collapsible: true,
          header: false,
          border: 0,
          items: [me.getRoleGrid()]
        }]
      }]
    });

    me.callParent(arguments);

    me._dfRole = PCL.getCmp(me.buildId(me, "dfRole"));

    me.refreshRoleGrid();

    // AFX 查询控件input List
    me.__editorList = [
      PCL.getCmp(me.buildId(me, "editQueryLoginName")),
      PCL.getCmp(me.buildId(me, "editQueryName"))];

    me._keyMapMain = PCL.create("PCL.util.KeyMap", PCL.getBody(), [{
      key: "N",
      ctrl: false,
      shift: false,
      alt: true,
      fn() {
        if (PSI.Const.msgBoxShowing) {
          return;
        }

        // 判断权限
        if (me.getPAdd() != "1") {
          return;
        }

        me._onAddRole.apply(me, []);
      },
      scope: me
    }, {
      key: "Q",
      ctrl: false,
      shift: false,
      alt: true,
      fn() {
        if (PSI.Const.msgBoxShowing) {
          return;
        }
        PCL.getCmp(me.buildId(me, "editQueryLoginName"))?.focus();
      },
      scope: me
    }, {
      key: "H",
      ctrl: false,
      shift: false,
      alt: true,
      fn() {
        if (PSI.Const.msgBoxShowing) {
          return;
        }

        const panel = PCL.getCmp(me.buildId(me, "panelQueryCmp"));
        if (panel.getCollapsed()) {
          panel.expand();
        } else {
          panel.collapse();
        };
      },
      scope: me
    },
    ]);
  },

  /**
   * @private
   */
  getQueryCmp() {
    const me = this;
    return [{
      xtype: "container",
      height: 26,
      html: `<h2 style='margin-left:20px;margin-top:0px;color:#595959;display:inline-block'>权限管理</h2>
              &nbsp;&nbsp;<span style='color:#8c8c8c'>基于角色和数据域的权限管理</span>
              <div style='float:right;display:inline-block;margin:0px 0px 0px 20px;border-left:1px solid #e5e6e8;height:26px'>&nbsp;</div>
              `
    }, {
      id: me.buildId(me, "editQueryLoginName"),
      labelWidth: 100,
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "登录名 <span class='PSI-shortcut-DS'>Alt + Q</span>",
      margin: "5, 0, 0, 0",
      xtype: "textfield",
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      }
    }, {
      id: me.buildId(me, "editQueryName"),
      labelWidth: 60,
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "用户姓名",
      margin: "5, 0, 0, 0",
      xtype: "textfield",
      listeners: {
        specialkey: {
          fn: me.__onLastEditSpecialKey,
          scope: me
        }
      }
    }, {
      xtype: "container",
      items: [{
        xtype: "button",
        text: "查询",
        cls: "PSI-Query-btn1",
        width: 100,
        height: 26,
        margin: "5, 0, 0, 20",
        handler: me._onQuery,
        scope: me
      }, {
        xtype: "button",
        text: "清空查询条件",
        cls: "PSI-Query-btn2",
        width: 100,
        height: 26,
        margin: "5, 0, 0, 5",
        handler: me._onClearQuery,
        scope: me
      }, {
        xtype: "button",
        text: "隐藏工具栏 <span class='PSI-shortcut-DS'>Alt + H</span>",
        cls: "PSI-Query-btn3",
        width: 140,
        height: 26,
        iconCls: "PSI-button-hide",
        margin: "5 0 0 20",
        handler() {
          PCL.getCmp(me.buildId(me, "panelQueryCmp")).collapse();
        },
        scope: me
      }]
    }];
  },

  /**
   * @private
   */
  getToolbarCmp() {
    const me = this;
    let result = [];
    if (me.getPAdd() == "1") {
      result.push({
        iconCls: "PSI-tb-new",
        text: "新建角色 <span class='PSI-shortcut-DS'>Alt + N</span>",
        tooltip: me.buildTooltip("快捷键：Alt + N"),
        ...PSI.Const.BTN_STYLE,
        handler: me._onAddRole,
        scope: me,
      });
    };

    if (me.getPEdit() == "1") {
      result.push({
        text: "编辑角色",
        ...PSI.Const.BTN_STYLE,
        handler: me._onEditRole,
        scope: me,
      });
    }

    if (me.getPDelete() == "1") {
      result.push({
        text: "删除角色",
        ...PSI.Const.BTN_STYLE,
        handler: me._onDeleteRole,
        scope: me,
      });
    }

    // 工具菜单
    if (me.getPAdd() == "1") {
      if (result.length > 0) {
        result.push("-");
      }
      result.push({
        text: "工具",
        ...PSI.Const.BTN_STYLE,
        menu: [{
          text: "以复制当前角色的方式新建角色",
          handler: me._onCopyRole,
          scope: me,
        }, "-", {
          text: "把当前角色的【用户管理-模块权限】数据域设置为全域",
          handler: me._onApplyFullDataOrg,
          scope: me,
        }]
      });
    }
    if (result.length > 0) {
      result.push("-");
    }
    result.push({
      iconCls: "PSI-tb-help",
      text: "指南",
      ...PSI.Const.BTN_STYLE,
      handler() {
        me.focus();
        window.open(me.URL("Home/Help/index?t=permission"));
      }
    }, "-", {
      iconCls: "PSI-tb-close",
      text: "关闭",
      ...PSI.Const.BTN_STYLE,
      handler() {
        me.focus();
        me.closeWindow();
      }
    }, {
      // 空容器，只是为了撑高工具栏
      xtype: "container", height: 28,
      items: []
    });

    result = result.concat(me.getShortcutCmp());

    return result;
  },

  /**
   * 快捷访问
   */
  getShortcutCmp() {
    return ["->",
      {
        cls: "PSI-Shortcut-Cmp",
        labelWidth: 0,
        emptyText: "快捷访问",
        xtype: "psi_mainmenushortcutfield",
        width: 90
      }
    ];
  },

  /**
   * @private
   */
  getRoleGrid() {
    const me = this;
    if (me._roleGrid) {
      return me._roleGrid;
    }

    const modelName = me.buildModelName(me, "RoleModel");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "name", "code"]
    });

    const roleStore = PCL.create("PCL.data.Store", {
      model: modelName,
      autoLoad: false,
      data: []
    });

    me._roleGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-FC",
      header: {
        height: 30,
        title: me.formatGridHeaderTitle(me.toTitleKeyWord("角色列表"))
      },
      tools: [{
        type: "close",
        handler() {
          PCL.getCmp("panelRole").collapse();
        }
      }],
      viewConfig: {
        enableTextSelection: true
      },
      store: roleStore,
      columnLines: true,
      columns: {
        defaults: {
          sortable: false,
          menuDisabled: true,
        },
        items: [{
          header: "编码",
          dataIndex: "code",
          width: 100,
        }, {
          header: "角色名称",
          dataIndex: "name",
          width: 130,
          renderer(value) {
            return me.toAutoWrap(value);
          },
        }]
      },
      listeners: {
        itemdblclick: {
          fn: me._onEditRole,
          scope: me
        },
        select: {
          fn: me._onRoleGridSelect,
          scope: me
        }
      }
    });

    return me._roleGrid;
  },

  /**
   * @private
   */
  getPermissionGrid() {
    const me = this;
    if (me._permissionGrid) {
      return me._permissionGrid;
    }

    const modelName = me.buildModelName(me, "PermissionModel");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "name", "dataOrg", "dataOrgWithName", "note", "category"]
    });

    const permissionStore = PCL.create("PCL.data.Store", {
      model: modelName,
      autoLoad: false,
      data: []
    });

    me._permissionGrid = PCL.create("PCL.grid.Panel", {
      title: "当前角色拥有的权限",
      cls: "PSI-HL",
      store: permissionStore,
      columnLines: true,
      viewConfig: {
        enableTextSelection: true
      },
      columns: {
        defaults: {
          sortable: false,
          menuDisabled: true,
        },
        items: [PCL.create("PCL.grid.RowNumberer", {
          text: "#",
          align: "right",
          width: 60
        }), {
          header: "按模块分类",
          dataIndex: "category",
          width: 120,
          renderer(value) {
            return me.toAutoWrap(value);
          }
        }, {
          header: "权限名称",
          dataIndex: "name",
          width: 300,
          renderer(value) {
            return me.toAutoWrap(value);
          }
        }, {
          header: "说明",
          dataIndex: "note",
          flex: 1,
          renderer(value) {
            return me.toAutoWrap(value);
          }
        }, {
          header: "数据域",
          dataIndex: "dataOrgWithName",
          width: 300,
          renderer(value) {
            return me.toAutoWrap(value);
          }
        }]
      },
    });

    return me._permissionGrid;
  },

  /**
   * @private
   */
  getUserGrid() {
    const me = this;
    if (me._userGrid) {
      return me._userGrid;
    }

    const modelName = me.buildModelName(me, "UserModel");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "loginName", "name", "orgFullName",
        "enabled"]
    });

    const userStore = PCL.create("PCL.data.Store", {
      model: modelName,
      autoLoad: false,
      data: []
    });

    me._userGrid = PCL.create("PCL.grid.Panel", {
      title: "属于当前角色的人员",
      viewConfig: {
        enableTextSelection: true
      },
      cls: "PSI-HL",
      store: userStore,
      columnLines: true,
      columns: {
        defaults: {
          sortable: false,
          menuDisabled: true,
        },
        items: [PCL.create("PCL.grid.RowNumberer", {
          text: "#",
          align: "right",
          width: 60
        }), {
          header: "用户姓名",
          dataIndex: "name",
          width: 160
        }, {
          header: "登录名",
          dataIndex: "loginName",
          width: 200
        }, {
          header: "所属组织",
          dataIndex: "orgFullName",
          flex: 1
        }]
      }
    });
    return me._userGrid;
  },

  /**
   * 刷新角色Grid
   * 
   * @private
   */
  refreshRoleGrid(id) {
    const me = this;

    const grid = me.getRoleGrid();
    const store = grid.getStore();

    PCL.getBody().mask("数据加载中...");
    me.ajax({
      url: me.URL("Home/Permission/roleList"),
      params: {
        queryLoginName: PCL.getCmp(me.buildId(me, "editQueryLoginName")).getValue(),
        queryName: PCL.getCmp(me.buildId(me, "editQueryName")).getValue()
      },
      callback(options, success, response) {
        store.removeAll();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          store.add(data);

          if (data.length > 0) {
            if (id) {
              const r = store.findExact("id", id);
              if (r != -1) {
                grid.getSelectionModel().select(r);
              }
            } else {
              grid.getSelectionModel().select(0);
            }
            me._onRoleGridSelect();
          }
        }

        PCL.getBody().unmask();
      }
    });
  },

  /**
   * @private
   */
  _onRoleGridSelect() {
    const me = this;

    const grid = me.getPermissionGrid();

    const item = me.getRoleGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      return;
    }

    const role = item[0];

    const info = "当前角色：" + me.toTitleKeyWord(`${role.get("code")} - ${role.get("name")}`);
    me._dfRole.setValue(info);

    const store = grid.getStore();

    const el = grid.getEl();

    el?.mask("数据加载中...");
    me.ajax({
      url: me.URL("Home/Permission/permissionList"),
      params: {
        roleId: role.get("id")
      },
      callback(options, success, response) {
        store.removeAll();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          store.add(data);
        }

        el?.unmask();
      }
    });

    const userGrid = me.getUserGrid();
    const userStore = userGrid.getStore();
    const userEl = userGrid.getEl();

    userEl?.mask("数据加载中...");
    me.ajax({
      url: me.URL("Home/Permission/userList"),
      params: {
        roleId: role.get("id")
      },
      callback(options, success, response) {
        userStore.removeAll();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          userStore.add(data);
        }

        userEl?.unmask();
      }
    });
  },

  /**
   * 新增角色
   * 
   * @private
   */
  _onAddRole() {
    const me = this;
    const form = PCL.create("PSI.Home.Permission.EditForm", {
      parentForm: me
    });

    form.show();
  },

  /**
   * 以复制方式新建角色
   * 
   * @private
   */
  _onCopyRole() {
    const me = this;
    const grid = me.getRoleGrid();
    const items = grid.getSelectionModel().getSelection();

    if (items == null || items.length != 1) {
      me.showInfo("请选择要编辑的角色");
      return;
    }

    const roleCopy = items[0];

    const form = PCL.create("PSI.Home.Permission.EditForm", {
      parentForm: me,
      roleCopy
    });

    form.show();
  },

  /**
   * 编辑角色
   * 
   * @private
   */
  _onEditRole() {
    const me = this;

    const grid = me.getRoleGrid();
    const items = grid.getSelectionModel().getSelection();

    if (items == null || items.length != 1) {
      me.showInfo("请选择要编辑的角色");
      return;
    }

    const role = items[0].data;

    const form = PCL.create("PSI.Home.Permission.EditForm", {
      entity: role,
      parentForm: me
    });

    form.show();
  },

  /**
   * 删除角色
   * 
   * @private
   */
  _onDeleteRole() {
    const me = this;
    const grid = me.getRoleGrid();
    const items = grid.getSelectionModel().getSelection();

    if (items == null || items.length != 1) {
      me.showInfo("请选择要删除的角色");
      return;
    }

    const role = items[0].data;

    const info = `请确认是否删除角色 <span style='color:red'>${role.name}</span> ?`;
    const funcConfirm = () => {
      PCL.getBody().mask("正在删除中...");
      const r = {
        url: me.URL("Home/Permission/deleteRole"),
        params: {
          id: role.id
        },
        callback(options, success, response) {
          PCL.getBody().unmask();

          if (success) {
            const data = PCL.JSON.decode(response.responseText);
            if (data.success) {
              me.refreshRoleGrid();
              me.tip("成功完成删除操作", true);
            } else {
              me.showInfo(data.msg);
            }
          }
        }
      };

      me.ajax(r);
    };

    me.confirm(info, funcConfirm);
  },

  /**
   * @private
   */
  _onClearQuery() {
    const me = this;

    PCL.getCmp(me.buildId(me, "editQueryLoginName")).setValue(null);
    PCL.getCmp(me.buildId(me, "editQueryName")).setValue(null);

    me._onQuery();
  },

  /**
   * @private
   */
  _onQuery() {
    const me = this;

    me.getPermissionGrid().getStore().removeAll();
    me.getPermissionGrid().setTitle("权限列表");
    me.getUserGrid().getStore().removeAll();
    me.getUserGrid().setTitle("用户列表");

    me.refreshRoleGrid();
  },

  /**
   * @private
   */
  _onApplyFullDataOrg() {
    const me = this;

    const grid = me.getRoleGrid();
    const items = grid.getSelectionModel().getSelection();

    if (items == null || items.length != 1) {
      me.showInfo("请先选择角色");
      return;
    }

    const role = items[0];

    const info = `请确认是否把角色 <span style='color:red'>${role.get("name")}</span> 的【用户管理-模块权限】的数据域设置为全域?`;
    const funcConfirm = () => {
      const r = {
        url: me.URL("Home/Permission/applyUserManagementFullDataOrg"),
        params: {
          id: role.get("id")
        },
        callback(options, success, response) {
          if (success) {
            const data = me.decodeJSON(response.responseText);
            if (data.success) {
              me._onRoleGridSelect();
              me.tip(`成功完成操作`, true);
            } else {
              me.showInfo(data.msg);
            }
          }
        }
      };

      me.ajax(r);
    };

    me.confirm(info, funcConfirm);
  },
});
