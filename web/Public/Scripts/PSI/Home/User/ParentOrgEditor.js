/**
 * 自定义字段 - 上级组织机构字段
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.Home.User.ParentOrgEditor", {
  extend: "PCL.form.field.Trigger",
  alias: "widget.PSI_parent_org_editor",

  mixins: ["PSI.AFX.Mix.Common"],

  config: {
    parentItem: null,
  },

  /**
   * @override
   */
  initComponent() {
    const me = this;

    me.enableKeyEvents = true;

    me.callParent(arguments);

    me.on("keydown", (field, e) => {
      if (e.getKey() === e.BACKSPACE) {
        e.preventDefault();
        return false;
      }

      if (e.getKey() !== e.ENTER) {
        me.onTriggerClick(e);
      }
    });

    me.on("render", (p) => {
      p.getEl().on("dblclick", () => {
        me.onTriggerClick();
      });
    });
  },

  /**
   * @override
   */
  onTriggerClick(e) {
    const me = this;
    const modelName = me.buildModelName(me, "FieldModels");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "text", "fullName", "orgCode",
        "leaf", "children"]
    });

    const orgStore = PCL.create("PCL.data.TreeStore", {
      model: modelName,
      proxy: {
        type: "ajax",
        actionMethods: {
          read: "POST"
        },
        extraParams: {
          enabled: -1
        },
        url: me.URL("Home/User/allOrgs")
      }
    });

    const orgTree = PCL.create("PCL.tree.Panel", {
      cls: "PSI",
      store: orgStore,
      rootVisible: false,
      useArrows: true,
      viewConfig: {
        // loadMask: true
      },
      columns: {
        defaults: {
          flex: 1,
          sortable: false,
          menuDisabled: true,
          draggable: false
        },
        items: [{
          xtype: "treecolumn",
          text: "名称",
          dataIndex: "text",
          flex: 2
        }, {
          text: "编码",
          dataIndex: "orgCode"
        }]
      }
    });
    orgTree.on("itemdblclick", me._onOK, me);
    me.tree = orgTree;

    const wnd = PCL.create("PCL.window.Window", {
      header: false,
      modal: true,
      width: 620,
      height: 300,
      layout: "fit",
      items: [orgTree],
      buttons: [{
        text: "没有上级组织",
        cls: "PSI-Lookup-btn",
        handler: me._onNone,
        scope: me
      }, {
        text: "确定",
        cls: "PSI-Lookup-btn",
        handler: me._onOK,
        scope: me
      }, {
        text: "取消",
        cls: "PSI-Lookup-btn",
        handler() {
          wnd.close();
        }
      }]
    });
    me.wnd = wnd;
    wnd.showBy(me);
  },

  /**
   * @private
   */
  _onOK() {
    const me = this;
    const tree = me.tree;
    const item = tree.getSelectionModel().getSelection();

    if (item === null || item.length !== 1) {
      me.showInfo("没有选择上级组织");

      return;
    }

    const data = item[0].data;
    const parentItem = me.getParentItem();
    me.focus();
    parentItem?.setParentOrg?.apply(parentItem, [data]);
    me.wnd.close();
    me.focus();
  },

  /**
   * @private
   */
  _onNone() {
    const me = this;
    const parentItem = me.getParentItem();
    parentItem?.setParentOrg?.apply(parentItem, [{
      id: "",
      fullName: ""
    }]);
    me.wnd.close();
    me.focus();
  }
});
