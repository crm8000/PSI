<?php

// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2014 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
// 应用入口文件

// 检测PSI所需的PHP版本
$line = __LINE__ + 1;
if (version_compare(PHP_VERSION, '8.2.12', '<')) {
  $version = PHP_VERSION;
  $info = "PHP版本不能低于8.2.12 !
          <br/><br/>
          当前服务器部署的PHP版本是{$version}
          <br/><br/>
          ";
  $info .= "编辑 " . __FILE__ . " " . $line . "行的代码可以临时移除对PHP版本的检查";
  die($info);
}

// 开启调试模式 建议开发阶段开启 部署阶段注释或者设为false
define('APP_DEBUG', true);

// 定义应用目录
define('APP_PATH', './Application/');

// 引入ThinkPHP入口文件
require './ThinkPHP/ThinkPHP.php';
