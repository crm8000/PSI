/**
 * 选择用户
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.Home.Permission.SelectUserForm", {
  extend: "PSI.AFX.Form.EditForm",

  config: {
    idList: null, // idList是数组
    parentForm: null
  },

  width: 1100,
  height: 570,
  modal: true,
  layout: "border",

  initComponent() {
    const me = this;
    PCL.define("PSIUser_SelectUserForm", {
      extend: "PCL.data.Model",
      fields: ["id", "loginName", "code", "name", "orgFullName"]
    });

    const userStore = PCL.create("PCL.data.Store", {
      model: "PSIUser_SelectUserForm",
      autoLoad: false,
      data: []
    });

    const grid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-HL",
      header: false,
      padding: 5,
      selModel: {
        mode: "MULTI"
      },
      selType: "checkboxmodel",
      viewConfig: {
        deferEmptyText: false,
      },
      store: userStore,
      columnLines: true,
      columns: [{
        header: "用户编码",
        dataIndex: "code",
        width: 120,
        menuDisabled: true
      }, {
        header: "用户姓名",
        dataIndex: "name",
        width: 120,
        menuDisabled: true
      }, {
        header: "登录名",
        dataIndex: "loginName",
        width: 200,
        menuDisabled: true
      }, {
        header: "所属组织",
        dataIndex: "orgFullName",
        flex: 1,
        menuDisabled: true
      }]
    });

    me._mainGrid = grid;

    PCL.apply(me, {
      header: false,
      items: [{
        region: "north",
        height: 50,
        border: 0,
        html: `<h2 style='display:inline-block; margin-left:10px;margin-top:15px;color:#595959'>可以添加到当前角色的用户</h2>
                <span style='color:#8c8c8c'>已禁止登录的用户不会出现在列表中</span>`,
      }, {
        region: "center",
        layout: "fit",
        border: 0,
        items: grid
      }, {
        region: "south",
        height: 32,
        border: 0,
        items: [{
          xtype: "form",
          layout: "form",
          border: 0,
          bodyPadding: 5,
          items: [{
            id: "PSI_Permission_SelectUserForm_editUser",
            xtype: "textfield",
            labelWidth: 0,
          }]
        }]
      }],
      buttons: [{
        xtype: "displayfield",
        value: "输入人员编码、登录名、拼音字头可以过滤"
      }, "->", {
        text: "确定",
        ...PSI.Const.BTN_STYLE,
        formBind: true,
        iconCls: "PSI-button-ok",
        handler: me.onOK,
        scope: me
      }, {
        text: "取消",
        ...PSI.Const.BTN_STYLE,
        handler() {
          me.close();
        },
        scope: me
      }],
      listeners: {
        show: {
          fn: me.onWndShow,
          scope: me
        }
      }
    });

    me.callParent(arguments);
  },

  refreshMainGrid() {
    const me = this;
    const idList = me.getIdList();
    const userStore = me._mainGrid.getStore();

    const el = me.getEl() || PCL.getBody();
    el.mask("数据加载中...");
    me.ajax({
      url: me.URL("Home/Permission/selectUsers"),
      params: {
        idList: idList.join(),
        name: me.editName.getValue()
      },
      callback(options, success, response) {
        el.unmask();
        userStore.removeAll();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          userStore.add(data);
        }
      }
    });

  },

  onWndShow() {
    const me = this;

    me.editName = PCL.getCmp("PSI_Permission_SelectUserForm_editUser");

    me.applyIMEHandler(me.editName);

    me.editName.on("change", () => {
      if (me.editName._inIME) {
        return;
      }

      me.refreshMainGrid();
    });

    me.editName.focus();

    me.refreshMainGrid();
  },

  onOK() {
    const me = this;
    const grid = me._mainGrid;

    const items = grid.getSelectionModel().getSelection();
    if (items == null || items.length == 0) {
      me.showInfo("没有选择用户");

      return;
    }

    const parentForm = me.getParentForm();
    if (parentForm) {
      parentForm.setSelectedUsers.apply(parentForm, [items]);
    }

    me.close();
  }
});
