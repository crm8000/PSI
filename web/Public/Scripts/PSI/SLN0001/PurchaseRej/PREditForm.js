/**
 * 采购退货出库单 - 新增或编辑界面
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.PurchaseRej.PREditForm", {
  extend: "PSI.AFX.Form.EditForm",

  /**
   * @override
   */
  initComponent() {
    const me = this;
    me._readonly = false;
    const entity = me.getEntity();
    me.adding = entity == null;

    const action = entity == null ? "新建" : "编辑";
    const title = me.formatTitleLabel("采购退货出库单", action);

    PCL.apply(me, {
      header: false,
      padding: "0 0 0 0",
      border: 0,
      maximized: true,
      tbar: [{
        id: me.buildId(me, "dfTitle"),
        value: title, xtype: "displayfield"
      }, "->", {
        text: "选择采购入库单",
        ...PSI.Const.BTN_STYLE,
        handler: me._onSelectPWBill,
        scope: me,
        disabled: me.entity != null
      }, "-", {
        text: "保存 <span class='PSI-shortcut-DS'>Alt + S</span>",
        tooltip: me.buildTooltip("快捷键：Alt + S"),
        ...PSI.Const.BTN_STYLE,
        iconCls: "PSI-button-ok",
        handler: me._onOK,
        scope: me,
        id: me.buildId(me, "buttonSave")
      }, "-", {
        text: "取消",
        iconCls: "PSI-tb-close",
        ...PSI.Const.BTN_STYLE,
        handler() {
          if (me._readonly) {
            me.close();
            return;
          }
          me.confirm("请确认是否取消当前操作?", () => {
            me.close();
          });
        },
        scope: me,
        id: me.buildId(me, "buttonCancel")
      }, "-", {
        text: "表单通用操作指南",
        ...PSI.Const.BTN_STYLE,
        iconCls: "PSI-help",
        handler() {
          me.focus();
          window.open(me.URL("Home/Help/index?t=commBill"));
        }
      }, "-", {
        margin: "5 5 5 0",
        cls: "PSI-toolbox",
        labelWidth: 0,
        width: 90,
        emptyText: "快捷访问",
        xtype: "psi_mainmenushortcutfield"
      }],
      layout: "border",
      items: [{
        region: "center",
        layout: "fit",
        border: 0,
        bodyPadding: 10,
        items: [me.getGoodsGrid()]
      }, {
        region: "north",
        id: "editForm",
        layout: {
          type: "table",
          columns: 4,
          tableAttrs: PSI.Const.TABLE_LAYOUT_SMALL,
        },
        height: 105,
        bodyPadding: 10,
        border: 0,
        items: [{
          xtype: "hidden",
          id: me.buildId(me, "hiddenId"),
          name: "id",
          value: entity == null ? null : entity.get("id")
        }, {
          id: me.buildId(me, "editSupplier"),
          xtype: "displayfield",
          fieldLabel: "供应商",
          labelWidth: 60,
          labelAlign: "right",
          labelSeparator: "",
          colspan: 4,
          width: 830
        }, {
          id: me.buildId(me, "editSupplierId"),
          xtype: "hidden"
        }, {
          id: me.buildId(me, "editRef"),
          labelWidth: 60,
          labelAlign: "right",
          labelSeparator: "",
          fieldLabel: "单号",
          xtype: "displayfield",
          value: me.toFieldNoteText("保存后自动生成")
        }, {
          id: me.buildId(me, "editBizDT"),
          fieldLabel: "业务日期",
          labelWidth: 65,
          labelAlign: "right",
          labelSeparator: "",
          allowBlank: false,
          blankText: "没有输入业务日期",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          xtype: "datefield",
          format: "Y-m-d",
          value: new Date(),
          name: "bizDT",
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          }
        }, {
          id: me.buildId(me, "editWarehouse"),
          labelWidth: 65,
          labelAlign: "right",
          labelSeparator: "",
          fieldLabel: "出库仓库",
          xtype: "psi_warehousefield",
          fid: "2007",
          allowBlank: false,
          blankText: "没有输入出库仓库",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          }
        }, {
          id: me.buildId(me, "editBizUser"),
          labelWidth: 60,
          labelAlign: "right",
          labelSeparator: "",
          fieldLabel: "业务员",
          xtype: "psi_userfield",
          allowBlank: false,
          blankText: "没有输入业务员",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          }
        }, {
          id: me.buildId(me, "editReceivingType"),
          labelWidth: 60,
          labelAlign: "right",
          labelSeparator: "",
          fieldLabel: "收款方式",
          xtype: "combo",
          queryMode: "local",
          editable: false,
          valueField: "id",
          store: PCL.create("PCL.data.ArrayStore", {
            fields: ["id", "text"],
            data: [["0", "记应收账款"],
            ["1", "现金收款"]]
          }),
          value: "0",
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          }
        }, {
          id: me.buildId(me, "editBillMemo"),
          labelWidth: 65,
          labelAlign: "right",
          labelSeparator: "",
          fieldLabel: "备注",
          xtype: "textfield",
          listeners: {
            specialkey: {
              fn: me._onEditBillMemoSpecialKey,
              scope: me
            }
          },
          colspan: 3,
          width: 665
        }]
      }],
      listeners: {
        show: {
          fn: me._onWndShow,
          scope: me
        },
        close: {
          fn: me._onWndClose,
          scope: me
        }
      }
    });

    me.callParent(arguments);

    me.dfTitle = PCL.getCmp(me.buildId(me, "dfTitle"));

    me._keyMap = PCL.create("PCL.util.KeyMap", PCL.getBody(), {
      key: "S",
      ctrl: false,
      shift: false,
      alt: true,
      fn() {
        if (PSI.Const.msgBoxShowing) {
          return;
        }

        if (me._readonly) {
          return;
        }

        me._onOK.apply(me, []);
      },
      scope: me
    });

    me.buttonSave = PCL.getCmp(me.buildId(me, "buttonSave"));
    me.buttonCancel = PCL.getCmp(me.buildId(me, "buttonCancel"));

    me.hiddenId = PCL.getCmp(me.buildId(me, "hiddenId"));
    me.editRef = PCL.getCmp(me.buildId(me, "editRef"));
    me.editSupplierId = PCL.getCmp(me.buildId(me, "editSupplierId"));
    me.editSupplier = PCL.getCmp(me.buildId(me, "editSupplier"));
    me.editWarehouse = PCL.getCmp(me.buildId(me, "editWarehouse"));
    me.editBizUser = PCL.getCmp(me.buildId(me, "editBizUser"));
    me.editBizDT = PCL.getCmp(me.buildId(me, "editBizDT"));
    me.editReceivingType = PCL.getCmp(me.buildId(me, "editReceivingType"));
    me.editBillMemo = PCL.getCmp(me.buildId(me, "editBillMemo"));

    // AFX
    me.__editorList = [
      me.editBizDT, me.editWarehouse, me.editBizUser,
      me.editReceivingType, me.editBillMemo];
  },

  /**
   * @private
   */
  _onWndClose() {
    const me = this;

    me._keyMap.destroy();

    PCL.WindowManager.hideAll();

    // AFX
    PCL.get(window).un('beforeunload', me.__onWindowBeforeUnload);

    const parentForm = me.getParentForm();
    if (parentForm) {
      parentForm._keyMapMain.enable();
    }
  },

  /**
   * @private
   */
  _onWndShow() {
    const me = this;

    // AFX
    PCL.get(window).on('beforeunload', me.__onWindowBeforeUnload);

    const parentForm = me.getParentForm();
    if (parentForm) {
      parentForm._keyMapMain.disable();
    }

    const el = me.getEl() || PCL.getBody();
    el.mask(PSI.Const.LOADING);
    me.ajax({
      url: me.URL("SLN0001/PurchaseRej/prBillInfo"),
      params: {
        id: me.hiddenId.getValue()
      },
      callback(options, success, response) {
        el.unmask();

        if (success) {
          const data = me.decodeJSON(response.responseText);

          if (data.ref) {
            me.editRef.setValue(me.toFieldNoteText(data.ref));
            me.editSupplierId.setValue(data.supplierId);
            me.editSupplier.setValue(me.toFieldNoteText(data.supplierName + " 采购入库单单号：" + data.pwbillRef));

            me.editWarehouse.setIdValue(data.warehouseId);
            me.editWarehouse.setValue(data.warehouseName);
            me.editBillMemo.setValue(data.billMemo);
          } else {
            // 新建采购退货出库单，第一步就是选择采购入库单
            me._onSelectPWBill();
          }

          me.editBizUser.setIdValue(data.bizUserId);
          me.editBizUser.setValue(data.bizUserName);
          if (data.bizDT) {
            me.editBizDT.setValue(data.bizDT);
          }

          if (data.receivingType) {
            me.editReceivingType.setValue(data.receivingType);
          }

          me._billId = data.pwbillId;

          const store = me.getGoodsGrid().getStore();
          store.removeAll();
          if (data.items) {
            store.add(data.items);
          }

          if (data.billStatus && data.billStatus != 0) {
            me.setBillReadonly();
          }

          me.editWarehouse.focus();
        }
      }
    });
  },

  /**
   * @private
   */
  _onOK() {
    const me = this;

    if (!me._billId) {
      me.showInfo("没有选择要退货的采购入库单，无法保存数据");
      return;
    }

    PCL.getBody().mask("正在保存中...");
    me.ajax({
      url: me.URL("SLN0001/PurchaseRej/editPRBill"),
      params: {
        adding: me.adding ? "1" : "0",
        jsonStr: me.getSaveData()
      },
      callback(options, success, response) {
        PCL.getBody().unmask();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          if (data.success) {
            me.close();
            const parentForm = me.getParentForm();
            if (parentForm) {
              parentForm.refreshMainGrid(parentForm, [data.id]);
            }
            me.tip("成功保存数据", true);
          } else {
            me.showInfo(data.msg, () => {
              me.editWarehouse.focus();
            });
          }
        }
      }
    });
  },

  /**
   * @private
   */
  _onEditBillMemoSpecialKey(field, e) {
    if (e.getKey() == e.ENTER) {
      const me = this;

      if (me._readonly) {
        return;
      }

      me.getGoodsGrid().focus();
      me._cellEditing.startEdit(0, 3);
    }
  },

  /**
   * @private
   */
  getGoodsGrid() {
    const me = this;
    if (me._goodsGrid) {
      return me._goodsGrid;
    }

    const modelName = me.buildModelName(me, "PRBillDetail_EditForm");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "goodsId", "goodsCode", "goodsName",
        "goodsSpec", "unitName", "goodsCount",
        "goodsMoney", "goodsPrice", "rejCount", "rejPrice",
        {
          name: "rejMoney",
          type: "float"
        }, "memo", "rejPriceWithTax", {
          name: "rejMoneyWithTax",
          type: "float"
        }, "goodsMoneyWithTax", "goodsPriceWithTax", "taxRate"]
    });
    const store = PCL.create("PCL.data.Store", {
      autoLoad: false,
      model: modelName,
      data: []
    });

    me._cellEditing = PCL.create("PSI.UX.CellEditing", {
      clicksToEdit: 1,
      listeners: {
        edit: {
          fn: me._onCellEditingAfterEdit,
          scope: me
        }
      }
    });

    me._goodsGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-EF",
      viewConfig: {
        enableTextSelection: true,
        markDirty: !me.adding
      },
      features: [{
        ftype: "summary"
      }],
      plugins: [me._cellEditing],
      columnLines: true,
      columns: [PCL.create("PCL.grid.RowNumberer", {
        text: "#",
        width: 30
      }), {
        header: "物料编码",
        dataIndex: "goodsCode",
        menuDisabled: true,
        sortable: false,
        draggable: false
      }, {
        header: "品名/规格型号",
        dataIndex: "goodsName",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        width: 330,
        renderer(value, metaData, record) {
          return record.get("goodsName") + " " + record.get("goodsSpec");
        }
      }, {
        header: "退货数量",
        dataIndex: "rejCount",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        align: "right",
        width: 80,
        editor: {
          xtype: "numberfield",
          allowDecimals: PSI.Const.GC_DEC_NUMBER > 0,
          decimalPrecision: PSI.Const.GC_DEC_NUMBER,
          minValue: 0,
          hideTrigger: true
        }
      }, {
        header: "单位",
        dataIndex: "unitName",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        width: 50,
        align: "center"
      }, {
        header: "退货单价(含税)",
        dataIndex: "rejPriceWithTax",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 100,
        editor: {
          xtype: "numberfield",
          hideTrigger: true
        },
        summaryRenderer() {
          return "金额合计";
        }
      }, {
        header: "退货金额(含税)",
        dataIndex: "rejMoneyWithTax",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 100,
        editor: {
          xtype: "numberfield",
          hideTrigger: true
        },
        summaryType: "sum"
      }, {
        header: "退货单价(不含税)",
        dataIndex: "rejPrice",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 110,
        editor: {
          xtype: "numberfield",
          hideTrigger: true
        }
      }, {
        header: "退货金额(不含税)",
        dataIndex: "rejMoney",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 110,
        editor: {
          xtype: "numberfield",
          hideTrigger: true
        },
        summaryType: "sum"
      }, {
        header: "税率(%)",
        dataIndex: "taxRate",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        align: "right",
        xtype: "numbercolumn",
        format: "#",
        width: 60
      }, {
        header: "原采购数量",
        dataIndex: "goodsCount",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        align: "right",
        width: 80
      }, {
        header: "原采购单价(不含税)",
        dataIndex: "goodsPrice",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 120
      }, {
        header: "原采购金额(不含税)",
        dataIndex: "goodsMoney",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 120
      }, {
        header: "原采购单价(含税)",
        dataIndex: "goodsPriceWithTax",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 110
      }, {
        header: "原采购金额(含税)",
        dataIndex: "goodsMoneyWithTax",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 110
      }, {
        header: "备注",
        dataIndex: "memo",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        editor: {
          xtype: "textfield"
        }
      }],
      store: store,
      listeners: {
        cellclick() {
          return !me._readonly;
        }
      }
    });

    return me._goodsGrid;
  },

  /**
   * @private
   */
  _onCellEditingAfterEdit(editor, e) {
    const me = this;

    const fieldName = e.field;
    const goods = e.record;
    const oldValue = e.originalValue;

    if (fieldName == "rejMoney") {
      if (goods.get(fieldName) != (new Number(oldValue)).toFixed(2)) {
        me.calcPrice(goods);
      }

      e.rowIdx += 1;
      me.getGoodsGrid().getSelectionModel().select(e.rowIdx);
      me._cellEditing.startEdit(e.rowIdx, 1);
    } else if (fieldName == "rejCount") {
      if (goods.get(fieldName) != oldValue) {
        me.calcMoney(goods);
      }
    } else if (fieldName == "rejPrice") {
      if (goods.get(fieldName) != (new Number(oldValue)).toFixed(2)) {
        me.calcMoney(goods);
      }
    } else if (fieldName == "rejPriceWithTax") {
      if (goods.get(fieldName) != (new Number(oldValue)).toFixed(2)) {
        me.calcMoney2(goods);
      }
    } else if (fieldName == "rejMoneyWithTax") {
      if (goods.get(fieldName) != (new Number(oldValue)).toFixed(2)) {
        me.calcPrice2(goods);
      }
    }
  },

  // 因为退货数量或不含税单价变化
  calcMoney(goods) {
    if (!goods) {
      return;
    }

    var rejCount = goods.get("rejCount");
    var rejPrice = goods.get("rejPrice");
    var taxRate = goods.get("taxRate") / 100;
    goods.set("rejMoney", rejCount * rejPrice);
    rejPriceWithTax = rejPrice * (1 + taxRate);
    goods.set("rejPriceWithTax", rejPriceWithTax);
    goods.set("rejMoneyWithTax", rejCount * rejPriceWithTax);
  },

  // 因为含税单价变化
  calcMoney2(goods) {
    if (!goods) {
      return;
    }

    var rejCount = goods.get("rejCount");
    var rejPriceWithTax = goods.get("rejPriceWithTax");
    var taxRate = goods.get("taxRate") / 100;

    goods.set("rejMoneyWithTax", rejCount * rejPriceWithTax);
    rejPrice = rejPriceWithTax / (1 + taxRate);
    goods.set("rejPrice", rejPrice);
    goods.set("rejMoney", rejCount * rejPrice);
  },

  // 因不含税金额变化
  calcPrice(goods) {
    if (!goods) {
      return;
    }
    var rejCount = goods.get("rejCount");
    var rejMoney = goods.get("rejMoney");
    var taxRate = goods.get("taxRate") / 100;
    var rejMoneyWithTax = rejMoney * (1 + taxRate);
    goods.set("rejMoneyWithTax", rejMoneyWithTax);
    if (rejCount && rejCount != 0) {
      goods.set("rejPrice", rejMoney / rejCount);
      goods.set("rejPriceWithTax", rejMoneyWithTax / rejCount);
    }
  },

  // 因含税金额变化
  calcPrice2(goods) {
    if (!goods) {
      return;
    }
    var rejCount = goods.get("rejCount");
    var rejMoneyWithTax = goods.get("rejMoneyWithTax");
    var taxRate = goods.get("taxRate") / 100;

    var rejMoney = rejMoneyWithTax / (1 + taxRate);
    goods.set("rejMoney", rejMoney);
    if (rejCount && rejCount != 0) {
      goods.set("rejPrice", rejMoney / rejCount);
      goods.set("rejPriceWithTax", rejMoneyWithTax / rejCount);
    }
  },

  /**
   * @privates
   */
  getSaveData() {
    const me = this;

    const result = {
      id: me.hiddenId.getValue(),
      bizDT: PCL.Date.format(me.editBizDT.getValue(), "Y-m-d"),
      warehouseId: me.editWarehouse.getIdValue(),
      bizUserId: me.editBizUser.getIdValue(),
      receivingType: me.editReceivingType.getValue(),
      billMemo: me.editBillMemo.getValue(),
      pwBillId: me._billId,
      items: []
    };

    const store = me.getGoodsGrid().getStore();
    for (let i = 0; i < store.getCount(); i++) {
      const item = store.getAt(i);
      result.items.push({
        id: item.get("id"),
        goodsId: item.get("goodsId"),
        goodsCount: item.get("goodsCount"),
        goodsPrice: item.get("goodsPrice"),
        rejCount: item.get("rejCount"),
        rejPrice: item.get("rejPrice"),
        rejMoney: item.get("rejMoney"),
        memo: item.get("memo"),
        taxRate: item.get("taxRate"),
        rejPriceWithTax: item.get("rejPriceWithTax"),
        rejMoneyWithTax: item.get("rejMoneyWithTax"),
        goodsPriceWithTax: item.get("goodsPriceWithTax"),
        goodsMoneyWithTax: item.get("goodsMoneyWithTax")
      });
    }

    return me.encodeJSON(result);
  },

  /**
   * @private
   */
  _onSelectPWBill() {
    const me = this;

    const form = PCL.create("PSI.SLN0001.PurchaseRej.PRSelectPWBillForm", {
      parentForm: me,
      renderTo: PSI.Const.RENDER_TO(),
    });
    form.show();
  },

  /**
   * PSI.SLN0001.PurchaseRej.PRSelectPWBillForm中调用本方法
   * 
   * @public
   */
  getPWBillInfo(id) {
    const me = this;
    me._billId = id;
    const el = me.getEl() || PCL.getBody();
    el.mask(PSI.Const.LOADING);
    me.ajax({
      url: me.URL("SLN0001/PurchaseRej/getPWBillInfoForPRBill"),
      params: {
        id: id
      },
      callback(options, success, response) {
        if (success) {
          const data = PCL.JSON.decode(response.responseText);
          me.editSupplier.setValue(me.toFieldNoteText(data.supplierName + " 采购入库单单号: " + data.ref));
          me.editSupplierId.setValue(data.supplierId);
          me.editWarehouse.setIdValue(data.warehouseId);
          me.editWarehouse.setValue(data.warehouseName);

          const store = me.getGoodsGrid().getStore();
          store.removeAll();
          store.add(data.items);
        }

        el.unmask();
      }
    });
  },

  /**
   * @private
   */
  setBillReadonly() {
    const me = this;
    me._readonly = true;
    me.dfTitle.setValue(me.formatTitleLabel("采购退货出库单", "查看"));

    me.buttonSave.setDisabled(true);
    me.buttonCancel.setText("关闭");
    me.editWarehouse.setReadOnly(true);
    me.editBizUser.setReadOnly(true);
    me.editBizDT.setReadOnly(true);
    me.editReceivingType.setReadOnly(true);
    me.editBillMemo.setReadOnly(true);
  }
});
