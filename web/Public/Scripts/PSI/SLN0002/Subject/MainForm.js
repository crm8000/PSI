/**
 * 会计科目 - 主界面
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0002.Subject.MainForm", {
  extend: "PSI.AFX.Form.MainForm",

  /**
   * 初始化组件
   * 
   * @override
   */
  initComponent() {
    const me = this;

    PCL.apply(me, {
      items: [{
        tbar: me.getToolbarCmp(),
        id: me.buildId(me, "panelQueryCmp"),
        animCollapse: false,
        region: "north",
        border: 0,
        height: 80,
        bodyPadding: "10 0 0 0",
        header: false,
        collapsible: true,
        collapseMode: "mini",
        layout: {
          type: "table",
          columns: 5
        },
        bodyCls: "PSI-Query-Panel",
        items: me.getQueryCmp()
      }, {
        region: "center",
        layout: "border",
        ...PSI.Const.BODY_PADDING,
        border: 0,
        items: [{
          id: me.buildId(me, "companyPanel"),
          animCollapse: false,
          region: "west",
          width: 300,
          layout: "fit",
          border: 0,
          split: true,
          items: [me.getCompanyGrid()]
        }, {
          region: "center",
          xtype: "panel",
          layout: "border",
          border: 0,
          items: [{
            region: "north",
            height: "40%",
            layout: "fit",
            split: true,
            collapsible: true,
            header: false,
            border: 0,
            id: me.buildId(me, "MainPanel"),
            animCollapse: false,
            items: me.getMainGrid()
          }, {
            region: "center",
            layout: "border",
            bodyStyle: "border-width:0px",
            cls: "PSI-Normal",
            tbar: me.getFmtToolbarCmp(),
            items: [{
              region: "west",
              id: me.buildId(me, "FmtPropPanel"),
              animCollapse: false,
              width: 310,
              layout: "fit",
              split: true,
              border: 0,
              items: me.getFmtPropGrid()
            }, {
              region: "center",
              border: 0,
              layout: "fit",
              items: me.getFmtColsGrid()
            }]
          }]
        }]
      }]
    });

    me.callParent(arguments);

    me.panelQueryCmp = PCL.getCmp(me.buildId(me, "panelQueryCmp"));

    me._keyMapMain = PCL.create("PCL.util.KeyMap", PCL.getBody(), [{
      key: "H",
      ctrl: false,
      shift: false,
      alt: true,
      fn() {
        if (PSI.Const.msgBoxShowing) {
          return;
        }

        const panel = me.panelQueryCmp;
        if (panel.getCollapsed()) {
          panel.expand();
        } else {
          panel.collapse();
        };
      },
      scope: me
    },
    ]);

    me.refreshCompanyGrid();
  },

  /**
   * @private
   */
  getToolbarCmp() {
    const me = this;
    return [{
      iconCls: "PSI-tb-new",
      text: "初始化国家标准科目",
      ...PSI.Const.BTN_STYLE,
      handler: me._onInitStandardSubject,
      scope: me
    }, "-", {
      iconCls: "PSI-tb-new-entity",
      text: "新建科目",
      ...PSI.Const.BTN_STYLE,
      handler: me._onAddSubject,
      scope: me
    }, {
      text: "编辑科目",
      ...PSI.Const.BTN_STYLE,
      handler: me._onEditSubject,
      scope: me
    }, {
      text: "删除科目",
      ...PSI.Const.BTN_STYLE,
      handler: me._onDeleteSubject,
      scope: me
    }, "-", {
      iconCls: "PSI-tb-help",
      text: "指南",
      ...PSI.Const.BTN_STYLE,
      handler() {
        me.focus();
        window.open(me.URL("/Home/Help/index?t=subject"));
      }
    }, "-", {
      iconCls: "PSI-tb-close",
      text: "关闭",
      ...PSI.Const.BTN_STYLE,
      handler() {
        me.focus();
        me.closeWindow();
      }
    }, {
      // 空容器，只是为了撑高工具栏
      xtype: "container", height: 28,
      items: []
    }].concat(me.getShortcutCmp());
  },

  /**
   * @private
   */
  getQueryCmp() {
    const me = this;
    return [{
      xtype: "container",
      height: 26,
      html: `<h2 style='margin-left:20px;margin-top:0px;color:#595959;display:inline-block'>会计科目</h2>
              &nbsp;&nbsp;<span style='color:#8c8c8c'>管理各独立核算组织机构的会计科目和账薄样式</span>
              <div style='float:right;display:inline-block;margin:0px 0px 0px 20px;border-left:1px solid #e5e6e8;height:26px'>&nbsp;</div>
              `
    }, {
      xtype: "container",
      items: [{
        xtype: "button",
        text: "隐藏工具栏 <span class='PSI-shortcut-DS'>Alt + H</span>",
        cls: "PSI-Query-btn3",
        width: 140,
        height: 26,
        iconCls: "PSI-button-hide",
        margin: "5 0 0 20",
        handler() {
          me.panelQueryCmp.collapse();
        },
        scope: me
      }]
    }]
  },

  /**
   * 快捷访问
   * 
   * @private
   */
  getShortcutCmp() {
    return ["->",
      {
        cls: "PSI-Shortcut-Cmp",
        labelWidth: 0,
        emptyText: "快捷访问",
        xtype: "psi_mainmenushortcutfield",
        width: 90
      }
    ];
  },

  /**
   * @private
   */
  getFmtToolbarCmp() {
    const me = this;
    return [{
      iconCls: "PSI-tb-new-subentity",
      text: "初始化科目的标准账样",
      ...PSI.Const.BTN_STYLE,
      handler: me._onInitFmt,
      scope: me
    }, "-", {
      text: "新建账样列",
      ...PSI.Const.BTN_STYLE,
      handler: me._onAddFmtCol,
      scope: me
    }, {
      text: "编辑账样列",
      ...PSI.Const.BTN_STYLE,
      handler: me._onEditFmtCol,
      scope: me
    }, {
      text: "删除账样列",
      ...PSI.Const.BTN_STYLE,
      handler: me._onDeleteFmtCol,
      scope: me
    }, "-", {
      text: "设置账样列显示次序和宽度",
      ...PSI.Const.BTN_STYLE,
      handler: me._onEditFmtColShowOrder,
      scope: me
    }, {
      // 空容器，只是为了撑高工具栏
      xtype: "container", height: 30,
      items: []
    }];
  },

  /**
   * @private
   */
  refreshCompanyGrid() {
    const me = this;
    const el = PCL.getBody();
    const store = me.getCompanyGrid().getStore();
    el.mask(PSI.Const.LOADING);
    const r = {
      url: me.URL("SLN0002/Subject/companyList"),
      callback(options, success, response) {
        store.removeAll();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          store.add(data);
          if (store.getCount() > 0) {
            me.getCompanyGrid().getSelectionModel().select(0);
          }
        }

        el.unmask();
      }
    };
    me.ajax(r);
  },

  /**
   * @private
   */
  getCompanyGrid() {
    const me = this;
    if (me._companyGrid) {
      return me._companyGrid;
    }

    const modelName = me.buildModelName(me, "Company");

    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "code", "name", "orgType"]
    });

    me._companyGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-FC",
      header: {
        height: 30,
        title: me.formatGridHeaderTitle(me.toTitleKeyWord("独立核算的组织机构"))
      },
      viewConfig: {
        enableTextSelection: true
      },
      tools: [{
        type: "close",
        handler() {
          PCL.getCmp(me.buildId(me, "companyPanel")).collapse();
        }
      }],
      columnLines: true,
      columns: [{
        header: "编码",
        dataIndex: "code",
        menuDisabled: true,
        sortable: false,
        width: 70
      }, {
        header: "组织机构名称",
        dataIndex: "name",
        width: 200,
        menuDisabled: true,
        sortable: false,
        renderer(value) {
          return me.toAutoWrap(value);
        }
      }, {
        header: "组织机构性质",
        dataIndex: "orgType",
        width: 100,
        menuDisabled: true,
        sortable: false
      }],
      store: PCL.create("PCL.data.Store", {
        model: modelName,
        autoLoad: false,
        data: []
      }),
      listeners: {
        select: {
          fn: me._onCompanyGridSelect,
          scope: me
        }
      }
    });
    return me._companyGrid;
  },

  /**
   * @private
   */
  _onCompanyGridSelect() {
    const me = this;
    me.refreshMainGrid();
  },

  /**
   * @public
   */
  refreshMainGrid(id) {
    const me = this;

    me.getFmtPropGrid().setTitle("账样属性");
    me.getFmtPropGrid().getStore().removeAll();
    me.getFmtColsGrid().setTitle("账样列");
    me.getFmtColsGrid().getStore().removeAll();

    me.getMainGrid().setTitle(me.formatGridHeaderTitle("会计科目"));
    const item = me.getCompanyGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      return;
    }

    const company = item[0];
    const title = `${me.toTitleKeyWord(company.get("name"))} - 会计科目`;
    me.getMainGrid().setTitle(me.formatGridHeaderTitle(title));

    me._lastSubjectId = id;

    const store = me.getMainGrid().getStore();
    store.load();
  },

  /**
   * @private
   */
  _onAddSubject() {
    const me = this;
    const item = me.getCompanyGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择公司");
      return;
    }

    const company = item[0];

    const form = PCL.create("PSI.SLN0002.Subject.EditForm", {
      parentForm: me,
      company,
      renderTo: PSI.Const.RENDER_TO(),
    });
    form.show();
  },

  /**
   * @private
   */
  _onEditSubject() {
    const me = this;
    let item = me.getCompanyGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择公司");
      return;
    }

    const company = item[0];

    item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择要编辑的科目");
      return;
    }

    const subject = item[0];
    if (!subject.get("id")) {
      me.showInfo("没有获得科目id，请刷新界面");
      return;
    }

    const form = PCL.create("PSI.SLN0002.Subject.EditForm", {
      parentForm: me,
      company,
      entity: subject,
      renderTo: PSI.Const.RENDER_TO(),
    });
    form.show();
  },

  /**
   * @private
   */
  _onDeleteSubject() {
    const me = this;
    let item = me.getCompanyGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择公司");
      return;
    }

    item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择要删除的科目");
      return;
    }

    const subject = item[0];
    if (!subject.get("id")) {
      me.showInfo("没有获得科目id，请刷新界面");
      return;
    }

    const code = subject.get("code");
    if (!code) {
      me.showInfo("没有获得科目码，请刷新界面");
      return;
    }
    if (code.length == 4) {
      me.showInfo("一级科目不能删除");
      return;
    }

    const info = `请确认是否删除科目: <span style='color:red'>${code}</span>?`;
    const funcConfirm = () => {
      const el = PCL.getBody();
      el.mask("正在删除中...");
      const r = {
        url: me.URL("SLN0002/Subject/deleteSubject"),
        params: {
          id: subject.get("id")
        },
        callback(options, success, response) {
          el.unmask();

          if (success) {
            const data = me.decodeJSON(response.responseText);
            if (data.success) {
              me.tip("成功完成删除操作");
              me._onCompanyGridSelect();
            } else {
              me.showInfo(data.msg);
            }
          } else {
            me.showInfo("网络错误");
          }
        }
      };
      me.ajax(r);
    };

    me.confirm(info, funcConfirm);
  },

  /**
   * @private
   */
  getMainGrid() {
    const me = this;
    if (me._mainGrid) {
      return me._mainGrid;
    }

    const modelName = me.buildModelName(me, "SubjectModel");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "code", "name", "category", "leaf",
        "children", "isLeaf", "balanceDir"]
    });

    const store = PCL.create("PCL.data.TreeStore", {
      model: modelName,
      proxy: {
        type: "ajax",
        actionMethods: {
          read: "POST"
        },
        url: me.URL("SLN0002/Subject/subjectList")
      },
      listeners: {
        beforeload: {
          fn() {
            PCL.apply(store.proxy.extraParams, me.getQueryParamForSubject());
          },
          scope: me
        },

        load: {
          fn() {
            me.getMainGrid().getSelectionModel().select(me.getMainGrid().getRootNode());
            const node = store.getNodeById(me._lastSubjectId);
            if (node) {
              me.getMainGrid().getSelectionModel().select(node);
            }
          },
          scope: me,
        }
      }
    });

    me._mainGrid = PCL.create("PCL.tree.Panel", {
      cls: "PSI",
      header: {
        height: 30,
        title: me.formatGridHeaderTitle("会计科目")
      },
      store,
      rootVisible: false,
      useArrows: true,
      viewConfig: {
        loadMask: true,
        enableTextSelection: true,
      },
      emptyText: me.toEmptyText("尚未初始化国家标准科目"),
      columnLines: true,
      columns: {
        defaults: {
          sortable: false,
          menuDisabled: true,
          draggable: false
        },
        items: [{
          xtype: "treecolumn",
          text: "科目码",
          dataIndex: "code",
          width: 200
        }, {
          text: "科目名称",
          dataIndex: "name",
          width: 500
        }, {
          text: "分类",
          dataIndex: "category",
          width: 100,
          renderer(value) {
            if (value == 1) {
              return "资产";
            } else if (value == 2) {
              return "负债";
            } else if (value == 4) {
              return "所有者权益";
            } else if (value == 5) {
              return "成本";
            } else if (value == 6) {
              return "损益";
            } else {
              return "";
            }
          }
        }, {
          text: "末级科目",
          dataIndex: "isLeaf",
          align: "center",
          width: 70
        }, {
          text: "余额方向",
          dataIndex: "balanceDir",
          width: 90
        }]
      },
      listeners: {
        select: {
          fn(rowModel, record) {
            me._onMainGridItemSelect(record);
          },
          scope: me
        },
        beforeitemdblclick: {
          fn() {
            me._onEditSubject.apply(me, []);
            return false;
          },
          scope: me
        }
      }
    });

    return me._mainGrid;
  },

  /**
   * @private
   */
  getQueryParamForSubject() {
    const me = this;
    const item = me.getCompanyGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      return {};
    }

    const company = item[0];

    const result = {
      companyId: company.get("id")
    };

    return result;
  },

  /**
   * 初始化国家标准科目
   * 
   * @private
   */
  _onInitStandardSubject() {
    const me = this;
    const item = me.getCompanyGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("请选择要初始化科目的组织机构");
      return;
    }

    const company = item[0];

    const confirmFunc = () => {
      const el = PCL.getBody();
      el.mask("正在操作中...");
      const r = {
        url: me.URL("SLN0002/Subject/initStandardSubject"),
        params: {
          id: company.get("id")
        },
        callback(options, success, response) {
          el.unmask();

          if (success) {
            const data = me.decodeJSON(response.responseText);
            if (data.success) {
              me.tip("成功完成初始化操作", true);
              me._onCompanyGridSelect();
            } else {
              me.showInfo(data.msg);
            }
          } else {
            me.showInfo("网络错误");
          }
        }
      };

      me.ajax(r);
    };

    const info = `请确认是否初始化<span style='color:red'>${company.get("name")}</span>的标准会计科目?`;
    me.confirm(info, confirmFunc);
  },

  /**
   * @private
   */
  getFmtPropGrid() {
    const me = this;
    if (me._fmtPropGrid) {
      return me._fmtPropGrid;
    }

    const modelName = me.buildModelName(me, "FMTProp");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "propName", "propValue"]
    });

    me._fmtPropGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-HL",
      header: {
        height: 30,
        title: me.formatGridHeaderTitle("账样属性")
      },
      viewConfig: {
        enableTextSelection: true
      },
      tools: [{
        type: "close",
        handler() {
          PCL.getCmp(me.buildId(me, "FmtPropPanel")).collapse();
        }
      }],
      columnLines: true,
      columns: {
        defaults: {
          menuDisabled: true,
          sortable: false
        },
        items: [{
          header: "属性名称",
          dataIndex: "propName",
          width: 90
        }, {
          header: "属性值",
          dataIndex: "propValue",
          width: 200
        }]
      },
      store: PCL.create("PCL.data.Store", {
        model: modelName,
        autoLoad: false,
        data: []
      })
    });

    return me._fmtPropGrid;
  },

  /**
   * 初始化科目的标准账样
   * 
   * @private
   */
  _onInitFmt() {
    const me = this;
    let item = me.getCompanyGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择公司");
      return;
    }

    const company = item[0];

    item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择要初始化账样的科目");
      return;
    }

    const subject = item[0];

    const info = `请确认是否初始化科目<span style='color:red'>${subject.get("code")}</span>的账样?`;
    const funcConfirm = () => {
      const el = PCL.getBody();
      el.mask("正在初始化中...");
      const r = {
        url: me.URL("SLN0002/Subject/initFmt"),
        params: {
          id: subject.get("id"),
          companyId: company.get("id")
        },
        callback(options, success, response) {
          el.unmask();

          if (success) {
            const data = me.decodeJSON(response.responseText);
            if (data.success) {
              me.tip("成功完成操作", true);
              me.refreshFmtPropGrid();
              me.refreshFmtColsGrid();
            } else {
              me.showInfo(data.msg);
            }
          } else {
            me.showInfo("网络错误");
          }
        }
      };
      me.ajax(r);
    };

    me.confirm(info, funcConfirm);
  },

  /**
   * @private
   */
  getFmtColsGrid() {
    const me = this;
    if (me._fmtColsGrid) {
      return me._fmtColsGrid;
    }

    const modelName = me.buildModelName(me, "FMTCols");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "showOrder", "caption", "fieldName", "sysCol", "sysColRaw",
        "fieldType", "fieldLength", "fieldDecimal",
        "voucherInput", "voucherInputShowOrder", "codeTableName",
        "voucherInputXtype", "voucherInputColspan", "voucherInputWidth",
        "subAccLevel", "colWidth"]
    });

    me._fmtColsGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-LC",
      header: {
        height: 30,
        title: me.formatGridHeaderTitle("账样列")
      },
      viewConfig: {
        enableTextSelection: true
      },
      columnLines: true,
      tools: [{
        itemId: "max-btn",
        type: "maximize",
        callback(panel) {
          panel.down("#max-btn").hide();
          panel.down("#restore-btn").show();

          PCL.getCmp(me.buildId(me, "companyPanel")).collapse();
          PCL.getCmp(me.buildId(me, "FmtPropPanel")).collapse();
          PCL.getCmp(me.buildId(me, "MainPanel")).collapse();
          me.panelQueryCmp.collapse();
        },
      }, {
        itemId: "restore-btn",
        type: "restore",
        hidden: true,
        callback(panel) {
          panel.down("#max-btn").show();
          panel.down("#restore-btn").hide();

          PCL.getCmp(me.buildId(me, "companyPanel")).expand();
          PCL.getCmp(me.buildId(me, "FmtPropPanel")).expand();
          PCL.getCmp(me.buildId(me, "MainPanel")).expand();
          me.panelQueryCmp.expand();
        },
      }],
      columns: {
        defaults: {
          menuDisabled: true,
          sortable: false
        },
        items: [{
          header: "#",
          dataIndex: "showOrder",
          width: 50,
          locked: true,
          tooltip: me.buildTooltip("账样列显示次序"),
        }, {
          header: "列标题",
          dataIndex: "caption",
          width: 200,
          locked: true,
        }, {
          header: "系统固有",
          align: "center",
          dataIndex: "sysCol",
          width: 90
        }, {
          header: "数据库字段名称",
          dataIndex: "fieldName",
          width: 150
        }, {
          header: "字段类型",
          dataIndex: "fieldType",
          width: 100
        }, {
          header: "字段长度",
          dataIndex: "fieldLength",
          width: 100
        }, {
          header: "字段小数位数",
          dataIndex: "fieldDecimal",
          width: 100
        }, {
          header: "凭证录入次序",
          dataIndex: "voucherInputShowOrder",
          width: 100
        }, {
          header: "凭证录入方式",
          dataIndex: "voucherInput",
          width: 100
        }, {
          header: "凭证录入控件",
          dataIndex: "voucherInputXtype",
          width: 100,
          renderer(value) {
            return value ? `<span data-qtip="${value}">${value}</span>` : "";
          },
        }, {
          header: "凭证录入控件列占位",
          dataIndex: "voucherInputColspan",
          width: 140
        }, {
          header: "凭证录入控件宽度",
          dataIndex: "voucherInputWidth",
          width: 130
        }, {
          header: "码表表名",
          dataIndex: "codeTableName",
          width: 120,
          renderer(value) {
            return value ? `<span data-qtip="${value}">${value}</span>` : "";
          },
        }, {
          header: "子账簿层级",
          dataIndex: "subAccLevel",
          width: 90
        }, {
          header: "列宽度(px)",
          dataIndex: "colWidth",
          width: 80,
          tooltip: me.buildTooltip("该列在账簿表格中的显示宽度"),
        }]
      },
      store: PCL.create("PCL.data.Store", {
        model: modelName,
        autoLoad: false,
        data: []
      }),
      listeners: {
        itemdblclick: {
          fn: me._onEditFmtCol,
          scope: me
        }
      }
    });

    return me._fmtColsGrid;
  },

  /**
   * @private
   */
  _onMainGridItemSelect(record) {
    const me = this;

    if (!record) {
      me.getFmtPropGrid().setTitle("账样属性");
      me.getFmtColsGrid().setTitle("账样列");
      return;
    }

    const code = record.get("code");
    const name = record.get("name");
    let title = `<span class='PSI-title-keyword'>${code} ${name}</span> - 账样属性`;
    me.getFmtPropGrid().setTitle(title);
    title = `<span class='PSI-title-keyword'>${code} ${name}</span> - 账样列`;
    me.getFmtColsGrid().setTitle(title);

    me.refreshFmtPropGrid();
    me.refreshFmtColsGrid();
  },

  /**
   * @private
   */
  refreshFmtPropGrid() {
    const me = this;
    let item = me.getCompanyGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      return;
    }
    const company = item[0];

    item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      return;
    }
    const subject = item[0];

    const grid = me.getFmtPropGrid();
    const el = grid.getEl();
    el && el.mask(PSI.Const.LOADING);

    const r = {
      url: me.URL("SLN0002/Subject/fmtPropList"),
      params: {
        id: subject.get("id"),
        companyId: company.get("id")
      },
      callback(options, success, response) {
        const store = grid.getStore();

        store.removeAll();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          store.add(data);
        }

        el && el.unmask();
      }
    };
    me.ajax(r);
  },

  /**
   * @private
   */
  refreshFmtColsGrid(id) {
    const me = this;
    let item = me.getCompanyGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      return;
    }
    const company = item[0];

    item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      return;
    }
    const subject = item[0];

    const grid = me.getFmtColsGrid();
    const el = grid.getEl();
    el?.mask(PSI.Const.LOADING);

    const r = {
      url: me.URL("SLN0002/Subject/fmtColsList"),
      params: {
        id: subject.get("id"),
        companyId: company.get("id")
      },
      callback(options, success, response) {
        const store = grid.getStore();

        store.removeAll();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          store.add(data);

          if (store.getCount() > 0) {
            if (id) {
              const r = store.findExact("id", id);
              if (r != -1) {
                grid.getSelectionModel().select(r);
              }
            } else {
              // grid.getSelectionModel().select(0);
            }
          }
        }

        el?.unmask();
      }
    };
    me.ajax(r);
  },

  /**
   * @private
   */
  _onAddFmtCol() {
    const me = this;

    let item = me.getCompanyGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择公司");
      return;
    }

    const company = item[0];

    item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择科目");
      return;
    }

    const subject = item[0];

    if (me.getFmtPropGrid().getStore().getCount() == 0) {
      me.showInfo("还没有初始化标准账样");
      return;
    }

    const form = PCL.create("PSI.SLN0002.Subject.FmtColEditForm", {
      parentForm: me,
      company: company,
      subject: subject,
      renderTo: PSI.Const.RENDER_TO(),
    });
    form.show();
  },

  /**
   * @private
   */
  _onEditFmtCol() {
    const me = this;

    let item = me.getCompanyGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择公司");
      return;
    }
    const company = item[0];

    item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择科目");
      return;
    }
    const subject = item[0];

    if (me.getFmtPropGrid().getStore().getCount() == 0) {
      me.showInfo("还没有初始化标准账样");
      return;
    }

    item = me.getFmtColsGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择要编辑的账样列");
      return;
    }
    const entity = item[0];

    const sysCol = entity.get("sysColRaw") == 1;
    if (sysCol) {
      me.showInfo(`<span style='color:red'>${entity.get("caption")}</span> 是标准账样字段，不能编辑`);
      return;
    }

    const form = PCL.create("PSI.SLN0002.Subject.FmtColEditForm", {
      parentForm: me,
      company,
      subject,
      entity,
      renderTo: PSI.Const.RENDER_TO(),
    });
    form.show();
  },

  /**
   * @private
   */
  _onDeleteFmtCol() {
    const me = this;
    const item = me.getFmtColsGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择要删除的账样列");
      return;
    }

    const fmtCol = item[0];
    const sysCol = fmtCol.get("sysColRaw") == 1;
    if (sysCol) {
      me.showInfo(`<span style='color:red'>${fmtCol.get("caption")}</span> 是标准账样字段，不能删除`);
      return;
    }

    const info = `请确认是否删除账样列: <span style='color:red'>${fmtCol.get("caption")}</span> ?
                  <br /><br />当前操作只删除账样列的元数据，数据库表的字段不会删除`;
    const funcConfirm = () => {
      const el = PCL.getBody();
      el.mask("正在删除中...");
      const r = {
        url: me.URL("SLN0002/Subject/deleteFmtCol"),
        params: {
          id: fmtCol.get("id")
        },
        callback(options, success, response) {
          el.unmask();

          if (success) {
            const data = me.decodeJSON(response.responseText);
            if (data.success) {
              me.tip("成功完成删除操作", true);
              me.refreshFmtColsGrid();
            } else {
              me.showInfo(data.msg);
            }
          } else {
            me.showInfo("网络错误");
          }
        }
      };
      me.ajax(r);
    };

    me.confirm(info, funcConfirm);
  },

  /**
   * @private
   */
  _onEditFmtColShowOrder() {
    const me = this;
    let item = me.getCompanyGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择公司");
      return;
    }

    item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择科目");
      return;
    }
    const subject = item[0];

    const store = me.getFmtPropGrid().getStore();
    if (store.getCount() == 0) {
      me.showInfo("还没有初始化标准账样");
      return;
    }

    const form = PCL.create("PSI.SLN0002.Subject.FmtColShowOrderEditForm", {
      parentForm: me,
      entity: subject,
      renderTo: PSI.Const.RENDER_TO(),
    });
    form.show();
  }
});
