# SLN0001 - 标准进销存 - 码表


# ------------------------------------------------------------------------------
# 码表分类
# ------------------------------------------------------------------------------

# 仓库
DELETE FROM t_code_table_category where id = '5558D8FD-07F4-11ED-B3DF-E86A641ED142';
INSERT INTO t_code_table_category(id, code, name, parent_id, is_system, sln_code)
VALUES ('5558D8FD-07F4-11ED-B3DF-E86A641ED142', 'SLN0001.01', '仓库', NULL, 1, 'SLN0001');

# 物料颜色
DELETE FROM t_code_table_category where id = 'EB423346-A110-11ED-877A-E86A641ED142';
INSERT INTO t_code_table_category(id, code, name, parent_id, is_system, sln_code)
VALUES ('EB423346-A110-11ED-877A-E86A641ED142', 'SLN0001.02', '物料', NULL, 1, 'SLN0001');

# 供应商
DELETE FROM t_code_table_category where id = '1289B469-B761-11EF-81D4-E86A641ED142';
INSERT INTO t_code_table_category(id, code, name, parent_id, is_system, sln_code)
VALUES ('1289B469-B761-11EF-81D4-E86A641ED142', 'SLN0001.03', '供应商档案', NULL, 1, 'SLN0001');

# 客户
DELETE FROM t_code_table_category where id = '3E8BBE6D-B761-11EF-81D4-E86A641ED142';
INSERT INTO t_code_table_category(id, code, name, parent_id, is_system, sln_code)
VALUES ('3E8BBE6D-B761-11EF-81D4-E86A641ED142', 'SLN0001.04', '客户资料', NULL, 1, 'SLN0001');

# 工厂
DELETE FROM t_code_table_category where id = 'B78B15AB-B765-11EF-81D4-E86A641ED142';
INSERT INTO t_code_table_category(id, code, name, parent_id, is_system, sln_code)
VALUES ('B78B15AB-B765-11EF-81D4-E86A641ED142', 'SLN0001.05', '工厂', NULL, 1, 'SLN0001');

# ------------------------------------------------------------------------------
# 码表：仓库
# 元数据
# ------------------------------------------------------------------------------
DELETE FROM `t_code_table_md` where `id` = '3C5249D7-657A-11EE-B423-E86A641ED142';
INSERT INTO `t_code_table_md` (`id`, `code`, `name`, `table_name`, `category_id`, `memo`, `py`, `fid`, `md_version`, `is_fixed`, `enable_parent_id`, `handler_class_name`, `module_name`, `edit_col_cnt`, `view_paging`, `input_company`, `sln_code`, `module_description`)
VALUES ('3C5249D7-657A-11EE-B423-E86A641ED142', 'SLN0001.01.01', '仓库', 't_warehouse', '5558D8FD-07F4-11ED-B3DF-E86A641ED142', '', 'CK', '1003', 1, 1, 0, '', '仓库', 1, 2, 1, 'SLN0001', '');

DELETE FROM `t_code_table_cols_md` where `table_id` = '3C5249D7-657A-11EE-B423-E86A641ED142';
INSERT INTO `t_code_table_cols_md` (`id`, `table_id`, `caption`, `db_field_name`, `db_field_type`, `db_field_length`, `db_field_decimal`, `show_order`, `value_from`, `value_from_table_name`, `value_from_col_name`, `value_from_col_name_display`, `must_input`, `sys_col`, `is_visible`, `width_in_view`, `note`, `show_order_in_view`, `editor_xtype`, `col_span`, `default_value`, `default_value_ext`) VALUES
('3C527A65-657A-11EE-B423-E86A641ED142', '3C5249D7-657A-11EE-B423-E86A641ED142', 'id', 'id', 'varchar', 255, 0, -1000, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('3C52C24F-657A-11EE-B423-E86A641ED142', '3C5249D7-657A-11EE-B423-E86A641ED142', '编码', 'code', 'varchar', 255, 0, 0, 1, '', '', '', 2, 1, 1, 120, '', 0, 'textfield', 1, 100, ''),
('3C52D2E5-657A-11EE-B423-E86A641ED142', '3C5249D7-657A-11EE-B423-E86A641ED142', '名称', 'name', 'varchar', 255, 0, 1, 1, '', '', '', 2, 1, 1, 200, '', 1, 'textfield', 1, 100, ''),
('3C52E380-657A-11EE-B423-E86A641ED142', '3C5249D7-657A-11EE-B423-E86A641ED142', '助记码', 'py', 'varchar', 255, 0, -900, 99, '', '', '', 1, 1, 2, 200, '', 4, 'textfield', 1, 100, ''),
('3C52F268-657A-11EE-B423-E86A641ED142', '3C5249D7-657A-11EE-B423-E86A641ED142', '数据域', 'data_org', 'varchar', 255, 0, -800, 99, '', '', '', 1, 1, 2, 150, '', 3, 'textfield', 1, 100, ''),
('3C5302FA-657A-11EE-B423-E86A641ED142', '3C5249D7-657A-11EE-B423-E86A641ED142', '公司', 'company_id', 'varchar', 255, 0, -700, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('3C5313FB-657A-11EE-B423-E86A641ED142', '3C5249D7-657A-11EE-B423-E86A641ED142', '记录创建时间', 'date_created', 'datetime', 0, 0, -699, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('3C53252F-657A-11EE-B423-E86A641ED142', '3C5249D7-657A-11EE-B423-E86A641ED142', '记录创建人', 'create_user_id', 'varchar', 255, 0, -698, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('3C5339B1-657A-11EE-B423-E86A641ED142', '3C5249D7-657A-11EE-B423-E86A641ED142', '最后编辑时间', 'update_dt', 'datetime', 0, 0, -697, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('3C534A5E-657A-11EE-B423-E86A641ED142', '3C5249D7-657A-11EE-B423-E86A641ED142', '最后编辑人', 'update_user_id', 'varchar', 255, 0, -696, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('3C535B16-657A-11EE-B423-E86A641ED142', '3C5249D7-657A-11EE-B423-E86A641ED142', '状态', 'record_status', 'int', 11, 0, 2, 2, 't_sysdict_sln0000_ct_record_status', 'code_int', 'name', 2, 1, 1, 80, '', 2, 'psi_sysdictfield', 1, 200, '1000');

# ------------------------------------------------------------------------------
# 码表：物料颜色
# 元数据
# ------------------------------------------------------------------------------
DELETE FROM `t_code_table_md` where `id` = '0BEEA468-A111-11ED-877A-E86A641ED142';
INSERT INTO `t_code_table_md` (`id`, `code`, `name`, `table_name`, `category_id`, `memo`, `py`, `fid`, `md_version`, `is_fixed`, `enable_parent_id`, `handler_class_name`, `module_name`, `edit_col_cnt`, `view_paging`, `input_company`, `sln_code`)
VALUES ('0BEEA468-A111-11ED-877A-E86A641ED142', 'SLN0001.02.01', '物料颜色', 't_sln0001_ct_material_color', 'EB423346-A110-11ED-877A-E86A641ED142', '', 'WLYS', 'ct20230131104330', 1, 1, 0, '', '物料颜色', 1, 2, 1, 'SLN0001');

DELETE FROM `t_code_table_cols_md` where `table_id` = '0BEEA468-A111-11ED-877A-E86A641ED142';
INSERT INTO `t_code_table_cols_md` (`id`, `table_id`, `caption`, `db_field_name`, `db_field_type`, `db_field_length`, `db_field_decimal`, `show_order`, `value_from`, `value_from_table_name`, `value_from_col_name`, `value_from_col_name_display`, `must_input`, `sys_col`, `is_visible`, `width_in_view`, `note`, `show_order_in_view`, `editor_xtype`, `col_span`, `default_value`, `default_value_ext`, `label_width`) VALUES
('0BEED6A5-A111-11ED-877A-E86A641ED142', '0BEEA468-A111-11ED-877A-E86A641ED142', 'id', 'id', 'varchar', 255, 0, -1000, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, '', 90),
('0BEF002F-A111-11ED-877A-E86A641ED142', '0BEEA468-A111-11ED-877A-E86A641ED142', '编码', 'code', 'varchar', 255, 0, 0, 1, '', '', '', 2, 1, 1, 120, '', 1, 'textfield', 1, 100, '', 40),
('0BEF144A-A111-11ED-877A-E86A641ED142', '0BEEA468-A111-11ED-877A-E86A641ED142', '名称', 'name', 'varchar', 255, 0, 1, 1, '', '', '', 2, 1, 1, 342, '', 2, 'textfield', 1, 100, '', 40),
('0BEF28B7-A111-11ED-877A-E86A641ED142', '0BEEA468-A111-11ED-877A-E86A641ED142', '助记码', 'py', 'varchar', 255, 0, -900, 99, '', '', '', 1, 1, 2, 200, '', 5, 'textfield', 1, 100, '', 90),
('0BEF3C33-A111-11ED-877A-E86A641ED142', '0BEEA468-A111-11ED-877A-E86A641ED142', '数据域', 'data_org', 'varchar', 255, 0, -800, 99, '', '', '', 1, 1, 2, 150, '', 4, 'textfield', 1, 100, '', 90),
('0BEF51D8-A111-11ED-877A-E86A641ED142', '0BEEA468-A111-11ED-877A-E86A641ED142', '公司', 'company_id', 'varchar', 255, 0, -700, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, '', 90),
('0BEF6324-A111-11ED-877A-E86A641ED142', '0BEEA468-A111-11ED-877A-E86A641ED142', '记录创建时间', 'date_created', 'datetime', 0, 0, -699, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, '', 90),
('0BEF754E-A111-11ED-877A-E86A641ED142', '0BEEA468-A111-11ED-877A-E86A641ED142', '记录创建人', 'create_user_id', 'varchar', 255, 0, -698, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, '', 90),
('0BEF887F-A111-11ED-877A-E86A641ED142', '0BEEA468-A111-11ED-877A-E86A641ED142', '最后编辑时间', 'update_dt', 'datetime', 0, 0, -697, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, '', 90),
('0BEF9D00-A111-11ED-877A-E86A641ED142', '0BEEA468-A111-11ED-877A-E86A641ED142', '最后编辑人', 'update_user_id', 'varchar', 255, 0, -696, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, '', 90),
('0BEFB3EA-A111-11ED-877A-E86A641ED142', '0BEEA468-A111-11ED-877A-E86A641ED142', '状态', 'record_status', 'int', 11, 0, 2, 2, 't_sysdict_sln0000_ct_record_status', 'code_int', 'name', 2, 1, 1, 80, '', 3, 'psi_sysdictfield', 1, 200, '1000', 40);

DELETE FROM `t_code_table_buttons` where `table_id` = '0BEEA468-A111-11ED-877A-E86A641ED142';
INSERT INTO `t_code_table_buttons` (`id`, `table_id`, `caption`, `fid`, `on_click_frontend`, `on_click_backend`, `show_order`) VALUES
('0BEFE1B9-A111-11ED-877A-E86A641ED142', '0BEEA468-A111-11ED-877A-E86A641ED142', '新建物料颜色', 'ct20230131104330-add', '_onAddCodeTableRecord', '', 1),
('0BF02D59-A111-11ED-877A-E86A641ED142', '0BEEA468-A111-11ED-877A-E86A641ED142', '编辑物料颜色', 'ct20230131104330-update', '_onEditCodeTableRecord', '', 2),
('0BF05733-A111-11ED-877A-E86A641ED142', '0BEEA468-A111-11ED-877A-E86A641ED142', '删除物料颜色', 'ct20230131104330-delete', '_onDeleteCodeTableRecord', '', 3),
('0BF07259-A111-11ED-877A-E86A641ED142', '0BEEA468-A111-11ED-877A-E86A641ED142', '-', 'ct20230131104330', '', '', 4),
('0BF087D3-A111-11ED-877A-E86A641ED142', '0BEEA468-A111-11ED-877A-E86A641ED142', '导出Excel', 'ct20230131104330-excel', '_onExcelCodeTableRecord', '', 5),
('0BF09FF0-A111-11ED-877A-E86A641ED142', '0BEEA468-A111-11ED-877A-E86A641ED142', '-', 'ct20230131104330', '', '', 6),
('0BF0AF40-A111-11ED-877A-E86A641ED142', '0BEEA468-A111-11ED-877A-E86A641ED142', '刷新', 'ct20230131104330', '_onRefreshCodeTableRecord', '', 7);

DELETE FROM `t_fid_plus` where `fid` = 'ct20230131104330';
INSERT INTO `t_fid_plus` (`fid`, `name`, `py`, `memo`, `sln_code`) VALUES
('ct20230131104330', '物料颜色', 'WLYS', '', 'SLN0001');

DELETE FROM `t_permission_plus` where fid = 'ct20230131104330' or parent_fid = 'ct20230131104330';
INSERT INTO `t_permission_plus` (`id`, `fid`, `name`, `note`, `category`, `py`, `show_order`, `parent_fid`) VALUES
('ct20230131104330', 'ct20230131104330', '物料颜色 - 模块权限', '通过菜单进入物料颜色模块的权限', '物料颜色', 'WLYS', 100, null),
('ct20230131104330-add', 'ct20230131104330-add', '物料颜色 - 按钮权限 - 新建物料颜色', '物料颜色模块中按钮[新建物料颜色]的启用权限', '物料颜色', '', 201, 'ct20230131104330'),
('ct20230131104330-update', 'ct20230131104330-update', '物料颜色 - 按钮权限 - 编辑物料颜色', '物料颜色模块中按钮[编辑物料颜色]的启用权限', '物料颜色', '', 202, 'ct20230131104330'),
('ct20230131104330-delete', 'ct20230131104330-delete', '物料颜色 - 按钮权限 - 删除物料颜色', '物料颜色模块中按钮[删除物料颜色]的启用权限', '物料颜色', '', 203, 'ct20230131104330'),
('ct20230131104330-excel', 'ct20230131104330-excel', '物料颜色 - 按钮权限 - 导出Excel', '物料颜色模块中按钮[导出Excel]的启用权限', '物料颜色', '', 205, 'ct20230131104330'),
('ct20230131104330-edit-dataorg', 'ct20230131104330-edit-dataorg', '物料颜色 - 按钮权限 - 修改数据域', '物料颜色模块中按钮[修改数据域]的启用权限', '物料颜色', '', 208, 'ct20230131104330'),
('ct20230131104330-edit-py', 'ct20230131104330-edit-py', '物料颜色 - 按钮权限 - 修改助记码', '物料颜色模块中按钮[修改助记码]的启用权限', '物料颜色', '', 209, 'ct20230131104330'),
('ct20230131104330-dataorg', 'ct20230131104330-dataorg', '物料颜色 - 数据域权限 - 物料颜色在业务单据中的使用权限', '物料颜色在业务单据中的使用权限', '物料颜色', '', 300, 'ct20230131104330');

# ------------------------------------------------------------------------------
# 码表：物料颜色
# CREATE DDL
# ------------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `t_sln0001_ct_material_color` (
  `id` varchar(255)  NOT NULL,
  `py` varchar(255)  NOT NULL,
  `data_org` varchar(255)  NOT NULL,
  `company_id` varchar(255)  NOT NULL,
  `date_created` datetime  NOT NULL,
  `create_user_id` varchar(255)  NOT NULL,
  `update_dt` datetime  DEFAULT NULL,
  `update_user_id` varchar(255)  DEFAULT NULL,
  `code` varchar(255)  NOT NULL,
  `name` varchar(255)  NOT NULL,
  `record_status` int(11)  NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


# 物料颜色 - 菜单项
DELETE FROM t_menu_item_plus where id = '3A5BC77A-A111-11ED-877A-E86A641ED142';
INSERT INTO t_menu_item_plus(id, caption, fid, parent_id, show_order, py, memo, sys_category)
VALUES ('3A5BC77A-A111-11ED-877A-E86A641ED142', '物料颜色', 'ct20230131104330', '0801', 1000, 'WLYS', '', 1);

# 默认角色的权限项
TRUNCATE TABLE `t_role_permission`;
INSERT INTO `t_role_permission` (`role_id`, `permission_id`) 
select 'A83F617E-A153-11E4-A9B8-782BCBD7746B' as role_id, fid as permission_id
from t_permission;
INSERT INTO `t_role_permission` (`role_id`, `permission_id`) 
select 'A83F617E-A153-11E4-A9B8-782BCBD7746B' as role_id, fid as permission_id
from t_permission_plus;

# ------------------------------------------------------------------------------
# 码表：物料
# 元数据
# ------------------------------------------------------------------------------
DELETE FROM `t_code_table_md` where `id` = '9E2EE7DB-B757-11EF-81D4-E86A641ED142';
INSERT INTO `t_code_table_md` (`id`, `code`, `name`, `table_name`, `category_id`, `memo`, `py`, `fid`, `md_version`, `is_fixed`, `enable_parent_id`, `handler_class_name`, `module_name`, `edit_col_cnt`, `view_paging`, `input_company`, `sln_code`, `module_description`)
VALUES ('9E2EE7DB-B757-11EF-81D4-E86A641ED142', 'SLN0001.02.02', '物料', 't_goods', 'EB423346-A110-11ED-877A-E86A641ED142', '', 'WL', '1001', 1, 1, 0, '', '物料', 1, 2, 1, 'SLN0001', '');

DELETE FROM `t_code_table_cols_md` where `table_id` = '9E2EE7DB-B757-11EF-81D4-E86A641ED142';
INSERT INTO `t_code_table_cols_md` (`id`, `table_id`, `caption`, `db_field_name`, `db_field_type`, `db_field_length`, `db_field_decimal`, `show_order`, `value_from`, `value_from_table_name`, `value_from_col_name`, `value_from_col_name_display`, `must_input`, `sys_col`, `is_visible`, `width_in_view`, `note`, `show_order_in_view`, `editor_xtype`, `col_span`, `default_value`, `default_value_ext`) VALUES
('9E2F09B2-B757-11EF-81D4-E86A641ED142', '9E2EE7DB-B757-11EF-81D4-E86A641ED142', 'id', 'id', 'varchar', 255, 0, -1000, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('9E2F1E2C-B757-11EF-81D4-E86A641ED142', '9E2EE7DB-B757-11EF-81D4-E86A641ED142', '编码', 'code', 'varchar', 255, 0, 0, 1, '', '', '', 2, 1, 1, 120, '', 0, 'textfield', 1, 100, ''),
('9E2F312A-B757-11EF-81D4-E86A641ED142', '9E2EE7DB-B757-11EF-81D4-E86A641ED142', '名称', 'name', 'varchar', 255, 0, 1, 1, '', '', '', 2, 1, 1, 200, '', 1, 'textfield', 1, 100, ''),
('9E2F440A-B757-11EF-81D4-E86A641ED142', '9E2EE7DB-B757-11EF-81D4-E86A641ED142', '助记码', 'py', 'varchar', 255, 0, -900, 99, '', '', '', 1, 1, 2, 200, '', 4, 'textfield', 1, 100, ''),
('9E2F56DD-B757-11EF-81D4-E86A641ED142', '9E2EE7DB-B757-11EF-81D4-E86A641ED142', '数据域', 'data_org', 'varchar', 255, 0, -800, 99, '', '', '', 1, 1, 2, 150, '', 3, 'textfield', 1, 100, ''),
('9E2F6A8D-B757-11EF-81D4-E86A641ED142', '9E2EE7DB-B757-11EF-81D4-E86A641ED142', '公司', 'company_id', 'varchar', 255, 0, -700, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('9E2F7D7A-B757-11EF-81D4-E86A641ED142', '9E2EE7DB-B757-11EF-81D4-E86A641ED142', '记录创建时间', 'date_created', 'datetime', 0, 0, -699, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('9E2F9099-B757-11EF-81D4-E86A641ED142', '9E2EE7DB-B757-11EF-81D4-E86A641ED142', '记录创建人', 'create_user_id', 'varchar', 255, 0, -698, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('9E2FA3FC-B757-11EF-81D4-E86A641ED142', '9E2EE7DB-B757-11EF-81D4-E86A641ED142', '最后编辑时间', 'update_dt', 'datetime', 0, 0, -697, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('9E2FB65D-B757-11EF-81D4-E86A641ED142', '9E2EE7DB-B757-11EF-81D4-E86A641ED142', '最后编辑人', 'update_user_id', 'varchar', 255, 0, -696, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('9E2FC6A5-B757-11EF-81D4-E86A641ED142', '9E2EE7DB-B757-11EF-81D4-E86A641ED142', '状态', 'record_status', 'int', 11, 0, 2, 2, 't_sysdict_sln0000_ct_record_status', 'code_int', 'name', 2, 1, 1, 80, '', 2, 'psi_sysdictfield', 1, 200, '1000');

# ------------------------------------------------------------------------------
# 码表：供应商
# 元数据
# ------------------------------------------------------------------------------
DELETE FROM `t_code_table_md` where `id` = 'A48BEB2F-B761-11EF-81D4-E86A641ED142';
INSERT INTO `t_code_table_md` (`id`, `code`, `name`, `table_name`, `category_id`, `memo`, `py`, `fid`, `md_version`, `is_fixed`, `enable_parent_id`, `handler_class_name`, `module_name`, `edit_col_cnt`, `view_paging`, `input_company`, `sln_code`, `module_description`)
VALUES ('A48BEB2F-B761-11EF-81D4-E86A641ED142', 'SLN0001.03.01', '供应商', 't_supplier', '1289B469-B761-11EF-81D4-E86A641ED142', '', 'GYSDA', '1004', 1, 1, 0, '', '供应商档案', 1, 2, 1, 'SLN0001', '');

DELETE FROM `t_code_table_cols_md` where `table_id` = 'A48BEB2F-B761-11EF-81D4-E86A641ED142';
INSERT INTO `t_code_table_cols_md` (`id`, `table_id`, `caption`, `db_field_name`, `db_field_type`, `db_field_length`, `db_field_decimal`, `show_order`, `value_from`, `value_from_table_name`, `value_from_col_name`, `value_from_col_name_display`, `must_input`, `sys_col`, `is_visible`, `width_in_view`, `note`, `show_order_in_view`, `editor_xtype`, `col_span`, `default_value`, `default_value_ext`) VALUES
('A48C0DBB-B761-11EF-81D4-E86A641ED142', 'A48BEB2F-B761-11EF-81D4-E86A641ED142', 'id', 'id', 'varchar', 255, 0, -1000, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('A48C24FB-B761-11EF-81D4-E86A641ED142', 'A48BEB2F-B761-11EF-81D4-E86A641ED142', '编码', 'code', 'varchar', 255, 0, 0, 1, '', '', '', 2, 1, 1, 120, '', 0, 'textfield', 1, 100, ''),
('A48C3A07-B761-11EF-81D4-E86A641ED142', 'A48BEB2F-B761-11EF-81D4-E86A641ED142', '名称', 'name', 'varchar', 255, 0, 1, 1, '', '', '', 2, 1, 1, 200, '', 1, 'textfield', 1, 100, ''),
('A48C4C49-B761-11EF-81D4-E86A641ED142', 'A48BEB2F-B761-11EF-81D4-E86A641ED142', '助记码', 'py', 'varchar', 255, 0, -900, 99, '', '', '', 1, 1, 2, 200, '', 4, 'textfield', 1, 100, ''),
('A48C5F17-B761-11EF-81D4-E86A641ED142', 'A48BEB2F-B761-11EF-81D4-E86A641ED142', '数据域', 'data_org', 'varchar', 255, 0, -800, 99, '', '', '', 1, 1, 2, 150, '', 3, 'textfield', 1, 100, ''),
('A48C7231-B761-11EF-81D4-E86A641ED142', 'A48BEB2F-B761-11EF-81D4-E86A641ED142', '公司', 'company_id', 'varchar', 255, 0, -700, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('A48C85CD-B761-11EF-81D4-E86A641ED142', 'A48BEB2F-B761-11EF-81D4-E86A641ED142', '记录创建时间', 'date_created', 'datetime', 0, 0, -699, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('A48C98CA-B761-11EF-81D4-E86A641ED142', 'A48BEB2F-B761-11EF-81D4-E86A641ED142', '记录创建人', 'create_user_id', 'varchar', 255, 0, -698, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('A48CABD6-B761-11EF-81D4-E86A641ED142', 'A48BEB2F-B761-11EF-81D4-E86A641ED142', '最后编辑时间', 'update_dt', 'datetime', 0, 0, -697, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('A48CBEEE-B761-11EF-81D4-E86A641ED142', 'A48BEB2F-B761-11EF-81D4-E86A641ED142', '最后编辑人', 'update_user_id', 'varchar', 255, 0, -696, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('A48CD297-B761-11EF-81D4-E86A641ED142', 'A48BEB2F-B761-11EF-81D4-E86A641ED142', '状态', 'record_status', 'int', 11, 0, 2, 2, 't_sysdict_sln0000_ct_record_status', 'code_int', 'name', 2, 1, 1, 80, '', 2, 'psi_sysdictfield', 1, 200, '1000');

# ------------------------------------------------------------------------------
# 码表：客户
# 元数据
# ------------------------------------------------------------------------------
DELETE FROM `t_code_table_md` where `id` = '54C0A4BD-B763-11EF-81D4-E86A641ED142';
INSERT INTO `t_code_table_md` (`id`, `code`, `name`, `table_name`, `category_id`, `memo`, `py`, `fid`, `md_version`, `is_fixed`, `enable_parent_id`, `handler_class_name`, `module_name`, `edit_col_cnt`, `view_paging`, `input_company`, `sln_code`, `module_description`)
VALUES ('54C0A4BD-B763-11EF-81D4-E86A641ED142', 'SLN0001.04.01', '客户', 't_customer', '3E8BBE6D-B761-11EF-81D4-E86A641ED142', '', 'KHZL', '1007', 1, 1, 0, '', '客户资料', 1, 2, 1, 'SLN0001', '');

DELETE FROM `t_code_table_cols_md` where `table_id` = '54C0A4BD-B763-11EF-81D4-E86A641ED142';
INSERT INTO `t_code_table_cols_md` (`id`, `table_id`, `caption`, `db_field_name`, `db_field_type`, `db_field_length`, `db_field_decimal`, `show_order`, `value_from`, `value_from_table_name`, `value_from_col_name`, `value_from_col_name_display`, `must_input`, `sys_col`, `is_visible`, `width_in_view`, `note`, `show_order_in_view`, `editor_xtype`, `col_span`, `default_value`, `default_value_ext`) VALUES
('54C0C9B0-B763-11EF-81D4-E86A641ED142', '54C0A4BD-B763-11EF-81D4-E86A641ED142', 'id', 'id', 'varchar', 255, 0, -1000, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('54C0E056-B763-11EF-81D4-E86A641ED142', '54C0A4BD-B763-11EF-81D4-E86A641ED142', '编码', 'code', 'varchar', 255, 0, 0, 1, '', '', '', 2, 1, 1, 120, '', 0, 'textfield', 1, 100, ''),
('54C0F436-B763-11EF-81D4-E86A641ED142', '54C0A4BD-B763-11EF-81D4-E86A641ED142', '名称', 'name', 'varchar', 255, 0, 1, 1, '', '', '', 2, 1, 1, 200, '', 1, 'textfield', 1, 100, ''),
('54C130F4-B763-11EF-81D4-E86A641ED142', '54C0A4BD-B763-11EF-81D4-E86A641ED142', '助记码', 'py', 'varchar', 255, 0, -900, 99, '', '', '', 1, 1, 2, 200, '', 4, 'textfield', 1, 100, ''),
('54C1450A-B763-11EF-81D4-E86A641ED142', '54C0A4BD-B763-11EF-81D4-E86A641ED142', '数据域', 'data_org', 'varchar', 255, 0, -800, 99, '', '', '', 1, 1, 2, 150, '', 3, 'textfield', 1, 100, ''),
('54C1564C-B763-11EF-81D4-E86A641ED142', '54C0A4BD-B763-11EF-81D4-E86A641ED142', '公司', 'company_id', 'varchar', 255, 0, -700, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('54C1675A-B763-11EF-81D4-E86A641ED142', '54C0A4BD-B763-11EF-81D4-E86A641ED142', '记录创建时间', 'date_created', 'datetime', 0, 0, -699, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('54C177A1-B763-11EF-81D4-E86A641ED142', '54C0A4BD-B763-11EF-81D4-E86A641ED142', '记录创建人', 'create_user_id', 'varchar', 255, 0, -698, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('54C186DC-B763-11EF-81D4-E86A641ED142', '54C0A4BD-B763-11EF-81D4-E86A641ED142', '最后编辑时间', 'update_dt', 'datetime', 0, 0, -697, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('54C195EC-B763-11EF-81D4-E86A641ED142', '54C0A4BD-B763-11EF-81D4-E86A641ED142', '最后编辑人', 'update_user_id', 'varchar', 255, 0, -696, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('54C1A4FC-B763-11EF-81D4-E86A641ED142', '54C0A4BD-B763-11EF-81D4-E86A641ED142', '状态', 'record_status', 'int', 11, 0, 2, 2, 't_sysdict_sln0000_ct_record_status', 'code_int', 'name', 2, 1, 1, 80, '', 2, 'psi_sysdictfield', 1, 200, '1000');

# ------------------------------------------------------------------------------
# 码表：工厂
# 元数据
# ------------------------------------------------------------------------------
DELETE FROM `t_code_table_md` where `id` = '0C1A5F3E-B766-11EF-81D4-E86A641ED142';
INSERT INTO `t_code_table_md` (`id`, `code`, `name`, `table_name`, `category_id`, `memo`, `py`, `fid`, `md_version`, `is_fixed`, `enable_parent_id`, `handler_class_name`, `module_name`, `edit_col_cnt`, `view_paging`, `input_company`, `sln_code`, `module_description`)
VALUES ('0C1A5F3E-B766-11EF-81D4-E86A641ED142', 'SLN0001.05.01', '工厂', 't_factory', 'B78B15AB-B765-11EF-81D4-E86A641ED142', '', 'GC', '2034', 1, 1, 0, '', '工厂', 1, 2, 1, 'SLN0001', '');

DELETE FROM `t_code_table_cols_md` where `table_id` = '0C1A5F3E-B766-11EF-81D4-E86A641ED142';
INSERT INTO `t_code_table_cols_md` (`id`, `table_id`, `caption`, `db_field_name`, `db_field_type`, `db_field_length`, `db_field_decimal`, `show_order`, `value_from`, `value_from_table_name`, `value_from_col_name`, `value_from_col_name_display`, `must_input`, `sys_col`, `is_visible`, `width_in_view`, `note`, `show_order_in_view`, `editor_xtype`, `col_span`, `default_value`, `default_value_ext`) VALUES
('0C1A7F8A-B766-11EF-81D4-E86A641ED142', '0C1A5F3E-B766-11EF-81D4-E86A641ED142', 'id', 'id', 'varchar', 255, 0, -1000, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('0C1A93D8-B766-11EF-81D4-E86A641ED142', '0C1A5F3E-B766-11EF-81D4-E86A641ED142', '编码', 'code', 'varchar', 255, 0, 0, 1, '', '', '', 2, 1, 1, 120, '', 0, 'textfield', 1, 100, ''),
('0C1AA76C-B766-11EF-81D4-E86A641ED142', '0C1A5F3E-B766-11EF-81D4-E86A641ED142', '名称', 'name', 'varchar', 255, 0, 1, 1, '', '', '', 2, 1, 1, 200, '', 1, 'textfield', 1, 100, ''),
('0C1ABA8C-B766-11EF-81D4-E86A641ED142', '0C1A5F3E-B766-11EF-81D4-E86A641ED142', '助记码', 'py', 'varchar', 255, 0, -900, 99, '', '', '', 1, 1, 2, 200, '', 4, 'textfield', 1, 100, ''),
('0C1ACDB8-B766-11EF-81D4-E86A641ED142', '0C1A5F3E-B766-11EF-81D4-E86A641ED142', '数据域', 'data_org', 'varchar', 255, 0, -800, 99, '', '', '', 1, 1, 2, 150, '', 3, 'textfield', 1, 100, ''),
('0C1AE0E0-B766-11EF-81D4-E86A641ED142', '0C1A5F3E-B766-11EF-81D4-E86A641ED142', '公司', 'company_id', 'varchar', 255, 0, -700, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('0C1AF3D2-B766-11EF-81D4-E86A641ED142', '0C1A5F3E-B766-11EF-81D4-E86A641ED142', '记录创建时间', 'date_created', 'datetime', 0, 0, -699, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('0C1B06BF-B766-11EF-81D4-E86A641ED142', '0C1A5F3E-B766-11EF-81D4-E86A641ED142', '记录创建人', 'create_user_id', 'varchar', 255, 0, -698, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('0C1B19E0-B766-11EF-81D4-E86A641ED142', '0C1A5F3E-B766-11EF-81D4-E86A641ED142', '最后编辑时间', 'update_dt', 'datetime', 0, 0, -697, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('0C1B2DE9-B766-11EF-81D4-E86A641ED142', '0C1A5F3E-B766-11EF-81D4-E86A641ED142', '最后编辑人', 'update_user_id', 'varchar', 255, 0, -696, 99, '', '', '', 1, 1, 2, 0, '', -1000, 'textfield', 1, 100, ''),
('0C1B41D7-B766-11EF-81D4-E86A641ED142', '0C1A5F3E-B766-11EF-81D4-E86A641ED142', '状态', 'record_status', 'int', 11, 0, 2, 2, 't_sysdict_sln0000_ct_record_status', 'code_int', 'name', 2, 1, 1, 80, '', 2, 'psi_sysdictfield', 1, 200, '1000');

