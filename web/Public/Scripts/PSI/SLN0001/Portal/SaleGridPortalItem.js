/**
 * 销售看板 - 列表
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.Portal.SaleGridPortalItem", {
  extend: "PSI.Home.Portal.BasePortalItem",

  /**
   * @override
   */
  initComponent() {
    const me = this;

    PCL.apply(me, {
      flex: 1,
      width: "100%",
      height: 270,
      margin: "5",
      header: {
        title: `<span style='font-size:120%;font-weight:normal;'>${me.getPortalItemName()}</span>`,
        iconCls: me.getPortalItemIconClass(),
        height: 40
      },
      layout: "fit",
      items: me.getSaleGrid()
    });

    me.callParent(arguments);
  },

  getSaleGrid() {
    const me = this;
    if (me._saleGrid) {
      return me._saleGrid;
    }

    const modelName = me.buildModelName(me, "PortalSale");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["month", "saleMoney", "profit", "rate"]
    });

    me._saleGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-KB",
      viewConfig: {
        enableTextSelection: true
      },
      columnLines: true,
      border: 0,
      columns: [{
        header: "月份",
        dataIndex: "month",
        width: 80,
        menuDisabled: true,
        sortable: false
      }, {
        header: "销售额(不含税)",
        dataIndex: "saleMoney",
        width: 120,
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn"
      }, {
        header: "毛利",
        dataIndex: "profit",
        width: 120,
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn"
      }, {
        header: "毛利率",
        dataIndex: "rate",
        menuDisabled: true,
        sortable: false,
        align: "right"
      }],
      store: PCL.create("PCL.data.Store", {
        model: modelName,
        autoLoad: false,
        data: []
      })
    });

    return me._saleGrid;
  },

  /**
   * @override
   */
  loadData() {
    const me = this;
    const el = me.getEl();
    el?.mask(PSI.Const.LOADING);
    me.ajax({
      url: me.URL("Home/Portal/salePortal"),
      callback(options, success, response) {
        const store = me.getSaleGrid().getStore();
        store.removeAll();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          store.add(data);
        }

        el?.unmask();
      }
    });

  },
});
