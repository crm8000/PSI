/**
 * 采购订单 - 新增或编辑界面
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.PurchaseOrder.POEditForm", {
  extend: "PSI.AFX.Form.EditForm",

  config: {
    showAddGoodsButton: "0",
    genBill: false,
    sobillRef: null
  },

  mixins: ["PSI.SLN0001.Mix.GoodsPrice"],

  /**
   * 初始化组件
   * 
   * @override
   */
  initComponent() {
    const me = this;
    me._readOnly = false;

    const entity = me.getEntity();
    me.adding = entity == null;

    const action = entity == null ? "新建" : "编辑";
    const title = me.formatTitleLabel("采购订单", action);

    PCL.apply(me, {
      header: false,
      padding: "0 0 0 0",
      border: 0,
      maximized: true,
      layout: "border",
      tbar: [{
        id: me.buildId(me, "dfTitle"),
        value: title, xtype: "displayfield"
      }, "->", {
        text: "保存 <span class='PSI-shortcut-DS'>Alt + S</span>",
        tooltip: me.buildTooltip("快捷键：Alt + S"),
        ...PSI.Const.BTN_STYLE,
        id: me.buildId(me, "buttonSave"),
        iconCls: "PSI-button-ok",
        handler: me._onOK,
        scope: me
      }, "-", {
        text: "取消",
        iconCls: "PSI-tb-close",
        ...PSI.Const.BTN_STYLE,
        id: me.buildId(me, "buttonCancel"),
        handler() {
          if (me._readonly) {
            me.close();
            return;
          }

          me.confirm("请确认是否取消当前操作？", () => {
            me.close();
          });
        },
        scope: me
      }, "-", {
        text: "表单通用操作指南",
        ...PSI.Const.BTN_STYLE,
        iconCls: "PSI-help",
        handler() {
          me.focus();
          window.open(me.URL("Home/Help/index?t=commBill"));
        }
      }, "-", {
        labelSeparator: "",
        margin: "5 5 5 0",
        cls: "PSI-toolbox",
        labelAlign: "right",
        labelWidth: 0,
        width: 90,
        emptyText: "快捷访问",
        xtype: "psi_mainmenushortcutfield"
      }],
      items: [{
        region: "center",
        layout: "fit",
        border: 0,
        bodyPadding: 10,
        items: [me.getGoodsGrid()]
      }, {
        region: "north",
        id: "editForm",
        layout: {
          type: "table",
          columns: 4,
          tableAttrs: PSI.Const.TABLE_LAYOUT_SMALL,
        },
        height: 135,
        bodyPadding: 10,
        border: 0,
        items: [{
          xtype: "hidden",
          id: me.buildId(me, "hiddenId"),
          name: "id",
          value: entity == null ? null : entity.get("id")
        }, {
          id: me.buildId(me, "editRef"),
          labelWidth: 60,
          labelAlign: "right",
          labelSeparator: "",
          fieldLabel: "单号",
          xtype: "displayfield",
          value: me.toFieldNoteText("保存后自动生成")
        }, {
          id: me.buildId(me, "editDealDate"),
          fieldLabel: "交货日期",
          labelWidth: 65,
          labelAlign: "right",
          labelSeparator: "",
          allowBlank: false,
          blankText: "没有输入交货日期",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          xtype: "datefield",
          format: "Y-m-d",
          value: new Date(),
          name: "bizDT",
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          }
        }, {
          id: me.buildId(me, "editSupplier"),
          colspan: 2,
          width: 430,
          labelWidth: 60,
          labelAlign: "right",
          labelSeparator: "",
          xtype: "psi_supplierfield",
          fieldLabel: "供应商",
          allowBlank: false,
          blankText: "没有输入供应商",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          },
          showAddButton: true,
          callbackFunc: me._setSupplierExtData,
          callbackScope: me
        }, {
          id: me.buildId(me, "editDealAddress"),
          labelWidth: 60,
          labelAlign: "right",
          labelSeparator: "",
          fieldLabel: "交货地址",
          colspan: 2,
          width: 430,
          xtype: "textfield",
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          }
        }, {
          id: me.buildId(me, "editContact"),
          labelWidth: 60,
          labelAlign: "right",
          labelSeparator: "",
          fieldLabel: "联系人",
          xtype: "textfield",
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          }
        }, {
          id: me.buildId(me, "editTel"),
          labelWidth: 60,
          labelAlign: "right",
          labelSeparator: "",
          fieldLabel: "电话",
          xtype: "textfield",
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          }
        }, {
          id: me.buildId(me, "editFax"),
          labelWidth: 60,
          labelAlign: "right",
          labelSeparator: "",
          fieldLabel: "传真",
          xtype: "textfield",
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          }
        }, {
          id: me.buildId(me, "editOrg"),
          labelWidth: 65,
          labelAlign: "right",
          labelSeparator: "",
          fieldLabel: "组织机构",
          xtype: "psi_orgwithdataorgfield",
          colspan: 2,
          width: 430,
          allowBlank: false,
          blankText: "没有输入组织机构",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          }
        }, {
          id: me.buildId(me, "editBizUser"),
          labelWidth: 60,
          labelAlign: "right",
          labelSeparator: "",
          fieldLabel: "业务员",
          xtype: "psi_userfield",
          allowBlank: false,
          blankText: "没有输入业务员",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          }
        }, {
          id: me.buildId(me, "editPaymentType"),
          labelWidth: 60,
          labelAlign: "right",
          labelSeparator: "",
          fieldLabel: "付款方式",
          xtype: "combo",
          queryMode: "local",
          editable: false,
          valueField: "id",
          store: PCL.create("PCL.data.ArrayStore", {
            fields: ["id", "text"],
            data: [["0", "记应付账款"],
            ["1", "现金付款"],
            ["2", "用预付款支付"]]
          }),
          value: "0",
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          }
        }, {
          id: me.buildId(me, "editBillMemo"),
          labelWidth: 65,
          labelAlign: "right",
          labelSeparator: "",
          fieldLabel: "备注",
          xtype: "textfield",
          colspan: 3,
          width: 655,
          listeners: {
            specialkey: {
              fn: me._onLastEditSpecialKey,
              scope: me
            }
          }
        }]
      }],
      listeners: {
        show: {
          fn: me._onWndShow,
          scope: me
        },
        close: {
          fn: me._onWndClose,
          scope: me
        }
      }
    });

    me.callParent(arguments);


    me.hiddenId = PCL.getCmp(me.buildId(me, "hiddenId"));

    me.editRef = PCL.getCmp(me.buildId(me, "editRef"));
    me.editDealDate = PCL.getCmp(me.buildId(me, "editDealDate"));
    me.editSupplier = PCL.getCmp(me.buildId(me, "editSupplier"));
    me.editDealAddress = PCL.getCmp(me.buildId(me, "editDealAddress"));
    me.editContact = PCL.getCmp(me.buildId(me, "editContact"));
    me.editTel = PCL.getCmp(me.buildId(me, "editTel"));
    me.editFax = PCL.getCmp(me.buildId(me, "editFax"));
    me.editOrg = PCL.getCmp(me.buildId(me, "editOrg"));
    me.editBizUser = PCL.getCmp(me.buildId(me, "editBizUser"));
    me.editPaymentType = PCL.getCmp(me.buildId(me, "editPaymentType"));
    me.editBillMemo = PCL.getCmp(me.buildId(me, "editBillMemo"));

    me.buttonSave = PCL.getCmp(me.buildId(me, "buttonSave"));
    me.buttonCancel = PCL.getCmp(me.buildId(me, "buttonCancel"));

    me.columnActionDelete = PCL.getCmp(me.buildId(me, "columnActionDelete"));
    me.columnActionAdd = PCL.getCmp(me.buildId(me, "columnActionAdd"));
    me.columnActionAppend = PCL.getCmp(me.buildId(me, "columnActionAppend"));

    me.dfTitle = PCL.getCmp(me.buildId(me, "dfTitle"));

    // AFX
    me.__editorList = [
      me.editDealDate, me.editSupplier, me.editDealAddress,
      me.editContact, me.editTel, me.editFax, me.editOrg, me.editBizUser,
      me.editPaymentType, me.editBillMemo];

    me._keyMap = PCL.create("PCL.util.KeyMap", PCL.getBody(), {
      key: "S",
      ctrl: false,
      shift: false,
      alt: true,
      fn() {
        if (PSI.Const.msgBoxShowing) {
          return;
        }

        if (me._readonly) {
          return;
        }

        me._onOK.apply(me, []);
      },
      scope: me
    });
  },

  /**
   * @private
   */
  _onWndClose() {
    const me = this;

    PCL.WindowManager.hideAll();

    // AFX
    PCL.get(window).un('beforeunload', me.__onWindowBeforeUnload);

    me._keyMap.destroy();

    const parentForm = me.getParentForm();
    if (parentForm) {
      parentForm._keyMapMain.enable();
    }
  },

  /**
   * @private
   */
  _onWndShow() {
    const me = this;

    PCL.get(window).on('beforeunload', me.__onWindowBeforeUnload);

    const parentForm = me.getParentForm();
    if (parentForm) {
      parentForm._keyMapMain.disable();
    }

    const el = me.getEl() || PCL.getBody();
    el.mask(PSI.Const.LOADING);
    me.ajax({
      url: me.URL("SLN0001/PurchaseOrder/poBillInfo"),
      params: {
        id: me.hiddenId.getValue(),
        genBill: me.getGenBill(),
        sobillRef: me.getSobillRef()
      },
      callback(options, success, response) {
        el.unmask();

        if (success) {
          const data = me.decodeJSON(response.responseText);

          if (data.ref) {
            me.editRef.setValue(me.toFieldNoteText(data.ref));
            me.editSupplier.setIdValue(data.supplierId);
            me.editSupplier.setValue(data.supplierName);
            me.editBillMemo.setValue(data.billMemo);
            me.editDealDate.setValue(data.dealDate);
            me.editDealAddress.setValue(data.dealAddress);
            me.editContact.setValue(data.contact);
            me.editTel.setValue(data.tel);
            me.editFax.setValue(data.fax);
          }

          me.editBizUser.setIdValue(data.bizUserId);
          me.editBizUser.setValue(data.bizUserName);
          if (data.orgId) {
            me.editOrg.setIdValue(data.orgId);
            me.editOrg.setValue(data.orgFullName);
          }

          if (data.paymentType) {
            me.editPaymentType.setValue(data.paymentType);
          }

          const store = me.getGoodsGrid().getStore();
          store.removeAll();
          if (data.items) {
            store.add(data.items);
          }
          if (store.getCount() == 0) {
            store.add({});
          }

          if (data.billStatus && data.billStatus != 0) {
            me.setBillReadonly();
          } else {
            me.setFocusAndCursorPosToLast(me.editSupplier);
          }
        }
      }
    });
  },

  /**
   * @private
   */
  _onOK() {
    const me = this;

    PCL.getBody().mask("正在保存中...");
    me.ajax({
      url: me.URL("SLN0001/PurchaseOrder/editPOBill"),
      params: {
        adding: me.adding ? "1" : "0",
        jsonStr: me.getSaveData()
      },
      callback(options, success, response) {
        PCL.getBody().unmask();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          if (data.success) {
            me.close();
            if (!me.getGenBill()) {
              me.getParentForm().refreshMainGrid(data.id);
            }
            me.tip("成功保存数据", true);
          } else {
            me.showInfo(data.msg, () => {
              me.editSupplier.focus();
            });
          }
        }
      }
    });
  },

  /**
   * @private
   */
  _onLastEditSpecialKey(field, e) {
    const me = this;

    if (me._readonly) {
      return;
    }

    if (e.getKey() == e.ENTER) {
      const store = me.getGoodsGrid().getStore();
      if (store.getCount() == 0) {
        store.add({});
      }
      me.getGoodsGrid().focus();
      me._cellEditing.startEdit(0, 1);
    }
  },

  /**
   * @private
   */
  getGoodsGrid() {
    const me = this;

    if (me._goodsGrid) {
      return me._goodsGrid;
    }

    const modelName = me.buildModelName(me, "POBill_EditForm");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "goodsId", "goodsCode", "goodsName",
        "goodsSpec", "unitName", "goodsCount", {
          name: "goodsMoney",
          type: "float"
        }, "goodsPrice", {
          name: "taxRate",
          type: "int"
        }, {
          name: "tax",
          type: "float"
        }, {
          name: "moneyWithTax",
          type: "float"
        }, "memo", "goodsPriceWithTax"]
    });
    const store = PCL.create("PCL.data.Store", {
      autoLoad: false,
      model: modelName,
      data: []
    });

    me._cellEditing = PCL.create("PSI.UX.CellEditing", {
      clicksToEdit: 1,
      listeners: {
        edit: {
          fn: me._onCellEditingAfterEdit,
          scope: me
        }
      }
    });

    me._goodsGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-EF",
      viewConfig: {
        enableTextSelection: true,
        markDirty: !me.adding
      },
      features: [{
        ftype: "summary"
      }],
      plugins: [me._cellEditing],
      columnLines: true,
      columns: [{
        xtype: "rownumberer",
        text: "#",
        width: 30
      }, {
        header: "物料编码",
        dataIndex: "goodsCode",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        editor: {
          xtype: "psi_goods_with_purchaseprice_field",
          parentCmp: me,
          showAddButton: me.getShowAddGoodsButton() == "1",
          supplierIdFunc: me._supplierIdFunc,
          supplierIdScope: me,
          goodsInfoFunc: me._setGoodsInfo,
          goodsInfoScope: me,
        }
      }, {
        header: "品名/规格型号",
        dataIndex: "goodsName",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        width: 330,
        renderer(value, metaData, record) {
          return record.get("goodsName") + " " + record.get("goodsSpec");
        }
      }, {
        header: "采购数量",
        dataIndex: "goodsCount",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        align: "right",
        width: 80,
        editor: {
          xtype: "numberfield",
          allowDecimals: PSI.Const.GC_DEC_NUMBER > 0,
          decimalPrecision: PSI.Const.GC_DEC_NUMBER,
          minValue: 0,
          hideTrigger: true
        }
      }, {
        header: "单位",
        dataIndex: "unitName",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        align: "center",
        width: 50
      }, {
        header: "采购单价",
        dataIndex: "goodsPrice",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 90,
        editor: {
          xtype: "numberfield",
          hideTrigger: true
        },
        summaryRenderer() {
          return "金额合计";
        }
      }, {
        header: "采购金额",
        dataIndex: "goodsMoney",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 90,
        editor: {
          xtype: "numberfield",
          hideTrigger: true
        },
        summaryType: "sum"
      }, {
        header: "含税价",
        dataIndex: "goodsPriceWithTax",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 90,
        editor: {
          xtype: "numberfield",
          hideTrigger: true
        }
      }, {
        header: "税率(%)",
        dataIndex: "taxRate",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        align: "right",
        format: "0",
        width: 60
      }, {
        header: "税金",
        dataIndex: "tax",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 90,
        editor: {
          xtype: "numberfield",
          hideTrigger: true
        },
        summaryType: "sum"
      }, {
        header: "价税合计",
        dataIndex: "moneyWithTax",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 90,
        editor: {
          xtype: "numberfield",
          hideTrigger: true
        },
        summaryType: "sum"
      }, {
        header: "备注",
        dataIndex: "memo",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        editor: {
          xtype: "textfield"
        }
      }, {
        header: "",
        id: me.buildId(me, "columnActionDelete"),
        align: "center",
        menuDisabled: true,
        draggable: false,
        width: 40,
        xtype: "actioncolumn",
        items: [{
          icon: me.URL("Public/Images/icons/delete.png"),
          tooltip: "删除当前记录",
          handler(grid, row) {
            const store = grid.getStore();
            store.remove(store.getAt(row));
            if (store.getCount() == 0) {
              store.add({});
            }
          },
          scope: me
        }]
      }, {
        header: "",
        id: me.buildId(me, "columnActionAdd"),
        align: "center",
        menuDisabled: true,
        draggable: false,
        width: 40,
        xtype: "actioncolumn",
        items: [{
          icon: me.URL("Public/Images/icons/insert.png"),
          tooltip: "在当前记录之前插入新记录",
          handler(grid, row) {
            const store = grid.getStore();
            store.insert(row, [{}]);
          },
          scope: me
        }]
      }, {
        header: "",
        id: me.buildId(me, "columnActionAppend"),
        align: "center",
        menuDisabled: true,
        draggable: false,
        width: 40,
        xtype: "actioncolumn",
        items: [{
          icon: me.URL("Public/Images/icons/add.png"),
          tooltip: "在当前记录之后新增记录",
          handler(grid, row) {
            const store = grid.getStore();
            store.insert(row + 1, [{}]);
          },
          scope: me
        }]
      }],
      store: store,
      listeners: {
        cellclick() {
          return !me._readonly;
        }
      }
    });

    return me._goodsGrid;
  },

  /**
   * @private
   */
  _setGoodsInfo(data) {
    const me = this;
    const item = me.getGoodsGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      return;
    }

    const goods = item[0];

    goods.set("goodsId", data.get("id"));
    goods.set("goodsCode", data.get("code"));
    goods.set("goodsName", data.get("name"));
    goods.set("unitName", data.get("unitName"));
    goods.set("goodsSpec", data.get("spec"));
    if (me._taxRateBySupplier) {
      if (data.taxRateType > 1) {
        // 该物料设置了自己的特定税率
        goods.set("taxRate", data.get("taxRate"));
      } else {
        // 设置了供应商税率，优先使用供应商税率
        goods.set("taxRate", me._taxRateBySupplier);
      }
    } else {
      // 没有设置供应商税率
      goods.set("taxRate", data.get("taxRate"));
    }

    // 设置建议采购价
    goods.set("goodsPrice", data.get("purchasePrice"));

    me.calcMoney(goods);
  },

  /**
   * @private
   */
  _onCellEditingAfterEdit(editor, e) {
    const me = this;

    if (me._readonly) {
      return;
    }

    const fieldName = e.field;
    const goods = e.record;
    const oldValue = e.originalValue;
    if (fieldName == "memo") {
      const store = me.getGoodsGrid().getStore();
      if (e.rowIdx == store.getCount() - 1) {
        store.add({});
        const row = e.rowIdx + 1;
        me.getGoodsGrid().getSelectionModel().select(row);
        me._cellEditing.startEdit(row, 1);
      }
    } else if (fieldName == "moneyWithTax") {
      if (goods.get(fieldName) != (new Number(oldValue)).toFixed(2)) {
        me.calcTax(goods);
      }
    } else if (fieldName == "tax") {
      if (goods.get(fieldName) != (new Number(oldValue)).toFixed(2)) {
        me.calcMoneyWithTax(goods);
      }
    } else if (fieldName == "goodsMoney") {
      if (goods.get(fieldName) != (new Number(oldValue)).toFixed(2)) {
        me.calcPrice(goods);
      }
    } else if (fieldName == "goodsCount") {
      if (goods.get(fieldName) != oldValue) {
        me.calcMoney(goods);
      }
    } else if (fieldName == "goodsPrice") {
      if (goods.get(fieldName) != (new Number(oldValue)).toFixed(2)) {
        me.calcMoney(goods);
      }
    } else if (fieldName == "goodsPriceWithTax") {
      if (goods.get(fieldName) != (new Number(oldValue)).toFixed(2)) {
        me.calcMoney2(goods);
      }
    }
  },

  /**
   * @private
   */
  getSaveData() {
    const me = this;

    const result = {
      id: me.hiddenId.getValue(),
      dealDate: PCL.Date.format(me.editDealDate.getValue(), "Y-m-d"),
      supplierId: me.editSupplier.getIdValue(),
      dealAddress: me.editDealAddress.getValue(),
      contact: me.editContact.getValue(),
      tel: me.editTel.getValue(),
      fax: me.editFax.getValue(),
      orgId: me.editOrg.getIdValue(),
      bizUserId: me.editBizUser.getIdValue(),
      paymentType: me.editPaymentType.getValue(),
      billMemo: me.editBillMemo.getValue(),
      sobillRef: me.getSobillRef(),
      items: []
    };

    const store = me.getGoodsGrid().getStore();
    for (let i = 0; i < store.getCount(); i++) {
      const item = store.getAt(i);
      result.items.push({
        id: item.get("id"),
        goodsId: item.get("goodsId"),
        goodsCount: item.get("goodsCount"),
        goodsPrice: item.get("goodsPrice"),
        goodsPriceWithTax: item.get("goodsPriceWithTax"),
        goodsMoney: item.get("goodsMoney"),
        tax: item.get("tax"),
        taxRate: item.get("taxRate"),
        moneyWithTax: item.get("moneyWithTax"),
        memo: item.get("memo")
      });
    }

    return me.encodeJSON(result);
  },

  /**
   * @private
   */
  setBillReadonly() {
    const me = this;

    me._readonly = true;
    me.dfTitle.setValue(me.formatTitleLabel("采购订单", "查看"));

    me.buttonSave.setDisabled(true);
    me.buttonCancel.setText("关闭");

    me.editDealDate.setReadOnly(true);
    me.editSupplier.setReadOnly(true);
    me.editDealAddress.setReadOnly(true);
    me.editContact.setReadOnly(true);
    me.editTel.setReadOnly(true);
    me.editFax.setReadOnly(true);
    me.editOrg.setReadOnly(true);
    me.editBizUser.setReadOnly(true);
    me.editPaymentType.setReadOnly(true);
    me.editBillMemo.setReadOnly(true);

    me.columnActionDelete.hide();
    me.columnActionAdd.hide();
    me.columnActionAppend.hide();
  },

  /**
   * @private
   */
  _setSupplierExtData(data) {
    const me = this;

    const editDealAddress = me.editDealAddress;
    if (!editDealAddress.getValue()) {
      editDealAddress.setValue(data.address_shipping);
    }
    const editTel = me.editTel;
    if (!editTel.getValue()) {
      editTel.setValue(data.tel01);
    }

    const editFax = me.editFax;
    if (!editFax.getValue()) {
      editFax.setValue(data.fax);
    }

    const editContact = me.editContact;
    if (!editContact.getValue()) {
      editContact.setValue(data.contact01);
    }

    me._taxRateBySupplier = data.taxRate;

    // 通常情况下，是先录入供应商，再录入物料明细，这时候其实并不需要调用recalcGoodsTaxRate
    // 但是如果先录入了物料明细，再录入供应商，就需要调用recalcGoodsTaxRate
    // 一个应用的场景：由销售订单创建采购订单的时候，就是先有了物料明细数据，再录入供应商
    me.recalcGoodsTaxRate();
  },

  /**
   * 重新计算税率、税金和价税合计
   * 
   * @private
   */
  recalcGoodsTaxRate() {
    const me = this;
    const store = me.getGoodsGrid().getStore();
    if (store.getCount() == 0) {
      return;
    }

    const list = [];
    for (let i = 0; i < store.getCount(); i++) {
      const goods = store.getAt(i);
      list.push(goods.get("goodsId"));
    }

    PCL.getBody().mask("数据查询中...");
    const r = {
      url: me.URL("SLN0001/PurchaseOrder/queryTaxRate"),
      params: {
        adding: me.adding ? "1" : "0",
        supplierId: me.editSupplier.getIdValue(),
        goodsIdList: list.join(","),
      },
      callback(options, success, response) {
        PCL.getBody().unmask();
        if (success) {
          const data = me.decodeJSON(response.responseText);
          if (data.length == store.getCount()) {
            for (let i = 0; i < store.getCount(); i++) {
              const taxRate = parseInt(data[i]);
              const goods = store.getAt(i);
              const r = parseInt(goods.get("taxRate"));
              if (taxRate != r) {
                // 税率不同，需要重新计算税金和价税合计
                goods.set("taxRate", taxRate);
                me.calcMoney(goods);
              }
            }
          }
        }
      }
    };

    me.ajax(r);
  },

  /**
   * @private
   */
  _supplierIdFunc() {
    const me = this;

    return me.editSupplier.getIdValue();
  }
});
