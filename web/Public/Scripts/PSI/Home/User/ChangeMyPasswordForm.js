﻿/**
 * 修改我的密码
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.Home.User.ChangeMyPasswordForm", {
  extend: "PCL.panel.Panel",

  mixins: ["PSI.AFX.Mix.Common"],

  config: {
    loginUserId: null,
    loginUserName: null,
    loginUserFullName: null
  },

  border: 0,
  layout: "border",

  /**
   * 初始化组件
   * 
   * @override
   */
  initComponent() {
    const me = this;
    const user = {
      id: me.getLoginUserId(),
      loginName: me.getLoginUserName(),
      name: me.getLoginUserFullName()
    };

    PCL.apply(me, {
      items: [{
        region: "center",
        xtype: "panel",
        layout: "absolute",
        border: 0,
        items: [{
          id: me.buildId(me, "editForm"),
          x: 200,
          y: 50,
          xtype: "form",
          layout: {
            type: "table",
            columns: 1,
            tableAttrs: PSI.Const.TABLE_LAYOUT,
          },
          height: 250,
          width: 600,
          defaultType: 'textfield',
          border: 0,
          fieldDefaults: {
            labelWidth: 65,
            labelAlign: "right",
            labelSeparator: "",
            msgTarget: 'side',
          },
          items: [{
            xtype: "hidden",
            name: "userId",
            value: user.id
          }, {
            fieldLabel: "登录名",
            xtype: "displayfield",
            value: me.toFieldNoteText(user.loginName),
          }, {
            fieldLabel: "用户名",
            xtype: "displayfield",
            value: me.toFieldNoteText(user.name),
          }, {
            id: "PSI_User_ChangeMyPasswordForm_editOldPassword",
            fieldLabel: "旧密码",
            allowBlank: false,
            blankText: "没有输入旧密码",
            beforeLabelTextTpl: PSI.Const.REQUIRED,
            inputType: "password",
            name: "oldPassword",
            listeners: {
              specialkey: {
                fn: me._onEditOldPasswordSpecialKey,
                scope: me
              }
            }
          }, {
            id: me.buildId(me, "editNewPassword"),
            fieldLabel: "新密码",
            allowBlank: false,
            blankText: "没有输入新密码",
            beforeLabelTextTpl: PSI.Const.REQUIRED,
            inputType: "password",
            name: "newPassword",
            listeners: {
              specialkey: {
                fn: me._onEditNewPasswordSpecialKey,
                scope: me
              }
            }
          }, {
            id: me.buildId(me, "editConfirmPassword"),
            fieldLabel: "确认密码",
            allowBlank: false,
            blankText: "没有输入确认密码",
            beforeLabelTextTpl: PSI.Const.REQUIRED,
            inputType: "password",
            listeners: {
              specialkey: {
                fn: me._onEditConfirmPasswordSpecialKey,
                scope: me
              }
            }
          }],
          buttons: [{
            id: me.buildId(me, "buttonOK"),
            text: "修改密码",
            ...PSI.Const.BTN_STYLE,
            formBind: true,
            handler: me._onOK,
            scope: me,
            iconCls: "PSI-button-ok"
          }, {
            text: "取消",
            ...PSI.Const.BTN_STYLE,
            handler() {
              me.closeWindow();
            }
          }, "->"]
        }]
      }]
    });

    me.callParent(arguments);

    me.editNewPassword = PCL.getCmp(me.buildId(me, "editNewPassword"));
    me.editConfirmPassword = PCL.getCmp(me.buildId(me, "editConfirmPassword"));
    me.editForm = PCL.getCmp(me.buildId(me, "editForm"));
    me.buttonOK = PCL.getCmp(me.buildId(me, "buttonOK"));
  },

  /**
   * 修改密码
   * 
   * @private
   */
  _onOK() {
    const me = this;

    const editNewPassword = me.editNewPassword;
    const editConfirmPassword = me.editConfirmPassword;

    const np = editNewPassword.getValue();
    const cp = editConfirmPassword.getValue();

    if (np != cp) {
      me.showInfo("确认密码与新密码不一致", () => {
        editNewPassword.focus();
      });
      return;
    }

    const form = me.editForm;
    const el = PCL.getBody();
    el.mask("数据保存中...");
    form.submit({
      url: me.URL("Home/User/changeMyPasswordPOST"),
      method: "POST",
      success(form, action) {
        el.unmask();
        me.showInfo("成功修改登录密码", () => {
          me.closeWindow();
        });
      },
      failure(form, action) {
        el.unmask();
        me.showInfo(action.result.msg);
      }
    });
  },

  /**
   * @private
   */
  _onEditOldPasswordSpecialKey(field, e) {
    const me = this;

    if (e.getKey() == e.ENTER) {
      me.editNewPassword.focus();
    }
  },

  /**
   * @private
   */
  _onEditNewPasswordSpecialKey(field, e) {
    const me = this;

    if (e.getKey() == e.ENTER) {
      me.editConfirmPassword.focus();
    }
  },

  /**
   * @private
   */
  _onEditConfirmPasswordSpecialKey(field, e) {
    const me = this;

    if (e.getKey() == e.ENTER) {
      me.buttonOK.focus();
    }
  },

  /**
   * @private
   */
  closeWindow() {
    if (PSI.Const.MOT == "0") {
      window.location.replace(PSI.Const.BASE_URL);
    } else {
      window.close();

      if (!window.closed) {
        window.location.replace(PSI.Const.BASE_URL);
      }
    }
  }
});
