<?php

namespace Home\Service;

/**
 * 关于 Service
 *
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
class AboutService extends PSIBaseExService
{
  /**
   * PHP 版本号
   *
   * @return string
   */
  public function getPHPVersion(): string
  {
    return phpversion();
  }

  /**
   * MySQL 版本号
   *
   * @return string
   */
  public function getMySQLVersion(): string
  {
    $db = $this->db();
    $sql = "select version() as v";
    $data = $db->query($sql);
    if (!$data) {
      return "MySQL版本号未知";
    } else {
      return $data[0]["v"];
    }
  }

  /**
   * 数据库结构版本号
   *
   * @return array
   */
  public function getPSIDBVersion(): array
  {
    $db = $this->db();
    $sql = "select db_version, update_dt from t_psi_db_version";
    $data = $db->query($sql);
    if (!$data) {
      return [
        "version" => "未知",
        "dt" => "未知",
      ];
    } else {
      return [
        "version" => $data[0]["db_version"],
        "dt" => $data[0]["update_dt"]
      ];
    }
  }
}
