/**
 * 销售订单 - 主界面
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.SaleOrder.SOMainForm", {
  extend: "PSI.AFX.Form.MainForm",

  config: {
    permission: null
  },

  /**
   * 初始化组件
   * 
   * @override
   */
  initComponent() {
    const me = this;

    PCL.apply(me, {
      items: [{
        tbar: me.getToolbarCmp(),
        id: me.buildId(me, "panelQueryCmp"),
        region: "north",
        height: 125,
        header: false,
        collapsible: true,
        collapseMode: "mini",
        border: 0,
        layout: {
          type: "table",
          columns: 5
        },
        bodyCls: "PSI-Query-Panel",
        items: me.getQueryCmp()
      }, {
        region: "center",
        layout: "border",
        border: 0,
        ...PSI.Const.BODY_PADDING,
        items: [{
          region: "center",
          layout: "fit",
          border: 0,
          items: [me.getMainGrid()]
        }, {
          region: "south",
          height: "60%",
          split: true,
          collapsible: true,
          header: false,
          layout: "border",
          border: 0,
          items: [{
            region: "north",
            bodyStyle: "border-width:1px 1px 0px 1px",
            height: 40,
            border: 1,
            layout: {
              type: "table",
              columns: 5
            },
            bodyPadding: 10,
            items: me.getBillSummaryCmp()
          }, {
            region: "center",
            xtype: "tabpanel",
            bodyStyle: { borderWidth: 0 },
            border: 0,
            items: [me.getDetailGrid(), me.getWSGrid()]
          }
          ]
        }]
      }]
    });

    me.callParent(arguments);

    me.editSummaryRef = PCL.getCmp(me.buildId(me, "editSummaryRef"));
    me.editSummaryCustomer = PCL.getCmp(me.buildId(me, "editSummaryCustomer"));
    me.editSummaryDealDate = PCL.getCmp(me.buildId(me, "editSummaryDealDate"));

    me._keyMapMain = PCL.create("PCL.util.KeyMap", PCL.getBody(), [{
      key: "N",
      ctrl: false,
      shift: false,
      alt: true,
      fn() {
        if (PSI.Const.msgBoxShowing) {
          return;
        }

        // 判断权限
        if (me.getPermission().add != "1") {
          return;
        }

        me._onAddBill.apply(me, []);
      },
      scope: me
    }, {
      key: "Q",
      ctrl: false,
      shift: false,
      alt: true,
      fn() {
        if (PSI.Const.msgBoxShowing) {
          return;
        }
        PCL.getCmp(me.buildId(me, "editQueryRef"))?.focus();
      },
      scope: me
    }, {
      key: "H",
      ctrl: false,
      shift: false,
      alt: true,
      fn() {
        if (PSI.Const.msgBoxShowing) {
          return;
        }

        const panel = PCL.getCmp(me.buildId(me, "panelQueryCmp"));
        if (panel.getCollapsed()) {
          panel.expand();
        } else {
          panel.collapse();
        };
      },
      scope: me
    },
    ]);

    me.editQueryBillStatus = PCL.getCmp(me.buildId(me, "editQueryBillStatus"));
    me.editQueryRef = PCL.getCmp(me.buildId(me, "editQueryRef"));
    me.editQueryFromDT = PCL.getCmp(me.buildId(me, "editQueryFromDT"));
    me.editQueryToDT = PCL.getCmp(me.buildId(me, "editQueryToDT"));
    me.editQueryCustomer = PCL.getCmp(me.buildId(me, "editQueryCustomer"));
    me.editQueryReceivingType = PCL.getCmp(me.buildId(me, "editQueryReceivingType"));
    me.editQueryGoods = PCL.getCmp(me.buildId(me, "editQueryGoods"));
    me.editQueryUser = PCL.getCmp(me.buildId(me, "editQueryUser"));

    // AFX: 查询控件input List
    me.__editorList = [
      me.editQueryBillStatus,
      me.editQueryRef,
      me.editQueryFromDT,
      me.editQueryToDT,
      me.editQueryCustomer,
      me.editQueryReceivingType,
      me.editQueryGoods,
      me.editQueryUser];

    me.buttonEdit = PCL.getCmp(me.buildId(me, "buttonEdit"));
    me.buttonDelete = PCL.getCmp(me.buildId(me, "buttonDelete"));
    me.buttonCommit = PCL.getCmp(me.buildId(me, "buttonCommit"));
    me.buttonCancelConfirm = PCL.getCmp(me.buildId(me, "buttonCancelConfirm"));
    me.buttonGenWSBill = PCL.getCmp(me.buildId(me, "buttonGenWSBill"));
    me.buttonGenPOBill = PCL.getCmp(me.buildId(me, "buttonGenPOBill"));

    me.columnActionChangeOrder = PCL.getCmp(me.buildId(me, "columnActionChangeOrder"));

    me.refreshMainGrid();
  },

  /**
   * @private
   */
  getBillSummaryCmp() {
    const me = this;

    const fieldProps = {
      xtype: "textfield",
      readOnly: true,
      labelSeparator: "",
      labelAlign: "right",
    };
    return [{
      xtype: "container",
      height: 22,
      html: "<span style='padding:0px;margin:0px 0px 0px -5px;border-left: 5px solid #9254de'></span>",
    }, {
      id: me.buildId(me, "editSummaryRef"),
      fieldLabel: "销售订单号",
      labelWidth: 75,
      value: "",
      ...fieldProps,
    }, {
      id: me.buildId(me, "editSummaryCustomer"),
      fieldLabel: "客户",
      labelWidth: 70,
      value: "",
      colspan: 2,
      width: 430,
      ...fieldProps,
    }, {
      id: me.buildId(me, "editSummaryDealDate"),
      fieldLabel: "交货日期",
      labelWidth: 70,
      value: "",
      ...fieldProps,
    }];
  },

  /**
   * @private
   */
  resetSummaryInput() {
    const me = this;

    me.editSummaryRef.setValue("");
    me.editSummaryCustomer.setValue("");
    me.editSummaryDealDate.setValue("");
  },

  /**
   * 工具栏
   * 
   * @private
   */
  getToolbarCmp() {
    const me = this;

    let result = [];
    let list = [];

    // 新建
    if (me.getPermission().add == "1") {
      result.push({
        iconCls: "PSI-tb-new",
        text: "新建销售订单 <span class='PSI-shortcut-DS'>Alt + N</span>",
        tooltip: me.buildTooltip("快捷键：Alt + N"),
        ...PSI.Const.BTN_STYLE,
        id: "buttonAdd",
        scope: me,
        handler: me._onAddBill
      });
    }

    // 变更
    list = [];
    if (me.getPermission().edit == "1") {
      list.push({
        text: "编辑销售订单",
        scope: me,
        handler: me._onEditBill,
        id: me.buildId(me, "buttonEdit")
      });
    }
    if (me.getPermission().del == "1") {
      list.push({
        text: "删除销售订单",
        hidden: me.getPermission().del == "0",
        scope: me,
        handler: me._onDeleteBill,
        id: me.buildId(me, "buttonDelete")
      });
    }
    if (list.length > 0) {
      if (result.length > 0) {
        result.push("-");
      }

      result.push({
        text: "变更",
        ...PSI.Const.BTN_STYLE,
        menu: list
      });
    }

    // 审核
    list = [];
    if (me.getPermission().confirm == "1") {
      list.push({
        text: "审核",
        scope: me,
        handler: me._onCommit,
        id: me.buildId(me, "buttonCommit")
      }, {
        text: "取消审核",
        scope: me,
        handler: me._onCancelConfirm,
        id: me.buildId(me, "buttonCancelConfirm")
      });
    }
    if (list.length > 0) {
      if (result.length > 0) {
        result.push("-");
      }

      result.push({
        text: "审核",
        ...PSI.Const.BTN_STYLE,
        menu: list
      });
    }

    // 生成
    list = [];
    if (me.getPermission().genPOBill == "1") {
      list.push({
        text: "生成采购订单",
        scope: me,
        handler: me._onGenPOBill,
        id: me.buildId(me, "buttonGenPOBill")
      });
    }
    if (me.getPermission().genWSBill == "1") {
      list.push({
        text: "生成销售出库单",
        hidden: me.getPermission().genWSBill == "0",
        scope: me,
        handler: me._onGenWSBill,
        id: me.buildId(me, "buttonGenWSBill")
      });
    }
    if (list.length > 0) {
      if (result.length > 0) {
        result.push("-");
      }

      result.push({
        text: "生成",
        ...PSI.Const.BTN_STYLE,
        menu: list
      });
    }

    // 终结
    list = [];
    if (me.getPermission().closeBill == "1") {
      if (result.length > 0) {
        result.push("-");
      }
      list.push({
        text: "关闭销售订单",
        iconCls: "PSI-button-commit",
        scope: me,
        handler: me._onCloseSO
      }, {
        text: "取消销售订单关闭状态",
        iconCls: "PSI-button-cancelconfirm",
        scope: me,
        handler: me._onCancelClosedSO
      });
      result.push({
        text: "终结",
        ...PSI.Const.BTN_STYLE,
        menu: list
      });
    }

    // 导出
    list = [];
    if (me.getPermission().genPDF == "1") {
      list.push({
        text: "单据生成pdf",
        iconCls: "PSI-button-pdf",
        id: "buttonPDF",
        scope: me,
        handler: me._onPDF
      });
    }
    if (list.length > 0) {
      if (result.length > 0) {
        result.push("-");
      }

      result.push({
        text: "导出",
        ...PSI.Const.BTN_STYLE,
        menu: list
      });
    }

    // 打印
    list = [];
    if (me.getPermission().print == "1") {
      list.push({
        text: "打印预览",
        iconCls: "PSI-button-print-preview",
        scope: me,
        handler: me._onPrintPreview
      }, {
        text: "直接打印",
        iconCls: "PSI-button-print",
        scope: me,
        handler: me._onPrint
      });
    }
    if (list.length > 0) {
      if (result.length > 0) {
        result.push("-");
      }

      result.push({
        text: "打印",
        ...PSI.Const.BTN_STYLE,
        menu: list
      });
    }

    // 指南
    // 关闭
    if (result.length > 0) {
      result.push("-");
    }
    result.push({
      iconCls: "PSI-tb-help",
      text: "指南",
      ...PSI.Const.BTN_STYLE,
      handler() {
        me.focus();
        window.open(me.URL("Home/Help/index?t=sobill"));
      }
    }, "-", {
      iconCls: "PSI-tb-close",
      text: "关闭",
      ...PSI.Const.BTN_STYLE,
      handler() {
        me.focus();
        me.closeWindow();
      }
    });

    result = result.concat(me.getPagination()).concat(me.getShortcutCmp());

    return result;
  },

  /**
   * 快捷访问
   * 
   * @private
   */
  getShortcutCmp() {
    return ["|", " ",
      {
        cls: "PSI-Shortcut-Cmp",
        labelWidth: 0,
        emptyText: "快捷访问",
        xtype: "psi_mainmenushortcutfield",
        width: 90
      }
    ];
  },

  /**
   * 查询条件
   * 
   * @private
   */
  getQueryCmp() {
    const me = this;
    return [{
      xtype: "container",
      height: 26,
      html: `<h2 style='margin-left:20px;margin-top:32px;color:#595959;display:inline-block'>销售订单</h2>
              &nbsp;&nbsp;<span style='color:#8c8c8c'>单据列表</span>
              <div style='float:right;display:inline-block;margin:10px 0px 0px 20px;border-left:1px solid #e5e6e8;height:70px'>&nbsp;</div>
              `
    }, {
      id: me.buildId(me, "editQueryBillStatus"),
      xtype: "combo",
      queryMode: "local",
      editable: false,
      valueField: "id",
      labelWidth: 50,
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "状态",
      margin: "5, 0, 0, 0",
      store: PCL.create("PCL.data.ArrayStore", {
        fields: ["id", "text"],
        data: [[-1, "全部"], [0, "待审核"], [1000, "已审核"],
        [2000, "部分出库"], [3000, "全部出库"], [4000, "订单关闭"]]
      }),
      value: -1,
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      }
    }, {
      id: me.buildId(me, "editQueryRef"),
      labelWidth: 90,
      width: 220,
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "单号 <span class='PSI-shortcut-DS'>Alt + Q</span>",
      margin: "5, 0, 0, 0",
      xtype: "textfield",
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      }
    }, {
      id: me.buildId(me, "editQueryFromDT"),
      xtype: "datefield",
      margin: "5, 0, 0, 0",
      format: "Y-m-d",
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "交货日期（起）",
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      }
    }, {
      id: me.buildId(me, "editQueryToDT"),
      xtype: "datefield",
      margin: "5, 0, 0, 0",
      format: "Y-m-d",
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "交货日期（止）",
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      }
    }, { xtype: "container" }, {
      id: me.buildId(me, "editQueryCustomer"),
      labelWidth: 50,
      xtype: "psi_customerfield",
      showModal: true,
      labelAlign: "right",
      labelSeparator: "",
      margin: "5, 0, 0, 0",
      fieldLabel: "客户",
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      }
    }, {
      id: me.buildId(me, "editQueryReceivingType"),
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "收款方式",
      labelWidth: 70,
      margin: "5, 0, 0, 0",
      xtype: "combo",
      queryMode: "local",
      editable: false,
      valueField: "id",
      store: PCL.create("PCL.data.ArrayStore", {
        fields: ["id", "text"],
        data: [[-1, "全部"], [0, "记应收账款"], [1, "现金收款"]]
      }),
      value: -1,
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      }
    },
    {
      xtype: "container",
      colspan: 2,
      items: [{
        xtype: "button",
        text: "查询",
        cls: "PSI-Query-btn1",
        width: 100,
        height: 26,
        margin: "5 0 0 10",
        handler: me._onQuery,
        scope: me
      }, {
        xtype: "button",
        text: "清空查询条件",
        cls: "PSI-Query-btn2",
        width: 100,
        height: 26,
        margin: "5, 0, 0, 10",
        handler: me._onClearQuery,
        scope: me
      }, {
        xtype: "button",
        text: "隐藏工具栏 <span class='PSI-shortcut-DS'>Alt + H</span>",
        cls: "PSI-Query-btn3",
        width: 140,
        height: 26,
        iconCls: "PSI-button-hide",
        margin: "5 0 0 20",
        handler() {
          PCL.getCmp(me.buildId(me, "panelQueryCmp")).collapse();
        },
        scope: me
      }]
    }, { xtype: "container" }, {
      id: me.buildId(me, "editQueryGoods"),
      labelWidth: 50,
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "商品",
      margin: "5, 0, 0, 0",
      xtype: "psi_goodsfield",
      showModal: true,
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      }
    }, {
      id: me.buildId(me, "editQueryUser"),
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "制单人",
      margin: "5, 0, 0, 0",
      xtype: "psi_userfield",
      labelWidth: 70,
      showModal: true,
      listeners: {
        specialkey: {
          fn: me.__onLastEditSpecialKey,
          scope: me
        }
      }
    }];
  },

  /**
   * 分页
   * 
   * @private
   */
  getPagination() {
    const me = this;
    const store = me.getMainGrid().getStore();
    const result = ["->", {
      id: "pagingToobar",
      xtype: "pagingtoolbar",
      cls: "PSI-Pagination",
      border: 0,
      store: store
    }, "-", {
        xtype: "displayfield",
        fieldStyle: "font-size:13px",
        value: "每页显示"
      }, {
        id: "comboCountPerPage",
        xtype: "combobox",
        cls: "PSI-Pagination",
        editable: false,
        width: 60,
        store: PCL.create("PCL.data.ArrayStore", {
          fields: ["text"],
          data: [["20"], ["50"], ["100"],
          ["300"], ["1000"]]
        }),
        value: 20,
        listeners: {
          change: {
            fn() {
              store.pageSize = PCL.getCmp("comboCountPerPage").getValue();
              store.currentPage = 1;
              PCL.getCmp("pagingToobar").doRefresh();
            },
            scope: me
          }
        }
      }, {
        xtype: "displayfield",
        fieldStyle: "font-size:13px",
        value: "张单据"
      }];

    return result;
  },

  /**
   * 销售订单主表
   * 
   * @private
   */
  getMainGrid() {
    const me = this;
    if (me._mainGrid) {
      return me._mainGrid;
    }

    const modelName = me.buildModelName(me, "SOBill");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "ref", "customerName", "contact", "tel",
        "fax", "inputUserName", "bizUserName",
        "billStatus", "goodsMoney", "dateCreated",
        "receivingType", "tax", "moneyWithTax", "dealDate",
        "dealAddress", "orgName", "confirmUserName",
        "confirmDate", "billMemo", "genPWBill"]
    });
    const store = PCL.create("PCL.data.Store", {
      autoLoad: false,
      model: modelName,
      data: [],
      pageSize: 20,
      proxy: {
        type: "ajax",
        actionMethods: {
          read: "POST"
        },
        url: me.URL("SLN0001/SaleOrder/sobillList"),
        reader: {
          root: 'dataList',
          totalProperty: 'totalCount'
        }
      }
    });
    store.on("beforeload", () => {
      store.proxy.extraParams = me.getQueryParam();
    });
    store.on("load", (e, records, successful) => {
      if (successful) {
        me.gotoMainGridRecord(me._lastId);
      }
    });

    me._mainGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-FC",
      viewConfig: {
        enableTextSelection: true
      },
      border: 1,
      columnLines: true,
      columns: [{
        xtype: "rownumberer",
        width: 50
      }, {
        header: "状态",
        dataIndex: "billStatus",
        menuDisabled: true,
        sortable: false,
        width: 110,
        renderer(value) {
          if (value == 0) {
            return "<span style='color:red'>待审核</span>";
          } else if (value == 1000) {
            return "已审核";
          } else if (value == 2000) {
            return "<span style='color:green'>部分出库</span>";
          } else if (value == 3000) {
            return "全部出库";
          } else if (value == 4000) {
            return "关闭(未出库)";
          } else if (value == 4001) {
            return "关闭(部分出库)";
          } else if (value == 4002) {
            return "关闭(全部出库)";
          } else {
            return "";
          }
        }
      }, {
        header: "销售订单号",
        dataIndex: "ref",
        width: 120,
        menuDisabled: true,
        sortable: false
      }, {
        header: "出库单?",
        dataIndex: "genPWBill",
        width: 70,
        align: "center",
        menuDisabled: true,
        sortable: false,
        tooltip: me.buildTooltip("是否已生成出库单")
      }, {
        header: "交货日期",
        dataIndex: "dealDate",
        menuDisabled: true,
        sortable: false,
        width: 90,
        align: "center"
      }, {
        header: "交货地址",
        dataIndex: "dealAddress",
        menuDisabled: true,
        sortable: false,
        width: 300
      }, {
        header: "客户",
        dataIndex: "customerName",
        width: 300,
        menuDisabled: true,
        sortable: false
      }, {
        header: "客户联系人",
        dataIndex: "contact",
        menuDisabled: true,
        sortable: false
      }, {
        header: "客户电话",
        dataIndex: "tel",
        menuDisabled: true,
        sortable: false
      }, {
        header: "客户传真",
        dataIndex: "fax",
        menuDisabled: true,
        sortable: false
      }, {
        header: "销售金额",
        dataIndex: "goodsMoney",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 90
      }, {
        header: "税金",
        dataIndex: "tax",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 90
      }, {
        header: "价税合计",
        dataIndex: "moneyWithTax",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 90
      }, {
        header: "收款方式",
        dataIndex: "receivingType",
        menuDisabled: true,
        sortable: false,
        width: 100,
        renderer(value) {
          if (value == 0) {
            return "记应收账款";
          } else if (value == 1) {
            return "现金收款";
          } else {
            return "";
          }
        }
      }, {
        header: "业务员",
        dataIndex: "bizUserName",
        menuDisabled: true,
        sortable: false
      }, {
        header: "组织机构",
        dataIndex: "orgName",
        menuDisabled: true,
        sortable: false,
        width: 200
      }, {
        header: "制单人",
        dataIndex: "inputUserName",
        menuDisabled: true,
        sortable: false
      }, {
        header: "制单时间",
        dataIndex: "dateCreated",
        menuDisabled: true,
        sortable: false,
        width: 150
      }, {
        header: "审核人",
        dataIndex: "confirmUserName",
        menuDisabled: true,
        sortable: false
      }, {
        header: "审核时间",
        dataIndex: "confirmDate",
        menuDisabled: true,
        sortable: false,
        width: 150
      }, {
        header: "备注",
        dataIndex: "billMemo",
        menuDisabled: true,
        sortable: false,
        width: 200
      }],
      store: store,
      listeners: {
        select: {
          fn: me._onMainGridSelect,
          scope: me
        },
        itemdblclick: {
          fn: me.getPermission().edit == "1"
            ? me._onEditBill
            : PCL.emptyFn,
          scope: me
        }
      }
    });

    return me._mainGrid;
  },

  /**
   * 销售订单明细记录
   * 
   * @private
   */
  getDetailGrid() {
    const me = this;
    if (me._detailGrid) {
      return me._detailGrid;
    }

    const modelName = me.buildModelName(me, "SOBillDetail");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "goodsCode", "goodsName", "goodsSpec",
        "unitName", "goodsCount", "goodsMoney",
        "goodsPrice", "taxRate", "tax", "moneyWithTax",
        "wsCount", "leftCount", "memo", "goodsPriceWithTax"]
    });
    const store = PCL.create("PCL.data.Store", {
      autoLoad: false,
      model: modelName,
      data: []
    });

    me._detailGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-HL",
      title: "订单明细",
      viewConfig: {
        enableTextSelection: true
      },
      columnLines: true,
      columns: [PCL.create("PCL.grid.RowNumberer", {
        text: "序号",
        width: 40
      }), {
        header: "商品编码",
        dataIndex: "goodsCode",
        menuDisabled: true,
        sortable: false,
        width: 120
      }, {
        menuDisabled: true,
        draggable: false,
        sortable: false,
        header: "品名/规格型号",
        dataIndex: "goodsName",
        width: 330,
        renderer(value, metaData, record) {
          return record.get("goodsName") + " " + record.get("goodsSpec");
        }
      }, {
        header: "销售数量",
        dataIndex: "goodsCount",
        menuDisabled: true,
        sortable: false,
        align: "right",
        width: 80
      }, {
        header: "出库数量",
        dataIndex: "wsCount",
        menuDisabled: true,
        sortable: false,
        align: "right",
        width: 80
      }, {
        header: "未出库数量",
        dataIndex: "leftCount",
        menuDisabled: true,
        sortable: false,
        align: "right",
        width: 80,
        renderer(value) {
          if (value > 0) {
            return `<span style='color:red'>${value}</span>`;
          } else {
            return value;
          }
        }
      }, {
        header: "单位",
        dataIndex: "unitName",
        menuDisabled: true,
        sortable: false,
        width: 60,
        align: "center"
      }, {
        header: "销售单价",
        dataIndex: "goodsPrice",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 90
      }, {
        header: "销售金额",
        dataIndex: "goodsMoney",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 90
      }, {
        header: "税率(%)",
        dataIndex: "taxRate",
        menuDisabled: true,
        sortable: false,
        xtype: "numbercolumn",
        format: "0",
        align: "right",
        width: 60
      }, {
        header: "税金",
        dataIndex: "tax",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 90
      }, {
        header: "价税合计",
        dataIndex: "moneyWithTax",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 90
      }, {
        header: "含税价",
        dataIndex: "goodsPriceWithTax",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 90
      }, {
        header: "备注",
        dataIndex: "memo",
        menuDisabled: true,
        sortable: false,
        width: 120
      }, {
        header: "",
        id: me.buildId(me, "columnActionChangeOrder"),
        align: "center",
        menuDisabled: true,
        draggable: false,
        hidden: true,
        width: 40,
        xtype: "actioncolumn",
        items: [{
          icon: me.URL("Public/Images/icons/edit.png"),
          tooltip: "订单变更",
          handler: me._onChangeOrder,
          scope: me
        }]
      }],
      store: store
    });

    return me._detailGrid;
  },

  /**
   * 刷新销售订单主表记录
   * 
   * @private
   */
  refreshMainGrid(id) {
    const me = this;

    me.resetSummaryInput();

    me.buttonEdit?.setDisabled(true);
    me.buttonDelete?.setDisabled(true);
    me.buttonCommit?.setDisabled(true);
    me.buttonCancelConfirm?.setDisabled(true);
    me.buttonGenWSBill?.setDisabled(true);
    me.buttonGenPOBill?.setDisabled(true);

    const gridDetail = me.getDetailGrid();
    gridDetail.getStore().removeAll();

    const grid = me.getWSGrid();
    grid.getStore().removeAll();

    PCL.getCmp("pagingToobar").doRefresh();
    me._lastId = id;
  },

  /**
   * 新建销售订单
   * 
   * @private
   */
  _onAddBill() {
    const me = this;

    const form = PCL.create("PSI.SLN0001.SaleOrder.SOEditForm", {
      parentForm: me,
      showAddGoodsButton: me.getPermission().showAddGoodsButton
    });
    form.show();
  },

  /**
   * 编辑销售订单
   * 
   * @private
   */
  _onEditBill() {
    const me = this;
    const item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择要编辑的销售订单");
      return;
    }

    const bill = item[0];

    const form = PCL.create("PSI.SLN0001.SaleOrder.SOEditForm", {
      parentForm: me,
      showAddGoodsButton: me.getPermission().showAddGoodsButton,
      entity: bill
    });
    form.show();
  },

  /**
   * 删除销售订单
   * 
   * @private
   */
  _onDeleteBill() {
    const me = this;
    const item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("请选择要删除的销售订单");
      return;
    }

    const bill = item[0];

    if (bill.get("billStatus") > 0) {
      me.showInfo("当前销售订单已经审核，不能删除");
      return;
    }

    const store = me.getMainGrid().getStore();
    let index = store.findExact("id", bill.get("id"));
    index--;
    let preIndex = null;
    const preItem = store.getAt(index);
    if (preItem) {
      preIndex = preItem.get("id");
    }

    const info = `请确认是否删除单号为: <span style='color:red'>${bill.get("ref")}</span> 的销售订单？`;
    const funcConfirm = () => {
      const el = PCL.getBody();
      el.mask("正在删除中...");
      const r = {
        url: me.URL("SLN0001/SaleOrder/deleteSOBill"),
        params: {
          id: bill.get("id")
        },
        callback(options, success, response) {
          el.unmask();

          if (success) {
            const data = me.decodeJSON(response.responseText);
            if (data.success) {
              me.refreshMainGrid(preIndex);
              me.tip("成功完成删除操作", true);
            } else {
              me.showInfo(data.msg);
            }
          } else {
            me.showInfo("网络错误");
          }
        }
      };
      me.ajax(r);
    };

    me.confirm(info, funcConfirm);
  },

  /**
   * @private
   */
  _onMainGridSelect() {
    const me = this;

    const item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.buttonEdit?.setDisabled(true);
      me.buttonDelete?.setDisabled(true);
      me.buttonCommit?.setDisabled(true);
      me.buttonCancelConfirm?.setDisabled(true);
      me.buttonGenWSBill?.setDisabled(true);
      me.buttonGenPOBill?.setDisabled(true);

      return;
    }

    const bill = item[0];
    const commited = bill.get("billStatus") >= 1000;

    const buttonEdit = me.buttonEdit;
    buttonEdit?.setDisabled(false);
    if (commited) {
      buttonEdit?.setText("查看销售订单");
      me.columnActionChangeOrder.show();
    } else {
      buttonEdit?.setText("编辑销售订单");
      me.columnActionChangeOrder.hide();
    }
    if (me.getPermission().confirm == "0") {
      // 没有审核权限就不能做订单变更
      me.columnActionChangeOrder.hide();
    }

    me.buttonDelete?.setDisabled(commited);
    me.buttonCommit?.setDisabled(commited);
    me.buttonCancelConfirm?.setDisabled(!commited);
    me.buttonGenWSBill?.setDisabled(!commited);
    me.buttonGenPOBill?.setDisabled(!commited);

    me.editSummaryRef.setValue(bill.get("ref"));
    me.editSummaryCustomer.setValue(bill.get("customerName"));
    me.editSummaryDealDate.setValue(bill.get("dealDate"));

    me.refreshDetailGrid();

    me.refreshWSGrid();
  },

  /**
   * 刷新销售订单明细记录
   * 
   * @private
   */
  refreshDetailGrid(id) {
    const me = this;

    const item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      return;
    }

    const bill = item[0];

    const grid = me.getDetailGrid();
    const el = grid.getEl();
    el?.mask(PSI.Const.LOADING);

    const r = {
      url: me.URL("SLN0001/SaleOrder/soBillDetailList"),
      params: {
        id: bill.get("id")
      },
      callback(options, success, response) {
        const store = grid.getStore();

        store.removeAll();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          store.add(data);

          if (store.getCount() > 0) {
            if (id) {
              const r = store.findExact("id", id);
              if (r != -1) {
                grid.getSelectionModel().select(r);
              }
            }
          }
        }

        el?.unmask();
      }
    };
    me.ajax(r);
  },

  /**
   * 审核销售订单
   * 
   * @private
   */
  _onCommit() {
    const me = this;
    const item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择要审核的销售订单");
      return;
    }

    const bill = item[0];

    if (bill.get("billStatus") > 0) {
      me.showInfo("当前销售订单已经审核，不能再次审核");
      return;
    }

    const detailCount = me.getDetailGrid().getStore().getCount();
    if (detailCount == 0) {
      me.showInfo("当前销售订单没有录入商品明细，不能审核");
      return;
    }

    const info = `请确认是否审核单号为: <span style='color:red'>${bill.get("ref")}</span> 的销售订单?`;
    const id = bill.get("id");

    const funcConfirm = () => {
      const el = PCL.getBody();
      el.mask("正在提交中...");
      const r = {
        url: me.URL("SLN0001/SaleOrder/commitSOBill"),
        params: {
          id: id
        },
        callback(options, success, response) {
          el.unmask();

          if (success) {
            const data = me.decodeJSON(response.responseText);
            if (data.success) {
              me.refreshMainGrid(id);
              me.tip("成功完成审核操作", true);
            } else {
              me.showInfo(data.msg);
            }
          } else {
            me.showInfo("网络错误");
          }
        }
      };
      me.ajax(r);
    };
    me.confirm(info, funcConfirm);
  },

  /**
   * 取消审核
   * 
   * @private
   */
  _onCancelConfirm() {
    const me = this;
    const item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择要取消审核的销售订单");
      return;
    }

    const bill = item[0];

    if (bill.get("billStatus") == 0) {
      me.showInfo("当前销售订单还没有审核，无法取消审核");
      return;
    }

    const info = `请确认是否取消审核单号为 <span style='color:red'>${bill.get("ref")}</span> 的销售订单?`;
    const id = bill.get("id");
    const funcConfirm = () => {
      const el = PCL.getBody();
      el.mask("正在提交中...");
      const r = {
        url: me.URL("SLN0001/SaleOrder/cancelConfirmSOBill"),
        params: {
          id: id
        },
        callback(options, success, response) {
          el.unmask();

          if (success) {
            const data = me.decodeJSON(response.responseText);
            if (data.success) {
              me.refreshMainGrid(id);
              me.tip("成功完成取消审核操作", true);
            } else {
              me.showInfo(data.msg);
            }
          } else {
            me.showInfo("网络错误");
          }
        }
      };
      me.ajax(r);
    };
    me.confirm(info, funcConfirm);
  },

  /**
   * @private
   */
  gotoMainGridRecord(id) {
    const me = this;
    const grid = me.getMainGrid();
    grid.getSelectionModel().deselectAll();
    const store = grid.getStore();
    if (id) {
      const r = store.findExact("id", id);
      if (r != -1) {
        grid.getSelectionModel().select(r);
      } else {
        grid.getSelectionModel().select(0);
      }
    } else {
      grid.getSelectionModel().select(0);
    }
  },

  /**
   * 查询
   * 
   * @private
   */
  _onQuery() {
    const me = this;

    me.getMainGrid().getStore().currentPage = 1;
    me.refreshMainGrid();
  },

  /**
   * 清除查询条件
   * 
   * @private
   */
  _onClearQuery() {
    const me = this;

    me.editQueryBillStatus.setValue(-1);
    me.editQueryRef.setValue(null);
    me.editQueryFromDT.setValue(null);
    me.editQueryToDT.setValue(null);
    me.editQueryCustomer.clearIdValue();
    me.editQueryReceivingType.setValue(-1);
    me.editQueryGoods.clearIdValue();
    me.editQueryUser.clearIdValue();

    me._onQuery();
  },

  /**
   * @private
   */
  getQueryParam() {
    const me = this;

    const result = {
      billStatus: me.editQueryBillStatus.getValue()
    };

    const ref = me.editQueryRef.getValue();
    if (ref) {
      result.ref = ref;
    }

    const customerId = me.editQueryCustomer.getIdValue();
    if (customerId) {
      result.customerId = customerId;
    }

    const fromDT = me.editQueryFromDT.getValue();
    if (fromDT) {
      result.fromDT = PCL.Date.format(fromDT, "Y-m-d");
    }

    const toDT = me.editQueryToDT.getValue();
    if (toDT) {
      result.toDT = PCL.Date.format(toDT, "Y-m-d");
    }

    const receivingType = me.editQueryReceivingType.getValue();
    result.receivingType = receivingType;

    const goodsId = me.editQueryGoods.getIdValue();
    if (goodsId) {
      result.goodsId = goodsId;
    }

    const userId = me.editQueryUser.getIdValue();
    if (userId) {
      result.userId = userId;
    }

    return result;
  },

  /**
   * @private
   */
  _onGenPOBill() {
    const me = this;

    const item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择要生成采购订单的销售订单");
      return;
    }

    const bill = item[0];

    if (bill.get("billStatus") < 1000) {
      me.showInfo("当前销售订单还没有审核，无法生成采购订单");
      return;
    }

    const funShowForm = () => {
      const form = PCL.create("PSI.SLN0001.PurchaseOrder.POEditForm", {
        parentForm: me,
        sobillRef: bill.get("ref"),
        genBill: true
      });
      form.show();
    };

    // 先判断是否已经生成过采购订单了
    // 如果已经生成过，就提醒用户
    const el = PCL.getBody();
    el.mask("正在查询是否已经生成过采购订单...");
    const r = {
      url: me.URL("SLN0001/SaleOrder/getPOBillRefListBySOBillRef"),
      params: {
        soRef: bill.get("ref")
      },
      callback(options, success, response) {
        el.unmask();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          if (data.length > 0) {
            //  已经生成过采购订单，提醒用户
            let poRefList = "";
            for (var i = 0; i < data.length; i++) {
              if (i > 0) {
                poRefList += "、";
              }
              poRefList += data[i].ref;
            }
            const info = `当前销售订单已经生成过采购订单，请确认是否继续生成新的采购订单？
                          <br/>已经生成的采购订单单号是：${poRefList}`;
            me.confirm(info, funShowForm);
          } else {
            // 没有生成过采购订单，直接显示UI界面
            funShowForm();
          }
        } else {
          me.showInfo("网络错误");
        }
      }
    };
    me.ajax(r);
  },

  /**
   * 生成销售出库单
   * 
   * @private
   */
  _onGenWSBill() {
    const me = this;
    const item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择要生成出库单的销售订单");
      return;
    }

    const bill = item[0];

    if (bill.get("billStatus") < 1000) {
      me.showInfo("当前销售订单还没有审核，无法生成销售出库单");
      return;
    }

    const funShowForm = () => {
      const form = PCL.create("PSI.SLN0001.Sale.WSEditForm", {
        genBill: true,
        sobillRef: bill.get("ref"),
        soKeyMap: me._keyMapMain,
      });
      form.show();
    };

    // 先判断是否已经生成过销售出库单了
    // 如果已经生成过，就提醒用户
    const el = PCL.getBody();
    el.mask("正在查询是否已经生成过销售出库单...");
    const r = {
      url: me.URL("SLN0001/SaleOrder/getWSBillRefListBySOBillRef"),
      params: {
        soRef: bill.get("ref")
      },
      callback(options, success, response) {
        el.unmask();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          if (data.length > 0) {
            //  已经生成过销售出库单，提醒用户
            let wsRefList = "";
            for (var i = 0; i < data.length; i++) {
              if (i > 0) {
                wsRefList += "、";
              }
              wsRefList += data[i].ref;
            }
            const info = `当前销售订单已经生成过销售出库单了，请确认是否继续生成新的销售出库单？
              <br/>已经生成的销售出库单单号是：${wsRefList}`;
            me.confirm(info, funShowForm);
          } else {
            // 没有生成过销售出库单，直接显示UI界面
            funShowForm();
          }
        } else {
          me.showInfo("网络错误");
        }
      }
    };
    me.ajax(r);
  },

  /**
   * @private
   */
  _onPDF() {
    const me = this;
    const item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择要生成pdf文件的销售订单");
      return;
    }

    const bill = item[0];

    const url = me.URL("SLN0001/SaleOrder/soBillPdf?ref=" + bill.get("ref"));
    window.open(url);

    me.focus();
  },

  /**
   * @private
   */
  getWSGrid() {
    const me = this;
    if (me._wsGrid) {
      return me._wsGrid;
    }

    const modelName = me.buildModelName(me, "SOBill_WSBill");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "ref", "bizDate", "customerName",
        "warehouseName", "inputUserName", "bizUserName",
        "billStatus", "amount", "dateCreated",
        "receivingType", "memo"]
    });
    const store = PCL.create("PCL.data.Store", {
      autoLoad: false,
      model: modelName,
      data: []
    });

    me._wsGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-HL",
      title: "出库详情",
      viewConfig: {
        enableTextSelection: true
      },
      columnLines: true,
      columns: [{
        xtype: "rownumberer",
        width: 50
      }, {
        header: "状态",
        dataIndex: "billStatus",
        menuDisabled: true,
        sortable: false,
        width: 80,
        renderer(value) {
          if (value == "待出库") {
            return `<span style='color:red'>${value}</span>`;
          } else if (value == "已退货") {
            return `<span style='color:blue'>${value}</span>`;
          } else {
            return value;
          }
        }
      }, {
        header: "单号",
        dataIndex: "ref",
        width: 120,
        menuDisabled: true,
        sortable: false,
        renderer(value, md, record) {
          return "<a href='"
            + PSI.Const.BASE_URL
            + "SLN0001/Bill/viewIndex?fid=2028&refType=销售出库&ref="
            + encodeURIComponent(record.get("ref"))
            + "' target='_blank'>" + value + "</a>";
        }
      }, {
        header: "业务日期",
        dataIndex: "bizDate",
        menuDisabled: true,
        sortable: false,
        width: 90
      }, {
        header: "客户",
        dataIndex: "customerName",
        width: 300,
        menuDisabled: true,
        sortable: false
      }, {
        header: "收款方式",
        dataIndex: "receivingType",
        menuDisabled: true,
        sortable: false,
        width: 100,
        renderer(value) {
          if (value == 0) {
            return "记应收账款";
          } else if (value == 1) {
            return "现金收款";
          } else if (value == 2) {
            return "用预收款支付";
          } else {
            return "";
          }
        }
      }, {
        header: "销售金额",
        dataIndex: "amount",
        menuDisabled: true,
        sortable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 90
      }, {
        header: "出库仓库",
        dataIndex: "warehouseName",
        menuDisabled: true,
        sortable: false,
        width: 150
      }, {
        header: "业务员",
        dataIndex: "bizUserName",
        menuDisabled: true,
        sortable: false
      }, {
        header: "制单人",
        dataIndex: "inputUserName",
        menuDisabled: true,
        sortable: false
      }, {
        header: "制单时间",
        dataIndex: "dateCreated",
        width: 150,
        menuDisabled: true,
        sortable: false
      }, {
        header: "备注",
        dataIndex: "memo",
        width: 200,
        menuDisabled: true,
        sortable: false
      }],
      store,
    });

    return me._wsGrid;
  },

  /**
   * @private
   */
  refreshWSGrid() {
    const me = this;
    const item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      return;
    }

    const bill = item[0];

    const grid = me.getWSGrid();
    const el = grid.getEl();
    el?.mask(PSI.Const.LOADING);

    const r = {
      url: me.URL("SLN0001/SaleOrder/soBillWSBillList"),
      params: {
        id: bill.get("id")
      },
      callback(options, success, response) {
        const store = grid.getStore();

        store.removeAll();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          store.add(data);
        }

        el?.unmask();
      }
    };
    me.ajax(r);
  },

  /**
   * @private
   */
  _onPrintPreview() {
    const me = this;

    if (PSI.Const.ENABLE_LODOP != "1") {
      me.showInfo("请先在业务设置模块中启用Lodop打印");
      return;
    }

    const lodop = getLodop();
    if (!lodop) {
      me.showInfo("没有安装Lodop控件，无法打印");
      return;
    }

    const item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择要打印的销售订单");
      return;
    }

    const bill = item[0];

    const el = PCL.getBody();
    el.mask("数据加载中...");
    var r = {
      url: me.URL("SLN0001/SaleOrder/genSOBillPrintPage"),
      params: {
        id: bill.get("id")
      },
      callback(options, success, response) {
        el.unmask();

        if (success) {
          const data = response.responseText;
          me.previewSOBill(bill.get("ref"), data);
        }
      }
    };
    me.ajax(r);
  },

  PRINT_PAGE_WIDTH: "200mm",
  PRINT_PAGE_HEIGHT: "95mm",

  /**
   * @private
   */
  previewSOBill(ref, data) {
    const me = this;

    const lodop = getLodop();
    if (!lodop) {
      me.showInfo("Lodop打印控件没有正确安装");
      return;
    }

    lodop.PRINT_INIT("销售订单" + ref);
    lodop.SET_PRINT_PAGESIZE(1, me.PRINT_PAGE_WIDTH, me.PRINT_PAGE_HEIGHT,
      "");
    lodop.ADD_PRINT_HTM("0mm", "0mm", "100%", "100%", data);
    lodop.PREVIEW("_blank");
  },

  /**
   * @private
   */
  _onPrint() {
    const me = this;

    if (PSI.Const.ENABLE_LODOP != "1") {
      me.showInfo("请先在业务设置模块中启用Lodop打印");
      return;
    }

    const lodop = getLodop();
    if (!lodop) {
      me.showInfo("没有安装Lodop控件，无法打印");
      return;
    }

    const item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择要打印的销售订单");
      return;
    }

    const bill = item[0];

    const el = PCL.getBody();
    el.mask("数据加载中...");
    const r = {
      url: me.URL("SLN0001/SaleOrder/genSOBillPrintPage"),
      params: {
        id: bill.get("id")
      },
      callback(options, success, response) {
        el.unmask();

        if (success) {
          const data = response.responseText;
          me.printSOBill(bill.get("ref"), data);
        }
      }
    };
    me.ajax(r);
  },

  /**
   * @private
   */
  printSOBill(ref, data) {
    const me = this;

    const lodop = getLodop();
    if (!lodop) {
      me.showInfo("Lodop打印控件没有正确安装");
      return;
    }

    lodop.PRINT_INIT("销售订单" + ref);
    lodop.SET_PRINT_PAGESIZE(1, me.PRINT_PAGE_WIDTH, me.PRINT_PAGE_HEIGHT,
      "");
    lodop.ADD_PRINT_HTM("0mm", "0mm", "100%", "100%", data);
    lodop.PRINT();
  },

  /**
   * @private
   */
  _onChangeOrder(grid, row) {
    const me = this;

    if (me.getPermission().confirm == "0") {
      me.showInfo("您没有订单变更的权限");
      return;
    }

    const item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择要变更的销售订单");
      return;
    }

    const bill = item[0];
    if (parseInt(bill.get("billStatus")) >= 4000) {
      me.showInfo("订单已经关闭，不能再做变更操作");
      return;
    }

    const entity = grid.getStore().getAt(row);
    if (!entity) {
      me.showInfo("请选择要变更的明细记录");
      return;
    }

    const form = PCL.create("PSI.SLN0001.SaleOrder.ChangeOrderEditForm", {
      entity: entity,
      parentForm: me,
      renderTo: PSI.Const.RENDER_TO(),
    });
    form.show();
  },

  /**
   * 订单变更后刷新Grid
   * 
   * @private
   */
  refreshAterChangeOrder(detailId) {
    const me = this;

    me.refreshDetailGrid(detailId);

    // 刷新主表中金额相关字段
    const item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      return;
    }

    const bill = item[0];

    const r = {
      url: me.URL("SLN0001/SaleOrder/getSOBillDataAterChangeOrder"),
      params: {
        id: bill.get("id")
      },
      callback(options, success, response) {

        if (success) {
          const data = me.decodeJSON(response.responseText);
          if (data.goodsMoney) {
            bill.set("goodsMoney", data.goodsMoney);
            bill.set("tax", data.tax);
            bill.set("moneyWithTax", data.moneyWithTax);
            me.getMainGrid().getStore().commitChanges();
          }
        }
      }
    };
    me.ajax(r);
  },

  /**
   * 关闭订单
   * 
   * @private
   */
  _onCloseSO() {
    const me = this;
    const item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择要关闭的销售订单");
      return;
    }

    const bill = item[0];

    const info = `请确认是否关闭单号为: <span style='color:red'>${bill.get("ref")}</span> 的销售订单?`;
    const id = bill.get("id");

    const funcConfirm = () => {
      const el = PCL.getBody();
      el.mask("正在提交中...");
      const r = {
        url: me.URL("SLN0001/SaleOrder/closeSOBill"),
        params: {
          id: id
        },
        callback(options, success, response) {
          el.unmask();

          if (success) {
            const data = me.decodeJSON(response.responseText);
            if (data.success) {
              me.refreshMainGrid(id);
              me.tip("成功关闭销售订单", true);
            } else {
              me.showInfo(data.msg);
            }
          } else {
            me.showInfo("网络错误");
          }
        }
      };
      me.ajax(r);
    };
    me.confirm(info, funcConfirm);
  },

  /**
   * 取消关闭订单
   * 
   * @private
   */
  _onCancelClosedSO() {
    const me = this;
    const item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("没有选择要取消关闭状态的销售订单");
      return;
    }

    const bill = item[0];

    const info = `请确认是否取消单号为: <span style='color:red'>${bill.get("ref")}</span> 销售订单的关闭状态?`;
    const id = bill.get("id");

    const funcConfirm = () => {
      const el = PCL.getBody();
      el.mask("正在提交中...");
      const r = {
        url: me.URL("SLN0001/SaleOrder/cancelClosedSOBill"),
        params: {
          id: id
        },
        callback(options, success, response) {
          el.unmask();

          if (success) {
            const data = me.decodeJSON(response.responseText);
            if (data.success) {
              me.refreshMainGrid(id);
              me.tip("成功取消销售订单关闭状态", true);
            } else {
              me.showInfo(data.msg);
            }
          } else {
            me.showInfo("网络错误");
          }
        }
      };
      me.ajax(r);
    };
    me.confirm(info, funcConfirm);
  }
});
