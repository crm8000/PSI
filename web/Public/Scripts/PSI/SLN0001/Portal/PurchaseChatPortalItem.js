/**
 * 采购看板 - Chat
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.Portal.PurchaseChatPortalItem", {
  extend: "PSI.Home.Portal.BasePortalItem",

  /**
   * @override
   */
  initComponent() {
    const me = this;

    PCL.apply(me, {
      flex: 1,
      width: "100%",
      height: 270,
      margin: "5",
      header: {
        title: `<span style='font-size:120%;font-weight:normal;'>${me.getPortalItemName()}</span>`,
        iconCls: me.getPortalItemIconClass(),
        height: 40
      },
      layout: "fit",
      items: me.getPurchaseChart()
    });

    me.callParent(arguments);
  },

  getPurchaseChart() {
    const me = this;
    if (me._purchaseChart) {
      return me._purchaseChart;
    }

    const modelName = me.buildModelName(me, "PurchaseChart");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["purchaseMoney", "month"]
    });
    const store = PCL.create("PCL.data.Store", {
      model: modelName,
      data: []
    });

    me._purchaseChart = PCL.create("PCL.chart.Chart", {
      theme: "Green",
      animate: true,
      store: store,
      axes: [{
        title: "采购金额",
        type: "Numeric",
        position: "left",
        grid: true,
        fields: ["purchaseMoney"],
        label: {
          renderer: function (v) {
            return me.formatMoney2(v);
          }
        }
      }, {
        type: "Category",
        position: "bottom",
        fields: ["month"]
      }],
      series: [{
        type: "line",
        xField: "month",
        yField: "purchaseMoney",
        highlight: {
          size: 7,
          radius: 7
        },
        tips: {
          trackMouse: true,
          width: 120,
          height: 50,
          renderer: function (storeItem, item) {
            this.setTitle("采购金额");
            this.update(me.formatMoney(storeItem.get("purchaseMoney")));
          }
        }
      }]
    });
    return me._purchaseChart;
  },

  formatMoney(v) {
    const value = parseFloat(v);
    const format = "0,000.00";
    if (value >= 0) {
      return PCL.util.Format.number(value, format);
    } else {
      return "-" + PCL.util.Format.number(Math.abs(value), format);
    }
  },

  formatMoney2(v) {
    const value = parseFloat(v);
    const format = "0,000";
    if (value >= 0) {
      return PCL.util.Format.number(value, format);
    } else {
      return "-" + PCL.util.Format.number(Math.abs(value), format);
    }
  },

  /**
   * @override
   */
  loadData() {
    const me = this;
    const el = me.getEl();
    el?.mask(PSI.Const.LOADING);
    me.ajax({
      url: me.URL("Home/Portal/purchasePortal"),
      callback(options, success, response) {
        const store = me.getPurchaseChart().getStore();
        store.removeAll();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          const len = data.length;
          for (let i = len - 1; i >= 0; i--) {
            const d = data[i];
            store.add(d);
          }
        }

        el?.unmask();
      }
    });
  },
});
