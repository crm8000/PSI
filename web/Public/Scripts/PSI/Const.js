/**
 * PSI UI 常数
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.Const", {
  statics: {
    REQUIRED: '<span style="color:red;font-weight:bold" data-qtip="必录项">*</span>',
    TABLE_LAYOUT: {
      style: {
        borderSpacing: "5px 8px",
      }
    },
    TABLE_LAYOUT_SMALL: {
      style: {
        borderSpacing: "3px 5px",
      }
    },
    BTN_STYLE: {
      height: 28,
    },
    BODY_PADDING: {
      bodyPadding: "0 20 5 15",
    },
    RENDER_TO() { return PCL.getCmp("_PSITopPanel")?.getEl() },

    /**
     * msgBoxShowing不是个常数，是个Flag性质的全局变量
     * 当需要找一个地方存储全局变量的时候，发现现有代码没有合适的地方
     * 就决定把它存储在PSI.Const中了，所以它的命名也没有全部大写，而是个普通变量命名风格
     * 
     * 应用场景：
     * 当MsgBox显示提示信息的时候，msgBoxShowing == true；否则msgBoxShowing == false
     * 
     * 这样在全局快捷键处理的时候就能判断当前MsgBox是否显示了窗体，从而避免这个时候触发全局快捷键
     * 这里的全局快捷键是指：单据中Alt+N新建单据这类操作
     */
    msgBoxShowing: false,

    SHOW_MAIN_MENU_ITEM_HINT: "1",
    LOADING: "数据加载中...",
    SAVING: "数据保存中...",
    BASE_URL: "",
    MOT: "0", // 模块打开方式
    GC_DEC_NUMBER: 0, // 物料数量小数位数
    ENABLE_LODOP: "0", //是否启用Lodop打印
    ENABLE_MATERIAL_COLOR: "0",// 是否启用物料颜色
    PROD_NAME: "PSI", // 产品名称
    VERSION: "PSI 2025 - built2025-03-13 08:35"
  }
});
