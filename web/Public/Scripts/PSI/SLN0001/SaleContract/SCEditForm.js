/**
 * 销售合同 - 新增或编辑界面
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.SaleContract.SCEditForm", {
  extend: "PSI.AFX.Form.EditForm",

  config: {
    showAddGoodsButton: false
  },

  mixins: ["PSI.SLN0001.Mix.GoodsPrice"],

  /**
   * @override
   */
  initComponent() {
    const me = this;
    me._readOnly = false;
    const entity = me.getEntity();
    me.adding = entity == null;

    const action = entity == null ? "新建" : "编辑";
    const title = me.formatTitleLabel("销售合同", action);

    PCL.apply(me, {
      header: false,
      padding: "0 0 0 0",
      border: 0,
      maximized: true,
      width: 1000,
      height: 600,
      layout: "border",
      tbar: [{
        id: me.buildId(me, "dfTitle"),
        value: title, xtype: "displayfield"
      }, "->", {
        text: "保存 <span class='PSI-shortcut-DS'>Alt + S</span>",
        tooltip: me.buildTooltip("快捷键：Alt + S"),
        ...PSI.Const.BTN_STYLE,
        id: me.buildId(me, "buttonSave"),
        iconCls: "PSI-button-ok",
        handler: me._onOK,
        scope: me
      }, "-", {
        text: "取消",
        iconCls: "PSI-tb-close",
        ...PSI.Const.BTN_STYLE,
        id: me.buildId(me, "buttonCancel"),
        handler() {
          if (me._readonly) {
            me.close();
            return;
          }

          me.confirm("请确认是否取消当前操作？",
            () => {
              me.close();
            });
        },
        scope: me
      }, "-", {
        text: "表单通用操作指南",
        ...PSI.Const.BTN_STYLE,
        iconCls: "PSI-help",
        handler() {
          me.focus();
          window.open(me.URL("Home/Help/index?t=commBill"));
        }
      }, "-", {
        margin: "5 5 5 0",
        cls: "PSI-toolbox",
        labelWidth: 0,
        width: 90,
        emptyText: "快捷访问",
        xtype: "psi_mainmenushortcutfield"
      }],
      items: [{
        region: "center",
        xtype: "tabpanel",
        border: 0,
        bodyPadding: 10,
        bodyStyle: { borderWidth: 0 },
        items: [me.getGoodsGrid(), me.getClausePanel()]
      }, {
        region: "north",
        id: "editForm",
        layout: {
          type: "table",
          columns: 4,
          tableAttrs: PSI.Const.TABLE_LAYOUT_SMALL,
        },
        height: 175,
        bodyPadding: 10,
        border: 0,
        items: me.getEditorList()
      }],
      listeners: {
        show: {
          fn: me._onWndShow,
          scope: me
        },
        close: {
          fn: me._onWndClose,
          scope: me
        }
      }
    });

    me.callParent(arguments);

    me.hiddenId = PCL.getCmp(me.buildId(me, "hiddenId"));
    me.editRef = PCL.getCmp(me.buildId(me, "editRef"));
    me.editCustomer = PCL.getCmp(me.buildId(me, "editCustomer"));
    me.editBeginDT = PCL.getCmp(me.buildId(me, "editBeginDT"));
    me.editEndDT = PCL.getCmp(me.buildId(me, "editEndDT"));
    me.editOrg = PCL.getCmp(me.buildId(me, "editOrg"));
    me.editBizDT = PCL.getCmp(me.buildId(me, "editBizDT"));
    me.editDealDate = PCL.getCmp(me.buildId(me, "editDealDate"));
    me.editDealAddress = PCL.getCmp(me.buildId(me, "editDealAddress"));
    me.editBizUser = PCL.getCmp(me.buildId(me, "editBizUser"));
    me.editDiscount = PCL.getCmp(me.buildId(me, "editDiscount"));
    me.editBillMemo = PCL.getCmp(me.buildId(me, "editBillMemo"));
    me.editQualityClause = PCL.getCmp(me.buildId(me, "editQualityClause"));
    me.editInsuranceClause = PCL.getCmp(me.buildId(me, "editInsuranceClause"));
    me.editTransportClause = PCL.getCmp(me.buildId(me, "editTrasportClause"));
    me.editOtherClause = PCL.getCmp(me.buildId(me, "editOtherClause"));

    me.dfTitle = PCL.getCmp(me.buildId(me, "dfTitle"));

    me.buttonSave = PCL.getCmp(me.buildId(me, "buttonSave"));
    me.buttonCancel = PCL.getCmp(me.buildId(me, "buttonCancel"));

    me.columnActionDelete = PCL.getCmp(me.buildId(me, "columnActionDelete"));
    me.columnActionAdd = PCL.getCmp(me.buildId(me, "columnActionAdd"));
    me.columnActionAppend = PCL.getCmp(me.buildId(me, "columnActionAppend"));

    // AFX
    me.__editorList = [
      me.editCustomer, me.editBeginDT, me.editEndDT,
      me.editOrg, me.editBizDT, me.editDealDate,
      me.editDealAddress, me.editBizUser, me.editDiscount,
      me.editBillMemo,
    ];

    me._keyMap = PCL.create("PCL.util.KeyMap", PCL.getBody(), {
      key: "S",
      ctrl: false,
      shift: false,
      alt: true,
      fn() {
        if (PSI.Const.msgBoxShowing) {
          return;
        }

        if (me._readonly) {
          return;
        }

        me._onOK.apply(me, []);
      },
      scope: me
    });
  },

  /**
   * @private
   */
  getEditorList() {
    const me = this;
    const entity = me.getEntity();

    return [{
      xtype: "hidden",
      id: me.buildId(me, "hiddenId"),
      value: entity == null ? null : entity.get("id")
    }, {
      id: me.buildId(me, "editRef"),
      labelWidth: 95,
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "销售合同号",
      xtype: "displayfield",
      colspan: 4,
      value: me.toFieldNoteText("保存后自动生成")
    }, {
      id: me.buildId(me, "editCustomer"),
      colspan: 2,
      width: 430,
      labelWidth: 95,
      labelAlign: "right",
      labelSeparator: "",
      xtype: "psi_customerfield",
      fieldLabel: "甲方客户",
      allowBlank: false,
      blankText: "没有输入客户",
      beforeLabelTextTpl: PSI.Const.REQUIRED,
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      },
      showAddButton: true,
      callbackFunc: me._setCustomerExtData,
      callbackFuncScope: me,
    }, {
      id: me.buildId(me, "editBeginDT"),
      fieldLabel: "合同开始日期",
      labelWidth: 95,
      labelAlign: "right",
      labelSeparator: "",
      allowBlank: false,
      blankText: "没有输入合同开始日期",
      beforeLabelTextTpl: PSI.Const.REQUIRED,
      xtype: "datefield",
      format: "Y-m-d",
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      }
    }, {
      id: me.buildId(me, "editEndDT"),
      fieldLabel: "合同结束日期",
      labelWidth: 95,
      labelAlign: "right",
      labelSeparator: "",
      allowBlank: false,
      blankText: "没有输入合同结束日期",
      beforeLabelTextTpl: PSI.Const.REQUIRED,
      xtype: "datefield",
      format: "Y-m-d",
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      }
    }, {
      id: me.buildId(me, "editOrg"),
      labelWidth: 95,
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "乙方组织机构",
      xtype: "psi_orgwithdataorgfield",
      colspan: 2,
      width: 430,
      allowBlank: false,
      blankText: "没有输入组织机构",
      beforeLabelTextTpl: PSI.Const.REQUIRED,
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      }
    }, {
      id: me.buildId(me, "editBizDT"),
      fieldLabel: "合同签订日期",
      labelWidth: 95,
      labelAlign: "right",
      labelSeparator: "",
      allowBlank: false,
      blankText: "没有输入合同签订日期",
      beforeLabelTextTpl: PSI.Const.REQUIRED,
      xtype: "datefield",
      format: "Y-m-d",
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      }
    }, {
      id: me.buildId(me, "editDealDate"),
      fieldLabel: "交货日期",
      labelWidth: 95,
      labelAlign: "right",
      labelSeparator: "",
      allowBlank: false,
      blankText: "没有输入交货日期",
      beforeLabelTextTpl: PSI.Const.REQUIRED,
      xtype: "datefield",
      format: "Y-m-d",
      value: new Date(),
      name: "bizDT",
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      }
    }, {
      id: me.buildId(me, "editDealAddress"),
      labelWidth: 95,
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "交货地址",
      colspan: 2,
      width: 430,
      xtype: "textfield",
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      }
    }, {
      id: me.buildId(me, "editBizUser"),
      labelWidth: 95,
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "业务员",
      xtype: "psi_userfield",
      allowBlank: false,
      blankText: "没有输入业务员",
      beforeLabelTextTpl: PSI.Const.REQUIRED,
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      }
    }, {
      id: me.buildId(me, "editDiscount"),
      labelWidth: 95,
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "折扣率(%)",
      xtype: "numberfield",
      hideTrigger: true,
      allowDecimals: false,
      value: 100,
      listeners: {
        specialkey: {
          fn: me.__onEditSpecialKey,
          scope: me
        }
      }
    }, {
      id: me.buildId(me, "editBillMemo"),
      labelWidth: 95,
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "备注",
      xtype: "textfield",
      colspan: 3,
      width: 670,
      listeners: {
        specialkey: {
          fn: me._onLastEditSpecialKey,
          scope: me
        }
      }
    }];
  },

  /**
   * @private
   */
  _onWndClose() {
    const me = this;

    me._keyMap.destroy();

    const parentForm = me.getParentForm();
    if (parentForm) {
      parentForm._keyMapMain.enable();
    }

    PCL.WindowManager.hideAll();

    // AFX
    PCL.get(window).un('beforeunload', me.__onWindowBeforeUnload);
  },

  /**
   * @private
   */
  _onWndShow() {
    const me = this;

    // AFX
    PCL.get(window).on('beforeunload', me.__onWindowBeforeUnload);

    const parentForm = me.getParentForm();
    if (parentForm) {
      parentForm._keyMapMain.disable();
    }

    const el = me.getEl() || PCL.getBody();
    el.mask(PSI.Const.LOADING);
    me.ajax({
      url: me.URL("SLN0001/SaleContract/scBillInfo"),
      params: {
        id: me.hiddenId.getValue()
      },
      callback(options, success, response) {
        el.unmask();

        if (success) {
          const data = me.decodeJSON(response.responseText);

          if (data.ref) {
            me.editRef.setValue(me.toFieldNoteText(data.ref));
            me.editCustomer.setIdValue(data.customerId);
            me.editCustomer.setValue(data.customerName);
            me.editBeginDT.setValue(data.beginDT);
            me.editEndDT.setValue(data.endDT);
            me.editBizDT.setValue(data.bizDT);
            me.editDealDate.setValue(data.dealDate);
            me.editDealAddress.setValue(data.dealAddress);
            me.editDiscount.setValue(data.discount);
            me.editBillMemo.setValue(data.billMemo);
            me.editOrg.setIdValue(data.orgId);
            me.editOrg.setValue(data.orgFullName);
            me.editQualityClause.setValue(data.qualityClause);
            me.editInsuranceClause.setValue(data.insuranceClause);
            me.editTransportClause.setValue(data.transportClause);
            me.editOtherClause.setValue(data.otherClause);
          }

          me.editBizUser.setIdValue(data.bizUserId);
          me.editBizUser.setValue(data.bizUserName);

          const store = me.getGoodsGrid().getStore();
          store.removeAll();
          if (data.items) {
            store.add(data.items);
          }
          if (store.getCount() == 0) {
            store.add({});
          }

          if (data.billStatus && data.billStatus != 0) {
            me.setBillReadonly();
          }

          me.editCustomer.focus();
        }
      }
    });
  },

  /**
   * @private
   */
  _onOK() {
    const me = this;
    const customerId = me.editCustomer.getIdValue();
    if (!customerId) {
      me.showInfo("没有输入甲方客户", () => {
        me.editCustomer.focus();
      });
      return;
    }
    const beginDT = me.editBeginDT.getValue();
    if (!beginDT) {
      me.showInfo("没有输入合同开始日期", () => {
        me.editBeginDT.focus();
      });
      return;
    }
    const endDT = me.editEndDT.getValue();
    if (!endDT) {
      me.showInfo("没有输入合同结束日期", () => {
        me.editEndDT.focus();
      });
      return;
    }
    const orgId = me.editOrg.getIdValue();
    if (!orgId) {
      me.showInfo("没有输入乙方组织机构", () => {
        me.editOrg.focus();
      });
      return;
    }
    const bizDT = me.editBizDT.getValue();
    if (!bizDT) {
      me.showInfo("没有输入合同签订日期", () => {
        me.editBizDT.focus();
      });
      return;
    }
    const dealDate = me.editDealDate.getValue();
    if (!dealDate) {
      me.showInfo("没有输入交货日期", () => {
        me.editDealDate.focus();
      });
      return;
    }

    PCL.getBody().mask("正在保存中...");
    me.ajax({
      url: me.URL("SLN0001/SaleContract/editSCBill"),
      params: {
        adding: me.adding ? "1" : "0",
        jsonStr: me.getSaveData()
      },
      callback(options, success, response) {
        PCL.getBody().unmask();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          if (data.success) {
            me.close();
            const parentForm = me.getParentForm();
            if (parentForm) {
              parentForm.refreshMainGrid.apply(parentForm, [data.id]);
            }
            me.tip("成功保存数据", true);
          } else {
            me.showInfo(data.msg, () => {
              me.editCustomer.focus();
            });
          }
        }
      }
    });
  },

  /**
   * @private
   */
  _onLastEditSpecialKey(field, e) {
    const me = this;

    if (me._readonly) {
      return;
    }

    if (e.getKey() == e.ENTER) {
      const store = me.getGoodsGrid().getStore();
      if (store.getCount() == 0) {
        store.add({});
      }
      me.getGoodsGrid().focus();
      me._cellEditing.startEdit(0, 1);
    }
  },

  /**
   * @private
   */
  getGoodsGrid() {
    const me = this;
    if (me._goodsGrid) {
      return me._goodsGrid;
    }

    const modelName = me.buildModelName(me, "SCBillDetail_EditForm");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "goodsId", "goodsCode", "goodsName",
        "goodsSpec", "unitName", "goodsCount", {
          name: "goodsMoney",
          type: "float"
        }, "goodsPrice", {
          name: "taxRate",
          type: "int"
        }, {
          name: "tax",
          type: "float"
        }, {
          name: "moneyWithTax",
          type: "float"
        }, "memo", "goodsPriceWithTax"]
    });
    const store = PCL.create("PCL.data.Store", {
      autoLoad: false,
      model: modelName,
      data: []
    });

    me._cellEditing = PCL.create("PSI.UX.CellEditing", {
      clicksToEdit: 1,
      listeners: {
        edit: {
          fn: me._onCellEditingAfterEdit,
          scope: me
        }
      }
    });

    me._goodsGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-EF",
      viewConfig: {
        enableTextSelection: true,
        markDirty: !me.adding
      },
      title: "商品明细",
      features: [{
        ftype: "summary"
      }],
      plugins: [me._cellEditing],
      columnLines: true,
      columns: [{
        xtype: "rownumberer",
        text: "#",
        width: 30
      }, {
        header: "商品编码",
        dataIndex: "goodsCode",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        editor: {
          xtype: "psi_goods_with_saleprice_field",
          showAddButton: me.getShowAddGoodsButton() == "1",
          parentCmp: me,
          editCustomerName: "editCustomer",
          sumInv: "1",
          goodsInfoCallbackFunc: me._setGoodsInfo,
          goodsInfoCallbackFuncScope: me,
        }
      }, {
        menuDisabled: true,
        draggable: false,
        sortable: false,
        header: "品名/规格型号",
        dataIndex: "goodsName",
        width: 330,
        renderer(value, metaData, record) {
          return record.get("goodsName") + " " + record.get("goodsSpec");
        }
      }, {
        header: "销售数量",
        dataIndex: "goodsCount",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        align: "right",
        width: 90,
        editor: {
          xtype: "numberfield",
          allowDecimals: PSI.Const.GC_DEC_NUMBER > 0,
          decimalPrecision: PSI.Const.GC_DEC_NUMBER,
          minValue: 0,
          hideTrigger: true
        }
      }, {
        header: "单位",
        dataIndex: "unitName",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        width: 60,
        align: "center"
      }, {
        header: "销售单价",
        dataIndex: "goodsPrice",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 90,
        editor: {
          xtype: "numberfield",
          hideTrigger: true
        },
        summaryRenderer() {
          return "金额合计";
        }
      }, {
        header: "销售金额",
        dataIndex: "goodsMoney",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 90,
        editor: {
          xtype: "numberfield",
          hideTrigger: true
        },
        summaryType: "sum"
      }, {
        header: "含税价",
        dataIndex: "goodsPriceWithTax",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 90,
        editor: {
          xtype: "numberfield",
          hideTrigger: true
        }
      }, {
        header: "税率(%)",
        dataIndex: "taxRate",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        align: "right",
        width: 80
      }, {
        header: "税金",
        dataIndex: "tax",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 90,
        editor: {
          xtype: "numberfield",
          hideTrigger: true
        },
        summaryType: "sum"
      }, {
        header: "价税合计",
        dataIndex: "moneyWithTax",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        align: "right",
        xtype: "numbercolumn",
        width: 90,
        editor: {
          xtype: "numberfield",
          hideTrigger: true
        },
        summaryType: "sum"
      }, {
        header: "备注",
        dataIndex: "memo",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        editor: {
          xtype: "textfield"
        }
      }, {
        header: "",
        id: me.buildId(me, "columnActionDelete"),
        align: "center",
        menuDisabled: true,
        draggable: false,
        width: 40,
        xtype: "actioncolumn",
        items: [{
          icon: me.URL("Public/Images/icons/delete.png"),
          tooltip: "删除当前记录",
          handler(grid, row) {
            const store = grid.getStore();
            store.remove(store.getAt(row));
            if (store.getCount() == 0) {
              store.add({});
            }
          },
          scope: me
        }]
      }, {
        header: "",
        id: me.buildId(me, "columnActionAdd"),
        align: "center",
        menuDisabled: true,
        draggable: false,
        width: 40,
        xtype: "actioncolumn",
        items: [{
          icon: me.URL("Public/Images/icons/insert.png"),
          tooltip: "在当前记录之前插入新记录",
          handler(grid, row) {
            const store = grid.getStore();
            store.insert(row, [{}]);
          },
          scope: me
        }]
      }, {
        header: "",
        id: me.buildId(me, "columnActionAppend"),
        align: "center",
        menuDisabled: true,
        draggable: false,
        width: 40,
        xtype: "actioncolumn",
        items: [{
          icon: me.URL("Public/Images/icons/add.png"),
          tooltip: "在当前记录之后新增记录",
          handler(grid, row) {
            const store = grid.getStore();
            store.insert(row + 1, [{}]);
          },
          scope: me
        }]
      }],
      store,
      listeners: {
        cellclick() {
          return !me._readonly;
        }
      }
    });

    return me._goodsGrid;
  },

  /**
   * @private
   */
  getClausePanel() {
    const me = this;
    if (me._clausePanel) {
      return me._clausePanel;
    }

    me._clausePanel = PCL.create("PCL.panel.Panel", {
      title: "合同条款",
      autoScroll: true,
      border: 0,
      layout: "form",
      bodyPadding: 5,
      defaults: {
        labelSeparator: "",
        hideLabel: true,
        rows: 3
      },
      cls: "PSI-SCBill",
      items: [{
        xtype: "displayfield",
        value: "品质条款"
      }, {
        xtype: "textareafield",
        id: me.buildId(me, "editQualityClause")
      }, {
        xtype: "displayfield",
        value: "保险条款"
      }, {
        xtype: "textareafield",
        id: me.buildId(me, "editInsuranceClause")
      }, {
        xtype: "displayfield",
        value: "运输条款"
      }, {
        xtype: "textareafield",
        id: me.buildId(me, "editTrasportClause")
      }, {
        xtype: "displayfield",
        value: "其他条款"
      }, {
        xtype: "textareafield",
        id: me.buildId(me, "editOtherClause")
      }]
    });

    return me._clausePanel;
  },

  /**
   * @private
   */
  _setGoodsInfo(data) {
    const me = this;
    const item = me.getGoodsGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      return;
    }
    const goods = item[0];

    goods.set("goodsId", data.id);
    goods.set("goodsCode", data.code);
    goods.set("goodsName", data.name);
    goods.set("unitName", data.unitName);
    goods.set("goodsSpec", data.spec);
    goods.set("goodsPrice", data.salePrice);
    goods.set("taxRate", data.taxRate);

    me.calcMoney(goods);
  },

  /**
   * @private
   */
  _onCellEditingAfterEdit(editor, e) {
    const me = this;

    if (me._readonly) {
      return;
    }

    const fieldName = e.field;
    const goods = e.record;
    const oldValue = e.originalValue;
    if (fieldName == "memo") {
      const store = me.getGoodsGrid().getStore();
      if (e.rowIdx == store.getCount() - 1) {
        store.add({});

        const row = e.rowIdx + 1;
        me.getGoodsGrid().getSelectionModel().select(row);
        me._cellEditing.startEdit(row, 1);
      }
    } else if (fieldName == "moneyWithTax") {
      if (goods.get(fieldName) != (new Number(oldValue)).toFixed(2)) {
        me.calcTax(goods);
      }
    } else if (fieldName == "tax") {
      if (goods.get(fieldName) != (new Number(oldValue)).toFixed(2)) {
        me.calcMoneyWithTax(goods);
      }
    } else if (fieldName == "goodsMoney") {
      if (goods.get(fieldName) != (new Number(oldValue)).toFixed(2)) {
        me.calcPrice(goods);
      }
    } else if (fieldName == "goodsCount") {
      if (goods.get(fieldName) != oldValue) {
        me.calcMoney(goods);
      }
    } else if (fieldName == "goodsPrice") {
      if (goods.get(fieldName) != (new Number(oldValue)).toFixed(2)) {
        me.calcMoney(goods);
      }
    } else if (fieldName == "goodsPriceWithTax") {
      if (goods.get(fieldName) != (new Number(oldValue)).toFixed(2)) {
        me.calcMoney2(goods);
      }
    }
  },

  /**
   * @private
   */
  getSaveData() {
    const me = this;

    const result = {
      id: me.hiddenId.getValue(),
      customerId: me.editCustomer.getIdValue(),
      beginDT: PCL.Date.format(me.editBeginDT.getValue(), "Y-m-d"),
      endDT: PCL.Date.format(me.editEndDT.getValue(), "Y-m-d"),
      orgId: me.editOrg.getIdValue(),
      bizDT: PCL.Date.format(me.editBizDT.getValue(), "Y-m-d"),
      dealDate: PCL.Date.format(me.editDealDate.getValue(), "Y-m-d"),
      dealAddress: me.editDealAddress.getValue(),
      bizUserId: me.editBizUser.getIdValue(),
      discount: me.editDiscount.getValue(),
      billMemo: me.editBillMemo.getValue(),
      qualityClause: me.editQualityClause.getValue(),
      insuranceClause: me.editInsuranceClause.getValue(),
      transportClause: me.editTransportClause.getValue(),
      otherClause: me.editOtherClause.getValue(),
      items: []
    };

    const store = me.getGoodsGrid().getStore();
    for (let i = 0; i < store.getCount(); i++) {
      const item = store.getAt(i);
      result.items.push({
        id: item.get("id"),
        goodsId: item.get("goodsId"),
        goodsCount: item.get("goodsCount"),
        goodsPrice: item.get("goodsPrice"),
        goodsMoney: item.get("goodsMoney"),
        tax: item.get("tax"),
        taxRate: item.get("taxRate"),
        moneyWithTax: item.get("moneyWithTax"),
        memo: item.get("memo"),
        goodsPriceWithTax: item.get("goodsPriceWithTax")
      });
    }

    return me.encodeJSON(result);
  },

  /**
   * @private
   */
  setBillReadonly() {
    const me = this;
    me._readonly = true;
    me.dfTitle.setValue(me.formatTitleLabel("销售合同", "查看"));

    me.buttonSave.setDisabled(true);
    me.buttonCancel.setText("关闭");
    me.editDealDate.setReadOnly(true);
    me.editCustomer.setReadOnly(true);
    me.editBeginDT.setReadOnly(true);
    me.editEndDT.setReadOnly(true);
    me.editBizDT.setReadOnly(true);
    me.editDealAddress.setReadOnly(true);
    me.editOrg.setReadOnly(true);
    me.editBizUser.setReadOnly(true);
    me.editBillMemo.setReadOnly(true);
    me.editDiscount.setReadOnly(true);

    me.editQualityClause.setReadOnly(true);
    me.editInsuranceClause.setReadOnly(true);
    me.editTransportClause.setReadOnly(true);
    me.editOtherClause.setReadOnly(true);

    me.columnActionDelete.hide();
    me.columnActionAdd.hide();
    me.columnActionAppend.hide();
  },

  /**
   * @private
   */
  _setCustomerExtData(data) {
    const me = this;

    me.editDealAddress.setValue(data.address_receipt);
  }
});
