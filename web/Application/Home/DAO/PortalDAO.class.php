<?php

namespace Home\DAO;

/**
 * Portal DAO
 *
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
class PortalDAO extends PSIBaseExDAO
{

  /**
   * 查询Portal项分类
   */
  public function queryPortalCategory($params)
  {
    $db = $this->db;

    $userId = $params["loginUserId"];
    $userDAO = new UserDAO($db);

    $result = [];

    $sql = "select id, name
            from t_portal_item_category
            order by show_order";

    $data = $db->query($sql);
    foreach ($data as $v) {
      $categoryId = $v["id"];

      $sql = "select name, icon_class, js_class_name, fid, col_span
              from t_portal_item
              where category_id = '%s'
              order by show_order";
      $list = $db->query($sql, $categoryId);
      $portalList = [];
      foreach ($list as $li) {
        $fid = $li["fid"];
        if (!$userDAO->hasPermission($userId, $fid)) {
          continue;
        }

        $portalList[] = [
          "name" => $li["name"],
          "iconClass" => $li["icon_class"],
          "jsClassName" => $li["js_class_name"],
          "colSpan" => $li["col_span"],
        ];
      }

      // $portalList里面存放的是当前分类下，用户具有权限的门户项
      if (count($portalList) > 0) {
        $result[] = [
          "caption" => $v["name"],
          "portalItemList" => $portalList,
        ];
      }
    }

    return $result;
  }
}
