/**
 * 自定义字段 - 上级商品品牌字段
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.Goods.ParentBrandEditor", {
  extend: "PCL.form.field.Trigger",
  alias: "widget.PSI_parent_brand_editor",

  config: {
    parentItem: null,
    parentBrandFunc: null,
  },

  /**
   * @override
   */
  initComponent() {
    const me = this;

    me.enableKeyEvents = true;

    me.callParent(arguments);

    me.on("keydown", (field, e) => {
      if (e.getKey() === e.BACKSPACE) {
        e.preventDefault();
        return false;
      }

      if (e.getKey() !== e.ENTER) {
        me.onTriggerClick(e);
      }
    });
  },

  /**
   * @override
   */
  onTriggerClick(e) {
    const me = this;

    const modelName = "PSIModel_ParentBrandEditor";
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "text", "fullName", "leaf",
        "children"]
    });

    const store = PCL.create("PCL.data.TreeStore", {
      model: modelName,
      proxy: {
        type: "ajax",
        actionMethods: {
          read: "POST"
        },
        url: PSI.Const.BASE_URL + "SLN0001/Goods/allBrands"
      }
    });

    const tree = PCL.create("PCL.tree.Panel", {
      cls: "PSI",
      store: store,
      rootVisible: false,
      useArrows: true,
      viewConfig: {
        loadMask: true
      },
      columns: {
        defaults: {
          flex: 1,
          sortable: false,
          menuDisabled: true,
          draggable: false
        },
        items: [{
          xtype: "treecolumn",
          text: "品牌",
          dataIndex: "text"
        }]
      }
    });
    tree.on("itemdblclick", me._onOK, me);
    me.tree = tree;

    const wnd = PCL.create("PCL.window.Window", {
      title: "选择上级品牌",
      modal: true,
      width: 400,
      height: 300,
      layout: "fit",
      items: [tree],
      buttons: [{
        text: "没有上级品牌",
        cls: "PSI-Lookup-btn",
        handler: me._onNone,
        scope: me
      }, {
        text: "确定",
        cls: "PSI-Lookup-btn",
        handler: me._onOK,
        scope: me
      }, {
        text: "取消",
        cls: "PSI-Lookup-btn",
        handler() {
          wnd.close();
        }
      }]
    });
    me.wnd = wnd;
    wnd.show();
  },

  /**
   * @private
   */
  _onOK() {
    const me = this;
    const tree = me.tree;
    const item = tree.getSelectionModel().getSelection();

    if (item === null || item.length !== 1) {
      PSI.MsgBox.showInfo("没有选择上级品牌");

      return;
    }

    const data = item[0].data;

    const func = me.getParentBrandFunc();
    if (func) {
      func.apply(me.getParentItem(), [data]);
    }

    me.wnd.close();
    me.focus();
  },

  /**
   * @private
   */
  _onNone() {
    const me = this;
    const func = me.getParentBrandFunc();
    if (func) {
      func.apply(me.getParentItem(), [{
        id: "",
        fullName: ""
      }]);
    }

    me.wnd.close();
    me.focus();
  }
});
