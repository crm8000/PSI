/**
 * 系统数据字典 - 主界面
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0000.SysDict.MainForm", {
  extend: "PSI.AFX.Form.MainForm",

  /**
   * @override
   */
  initComponent() {
    const me = this;

    PCL.apply(me, {
      items: [{
        tbar: me.getToolbarCmp(),
        id: me.buildId(me, "panelQueryCmp"),
        region: "north",
        border: 0,
        height: 85,
        bodyPadding: "10 0 0 0",
        header: false,
        collapsible: true,
        collapseMode: "mini",
        layout: {
          type: "table",
          columns: 5
        },
        bodyCls: "PSI-Query-Panel",
        items: me.getQueryCmp()
      }, {
        region: "center",
        layout: "border",
        border: 0,
        items: [{
          region: "center",
          xtype: "panel",
          layout: "border",
          border: 0,
          bodyPadding: "0 20 5 0",
          items: [{
            region: "center",
            layout: "fit",
            border: 0,
            items: me.getMainGrid()
          }, {
            region: "south",
            layout: "fit",
            border: 0,
            height: "60%",
            split: true,
            items: [me.getDictDataGrid()]
          }]
        }, {
          id: me.buildId(me, "panelCategory"),
          xtype: "panel",
          region: "west",
          layout: "fit",
          width: 300,
          split: true,
          collapsible: true,
          header: false,
          border: 0,
          bodyPadding: "0 0 5 15",
          items: [me.getCategoryGrid()]
        }]
      }]
    });

    me.callParent(arguments);

    me.refreshCategoryGrid();
  },

  /**
   * @private
   */
  getToolbarCmp() {
    const me = this;

    return [{
      iconCls: "PSI-tb-help",
      ...PSI.Const.BTN_STYLE,
      text: "指南",
      handler() {
        me.focus();
        window.open(me.URL("Home/Help/index?t=sysdict"));
      }
    }, "-", {
      iconCls: "PSI-tb-close",
      ...PSI.Const.BTN_STYLE,
      text: "关闭",
      handler() {
        me.closeWindow();
      }
    }, {
      // 空容器，只是为了撑高工具栏
      xtype: "container", height: 28,
      items: []
    }].concat(me.getShortcutCmp());
  },

  /**
   * 快捷访问
   * 
   * @private
   */
  getShortcutCmp() {
    return ["->",
      {
        cls: "PSI-Shortcut-Cmp",
        labelWidth: 0,
        emptyText: "快捷访问",
        xtype: "psi_mainmenushortcutfield",
        width: 90
      }
    ];
  },

  /**
  * @private
  */
  getQueryCmp() {
    const me = this;
    return [{
      xtype: "container",
      height: 26,
      html: `<h2 style='margin-left:20px;margin-top:3px;color:#595959;display:inline-block'>系统数据字典</h2>
              &nbsp;&nbsp;<span style='color:#8c8c8c'>系统后台字典，供实施中速查</span>
              <div style='float:right;display:inline-block;margin:0px 0px 0px 20px;border-left:1px solid #e5e6e8;height:26px'>&nbsp;</div>
              `
    }, {
      id: me.buildId(me, "editQueryTableName"),
      labelWidth: 85,
      labelAlign: "right",
      labelSeparator: "",
      fieldLabel: "数据库表名",
      margin: "5, 0, 0, 0",
      xtype: "textfield",
      width: 300,
      listeners: {
        specialkey: {
          fn: me.__onLastEditSpecialKey,
          scope: me
        }
      }
    }, {
      xtype: "container",
      items: [{
        xtype: "button",
        text: "查询",
        cls: "PSI-Query-btn1",
        width: 100,
        height: 26,
        margin: "5, 0, 0, 20",
        handler: me._onQuery,
        scope: me
      }, {
        xtype: "button",
        text: "清空查询条件",
        cls: "PSI-Query-btn2",
        width: 100,
        height: 26,
        margin: "5, 0, 0, 5",
        handler: me._onClearQuery,
        scope: me
      }, {
        xtype: "button",
        text: "隐藏工具栏",
        cls: "PSI-Query-btn3",
        width: 110,
        height: 26,
        iconCls: "PSI-button-hide",
        margin: "5 0 0 20",
        handler() {
          const id = me.buildId(me, "panelQueryCmp");
          PCL.getCmp(id).collapse();
        },
        scope: me
      }]
    }];
  },

  /**
   * @private
   */
  getCategoryGrid() {
    const me = this;

    if (me._categoryGrid) {
      return me._categoryGrid;
    }

    const modelName = me.buildModelName(me, "SysDictCategory");

    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "code", "name"]
    });

    me._categoryGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-FC",
      viewConfig: {
        enableTextSelection: true
      },
      header: {
        height: 30,
        title: me.formatGridHeaderTitle("系统数据字典分类")
      },
      tools: [{
        type: "close",
        handler() {
          PCL.getCmp(me.buildId(me, "panelCategory")).collapse();
        }
      }],
      columnLines: true,
      columns: [{
        header: "分类编码",
        dataIndex: "code",
        width: 100,
        menuDisabled: true,
        sortable: false
      }, {
        header: "分类",
        dataIndex: "name",
        width: 200,
        menuDisabled: true,
        sortable: false
      }],
      store: PCL.create("PCL.data.Store", {
        model: modelName,
        autoLoad: false,
        data: []
      }),
      listeners: {
        select: {
          fn: me._onCategoryGridSelect,
          scope: me
        }
      }
    });

    return me._categoryGrid;
  },

  /**
   * @private
   */
  getMainGrid() {
    const me = this;

    if (me._mainGrid) {
      return me._mainGrid;
    }

    const modelName = me.buildModelName(me, "SysDictModel");

    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "code", "name", "tableName", "memo"]
    });

    me._mainGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-HL",
      viewConfig: {
        enableTextSelection: true
      },
      header: {
        height: 30,
        title: me.formatGridHeaderTitle("数据字典")
      },
      columnLines: true,
      columns: {
        defaults: {
          menuDisabled: true,
          sortable: false
        },
        items: [{
          header: "编码",
          dataIndex: "code",
          width: 140,
        }, {
          header: "数据字典名称",
          dataIndex: "name",
          width: 200,
        }, {
          header: "数据库表名",
          dataIndex: "tableName",
          width: 300,
        }, {
          header: "备注",
          dataIndex: "memo",
          width: 400,
          renderer(value) {
            return me.toAutoWrap(value);
          }
        }]
      },
      store: PCL.create("PCL.data.Store", {
        model: modelName,
        autoLoad: false,
        data: []
      }),
      listeners: {
        select: {
          fn: me._onMainGridSelect,
          scope: me
        }
      }
    });

    return me._mainGrid;
  },

  /**
   * @private
   */
  getDictDataGrid() {
    const me = this;

    if (me._dataGrid) {
      return me._dataGrid;
    }

    const modelName = me.buildModelName(me, "SysDictData");

    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "code", "codeInt", "name", "memo"]
    });

    me._dataGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-HL",
      viewConfig: {
        enableTextSelection: true
      },
      header: {
        height: 30,
        title: me.formatGridHeaderTitle("数据")
      },
      columnLines: true,
      columns: {
        defaults: {
          menuDisabled: true,
          sortable: false
        },
        items: [PCL.create("PCL.grid.RowNumberer", {
          text: "#",
          align: "right",
          width: 60
        }), {
          header: "编码(字符类型)",
          dataIndex: "code",
          width: 150
        }, {
          header: "编码(整数类型)",
          dataIndex: "codeInt",
          width: 150,
          align: "right"
        }, {
          header: "值",
          dataIndex: "name",
          width: 250
        }, {
          header: "备注",
          dataIndex: "memo",
          width: 400,
          renderer(value) {
            return me.toAutoWrap(value);
          }
        }]
      },
      store: PCL.create("PCL.data.Store", {
        model: modelName,
        autoLoad: false,
        data: []
      })
    });

    return me._dataGrid;
  },

  /**
   * @private
   */
  refreshCategoryGrid(id) {
    const me = this;
    const grid = me.getCategoryGrid();
    const el = grid.getEl() || PCL.getBody();
    el.mask(PSI.Const.LOADING);
    const r = {
      url: me.URL("SLN0000/SysDict/categoryList"),
      params: {
        queryTableName: PCL.getCmp(me.buildId(me, "editQueryTableName")).getValue(),
      },
      callback(options, success, response) {
        const store = grid.getStore();

        store.removeAll();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          store.add(data);

          if (store.getCount() > 0) {
            if (id) {
              const r = store.findExact("id", id);
              if (r != -1) {
                grid.getSelectionModel().select(r);
              }
            } else {
              grid.getSelectionModel().select(0);
            }
          }
        }

        el.unmask();
      }
    };

    me.ajax(r);
  },

  /**
   * @private
   */
  _onCategoryGridSelect() {
    const me = this;
    me.refreshMainGrid();
  },

  /**
   * @private
   */
  refreshMainGrid(id) {
    const me = this;
    me.getDictDataGrid().getStore().removeAll();
    me.getDictDataGrid().setTitle(me.formatGridHeaderTitle("数据"));

    const item = me.getCategoryGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.getMainGrid().setTitle(me.formatGridHeaderTitle("数据字典"));
      return;
    }

    const category = item[0];

    const grid = me.getMainGrid();
    grid.setTitle(me.formatGridHeaderTitle(`<span class='PSI-title-keyword'>${category.get("name")}</span> - 数据字典列表`));
    const el = grid.getEl() || PCL.getBody();
    el.mask(PSI.Const.LOADING);
    const r = {
      url: me.URL("SLN0000/SysDict/sysDictList"),
      params: {
        categoryId: category.get("id"),
        queryTableName: PCL.getCmp(me.buildId(me, "editQueryTableName")).getValue(),
      },
      callback(options, success, response) {
        const store = grid.getStore();

        store.removeAll();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          store.add(data);

          if (store.getCount() > 0) {
            if (id) {
              const r = store.findExact("id", id);
              if (r != -1) {
                grid.getSelectionModel().select(r);
              }
            } else {
              grid.getSelectionModel().select(0);
            }
          }
        }

        el.unmask();
      }
    };

    me.ajax(r);
  },

  /**
   * @private
   */
  _onMainGridSelect() {
    const me = this;
    me.refreshDictDataGrid();
  },

  /**
   * @private
   */
  refreshDictDataGrid(id) {
    const me = this;
    const item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.getMainGrid().setTitle(me.formatGridHeaderTitle("数据字典"));
      me.getDictDataGrid().setTitle(me.formatGridHeaderTitle("数据"));
      return;
    }

    const sysDict = item[0];

    const grid = me.getDictDataGrid();
    grid.setTitle(me.formatGridHeaderTitle(`<span class='PSI-title-keyword'>${sysDict.get("name")} ${sysDict.get("tableName")}</span> - 数据`));
    const el = grid.getEl() || PCL.getBody();
    el.mask(PSI.Const.LOADING);
    const r = {
      url: me.URL("SLN0000/SysDict/dictDataList"),
      params: {
        id: sysDict.get("id")
      },
      callback(options, success, response) {
        const store = grid.getStore();

        store.removeAll();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          store.add(data);

          if (store.getCount() > 0) {
            if (id) {
              const r = store.findExact("id", id);
              if (r != -1) {
                grid.getSelectionModel().select(r);
              }
            } else {
              grid.getSelectionModel().select(0);
            }
          }
        }

        el.unmask();
      }
    };

    me.ajax(r);
  },

  /**
 * @private
 */
  _onQuery() {
    const me = this;


    me.refreshCategoryGrid();
  },

  /**
   * @private
   */
  _onClearQuery() {
    const me = this;

    PCL.getCmp(me.buildId(me, "editQueryTableName")).setValue(null);

    me._onQuery();
  },
});
