<?php

namespace SLN0002\Service;

use Home\Common\FIdConst;
use Home\DAO\OrgDAO;
use Home\Service\BizlogService;
use Home\Service\PSIBaseExService;
use Home\Service\UserService;
use SLN0002\DAO\VoucherDAO;

/**
 * 记账凭证 Service
 *
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
class VoucherService extends PSIBaseExService
{
  private $LOG_CATEGORY = "记账凭证";

  /**
   * 返回所有的公司列表
   *
   * @return array
   */
  public function companyList()
  {
    if ($this->isNotOnline()) {
      return $this->emptyResult();
    }

    $params = [
      "loginUserId" => $this->getLoginUserId(),
      "fid" => FIdConst::VOUCHER
    ];

    $dao = new OrgDAO($this->db());
    return $dao->getCompanyExList($params);
  }

  /**
   * 凭证主表列表
   */
  public function voucherList($params)
  {
    if ($this->isNotOnline()) {
      return $this->emptyResult();
    }

    $params["loginUserId"] = $this->getLoginUserId();

    $dao = new VoucherDAO($this->db());
    return $dao->voucherList($params);
  }

  /**
   * 查询凭证字列表
   */
  function queryVoucherWord($params)
  {
    if ($this->isNotOnline()) {
      return $this->emptyResult();
    }

    $dao = new VoucherDAO($this->db());
    return $dao->queryVoucherWord($params);
  }

  /**
   * 查询账样的扩展项
   */
  function queryFmtEx($params)
  {
    if ($this->isNotOnline()) {
      return $this->emptyResult();
    }

    $dao = new VoucherDAO($this->db());
    return $dao->queryFmtEx($params);
  }

  /**
   * 新建或编辑凭证
   */
  public function editVoucher($json)
  {
    if ($this->isNotOnline()) {
      return $this->notOnlineError();
    }

    $bill = json_decode(html_entity_decode($json), true);
    if ($bill == null) {
      return $this->bad("传入的参数错误，不是正确的JSON格式");
    }

    $db = $this->db();

    $db->startTrans();

    $dao = new VoucherDAO($db);

    $us = new UserService();
    $bill["loginUserId"] = $us->getLoginUserId();
    $bill["dataOrg"] = $us->getLoginUserDataOrg();

    $id = $bill["id"];

    if ($id) {
      // 编辑
      $rc = $dao->updateVoucher($bill);
      if ($rc) {
        $db->rollback();
        return $rc;
      }
    } else {
      // 新建
      $rc = $dao->addVoucher($bill);
      if ($rc) {
        $db->rollback();
        return $rc;
      }

      $id = $bill["id"];
    }

    // 记录业务日志
    $log = $bill["log"];
    $bs = new BizlogService($db);
    $bs->insertBizlog($log, $this->LOG_CATEGORY);

    $db->commit();

    return $this->ok($id);
  }

  /**
   * 凭证分录
   */
  public function voucherDetailList($params)
  {
    if ($this->isNotOnline()) {
      return $this->emptyResult();
    }

    $dao = new VoucherDAO($this->db());
    return $dao->voucherDetailList($params);
  }

  /**
   * 删除凭证
   */
  public function deleteVoucher($id)
  {
    if ($this->isNotOnline()) {
      return $this->notOnlineError();
    }

    $db = $this->db();
    $db->startTrans();

    $dao = new VoucherDAO($db);
    $params = [
      "id" => $id,
    ];
    $rc = $dao->deleteVoucher($params);
    if ($rc) {
      $db->rollback();
      return $rc;
    }

    // 记录业务日志
    $log = $params["log"];
    $bs = new BizlogService($db);
    $bs->insertBizlog($log, $this->LOG_CATEGORY);

    $db->commit();

    return $this->ok();
  }

  /**
   * 凭证详情
   *
   * @param array $params
   */
  public function voucherInfo($params)
  {
    if ($this->isNotOnline()) {
      return $this->emptyResult();
    }

    $dao = new VoucherDAO($this->db());
    return $dao->voucherInfo($params);
  }

  /**
   * 复核凭证
   */
  public function commitVoucher($id)
  {
    if ($this->isNotOnline()) {
      return $this->notOnlineError();
    }

    $db = $this->db();
    $db->startTrans();

    $dao = new VoucherDAO($db);
    $params = [
      "id" => $id,
      "loginUserId" => $this->getLoginUserId(),
      "loginUserName" => $this->getLoginUserName(),
    ];
    $rc = $dao->commitVoucher($params);
    if ($rc) {
      $db->rollback();
      return $rc;
    }

    // 记录业务日志
    $log = $params["log"];
    $bs = new BizlogService($db);
    $bs->insertBizlog($log, $this->LOG_CATEGORY);

    $db->commit();

    return $this->ok();
  }

  /**
   * 取消复核凭证
   */
  public function cancelCommitVoucher($id)
  {
    if ($this->isNotOnline()) {
      return $this->notOnlineError();
    }

    $db = $this->db();
    $db->startTrans();

    $dao = new VoucherDAO($db);
    $params = [
      "id" => $id,
    ];
    $rc = $dao->cancelCommitVoucher($params);
    if ($rc) {
      $db->rollback();
      return $rc;
    }

    // 记录业务日志
    $log = $params["log"];
    $bs = new BizlogService($db);
    $bs->insertBizlog($log, $this->LOG_CATEGORY);

    $db->commit();

    return $this->ok();
  }

  /**
   * 登记明细分类账
   */
  public function voucherToAccDetail($params)
  {
    if ($this->isNotOnline()) {
      return $this->notOnlineError();
    }

    $db = $this->db();
    $dao = new VoucherDAO($db);
    // 第一步：先检查凭证号是否连续
    // 如果凭证断号，需要首先通过维护工具把凭证号调整到连续
    $rc = $dao->checkVoucherRef($params);
    if ($rc) {
      return $rc;
    }

    // 第二步：查询出所有未记账的凭证
    // 第三步：对每张凭证分别记账

    // 关于事务处理的说明：
    // 因为一次登记明细分类账的凭证量可能很多
    // 所以，事务是每张凭证处理的时候分别控制，这样可以避免事务超时

    return $this->todo();
  }

  /**
   * 凭证断号重排
   */
  public function refReorder($params)
  {
    if ($this->isNotOnline()) {
      return $this->notOnlineError();
    }

    $db = $this->db();
    $db->startTrans();

    $dao = new VoucherDAO($db);

    $rc = $dao->refReorder($params);
    if ($rc) {
      $db->rollback();
      return $rc;
    }

    // 记录业务日志
    $log = $params["log"];
    $bs = new BizlogService($db);
    $bs->insertBizlog($log, $this->LOG_CATEGORY);

    $db->commit();

    return $this->ok();
  }
}
