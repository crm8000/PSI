/**
 * 当创建Portal项失败的时候，创建本Portal项，用于展示提示信息
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.Home.Portal.ErrorInfoPortalItem", {
  extend: "PSI.Home.Portal.BasePortalItem",

  /**
   * @override
   */
  initComponent() {
    const me = this;

    PCL.apply(me, {
      flex: 1,
      border: 0,
      layout: "fit",
      items: [{
        border: 0,
        html: `<h2><span style='color:blue'>${me.getPortalItemName()}</span>尚未实现，请联系系统管理员<h2>`,
      }],
    });


    me.callParent(arguments);
  },
});
