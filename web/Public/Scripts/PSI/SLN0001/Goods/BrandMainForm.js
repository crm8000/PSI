/**
 * 物料品牌 - 主界面
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.Goods.BrandMainForm", {
  extend: "PSI.AFX.Form.MainForm",

  /**
   * @override
   */
  initComponent() {
    const me = this;

    PCL.apply(me, {
      border: 0,
      layout: "border",
      tbar: me.getToolbarCmp(),
      items: [{
        region: "north",
        height: 55,
        border: 0,
        bodyPadding: "0 20 0 20",
        html: "<h2 style='color:#595959;display:inline-block'>物料品牌</h2>&nbsp;&nbsp;<span style='color:#8c8c8c'>基础数据</span>",
      }, {
        region: "center",
        xtype: "panel",
        layout: "fit",
        bodyPadding: "0 20 5 15",
        border: 0,
        items: [me.getMainGrid()]
      }]
    });

    me.callParent();
  },

  /**
   * @private
   */
  getToolbarCmp() {
    const me = this;
    return [{
      iconCls: "PSI-tb-new",
      text: "新建品牌",
      ...PSI.Const.BTN_STYLE,
      handler: me._onAddBrand,
      scope: me
    }, {
      text: "编辑品牌",
      ...PSI.Const.BTN_STYLE,
      handler: me._onEditBrand,
      scope: me
    }, {
      text: "删除品牌",
      ...PSI.Const.BTN_STYLE,
      handler: me._onDeleteBrand,
      scope: me
    }, "-", {
      text: "刷新",
      ...PSI.Const.BTN_STYLE,
      handler: me._onRefreshGrid,
      scope: me
    }, "-", {
      iconCls: "PSI-tb-help",
      text: "指南",
      ...PSI.Const.BTN_STYLE,
      handler() {
        me.focus();
        window.open(me.URL("Home/Help/index?t=goodsBrand"));
      }
    }, "-", {
      iconCls: "PSI-tb-close",
      text: "关闭",
      ...PSI.Const.BTN_STYLE,
      handler() {
        me.focus();
        me.closeWindow();
      }
    }, {
      // 空容器，只是为了撑高工具栏
      xtype: "container", height: 28,
      items: []
    }].concat(me.getShortcutCmp());;
  },

  /**
 * 快捷访问
 * 
 * @private
 */
  getShortcutCmp() {
    return ["->",
      {
        cls: "PSI-Shortcut-Cmp",
        labelWidth: 0,
        emptyText: "快捷访问",
        xtype: "psi_mainmenushortcutfield",
        width: 90
      }
    ];
  },


  /**
   * @private
   */
  getMainGrid() {
    const me = this;
    if (me._mainGrid) {
      return me._mainGrid;
    }

    const modelName = me.buildModelName(me, "GoodsBrand");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "text", "fullName", "recordStatus", "leaf",
        "children", "goodsCount", "goodsEnabledCount",
        "goodsDisabledCount"]
    });

    const store = PCL.create("PCL.data.TreeStore", {
      model: modelName,
      proxy: {
        type: "ajax",
        actionMethods: {
          read: "POST"
        },
        url: me.URL("SLN0001/Goods/allBrands")
      }
    });

    me._mainGrid = PCL.create("PCL.tree.Panel", {
      cls: "PSI",
      border: 1,
      store: store,
      rootVisible: false,
      useArrows: true,
      viewConfig: {
        loadMask: true
      },
      columns: {
        defaults: {
          sortable: false,
          menuDisabled: true,
          draggable: false
        },
        items: [{
          xtype: "treecolumn",
          text: "品牌",
          dataIndex: "text",
          flex: 1,
          renderer(value, metaData, record) {
            if (parseInt(record.get("recordStatus")) == 1) {
              return value;
            } else {
              return `<span class="PSI-record-disabled">${value}</span>`;
            }
          }
        }, {
          text: "全名",
          dataIndex: "fullName",
          flex: 2,
          renderer(value, metaData, record) {
            if (parseInt(record.get("recordStatus")) == 1) {
              return value;
            } else {
              return `<span class="PSI-record-disabled">${value}</span>`;
            }
          }
        }, {
          text: "状态",
          dataIndex: "recordStatus",
          width: 80,
          renderer(value, metaData, record) {
            if (parseInt(record.get("recordStatus")) == 1) {
              return "启用";
            } else {
              return "<span style='color:red;'>停用</span>";
            }
          }
        }, {
          header: "使用该品牌的物料数",
          align: "right",
          width: 180,
          columns: [{
            header: "启用状态物料数",
            dataIndex: "goodsEnabledCount",
            align: "right",
            menuDisabled: true,
            sortable: false,
            width: 120

          }, {
            header: "停用状态物料数",
            dataIndex: "goodsDisabledCount",
            align: "right",
            menuDisabled: true,
            sortable: false,
            width: 120

          }, {
            header: "总物料数",
            dataIndex: "goodsCount",
            align: "right",
            menuDisabled: true,
            sortable: false
          }]
        }]
      },
      listeners: {
        beforeitemdblclick: {
          fn() {
            me._onEditBrand();
            return false;
          }
        }
      }
    });

    return me._mainGrid;
  },

  /**
   * @private
   */
  refreshGrid(id) {
    const me = this;

    const store = me.getMainGrid().getStore();
    store.load();
  },

  /**
   * 新建品牌
   * 
   * @private
   */
  _onAddBrand() {
    const me = this;
    const form = PCL.create("PSI.SLN0001.Goods.BrandEditForm", {
      renderTo: PSI.Const.RENDER_TO(),
      parentForm: me
    });
    form.show();
  },

  /**
   * 编辑品牌
   * 
   * @private
   */
  _onEditBrand() {
    const me = this;
    const item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("请选择要编辑的品牌");
      return;
    }

    const brand = item[0];

    const form = PCL.create("PSI.SLN0001.Goods.BrandEditForm", {
      renderTo: PSI.Const.RENDER_TO(),
      parentForm: me,
      entity: brand
    });

    form.show();
  },

  /**
   * 删除商品品牌
   * 
   * @private
   */
  _onDeleteBrand() {
    const me = this;
    const item = me.getMainGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("请选择要删除的品牌");
      return;
    }

    const brand = item[0];
    const info = `请确认是否删除品牌: <span style='color:red'>${brand.get("text")}</span> ？`;
    const confimFunc = () => {
      const el = PCL.getBody();
      el.mask("正在删除中...");
      const r = {
        url: me.URL("SLN0001/Goods/deleteBrand"),
        params: {
          id: brand.get("id")
        },
        callback(options, success, response) {
          el.unmask();

          if (success) {
            const data = me.decodeJSON(response.responseText);
            if (data.success) {
              me.tip("成功完成删除操作", true);
              me.refreshGrid();
            } else {
              me.showInfo(data.msg);
            }
          } else {
            me.showInfo("网络错误");
          }
        }
      };
      me.ajax(r);
    };

    me.confirm(info, confimFunc);
  },

  /**
   * @private
   */
  _onRefreshGrid() {
    const me = this;

    me.refreshGrid();
    me.focus();
  }
});
