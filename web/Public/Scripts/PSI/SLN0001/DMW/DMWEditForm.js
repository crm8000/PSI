/**
 * 成品委托生产入库单 - 新增或编辑界面
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.DMW.DMWEditForm", {
  extend: "PSI.AFX.Form.EditForm",
  config: {
    genBill: false,
    dmobillRef: null,
    showAddGoodsButton: "0",
    dmoKeyMap: null, // 当从委托订单创建入库单的时候，传入本KeyMap
  },

  mixins: ["PSI.SLN0001.Mix.GoodsPrice"],

  /**
   * @override
   */
  initComponent() {
    const me = this;
    me._readOnly = false;
    const entity = me.getEntity();
    me.adding = entity == null;

    const action = entity == null ? "新建" : "编辑";
    const title = me.formatTitleLabel("成品委托生产入库单", action);

    PCL.apply(me, {
      header: false,
      padding: "0 0 0 0",
      border: 0,
      maximized: true,
      layout: "border",
      defaultFocus: "editSupplier",
      tbar: [{
        id: me.buildId(me, "dfTitle"),
        value: title, xtype: "displayfield"
      }, "->", {
        text: "保存 <span class='PSI-shortcut-DS'>Alt + S</span>",
        tooltip: me.buildTooltip("快捷键：Alt + S"),
        ...PSI.Const.BTN_STYLE,
        id: "buttonSave",
        iconCls: "PSI-button-ok",
        handler: me._onOK,
        scope: me
      }, "-", {
        text: "取消",
        iconCls: "PSI-tb-close",
        ...PSI.Const.BTN_STYLE,
        id: "buttonCancel",
        handler() {
          if (me._readonly) {
            me.close();
            return;
          }

          me.confirm("请确认是否取消当前操作？", () => {
            me.close();
          });
        },
        scope: me
      }, "-", {
        text: "表单通用操作指南",
        ...PSI.Const.BTN_STYLE,
        iconCls: "PSI-help",
        handler() {
          me.focus();
          window.open(me.URL("Home/Help/index?t=commBill"));
        }
      }, "-", {
        margin: "5 5 5 0",
        cls: "PSI-toolbox",
        labelWidth: 0,
        width: 90,
        emptyText: "快捷访问",
        xtype: "psi_mainmenushortcutfield"
      }],
      items: [{
        region: "center",
        layout: "fit",
        border: 0,
        bodyPadding: 10,
        items: [me.getGoodsGrid()]
      }, {
        region: "north",
        id: "editForm",
        layout: {
          type: "table",
          columns: 4,
          tableAttrs: PSI.Const.TABLE_LAYOUT_SMALL,
        },
        height: 105,
        bodyPadding: 10,
        border: 0,
        items: [{
          xtype: "hidden",
          id: "hiddenId",
          name: "id",
          value: entity == null ? null : entity.get("id")
        }, {
          id: "editRef",
          labelWidth: 65,
          labelAlign: "right",
          labelSeparator: "",
          fieldLabel: "单号",
          xtype: "displayfield",
          value: me.toFieldNoteText("保存后自动生成")
        }, {
          id: "editBizDT",
          fieldLabel: "业务日期",
          labelWidth: 65,
          labelAlign: "right",
          labelSeparator: "",
          allowBlank: false,
          blankText: "没有输入业务日期",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          xtype: "datefield",
          format: "Y-m-d",
          value: new Date(),
          name: "bizDT",
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          }
        }, {
          id: "editFactory",
          colspan: 2,
          width: 430,
          labelWidth: 60,
          labelAlign: "right",
          labelSeparator: "",
          xtype: "psi_factoryfield",
          fieldLabel: "工厂",
          allowBlank: false,
          blankText: "没有输入工厂",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          },
          showAddButton: true
        }, {
          id: "editWarehouse",
          labelWidth: 65,
          labelAlign: "right",
          labelSeparator: "",
          fieldLabel: "入库仓库",
          xtype: "psi_warehousefield",
          fid: "2036",
          allowBlank: false,
          blankText: "没有输入入库仓库",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          }
        }, {
          id: "editBizUser",
          labelWidth: 60,
          labelAlign: "right",
          labelSeparator: "",
          fieldLabel: "业务员",
          xtype: "psi_userfield",
          allowBlank: false,
          blankText: "没有输入业务员",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          }
        }, {
          id: "editPaymentType",
          labelWidth: 60,
          labelAlign: "right",
          labelSeparator: "",
          fieldLabel: "付款方式",
          xtype: "combo",
          queryMode: "local",
          editable: false,
          valueField: "id",
          store: PCL.create("PCL.data.ArrayStore", {
            fields: ["id", "text"],
            data: [["0", "记应付账款"]]
          }),
          value: "0",
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          },
          colspan: 2
        }, {
          id: "editBillMemo",
          labelWidth: 60,
          labelAlign: "right",
          labelSeparator: "",
          fieldLabel: "备注",
          xtype: "textfield",
          colspan: 4,
          width: 860,
          listeners: {
            specialkey: {
              fn: me._onEditBillMemoSpecialKey,
              scope: me
            }
          }
        }]
      }],
      listeners: {
        show: {
          fn: me._onWndShow,
          scope: me
        },
        close: {
          fn: me._onWndClose,
          scope: me
        }
      }
    });

    me.callParent(arguments);

    me.editRef = PCL.getCmp("editRef");
    me.editBizDT = PCL.getCmp("editBizDT");
    me.editFactory = PCL.getCmp("editFactory");
    me.editWarehouse = PCL.getCmp("editWarehouse");
    me.editBizUser = PCL.getCmp("editBizUser");
    me.editPaymentType = PCL.getCmp("editPaymentType");
    me.editBillMemo = PCL.getCmp("editBillMemo");

    me.editHiddenId = PCL.getCmp("hiddenId");

    me.columnActionDelete = PCL.getCmp("columnActionDelete");
    me.columnActionAdd = PCL.getCmp("columnActionAdd");
    me.columnActionAppend = PCL.getCmp("columnActionAppend");

    me.columnGoodsCode = PCL.getCmp("columnGoodsCode");
    me.columnGoodsPrice = PCL.getCmp("columnGoodsPrice");
    me.columnGoodsMoney = PCL.getCmp("columnGoodsMoney");

    me.buttonSave = PCL.getCmp("buttonSave");
    me.buttonCancel = PCL.getCmp("buttonCancel");

    me.dfTitle = PCL.getCmp(me.buildId(me, "dfTitle"));

    // AFX
    me.__editorList = [
      me.editBizDT, me.editFactory, me.editWarehouse,
      me.editBizUser, me.editPaymentType,
      me.editBillMemo,
    ];

    me._keyMap = PCL.create("PCL.util.KeyMap", PCL.getBody(), {
      key: "S",
      ctrl: false,
      shift: false,
      alt: true,
      fn() {
        if (PSI.Const.msgBoxShowing) {
          return;
        }

        if (me._readonly) {
          return;
        }

        me._onOK.apply(me, []);
      },
      scope: me
    });
  },

  /**
   * @private
   */
  _onWndClose() {
    const me = this;

    me._keyMap.destroy();

    PCL.WindowManager.hideAll();

    PCL.get(window).un('beforeunload', me.__onWindowBeforeUnload);

    const parentForm = me.getParentForm();
    if (parentForm) {
      parentForm._keyMapMain.enable();
    }

    const km = me.getDmoKeyMap();
    if (km) {
      km.enable();
    }
  },

  /**
   * @private
   */
  _onWndShow() {
    const me = this;

    PCL.get(window).on('beforeunload', me.__onWindowBeforeUnload);

    const parentForm = me.getParentForm();
    if (parentForm) {
      parentForm._keyMapMain.disable();
    }

    const km = me.getDmoKeyMap();
    if (km) {
      km.disable();
    }

    const el = me.getEl() || PCL.getBody();
    el.mask(PSI.Const.LOADING);
    me.ajax({
      url: me.URL("SLN0001/DMW/dmwBillInfo"),
      params: {
        id: me.editHiddenId.getValue(),
        dmobillRef: me.getDmobillRef()
      },
      callback(options, success, response) {
        el.unmask();

        if (success) {
          me.editFactory.focus();

          const data = me.decodeJSON(response.responseText);
          me.editBillMemo.setValue(data.billMemo);

          if (me.getGenBill()) {
            // 从成品委托生产订单生成成品委托生产入库单
            me.editFactory.setIdValue(data.factoryId);
            me.editFactory.setValue(data.factoryName);
            me.editBizUser.setIdValue(data.bizUserId);
            me.editBizUser.setValue(data.bizUserName);
            me.editBizDT.setValue(data.dealDate);
            me.editPaymentType.setValue(`${data.paymentType}`);
            const store = me.getGoodsGrid().getStore();
            store.removeAll();
            store.add(data.items);

            me.editFactory.setReadOnly(true);
            me.columnActionDelete.hide();
            me.columnActionAdd.hide();
            me.columnActionAppend.hide();
          } else {
            if (!data.genBill) {
              me.columnGoodsCode.setEditor({
                xtype: "psi_goodsfield",
                callbackFunc: me._setGoodsInfo,
                callbackScope: me,
                parentCmp: me,
                showAddButton: me.getShowAddGoodsButton() == "1"
              });
              me.columnGoodsPrice.setEditor({
                xtype: "numberfield",
                hideTrigger: true
              });
              me.columnGoodsMoney.setEditor({
                xtype: "numberfield",
                hideTrigger: true
              });
            } else {
              me.editFactory.setReadOnly(true);
              me.columnActionDelete.hide();
              me.columnActionAdd.hide();
              me.columnActionAppend.hide();
            }

            if (data.ref) {
              me.editRef.setValue(me.toFieldNoteText(data.ref));
            }

            me.editFactory.setIdValue(data.factoryId);
            me.editFactory.setValue(data.factoryName);

            me.editWarehouse.setIdValue(data.warehouseId);
            me.editWarehouse.setValue(data.warehouseName);

            me.editBizUser.setIdValue(data.bizUserId);
            me.editBizUser.setValue(data.bizUserName);
            if (data.bizDT) {
              me.editBizDT.setValue(data.bizDT);
            }
            if (data.paymentType) {
              me.editPaymentType.setValue(data.paymentType);
            }
            if (data.expandByBOM) {
              me.editExpand.setValue(data.expandByBOM);
            }

            const store = me.getGoodsGrid().getStore();
            store.removeAll();
            if (data.items) {
              store.add(data.items);
            }
            if (store.getCount() == 0) {
              store.add({});
            }

            if (data.billStatus && data.billStatus != 0) {
              me.setBillReadonly();
            }
          }
        }
      }
    });
  },

  /**
   * @private
   */
  _onOK() {
    const me = this;
    PCL.getBody().mask("正在保存中...");
    const r = {
      url: me.URL("SLN0001/DMW/editDMWBill"),
      params: {
        adding: me.adding ? "1" : "0",
        jsonStr: me.getSaveData()
      },
      callback(options, success, response) {
        PCL.getBody().unmask();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          if (data.success) {
            me.close();
            const pf = me.getParentForm();
            if (pf) {
              pf.refreshMainGrid.apply(pf, [data.id]);
            }
            me.tip("成功保存数据", true);
          } else {
            me.showInfo(data.msg);
          }
        }
      }
    };
    me.ajax(r);
  },

  /**
   * @private
   */
  _onEditBillMemoSpecialKey(field, e) {
    const me = this;

    if (me._readonly) {
      return;
    }

    if (e.getKey() == e.ENTER) {
      const store = me.getGoodsGrid().getStore();
      if (store.getCount() == 0) {
        store.add({});
      }
      me.getGoodsGrid().focus();
      me._cellEditing.startEdit(0, 1);
    }
  },

  onEditPaymentTypeSpecialKey(field, e) {
    var me = this;

    if (me._readonly) {
      return;
    }

    if (e.getKey() == e.ENTER) {
      me.editBillMemo.focus();
    }
  },

  /**
   * @private
   */
  getGoodsGrid() {
    const me = this;
    if (me._goodsGrid) {
      return me._goodsGrid;
    }

    const modelName = me.buildModelName(me, "DMWBillDetail_EditForm");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "goodsId", "goodsCode", "goodsName",
        "goodsSpec", "unitName", "goodsCount", {
          name: "goodsMoney",
          type: "float"
        }, "goodsPrice", "memo", "dmoBillDetailId", {
          name: "taxRate",
          type: "int"
        }, {
          name: "tax",
          type: "float"
        }, {
          name: "moneyWithTax",
          type: "float"
        }, "goodsPriceWithTax"]
    });
    const store = PCL.create("PCL.data.Store", {
      autoLoad: false,
      model: modelName,
      data: []
    });

    me._cellEditing = PCL.create("PSI.UX.CellEditing", {
      clicksToEdit: 1,
      listeners: {
        edit: {
          fn: me._onCellEditingAfterEdit,
          scope: me
        }
      }
    });

    me._goodsGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-EF",
      viewConfig: {
        enableTextSelection: true,
        markDirty: !me.adding
      },
      features: [{
        ftype: "summary"
      }],
      plugins: [me._cellEditing],
      columnLines: true,
      columns: {
        defaults: {
          menuDisabled: true,
          sortable: false,
          draggable: false
        },
        items: [{
          xtype: "rownumberer",
          text: "#",
          width: 30
        }, {
          header: "物料编码",
          dataIndex: "goodsCode",
          id: "columnGoodsCode"
        }, {
          menuDisabled: true,
          draggable: false,
          sortable: false,
          header: "品名/规格型号",
          dataIndex: "goodsName",
          width: 330,
          renderer(value, metaData, record) {
            return record.get("goodsName") + " " + record.get("goodsSpec");
          }
        }, {
          header: "入库数量",
          dataIndex: "goodsCount",
          align: "right",
          width: 90,
          editor: {
            xtype: "numberfield",
            allowDecimals: PSI.Const.GC_DEC_NUMBER > 0,
            decimalPrecision: PSI.Const.GC_DEC_NUMBER,
            minValue: 0,
            hideTrigger: true
          }
        }, {
          header: "单位",
          dataIndex: "unitName",
          width: 60,
          align: "center"
        }, {
          header: "单价",
          dataIndex: "goodsPrice",
          align: "right",
          xtype: "numbercolumn",
          width: 90,
          id: "columnGoodsPrice",
          summaryRenderer() {
            return "金额合计";
          }
        }, {
          header: "金额",
          dataIndex: "goodsMoney",
          align: "right",
          xtype: "numbercolumn",
          width: 90,
          id: "columnGoodsMoney",
          summaryType: "sum"
        }, {
          header: "含税价",
          dataIndex: "goodsPriceWithTax",
          align: "right",
          xtype: "numbercolumn",
          width: 90,
          editor: {
            xtype: "numberfield",
            hideTrigger: true
          }
        }, {
          header: "税率(%)",
          dataIndex: "taxRate",
          menuDisabled: true,
          sortable: false,
          draggable: false,
          align: "right",
          format: "0",
          width: 60
        }, {
          header: "税金",
          dataIndex: "tax",
          menuDisabled: true,
          sortable: false,
          draggable: false,
          align: "right",
          xtype: "numbercolumn",
          width: 90,
          editor: {
            xtype: "numberfield",
            hideTrigger: true
          },
          summaryType: "sum"
        }, {
          header: "价税合计",
          dataIndex: "moneyWithTax",
          menuDisabled: true,
          sortable: false,
          draggable: false,
          align: "right",
          xtype: "numbercolumn",
          width: 90,
          editor: {
            xtype: "numberfield",
            hideTrigger: true
          },
          summaryType: "sum"
        }, {
          header: "备注",
          dataIndex: "memo",
          width: 200,
          editor: {
            xtype: "textfield"
          }
        }, {
          header: "",
          id: "columnActionDelete",
          align: "center",
          width: 50,
          xtype: "actioncolumn",
          items: [{
            icon: me.URL("Public/Images/icons/delete.png"),
            tooltip: "删除当前记录",
            handler(grid, row) {
              const store = grid.getStore();
              store.remove(store.getAt(row));
              if (store.getCount() == 0) {
                store.add({});
              }
            },
            scope: me
          }]
        }, {
          header: "",
          id: "columnActionAdd",
          align: "center",
          width: 50,
          xtype: "actioncolumn",
          items: [{
            icon: me.URL("Public/Images/icons/insert.png"),
            tooltip: "在当前记录之前插入新记录",
            handler(grid, row) {
              const store = grid.getStore();
              store.insert(row, [{}]);
            },
            scope: me
          }]
        }, {
          header: "",
          id: "columnActionAppend",
          align: "center",
          width: 50,
          xtype: "actioncolumn",
          items: [{
            icon: me
              .URL("Public/Images/icons/add.png"),
            tooltip: "在当前记录之后新增记录",
            handler(grid, row) {
              const store = grid.getStore();
              store.insert(row + 1, [{}]);
            },
            scope: me
          }]
        }]
      },
      store,
      listeners: {
        cellclick() {
          return !me._readonly;
        }
      }
    });

    return me._goodsGrid;
  },

  /**
   * @private
   */
  _setGoodsInfo(data) {
    const me = this;
    const item = me.getGoodsGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      return;
    }
    const goods = item[0];

    goods.set("goodsId", data.id);
    goods.set("goodsCode", data.code);
    goods.set("goodsName", data.name);
    goods.set("unitName", data.unitName);
    goods.set("goodsSpec", data.spec);
    goods.set("taxRate", data.taxRate);

    me.calcMoney(goods);
  },

  /**
   * @private
   */
  _onCellEditingAfterEdit(editor, e) {
    const me = this;

    if (me._readonly) {
      return;
    }

    const fieldName = e.field;
    const goods = e.record;
    const oldValue = e.originalValue;
    if (fieldName == "memo") {
      const store = me.getGoodsGrid().getStore();
      if (e.rowIdx == store.getCount() - 1) {
        store.add({});
        const row = e.rowIdx + 1;
        me.getGoodsGrid().getSelectionModel().select(row);
        me._cellEditing.startEdit(row, 1);
      }
    } else if (fieldName == "goodsMoney") {
      if (goods.get(fieldName) != (new Number(oldValue)).toFixed(2)) {
        me.calcPrice(goods);
      }
    } else if (fieldName == "goodsCount") {
      if (goods.get(fieldName) != oldValue) {
        me.calcMoney(goods);
      }
    } else if (fieldName == "goodsPrice") {
      if (goods.get(fieldName) != (new Number(oldValue)).toFixed(2)) {
        me.calcMoney(goods);
      }
    } else if (fieldName == "moneyWithTax") {
      if (goods.get(fieldName) != (new Number(oldValue)).toFixed(2)) {
        me.calcTax(goods);
      }
    } else if (fieldName == "tax") {
      if (goods.get(fieldName) != (new Number(oldValue)).toFixed(2)) {
        me.calcMoneyWithTax(goods);
      }
    } else if (fieldName == "goodsPriceWithTax") {
      if (goods.get(fieldName) != (new Number(oldValue)).toFixed(2)) {
        me.calcMoney2(goods);
      }
    }
  },

  /**
   * @private
   */
  getSaveData() {
    const me = this;

    const result = {
      id: me.editHiddenId.getValue(),
      bizDT: PCL.Date.format(me.editBizDT.getValue(), "Y-m-d"),
      factoryId: me.editFactory.getIdValue(),
      warehouseId: me.editWarehouse.getIdValue(),
      bizUserId: me.editBizUser.getIdValue(),
      paymentType: me.editPaymentType.getValue(),
      dmobillRef: me.getDmobillRef(),
      billMemo: me.editBillMemo.getValue(),
      items: []
    };

    const store = me.getGoodsGrid().getStore();
    for (let i = 0; i < store.getCount(); i++) {
      const item = store.getAt(i);
      result.items.push({
        id: item.get("id"),
        goodsId: item.get("goodsId"),
        goodsCount: item.get("goodsCount"),
        goodsPrice: item.get("goodsPrice"),
        goodsMoney: item.get("goodsMoney"),
        memo: item.get("memo"),
        dmoBillDetailId: item.get("dmoBillDetailId"),
        taxRate: item.get("taxRate"),
        tax: item.get("tax"),
        moneyWithTax: item.get("moneyWithTax"),
        goodsPriceWithTax: item.get("goodsPriceWithTax")
      });
    }

    return me.encodeJSON(result);
  },

  /**
   * @private
   */
  setBillReadonly() {
    const me = this;
    me._readonly = true;
    me.dfTitle.setValue(me.formatTitleLabel("成品委托生产入库单", "查看"));

    me.buttonSave.setDisabled(true);
    me.buttonCancel.setText("关闭");
    me.editBizDT.setReadOnly(true);
    me.editFactory.setReadOnly(true);
    me.editWarehouse.setReadOnly(true);
    me.editBizUser.setReadOnly(true);
    me.editPaymentType.setReadOnly(true);
    me.editBillMemo.setReadOnly(true);
    me.columnActionDelete.hide();
    me.columnActionAdd.hide();
    me.columnActionAppend.hide();
  }
});
