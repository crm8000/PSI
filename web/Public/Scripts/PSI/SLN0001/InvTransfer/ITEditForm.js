/**
 * 调拨单
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.InvTransfer.ITEditForm", {
  extend: "PSI.AFX.Form.EditForm",

  /**
   * @override
   */
  initComponent() {
    const me = this;
    me._readonly = false;
    const entity = me.getEntity();
    me.adding = entity == null;

    const action = entity == null ? "新建" : "编辑";
    const title = me.formatTitleLabel("调拨单", action);

    PCL.apply(me, {
      header: false,
      padding: "0 0 0 0",
      border: 0,
      maximized: true,
      layout: "border",
      tbar: [{
        id: me.buildId(me, "dfTitle"),
        value: title, xtype: "displayfield"
      }, "->", {
        text: "保存 <span class='PSI-shortcut-DS'>Alt + S</span>",
        tooltip: me.buildTooltip("快捷键：Alt + S"),
        ...PSI.Const.BTN_STYLE,
        iconCls: "PSI-button-ok",
        handler: me._onOK,
        scope: me,
        id: me.buildId(me, "buttonSave")
      }, "-", {
        text: "取消",
        iconCls: "PSI-tb-close",
        ...PSI.Const.BTN_STYLE,
        handler() {
          if (me._readonly) {
            me.close();
            return;
          }
          me.confirm("请确认是否取消当前操作?", () => {
            me.close();
          });
        },
        scope: me,
        id: me.buildId(me, "buttonCancel")
      }, "-", {
        text: "表单通用操作指南",
        ...PSI.Const.BTN_STYLE,
        iconCls: "PSI-help",
        handler() {
          me.focus();
          window.open(me.URL("Home/Help/index?t=commBill"));
        }
      }, "-", {
        margin: "5 5 5 0",
        cls: "PSI-toolbox",
        labelWidth: 0,
        width: 90,
        emptyText: "快捷访问",
        xtype: "psi_mainmenushortcutfield"
      }],
      items: [{
        region: "center",
        border: 0,
        bodyPadding: 10,
        layout: "fit",
        items: [me.getGoodsGrid()]
      }, {
        region: "north",
        border: 0,
        layout: {
          type: "table",
          columns: 4,
          tableAttrs: PSI.Const.TABLE_LAYOUT_SMALL,
        },
        height: 75,
        bodyPadding: 10,
        items: [{
          xtype: "hidden",
          id: me.buildId(me, "hiddenId"),
          name: "id",
          value: entity == null ? null : entity.get("id")
        }, {
          id: me.buildId(me, "editRef"),
          fieldLabel: "单号",
          labelWidth: 70,
          labelAlign: "right",
          labelSeparator: "",
          xtype: "displayfield",
          value: me.toFieldNoteText("保存后自动生成")
        }, {
          id: me.buildId(me, "editBizDT"),
          fieldLabel: "业务日期",
          allowBlank: false,
          blankText: "没有输入业务日期",
          labelWidth: 70,
          labelAlign: "right",
          labelSeparator: "",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          xtype: "datefield",
          format: "Y-m-d",
          value: new Date(),
          name: "bizDT",
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          }
        }, {
          id: me.buildId(me, "editFromWarehouse"),
          fieldLabel: "调出仓库",
          labelWidth: 70,
          labelAlign: "right",
          labelSeparator: "",
          xtype: "psi_warehousefield",
          fid: "2009",
          allowBlank: false,
          blankText: "没有输入调出仓库",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          }
        }, {
          id: me.buildId(me, "editToWarehouse"),
          fieldLabel: "调入仓库",
          labelWidth: 70,
          labelAlign: "right",
          labelSeparator: "",
          xtype: "psi_warehousefield",
          fid: "2009",
          allowBlank: false,
          blankText: "没有输入调入仓库",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          }
        }, {
          id: me.buildId(me, "editBizUser"),
          fieldLabel: "业务员",
          xtype: "psi_userfield",
          labelWidth: 70,
          labelAlign: "right",
          labelSeparator: "",
          allowBlank: false,
          blankText: "没有输入业务员",
          beforeLabelTextTpl: PSI.Const.REQUIRED,
          listeners: {
            specialkey: {
              fn: me.__onEditSpecialKey,
              scope: me
            }
          }
        }, {
          id: me.buildId(me, "editBillMemo"),
          fieldLabel: "备注",
          labelWidth: 70,
          labelAlign: "right",
          labelSeparator: "",
          colspan: 3,
          width: 680,
          xtype: "textfield",
          listeners: {
            specialkey: {
              fn: me._onEditBillMemoSpecialKey,
              scope: me
            }
          }
        }]
      }],
      listeners: {
        show: {
          fn: me._onWndShow,
          scope: me
        },
        close: {
          fn: me._onWndClose,
          scope: me
        }
      }
    });

    me.callParent(arguments);

    me.dfTitle = PCL.getCmp(me.buildId(me, "dfTitle"));

    me.buttonSave = PCL.getCmp(me.buildId(me, "buttonSave"));
    me.buttonCancel = PCL.getCmp(me.buildId(me, "buttonCancel"));

    me.hiddenId = PCL.getCmp(me.buildId(me, "hiddenId"));
    me.editRef = PCL.getCmp(me.buildId(me, "editRef"));
    me.editBizDT = PCL.getCmp(me.buildId(me, "editBizDT"));
    me.editFromWarehouse = PCL.getCmp(me.buildId(me, "editFromWarehouse"));
    me.editToWarehouse = PCL.getCmp(me.buildId(me, "editToWarehouse"));
    me.editBizUser = PCL.getCmp(me.buildId(me, "editBizUser"));
    me.editBillMemo = PCL.getCmp(me.buildId(me, "editBillMemo"));

    me.columnActionDelete = PCL.getCmp(me.buildId(me, "columnActionDelete"));
    me.columnActionAdd = PCL.getCmp(me.buildId(me, "columnActionAdd"));
    me.columnActionAppend = PCL.getCmp(me.buildId(me, "columnActionAppend"));

    // AFX
    me.__editorList = [
      me.editBizDT, me.editFromWarehouse, me.editToWarehouse,
      me.editBizUser, me.editBillMemo
    ];

    me._keyMap = PCL.create("PCL.util.KeyMap", PCL.getBody(), {
      key: "S",
      ctrl: false,
      shift: false,
      alt: true,
      fn() {
        if (PSI.Const.msgBoxShowing) {
          return;
        }

        if (me._readonly) {
          return;
        }

        me._onOK.apply(me, []);
      },
      scope: me
    });
  },

  /**
   * @private
   */
  _onWndClose() {
    const me = this;

    PCL.WindowManager.hideAll();

    // AFX
    PCL.get(window).un('beforeunload', me.__onWindowBeforeUnload);

    me._keyMap.destroy();

    const parentForm = me.getParentForm();
    if (parentForm) {
      parentForm._keyMapMain.enable();
    }
  },

  /**
   * @private
   */
  _onWndShow() {
    const me = this;

    const parentForm = me.getParentForm();
    if (parentForm) {
      parentForm._keyMapMain.disable();
    }

    // AFX
    PCL.get(window).on('beforeunload', me.__onWindowBeforeUnload);

    const el = me.getEl() || PCL.getBody();
    el.mask(PSI.Const.LOADING);
    me.ajax({
      url: me.URL("SLN0001/InvTransfer/itBillInfo"),
      params: {
        id: me.hiddenId.getValue()
      },
      callback(options, success, response) {
        el.unmask();

        if (success) {
          me.editFromWarehouse.focus();

          const data = me.decodeJSON(response.responseText);

          if (data.ref) {
            me.editRef.setValue(me.toFieldNoteText(data.ref));
          }

          me.editBizUser.setIdValue(data.bizUserId);
          me.editBizUser.setValue(data.bizUserName);
          if (data.bizDT) {
            me.editBizDT.setValue(data.bizDT);
          }
          if (data.fromWarehouseId) {
            me.editFromWarehouse.setIdValue(data.fromWarehouseId);
            me.editFromWarehouse.setValue(data.fromWarehouseName);
          }
          if (data.toWarehouseId) {
            me.editToWarehouse.setIdValue(data.toWarehouseId);
            me.editToWarehouse.setValue(data.toWarehouseName);
          }
          if (data.billMemo) {
            me.editBillMemo.setValue(data.billMemo);
          }

          const store = me.getGoodsGrid().getStore();
          store.removeAll();
          if (data.items) {
            store.add(data.items);
          }
          if (store.getCount() == 0) {
            store.add({});
          }

          if (data.billStatus && data.billStatus != 0) {
            me.setBillReadonly();
          }

        } else {
          me.showInfo("网络错误", () => {
            me.editFromWarehouse.focus();
          });
        }
      }
    });
  },

  /**
   * @private
   */
  _onOK() {
    const me = this;
    PCL.getBody().mask("正在保存中...");

    me.ajax({
      url: me.URL("SLN0001/InvTransfer/editITBill"),
      params: {
        adding: me.adding ? "1" : "0",
        jsonStr: me.getSaveData()
      },
      callback(options, success, response) {
        PCL.getBody().unmask();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          if (data.success) {
            me.close();
            const parentForm = me.getParentForm();
            if (parentForm) {
              parentForm.refreshMainGrid.apply(parentForm, [data.id]);
            }

            me.tip("成功保存数据", true);
          } else {
            me.showInfo(data.msg, () => {
              me.editFromWarehouse.focus();
            });
          }
        }
      }
    });
  },

  /**
   * @private
   */
  _onEditBillMemoSpecialKey(field, e) {
    const me = this;

    if (me._readonly) {
      return;
    }

    if (e.getKey() == e.ENTER) {
      const store = me.getGoodsGrid().getStore();
      if (store.getCount() == 0) {
        store.add({});
      }
      me.getGoodsGrid().focus();
      me._cellEditing.startEdit(0, 1);
    }
  },

  /**
   * @private
   */
  getGoodsGrid() {
    const me = this;
    if (me._goodsGrid) {
      return me._goodsGrid;
    }

    const modelName = me.buildModelName(me, "ITBillDetail_EditForm");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "goodsId", "goodsCode", "goodsName",
        "goodsSpec", "unitName", "goodsCount", "memo"]
    });
    const store = PCL.create("PCL.data.Store", {
      autoLoad: false,
      model: modelName,
      data: []
    });

    me._cellEditing = PCL.create("PSI.UX.CellEditing", {
      clicksToEdit: 1,
      listeners: {
        edit: {
          fn: me._onCellEditingAfterEdit,
          scope: me
        }
      }
    });

    me._goodsGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI-EF",
      viewConfig: {
        enableTextSelection: true,
        markDirty: !me.adding
      },
      plugins: [me._cellEditing],
      columnLines: true,
      columns: [PCL.create("PCL.grid.RowNumberer", {
        text: "#",
        width: 30
      }), {
        header: "物料编码",
        dataIndex: "goodsCode",
        menuDisabled: true,
        draggable: false,
        sortable: false,
        editor: {
          xtype: "psi_goodsfield",
          parentCmp: me,
          callbackFunc: me._setGoodsInfo,
          callbackScope: me,
        }
      }, {
        menuDisabled: true,
        draggable: false,
        sortable: false,
        header: "品名/规格型号",
        dataIndex: "goodsName",
        width: 330,
        renderer(value, metaData, record) {
          return record.get("goodsName") + " " + record.get("goodsSpec");
        }
      }, {
        header: "调拨数量",
        dataIndex: "goodsCount",
        menuDisabled: true,
        draggable: false,
        sortable: false,
        align: "right",
        width: 90,
        editor: {
          xtype: "numberfield",
          allowDecimals: PSI.Const.GC_DEC_NUMBER > 0,
          decimalPrecision: PSI.Const.GC_DEC_NUMBER,
          minValue: 0,
          hideTrigger: true
        }
      }, {
        header: "单位",
        dataIndex: "unitName",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        width: 60,
        algin: "center"
      }, {
        header: "备注",
        dataIndex: "memo",
        menuDisabled: true,
        sortable: false,
        draggable: false,
        width: 200,
        editor: {
          xtype: "textfield"
        }
      }, {
        header: "",
        align: "center",
        menuDisabled: true,
        draggable: false,
        width: 50,
        xtype: "actioncolumn",
        id: me.buildId(me, "columnActionDelete"),
        items: [{
          icon: me.URL("Public/Images/icons/delete.png"),
          tooltip: "删除当前记录",
          handler(grid, row) {
            const store = grid.getStore();
            store.remove(store.getAt(row));
            if (store.getCount() == 0) {
              store.add({});
            }
          },
          scope: me
        }]
      }, {
        header: "",
        id: me.buildId(me, "columnActionAdd"),
        align: "center",
        menuDisabled: true,
        draggable: false,
        width: 50,
        xtype: "actioncolumn",
        items: [{
          icon: me.URL("Public/Images/icons/insert.png"),
          tooltip: "在当前记录之前插入新记录",
          handler(grid, row) {
            const store = grid.getStore();
            store.insert(row, [{}]);
          },
          scope: me
        }]
      }, {
        header: "",
        id: me.buildId(me, "columnActionAppend"),
        align: "center",
        menuDisabled: true,
        draggable: false,
        width: 50,
        xtype: "actioncolumn",
        items: [{
          icon: me.URL("Public/Images/icons/add.png"),
          tooltip: "在当前记录之后新增记录",
          handler(grid, row) {
            const store = grid.getStore();
            store.insert(row + 1, [{}]);
          },
          scope: me
        }]
      }],
      store,
      listeners: {
        cellclick() {
          return !me._readonly;
        }
      }
    });

    return me._goodsGrid;
  },

  /**
   * @private
   */
  _onCellEditingAfterEdit(editor, e) {
    const me = this;

    const fieldName = e.field;
    if (fieldName == "memo") {
      const store = me.getGoodsGrid().getStore();
      if (e.rowIdx == store.getCount() - 1) {
        store.add({});
        const row = e.rowIdx + 1;
        me.getGoodsGrid().getSelectionModel().select(row);
        me._cellEditing.startEdit(row, 1);
      }
    }
  },

  /**
   * @private
   */
  _setGoodsInfo(data) {
    const me = this;
    const item = me.getGoodsGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      return;
    }
    const goods = item[0];

    goods.set("goodsId", data.id);
    goods.set("goodsCode", data.code);
    goods.set("goodsName", data.name);
    goods.set("unitName", data.unitName);
    goods.set("goodsSpec", data.spec);
  },

  /**
   * @private
   */
  getSaveData() {
    const me = this;

    const result = {
      id: me.hiddenId.getValue(),
      bizDT: PCL.Date.format(me.editBizDT.getValue(), "Y-m-d"),
      fromWarehouseId: me.editFromWarehouse.getIdValue(),
      toWarehouseId: me.editToWarehouse.getIdValue(),
      bizUserId: me.editBizUser.getIdValue(),
      billMemo: me.editBillMemo.getValue(),
      items: []
    };

    const store = me.getGoodsGrid().getStore();
    for (let i = 0; i < store.getCount(); i++) {
      const item = store.getAt(i);
      result.items.push({
        id: item.get("id"),
        goodsId: item.get("goodsId"),
        goodsCount: item.get("goodsCount"),
        memo: item.get("memo")
      });
    }

    return me.encodeJSON(result);
  },

  /**
   * @private
   */
  setBillReadonly() {
    const me = this;

    me._readonly = true;
    me.dfTitle.setValue(me.formatTitleLabel("调拨单", "查看"));

    me.buttonSave.setDisabled(true);
    me.buttonCancel.setText("关闭");
    me.editBizDT.setReadOnly(true);
    me.editFromWarehouse.setReadOnly(true);
    me.editToWarehouse.setReadOnly(true);
    me.editBizUser.setReadOnly(true);
    me.columnActionDelete.hide();
    me.columnActionAdd.hide();
    me.columnActionAppend.hide();
  }
});
