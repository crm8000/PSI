/**
 * 仓库 - 编辑数据域
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.Warehouse.EditDataOrgForm", {
  extend: "PSI.AFX.Form.EditForm",

  /**
   * 初始化组件
   * 
   * @override
   */
  initComponent() {
    const me = this;

    const entity = me.getEntity();

    const buttons = [{
      text: "数据域应用详解",
      ...PSI.Const.BTN_STYLE,
      iconCls: "PSI-help",
      handler() {
        const url = me.URL("Home/Help/index?t=dataOrg")
        window.open(url);
      }
    }, "->"];

    let btn = {
      text: "保存",
      ...PSI.Const.BTN_STYLE,
      formBind: true,
      iconCls: "PSI-button-ok",
      handler() {
        me._onOK(false);
      },
      scope: me
    };
    buttons.push(btn);

    btn = {
      text: entity == null ? "关闭" : "取消",
      ...PSI.Const.BTN_STYLE,
      handler() {
        me.close();
      },
      scope: me
    };
    buttons.push(btn);

    const logoHtml = `
      <img style='float:left;margin:0px 10px 0px 20px;width:48px;height:48px;' 
        src='${PSI.Const.BASE_URL}Public/Images/edit-form-data.png'></img>
      <h2 style='color:#595959;margin-top:0px;'>修改数据域</h2>
      <p style='color:#8c8c8c'>点击数据域应用详解按钮可以了解更多数据域的应用场景</p>
      <div style='margin:0px;border-bottom:1px solid #e6f7ff;height:1px' /></div>
      `;

    PCL.apply(me, {
      header: {
        title: me.formatTitle(PSI.Const.PROD_NAME),
        height: 40
      },
      width: 800,
      height: 290,
      layout: "border",
      listeners: {
        show: {
          fn: me._onWndShow,
          scope: me
        },
        close: {
          fn: me._onWndClose,
          scope: me
        }
      },
      items: [{
        region: "north",
        height: 70,
        border: 0,
        html: logoHtml
      }, {
        region: "center",
        border: 0,
        id: me.buildId(me, "editForm"),
        xtype: "form",
        layout: {
          type: "table",
          columns: 2,
          tableAttrs: PSI.Const.TABLE_LAYOUT,
        },
        height: "100%",
        bodyPadding: 5,
        defaultType: 'textfield',
        fieldDefaults: {
          labelWidth: 70,
          labelAlign: "right",
          labelSeparator: "",
          msgTarget: 'side',
          width: 370,
          margin: "5"
        },
        items: [{
          id: me.buildId(me, "editId"),
          xtype: "hidden",
          value: entity.get("id")
        }, {
          readOnly: true,
          fieldLabel: "仓库编码",
          value: entity.get("code")
        }, {
          readOnly: true,
          fieldLabel: "仓库名称",
          value: entity.get("name")
        }, {
          readOnly: true,
          fieldLabel: "当前数据域",
          value: entity.get("dataOrg"),
          id: me.buildId(me, "editOldDataOrg"),
        }, {
          id: me.buildId(me, "editDataOrg"),
          fieldLabel: "新数据域",
          name: "dataOrg",
          xtype: "psi_selectuserdataorgfield"
        }],
        buttons
      }]
    });

    me.callParent(arguments);

    me.editForm = PCL.getCmp(me.buildId(me, "editForm"));

    me.editId = PCL.getCmp(me.buildId(me, "editId"));
    me.editOldDataOrg = PCL.getCmp(me.buildId(me, "editOldDataOrg"));
    me.editDataOrg = PCL.getCmp(me.buildId(me, "editDataOrg"));
  },

  /**
   * 保存
   * 
   * @private
   */
  _onOK() {
    const me = this;

    const oldDataOrg = me.editOldDataOrg.getValue();
    const newDataOrg = me.editDataOrg.getValue();
    if (!newDataOrg) {
      me.showInfo("没有输入新数据域", () => {
        me.editDataOrg.focus();
      });

      return;
    }
    if (oldDataOrg == newDataOrg) {
      me.showInfo("新数据域没有变动，不用保存");

      return;
    }

    const f = me.editForm;
    const el = f.getEl();
    el.mask(PSI.Const.SAVING);

    const r = {
      url: me.URL("SLN0001/Warehouse/editDataOrg"),
      params: {
        id: me.editId.getValue(),
        dataOrg: newDataOrg
      },
      callback(options, success, response) {
        el.unmask();
        if (success) {
          const data = PCL.JSON.decode(response.responseText);
          if (data.success) {
            me._lastId = data.id;
            me.tip("成功修改数据域", true);
            me.close();
          } else {
            me.showInfo(data.msg);
          }
        } else {
          me.showInfo("网络错误");
        }
      }
    };

    me.ajax(r);
  },

  /**
   * @private
   */
  _onWndClose() {
    const me = this;

    // AFX
    PCL.get(window).un('beforeunload', me.__onWindowBeforeUnload);

    const parentForm = me.getParentForm();
    if (parentForm) {
      parentForm._keyMapMain.enable();
    }

    if (me._lastId) {
      const parentForm = me.getParentForm();
      if (parentForm) {
        parentForm.refreshGrid.apply(parentForm, [me._lastId]);
      }
    }
  },

  /**
   * @private
   */
  _onWndShow() {
    const me = this;

    // AFX
    PCL.get(window).on('beforeunload', me.__onWindowBeforeUnload);

    const parentForm = me.getParentForm();
    if (parentForm) {
      parentForm._keyMapMain.disable();
    }

    me.editDataOrg.focus();
  }
});
