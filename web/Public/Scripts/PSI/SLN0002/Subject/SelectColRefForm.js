/**
 * 值来源的引用列
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0002.Subject.SelectColRefForm", {
  extend: "PSI.AFX.Form.EditForm",

  /**
   * @override
   */
  initComponent() {
    const me = this;

    PCL.apply(me, {
      header: {
        title: me.formatTitle(PSI.Const.PROD_NAME),
        height: 40
      },
      width: 700,
      height: 600,
      layout: "border",
      bodyPadding: 5,
      items: [{
        region: "center",
        layout: "fit",
        border: 0,
        items: me.getTableGrid()
      }, {
        region: "south",
        height: 32,
        border: 0,
        layout: {
          type: "table",
          columns: 2
        },
        items: [
          {
            id: me.buildId(me, "editTableName"),
            fieldStyle: "font-size:13px;margin-top:10px",
            xtype: "textfield",
            width: 700,
            emptyText: "输入表名可以过滤列表",
            listeners: {
              change: {
                fn() {
                  me.refreshTableGrid();
                },
                scope: me
              }
            }
          }
        ]
      }],
      buttons: [{
        text: "确定",
        ...PSI.Const.BTN_STYLE,
        handler: me._onOK,
        scope: me
      }, {
        text: "取消",
        ...PSI.Const.BTN_STYLE,
        handler() {
          me.close();
        },
        scope: me
      }],
      listeners: {
        show: {
          fn: me._onWndShow,
          scope: me
        }
      }
    });

    me.callParent(arguments);

    me.editTableName = PCL.getCmp(me.buildId(me, "editTableName"));

    me.refreshTableGrid();
  },

  /**
   * @private
   */
  _onWndShow() {
    const me = this;

    const edit = PCL.getCmp("editTableName");
    if (edit) {
      edit.focus();
    }
  },

  /**
   * 显示表的Grid
   * @private
   */
  getTableGrid() {
    const me = this;

    if (me._tableGrid) {
      return me._tableGrid;
    }

    const modelName = me.buildModelName(me, "TableModel");

    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["name", "caption"]
    });

    me._tableGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI",
      viewConfig: {
        enableTextSelection: true
      },
      border: 1,
      columnLines: true,
      columns: {
        defaults: {
          menuDisabled: true,
          sortable: false,
        },
        items: [{
          header: "数据库表名",
          dataIndex: "name",
          width: 350,
        }, {
          header: "名称",
          dataIndex: "caption",
          width: 350,
        }]
      },
      store: PCL.create("PCL.data.Store", {
        model: modelName,
        autoLoad: false,
        data: []
      }),
      listeners: {
        itemdblclick: {
          fn: me._onOK,
          scope: me
        }
      },
    });

    return me._tableGrid;
  },

  /**
   * @private
   */
  refreshTableGrid() {
    const me = this;

    const grid = me.getTableGrid();
    const el = grid.getEl();
    el?.mask(PSI.Const.LOADING);
    const r = {
      url: me.URL("SLN0002/Subject/queryTablesForColRef"),
      params: {
        searchKey: me.editTableName.getValue(),
      },
      callback(options, success, response) {
        const store = grid.getStore();

        store.removeAll();

        if (success) {
          const data = me.decodeJSON(response.responseText);
          store.add(data);
        }
        el?.unmask();

        me.editTableName.focus();
      }
    };

    me.ajax(r);
  },

  /**
   * @private
   */
  _onOK() {
    const me = this;

    let item = me.getTableGrid().getSelectionModel().getSelection();
    if (item == null || item.length != 1) {
      me.showInfo("请选择码表");
      return;
    }

    const table = item[0];
    const tableName = table.get("name");

    // 回调方法
    const parentForm = me.getParentForm();
    if (parentForm && parentForm._refColCallbackFn) {
      parentForm._refColCallbackFn.apply(parentForm, [{
        tableName
      }]);
    }
    me.close();
  }
});
