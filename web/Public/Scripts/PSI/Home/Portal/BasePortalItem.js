/**
 * Portal项的基类，其他的Portal项需要继承本类
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.Home.Portal.BasePortalItem", {
  extend: "PCL.panel.Panel",

  mixins: ["PSI.AFX.Mix.Common"],

  config: {
    portalItemName: "",
    portalItemIconClass: "",
  },

  /**
   * @override
   */
  initComponent() {
    const me = this;

    me.callParent(arguments);

    me.loadData();
  },

  /**
   * @protected
   * 
   * 刷新数据
   * 
   * 供子类重载
   */
  loadData() {
    // do nothing
  }
});
