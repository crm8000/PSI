/**
 * 码表视图自定义控件
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0000.FormView.CodeTableViewCmp", {
  extend: "PCL.panel.Panel",
  alias: "widget.psi_codetable_view_cmp",

  config: {
    fid: null
  },

  /**
   * @override
   */
  initComponent() {
    const me = this;

    PCL.apply(me, {
      layout: "fit",
      border: 0,
      items: [me.getMainGrid()]
    });

    me.callParent(arguments);
  },

  /**
   * @private
   */
  getMainGrid() {
    const me = this;

    if (me._mainGrid) {
      return me._mainGrid;
    }

    const modelName = "PSICodeTableViewCmp" + PCL.id();

    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id"]
    });

    me._mainGrid = PCL.create("PCL.grid.Panel", {
      cls: "PSI",
      viewConfig: {
        enableTextSelection: true
      },
      columnLines: true,
      columns: [{
        header: "编码",
        dataIndex: "code",
        menuDisabled: true,
        sortable: false
      }],
      store: PCL.create("PCL.data.Store", {
        model: modelName,
        autoLoad: false,
        data: []
      })
    });

    return me._mainGrid;
  }
});
