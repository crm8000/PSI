/**
 * 自定义字段 - 物料上级分类字段
 * 
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
PCL.define("PSI.SLN0001.Goods.GoodsParentCategoryField", {
  extend: "PCL.form.field.Trigger",
  alias: "widget.psi_goodsparentcategoryfield",

  mixins: ["PSI.AFX.Mix.Common"],

  /**
   * 初始化组件
   * 
   * @override
   */
  initComponent() {
    const me = this;

    me._idValue = null;

    me.enableKeyEvents = true;

    me.callParent(arguments);

    me.on("keydown", (field, e) => {
      if (e.getKey() == e.BACKSPACE) {
        field.setValue(null);
        me.setIdValue(null);
        e.preventDefault();
        return false;
      }

      if (e.getKey() !== e.ENTER) {
        this.onTriggerClick(e);
      }
    });
  },

  /**
   * 点击下拉按钮
   * 
   * @overrride
   */
  onTriggerClick(e) {
    const me = this;

    const modelName = me.buildModelName(me, "GoodsParentCategory");
    PCL.define(modelName, {
      extend: "PCL.data.Model",
      fields: ["id", "text", "fullName", "code", "leaf",
        "children"]
    });

    const orgStore = PCL.create("PCL.data.TreeStore", {
      model: modelName,
      proxy: {
        type: "ajax",
        actionMethods: {
          read: "POST"
        },
        url: me.URL("SLN0001/Goods/allCategories")
      }
    });

    const orgTree = PCL.create("PCL.tree.Panel", {
      store: orgStore,
      rootVisible: false,
      useArrows: true,
      viewConfig: {
        loadMask: true
      },
      columns: {
        defaults: {
          flex: 1,
          sortable: false,
          menuDisabled: true,
          draggable: false
        },
        items: [{
          xtype: "treecolumn",
          text: "名称",
          dataIndex: "text"
        }, {
          text: "编码",
          dataIndex: "code"
        }]
      }
    });
    orgTree.on("itemdblclick", this.onOK, this);
    me.tree = orgTree;

    const wnd = PCL.create("PCL.window.Window", {
      header: {
        title: "<span style='font-size:160%'>选择物料分类</span>",
        height: 40
      },
      modal: true,
      width: 400,
      height: 300,
      layout: "fit",
      items: [orgTree],
      buttons: [{
        text: "没有上级分类",
        handler: me._onNone,
        scope: me
      }, {
        text: "确定",
        handler: me._onOK,
        scope: me
      }, {
        text: "取消",
        handler() {
          wnd.close();
        }
      }]
    });
    me.wnd = wnd;
    wnd.show();
  },

  /**
   * @private
   */
  _onOK() {
    const me = this;

    const tree = me.tree;
    const item = tree.getSelectionModel().getSelection();

    if (item === null || item.length !== 1) {
      me.showInfo("没有选择物料分类");

      return;
    }

    const data = item[0];
    me.setIdValue(data.get("id"));
    me.setValue(data.get("fullName"));
    me.wnd.close();
    me.focus();
  },

  /**
   * @public
   */
  setIdValue(id) {
    this._idValue = id;
  },

  /**
   * @public
   */
  getIdValue() {
    return this._idValue;
  },

  /**
   * 没有上级分类
   * 
   * @private
   */
  _onNone() {
    const me = this;

    me.setIdValue(null);
    me.setValue(null);
    me.wnd.close();
    me.focus();
  }
});
