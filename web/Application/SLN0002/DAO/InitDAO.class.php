<?php

namespace SLN0002\DAO;

use Home\DAO\PSIBaseExDAO;

/**
 * 总账建账 DAO
 *
 * @author PSI
 * @copyright 2015 - present
 * @license GPL v3
 */
class InitDAO extends PSIBaseExDAO
{

  /**
   * 根据科目类型获得其图标的css class
   */
  private function subjectIconCls($category)
  {
    // 1：资产、2：负债、4：所有者权益、5：成本、6：损益
    switch ($category) {
      case 1:
        return "PSI-Subject-1";
      case 2:
        return "PSI-Subject-2";
      case 4:
        return "PSI-Subject-4";
      case 5:
        return "PSI-Subject-5";
      case 6:
        return "PSI-Subject-6";
      default:
        return "PSI-Subject";
    }
  }

  /**
   * 查询明细分类账建账数据
   */
  public function queryAccDetailData($params)
  {
    $db = $this->db;

    $companyId = $params["companyId"];

    $result = [];

    $sql = "select f.subject_code, f.db_table_name_prefix, s.category, s.name as subject_name
            from t_acc_fmt f, t_subject s
            where f.company_id = '%s' and f.subject_code = s.code
            order by f.subject_code";
    $data = $db->query($sql, $companyId);

    foreach ($data as $v) {
      $subjectCode = $v["subject_code"];
      $subjectName = $v["subject_name"];
      $category = $v["category"];

      $tableName = $v["db_table_name_prefix"] . "_detail";
      $sql = "select sum(acc_db) as acc_db, sum(acc_cr) as acc_cr, 
                sum(acc_balance) as acc_balance, acc_balance_dbcr
              from {$tableName}
              where je_number = -1000 and company_id = '%s' and subject_code = '%s' 
              group by acc_balance_dbcr";
      $detailData = $db->query($sql, $companyId, $subjectCode);
      foreach ($detailData as $d) {
        $result[] = [
          "id" => $subjectCode,
          "subject" => "{$subjectCode} - {$subjectName}",
          "debit" => $d["acc_db"],
          "credit" => $d["acc_cr"],
          "balance" => $d["acc_balance"],
          "balanceDir" => $d["acc_balance_dbcr"],
          "iconCls" => $this->subjectIconCls($category),
          "leaf" => true,
          "children" => [],
          "expanded" => true,
        ];
      }
    }

    return $result;
  }

  private function balanceDirCodeToName($code)
  {
    switch ($code) {
      case 1:
        return "借方";
      case 2:
        return "贷方";
      default:
        // 莫名的bug
        return "[未定义]";
    }
  }

  /**
   * 录入明细分类账期初数据
   * 
   */
  public function editAccDetailInitRecord(&$params)
  {
    $db = $this->db;

    $companyId = $params["companyId"];
    $dt = $params["dt"];
    $subjectCode = $params["subjectCode"];
    $money = $params["money"];
    $balanceDir = intval($params["balanceDir"]);
    if (!in_array($balanceDir, [1, 2])) {
      return $this->badParam("balanceDir");
    }
    // 账簿扩展项
    $ex = $params["ex"];

    // 检查companyId
    $sql = "select name from t_org where id = '%s' and parent_id is null";
    $data = $db->query($sql, $companyId);
    if (!$data) {
      return $this->badParam("companyId");
    }
    $companyName = $data[0]["name"];

    // 检查余额方向
    $sql = "select name, balance_dir 
            from t_subject
            where is_leaf = 1 and code = '%s' and company_id = '%s' ";
    $data = $db->query($sql, $subjectCode, $companyId);
    if (!$data) {
      return $this->badParam("subjectCode");
    }
    $v = $data[0];
    $subjectName = $v["name"];
    $bd = $v["balance_dir"];

    if ($bd === 0) {
      // 借贷均可
      if (!in_array($balanceDir, [1, 2])) {
        return $this->badParam("balanceDir");
      }
    } else {
      if ($balanceDir != $bd) {
        return $this->badParam("balanceDir");
      }
    }
    $balanceDbCr = $this->balanceDirCodeToName($balanceDir);

    $sql = "select id, db_table_name_prefix 
            from t_acc_fmt 
            where company_id = '%s' and subject_code = '%s' ";
    $data = $db->query($sql, $companyId, $subjectCode);
    if (!$data) {
      return $this->badParam("subjectCode");
    }
    $v = $data[0];
    $tableName = $v["db_table_name_prefix"] . "_detail";
    $fmtId = $v["id"];

    // 查询子账簿层级字段
    // 目前的设计是：最多有三个层级字段
    $sql = "select id, db_field_name, voucher_input
            from t_acc_fmt_cols
            where fmt_id = '%s' and sub_acc_level > 0 and sub_acc_level < 4
            order by sub_acc_level
            limit 3";
    $data = $db->query($sql, $fmtId);
    $subLevelFields = [];
    foreach ($data as $v) {
      $n = $v["db_field_name"];
      $cid = $v["id"];
      $voucherInput = $v["voucher_input"];

      // 从参数中查找对应的值
      foreach ($ex as $x) {
        $colId = $x["id"];
        if ($cid == $colId) {
          $value = $x["value"];
          if ($voucherInput == 2) {
            // 码表录入
            $value = $x["codeValue"];
          }

          $subLevelFields[] = [
            "name" => $n,
            "value" => $value,
          ];

          // 跳出foreach循环
          break;
        }
      }
    }

    // 检查期初日期
    // 保证$dt和已经录入的数据在一个会计期间里面
    $dtTemp = strtotime($dt);
    $initYear = date("Y", $dtTemp);
    $initMonth = date("m", $dtTemp);

    $sql = "select acc_year, acc_month, acc_detail_inited
            from t_acc_init
            where company_id = '%s' ";
    $data = $db->query($sql, $companyId);
    // 第一次录入数据的时候，t_acc_init表中是没有记录的
    // 所以$data可以为null
    if ($data) {
      $v = $data[0];
      $accDetailInited = $v["acc_detail_inited"];
      if ($accDetailInited == 1) {
        return $this->bad("已经完成明细分类账建账了，不能再录入数据");
      }
      $accYear = $v["acc_year"];
      $accMonth = $v["acc_month"];
      if ($accYear != $initYear || $accMonth != $initMonth) {
        return $this->bad("期初日期需要在{$accYear}年{$accMonth}月内");
      }
    } else {
      // 首次录入明细分类账建账数据
      $id = $this->newId();
      $sql = "insert into t_acc_init (id, company_id, acc_year, acc_month, acc_gl_inited, acc_detail_inited)
              values ('%s', '%s', %d, %d, 0, 0)";
      $rc = $db->execute($sql, $id, $companyId, $initYear, $initMonth);
      if ($rc === false) {
        return $this->sqlError(__METHOD__, __LINE__);
      }
    }

    $sql = "select voucher_year, voucher_month
            from {$tableName}
            where company_id = '%s'  
              and je_number = -1000 limit 1"; // je_number = -1000：期初建账数据
    $data = $db->query($sql, $companyId);
    if ($data) {
      $year = $data[0]["voucher_year"];
      $month = $data[0]["voucher_month"];

      if (!($year == $initYear && $month == $initMonth)) {
        return $this->bad("期初日期需要在{$year}年{$month}月内");
      }
    }

    $id = null;
    $sql = "select id
            from {$tableName}
            where company_id = '%s' and subject_code = '%s' 
              and je_number = -1000 "; // je_number = -1000：期初建账数据
    $queryParams = [];
    $queryParams[] = $companyId;
    $queryParams[] = $subjectCode;
    foreach ($subLevelFields as $v) {
      $sql .= " and " . $v["name"] . " = '%s' ";
      $queryParams[] = $v["value"];
    }
    $data = $db->query($sql, $queryParams);
    if ($data) {
      $id = $data[0]["id"];

      // 删除建账旧数据
      $sql = "delete from {$tableName} where id = '%s' ";
      $rc = $db->execute($sql, $id);
      if ($rc === false) {
        return $this->sqlError(__METHOD__, __LINE__);
      }
    }

    if (abs($money) < 0.000001) {
      // 录入金额为0，则视为用户要删除该条建账数据。

      // 如果明细账数据都没有了，则把t_acc_init中的数据也删除，恢复到最初没有任何数据的状态
      // 注意：需要去检查所有的账簿，不能仅仅检查本账簿
      $sql = "select db_table_name_prefix
              from t_acc_fmt
              where company_id = '%s' ";
      $data = $db->query($sql, $companyId);
      $hasData = false;
      foreach ($data as $v) {
        $tableName = $v["db_table_name_prefix"] . "_detail";
        $sql = "select count(*) as cnt from {$tableName}";
        $d = $db->query($sql);
        $cnt = $d["cnt"];
        if ($cnt > 0) {
          $hasData = true;

          break;
        }
      }
      if (!$hasData) {
        $sql = "delete from t_acc_init where company_id = '%s' ";
        $rc = $db->execute($sql, $companyId);
        if ($rc === false) {
          return $this->sqlError(__METHOD__, __LINE__);
        }
      }

      // 业务日志
      $params["log"] = "组织机构[{$companyName}] - 删除科目[{$subjectCode} - {$subjectName}]明细分类账建账数据";

      // 操作成功
      $params["id"] = $id;
      return null;
    }

    // 插入建账新数据
    if (!$id) {
      $id = $this->newId();
    }
    $sql = "insert into {$tableName} (
              id, company_id, subject_code, voucher_dt, voucher_year, 
              voucher_month, je_number, acc_balance_dbcr, acc_balance,
              acc_db, acc_cr 
            )
            values ('%s', '%s', '%s', '%s', %d,
              %d, -1000, '%s', %f, ";
    if ($balanceDir == 1) {
      // 余额在借方
      $sql .= " %f, null";
    } elseif ($balanceDir == 2) {
      // 余额在贷方
      $sql .= " null, %f";
    } else {
      // 前面已经做了参数检查，不会执行到这里
    }
    $sql .= ")";

    $rc = $db->execute(
      $sql,
      $id,
      $companyId,
      $subjectCode,
      $dt,
      $initYear,
      $initMonth,
      $balanceDbCr,
      $money,
      $money
    );
    if ($rc === false) {
      return $this->sqlError(__METHOD__, __LINE__);
    }

    // 账簿扩展项
    foreach ($ex as $x) {
      $colId = $x["id"];
      $colValue = $x["value"];
      $colCodeId = $x["codeId"];
      $colCodeValue = $x["codeValue"];
      $colNameValue = $x["nameValue"];

      $sql = "select db_field_name, voucher_input
              from t_acc_fmt_cols
              where id = '%s' ";
      $data = $db->query($sql, $colId);
      if (!$data) {
        // 如果执行到这里，则是莫名bug
        continue;
      }

      $fieldName = $data[0]["db_field_name"];
      $voucherInput = $data[0]["voucher_input"];
      if ($voucherInput == 1) {
        // 直接录入
        $sql = "update {$tableName}
                    set {$fieldName} = '%s'
                  where id = '%s' ";
        $rc = $db->execute($sql, $colValue, $id);
        if ($rc === false) {
          return $this->sqlError(__METHOD__, __LINE__);
        }
      } elseif ($voucherInput == 2) {
        // 码表
        $sql = "update {$tableName}
                    set {$fieldName} = '%s', {$fieldName}_name = '%s', {$fieldName}_id = '%s'
                  where id = '%s' ";
        $rc = $db->execute($sql, $colCodeValue, $colNameValue, $colCodeId, $id);
        if ($rc === false) {
          return $this->sqlError(__METHOD__, __LINE__);
        }
      }
    }

    // 业务日志
    $params["log"] = "组织机构[{$companyName}] - 录入科目[{$subjectCode} - {$subjectName}]明细分类账建账数据";

    // 操作成功
    $params["id"] = $id;
    return null;
  }

  /**
   * 查询账样的扩展项
   */
  function queryFmtEx($params)
  {
    $db = $this->db;

    $companyId = $params["companyId"];
    $subjectCode = $params["subjectCode"];

    $result = [];

    $sql = "select name, balance_dir
            from t_subject
            where company_id = '%s' and code = '%s' and is_leaf = 1";
    $data = $db->query($sql, $companyId, $subjectCode);
    if (!$data) {
      return $result;
    }
    $v = $data[0];
    $subjectName = $v["name"];
    $balanceDir = $v["balance_dir"];

    $sql = "select id 
            from t_acc_fmt 
            where company_id = '%s' and subject_code = '%s' ";
    $data = $db->query($sql, $companyId, $subjectCode);
    if (!$data) {
      return $result;
    }

    $fmtId = $data[0]["id"];

    $sql = "select id, caption, voucher_input,
              code_table_name, voucher_input_xtype, db_field_decimal
            from t_acc_fmt_cols
            where fmt_id = '%s' and sys_col = 0 and voucher_input != 5
            order by voucher_input_show_order ";
    $data = $db->query($sql, $fmtId);
    foreach ($data as $v) {
      $voucherInput = $v["voucher_input"];
      $fid = "";
      if ($voucherInput == 2) {
        // 码表录入
        $codeTableName = $v["code_table_name"];

        $sql = "select fid
                from t_code_table_md
                where table_name = '%s' ";
        $d = $db->query($sql, $codeTableName);
        $fid = $d[0]["fid"];
      }

      $result[] = [
        "id" => $v["id"],
        "caption" => $v["caption"],
        "voucherInput" => $voucherInput,
        "voucherInputXtype" => $v["voucher_input_xtype"],
        "fieldDec" => $v["db_field_decimal"],
        "fid" => $fid,
      ];
    }

    return [
      "subjectName" => $subjectName,
      "balanceDir" => $balanceDir,
      "fmtList" => $result
    ];
  }

  /**
   * 查询账样列
   */
  public function queryFmtColsForDetailAcc($params)
  {
    $db = $this->db;

    $companyId = $params["companyId"];
    $subjectCode = $params["subjectCode"];

    $result = [];

    $sql = "select id 
            from t_acc_fmt 
            where company_id = '%s' and subject_code = '%s' ";
    $data = $db->query($sql, $companyId, $subjectCode);
    if (!$data) {
      return $result;
    }

    $fmtId = $data[0]["id"];

    $ignorFields = [
      "voucher_year",
      "voucher_month",
      "voucher_number",
      "je_number",
      "voucher_word",
      "acc_user_name",
      "biz_user_name"
    ];
    $sql = "select id, caption, db_field_name, col_width
            from t_acc_fmt_cols
            where fmt_id = '%s' and show_order > 0
            order by show_order ";
    $data = $db->query($sql, $fmtId);
    foreach ($data as $v) {
      $fieldName = $v["db_field_name"];
      if (in_array($fieldName, $ignorFields)) {
        continue;
      }

      $result[] = [
        "id" => $v["id"],
        "caption" => $v["caption"],
        "fieldName" => $fieldName,
        "colWidth" => $v["col_width"],
      ];
    }

    return $result;
  }

  /**
   * 查询明细分类账建账数据
   */
  public function queryDataForDetailAcc($params)
  {
    $db = $this->db;

    $companyId = $params["companyId"];
    $subjectCode = $params["subjectCode"];

    $result = [];

    $sql = "select name, balance_dir
            from t_subject
            where company_id = '%s' and code = '%s' and is_leaf = 1";
    $data = $db->query($sql, $companyId, $subjectCode);
    if (!$data) {
      return $result;
    }
    $v = $data[0];
    $subjectName = $v["name"];

    $sql = "select id, db_table_name_prefix 
            from t_acc_fmt 
            where company_id = '%s' and subject_code = '%s' ";
    $data = $db->query($sql, $companyId, $subjectCode);
    if (!$data) {
      return $result;
    }
    $v = $data[0];
    $fmtId = $v["id"];
    $tableName = $v["db_table_name_prefix"] . "_detail";

    // 子账簿层级
    $sql = "select db_field_name
            from t_acc_fmt_cols
            where fmt_id = '%s' and sub_acc_level between 1 and 3
            order by sub_acc_level ";
    $data = $db->query($sql, $fmtId);
    $subAccLevelFields = [];
    foreach ($data as $v) {
      $subAccLevelFields[] = $v["db_field_name"];
    }

    $sqlForData = "select ";
    $fields = [];
    $ignorFields = [
      "voucher_word",
      "acc_user_name",
      "biz_user_name"
    ];
    $sql = "select id, caption, db_field_name, voucher_input
            from t_acc_fmt_cols
            where fmt_id = '%s' and show_order > 0
            order by show_order ";
    $data = $db->query($sql, $fmtId);
    $firstFlag = true;
    foreach ($data as $v) {
      $fieldName = $v["db_field_name"];
      if (in_array($fieldName, $ignorFields)) {
        continue;
      }

      $voucherInput = $v["voucher_input"];

      if (!$firstFlag) {
        $sqlForData .= ", ";
      }

      $sqlForData .= $fieldName;
      $firstFlag = false;

      $fields[] = [
        "name" => $fieldName,
        "voucherInput" => $voucherInput
      ];

      if ($voucherInput == 2) {
        // 码表录入
        $sqlForData .= ", {$fieldName}_name";
      }
    }

    $sqlForData .= " from " . $tableName;
    $sqlForData .= " where je_number = -1000"; // je_number = -1000：期初建账数据

    // 按子账簿层级排序
    if (count($subAccLevelFields) > 0) {
      $sqlForData .= " order by ";
      foreach ($subAccLevelFields as $i => $v) {
        if ($i > 0) {
          $sqlForData .= ",";
        }
        $sqlForData .= $v;
      }
    }
    $data = $db->query($sqlForData);
    foreach ($data as $v) {
      $item = [];
      foreach ($fields as $f) {
        $name = $f["name"];

        if ($name == "subject_code") {
          $item["subject_code"] = $v["subject_code"] . " - " . $subjectName;
          continue;
        }

        $voucherInput = $f["voucherInput"];
        if ($voucherInput == 2) {
          // 码表录入
          $name2 = $name . "_name";
          $item[$name] = $v[$name] . " - " . $v[$name2];
        } else {
          $item[$name] = $v[$name];
        }
      }

      $result[] = $item;
    }

    return $result;
  }

  /**
   * 查询总账建账期间
   */
  public function queryAccInitYearAndMonth($params)
  {
    $db = $this->db;

    $companyId = $params["companyId"];

    $sql = "select acc_year, acc_month
            from t_acc_init
            where company_id = '%s' ";
    $data = $db->query($sql, $companyId);
    if (!$data) {
      return $this->emptyResult();
    }
    $v = $data[0];
    $month = $v["acc_month"];
    $m = $month;
    if ($month < 10) {
      // 把月份补足两位
      $m = "0{$month}";
    }
    return [
      "year" => $v["acc_year"],
      "month" => $m,
    ];
  }

  /**
   * 完成明细分类账建账
   */
  public function commitAccDetailInit(&$params)
  {
    $db = $this->db;

    $companyId = $params["companyId"];
    $sql = "select name from t_org where id = '%s' and parent_id is null ";
    $data = $db->query($sql, $companyId);
    if (!$data) {
      return $this->badParam("companyId");
    }
    $companyName = $data[0]["name"];

    $sql = "select acc_detail_inited
            from t_acc_init
            where company_id = '%s' ";
    $data = $db->query($sql, $companyId);

    if (!$data) {
      return $this->bad("[{$companyName}]还没有录入建账数据");
    }

    $inited = $data[0]["acc_detail_inited"];
    if ($inited == 1) {
      return $this->bad("[{$companyName}]已经完成建账，不能再次标记");
    }

    $sql = "update t_acc_init 
              set acc_detail_inited = 1, acc_detail_inited_dt = now()
            where company_id = '%s' ";
    $rc = $db->execute($sql, $companyId);
    if ($rc === false) {
      return $this->sqlError(__METHOD__, __LINE__);
    }

    // 业务日志
    $log = "[{$companyName}]已经完成明细分类账建账";
    $params["log"] = $log;

    // 操作成功
    return null;
  }

  /**
   * 取消明细分类账建账
   */
  public function cancelAccDetailInit(&$params)
  {
    $db = $this->db;

    $companyId = $params["companyId"];
    $sql = "select name from t_org where id = '%s' and parent_id is null ";
    $data = $db->query($sql, $companyId);
    if (!$data) {
      return $this->badParam("companyId");
    }
    $companyName = $data[0]["name"];

    $sql = "select acc_detail_inited, acc_gl_inited
            from t_acc_init
            where company_id = '%s' ";
    $data = $db->query($sql, $companyId);
    if (!$data) {
      return $this->bad("[{$companyName}]还没有录入建账数据");
    }

    $inited = $data[0]["acc_detail_inited"];
    if ($inited == 0) {
      return $this->bad("[{$companyName}]还没有标记建账完毕，无须进行取消操作");
    }

    $glInited = $data[0]["acc_gl_inited"];
    if ($glInited == 1) {
      return $this->bad("[{$companyName}]的总分类账已经完成建账，此时不能取消明细分类账建账");
    }

    // 检查是否有了日常的业务，如果有则不能取消标志
    $sql = "select db_table_name_prefix
            from t_acc_fmt
            where company_id = '%s' ";
    $data = $db->query($sql, $companyId);
    foreach ($data as $v) {
      $tableName = $v["db_table_name_prefix"] . "_detail";

      $sql = "select count(*) as cnt from {$tableName} where je_number > 0";
      $d = $db->query($sql);
      $cnt = $d["cnt"];
      if ($cnt > 0) {
        return $this->bad("已经有了日常业务发生，不能取消建账标志了");
      }
    }

    $sql = "update t_acc_init 
              set acc_detail_inited = 0, acc_detail_inited_dt = null
            where company_id = '%s' ";
    $rc = $db->execute($sql, $companyId);
    if ($rc === false) {
      return $this->sqlError(__METHOD__, __LINE__);
    }

    // 业务日志
    $log = "[{$companyName}]取消明细分类账建账完成标记";
    $params["log"] = $log;

    // 操作成功
    return null;
  }

  /**
   * 查询建账完成情况
   * 
   */
  public function queryInitStatus($params)
  {
    $db = $this->db;

    $companyId = $params["companyId"];

    $sql = "select acc_year, acc_month, acc_detail_inited, acc_detail_inited_dt,
              acc_gl_inited, acc_gl_inited_dt
            from t_acc_init
            where company_id = '%s' ";
    $data = $db->query($sql, $companyId);
    if (!$data) {
      return $this->emptyResult();
    }
    $v = $data[0];

    $result = [
      "year" => $v["acc_year"],
      "month" => $v["acc_month"],
      "detailInited" => $v["acc_detail_inited"],
      "detailDT" => $v["acc_detail_inited_dt"],
      "glInited" => $v["acc_gl_inited"],
      "glDT" => $v["acc_gl_inited_dt"],
    ];

    return $result;
  }

  private function genInitData($companyId)
  {
    $db = $this->db;

    $sql = "select id, db_table_name_prefix
            from t_acc_fmt
            where company_id = '%s'
            order by subject_code";
    $data = $db->query($sql, $companyId);

    foreach ($data as $v) {
      $fmtId = $v["id"];
      $tableName = $v["db_table_name_prefix"];

      $sql = "select db_field_name,	voucher_input
              from t_acc_fmt_cols
              where fmt_id = '%s' 
              order by show_order";
      $d = $db->query($sql, $fmtId);
      $fields = ["id", "company_id"];
      foreach ($d as $f) {
        $field = $f["db_field_name"];
        $voucherInput = $f["voucher_input"];

        $fields[] = $field;

        if ($voucherInput == 2) {
          // 码表录入
          $fields[] = $field . "_id";
          $fields[] = $field . "_name";
        }
      }

      // 清空旧数据
      $sql = "delete from {$tableName}";
      $rc = $db->execute($sql);
      if ($rc === false) {
        return $this->sqlError(__METHOD__, __LINE__);
      }

      // 插入新数据
      $fieldList = implode(",", $fields);
      $tableNameDetail = $tableName . "_detail";
      $sql = "insert into {$tableName} ($fieldList)
              select {$fieldList}
              from {$tableNameDetail}
              where je_number = -1000";
      $rc = $db->execute($sql);
      if ($rc === false) {
        return $this->sqlError(__METHOD__, __LINE__);
      }
    } // end of foreach

    // 操作成功
    return null;
  }

  /**
   * 从明细分类账生成总账建账数据
   */
  public function genInitDataFromAccDetail(&$params)
  {
    $db = $this->db;

    $companyId = $params["companyId"];

    $sql = "select name 
            from t_org
            where id = '%s' and parent_id is null ";
    $data = $db->query($sql, $companyId);
    if (!$data) {
      return $this->badParam("companyId");
    }

    $companyName = $data[0]["name"];

    $sql = "select acc_detail_inited
            from t_acc_init
            where company_id = '%s' ";
    $data = $db->query($sql, $companyId);
    if (!$data) {
      return $this->bad("<span style='color:red'>{$companyName}</span> 明细分类账还没有录入数据");
    }
    $inited = $data[0]["acc_detail_inited"];
    if ($inited != 1) {
      return $this->bad("<span style='color:red'>{$companyName}</span> 明细分类账还没有完成建账");
    }

    $rc = $this->genInitData($companyId);
    if ($rc) {
      return $rc;
    }

    // 业务日志
    $log = "[$companyName] - 从明细分类账生成总分类账建账数据";
    $params["log"] = $log;

    // 操作成功
    return null;
  }

  /**
   * 查询总分类账建账数据
   */
  public function queryAccGlData($params)
  {
    $db = $this->db;

    $companyId = $params["companyId"];

    $result = [];

    $sql = "select f.subject_code, f.db_table_name_prefix, s.category, s.name as subject_name
            from t_acc_fmt f, t_subject s
            where f.company_id = '%s' and f.subject_code = s.code
            order by f.subject_code";
    $data = $db->query($sql, $companyId);

    foreach ($data as $v) {
      $subjectCode = $v["subject_code"];
      $subjectName = $v["subject_name"];
      $category = $v["category"];

      $tableName = $v["db_table_name_prefix"];
      $sql = "select sum(acc_db) as acc_db, sum(acc_cr) as acc_cr, 
                sum(acc_balance) as acc_balance, acc_balance_dbcr
              from {$tableName}
              where je_number = -1000 and company_id = '%s' and subject_code = '%s' 
              group by acc_balance_dbcr";
      $detailData = $db->query($sql, $companyId, $subjectCode);
      foreach ($detailData as $d) {
        $result[] = [
          "id" => $subjectCode,
          "subject" => "{$subjectCode} - {$subjectName}",
          "debit" => $d["acc_db"],
          "credit" => $d["acc_cr"],
          "balance" => $d["acc_balance"],
          "balanceDir" => $d["acc_balance_dbcr"],
          "iconCls" => $this->subjectIconCls($category),
          "leaf" => true,
          "children" => [],
          "expanded" => true,
        ];
      }
    }

    return $result;
  }

  /**
   * 查询总分类账建账数据
   */
  public function queryDataForGlAcc($params)
  {
    $db = $this->db;

    $companyId = $params["companyId"];
    $subjectCode = $params["subjectCode"];

    $result = [];

    $sql = "select name, balance_dir
            from t_subject
            where company_id = '%s' and code = '%s' and is_leaf = 1";
    $data = $db->query($sql, $companyId, $subjectCode);
    if (!$data) {
      return $result;
    }
    $v = $data[0];
    $subjectName = $v["name"];

    $sql = "select id, db_table_name_prefix 
            from t_acc_fmt 
            where company_id = '%s' and subject_code = '%s' ";
    $data = $db->query($sql, $companyId, $subjectCode);
    if (!$data) {
      return $result;
    }
    $v = $data[0];
    $fmtId = $v["id"];
    $tableName = $v["db_table_name_prefix"];

    // 子账簿层级
    $sql = "select db_field_name
            from t_acc_fmt_cols
            where fmt_id = '%s' and sub_acc_level between 1 and 3
            order by sub_acc_level ";
    $data = $db->query($sql, $fmtId);
    $subAccLevelFields = [];
    foreach ($data as $v) {
      $subAccLevelFields[] = $v["db_field_name"];
    }

    $sqlForData = "select ";
    $fields = [];
    $ignorFields = [
      "voucher_word",
      "acc_user_name",
      "biz_user_name"
    ];
    $sql = "select id, caption, db_field_name, voucher_input
            from t_acc_fmt_cols
            where fmt_id = '%s' and show_order > 0
            order by show_order ";
    $data = $db->query($sql, $fmtId);
    $firstFlag = true;
    foreach ($data as $v) {
      $fieldName = $v["db_field_name"];
      if (in_array($fieldName, $ignorFields)) {
        continue;
      }

      $voucherInput = $v["voucher_input"];

      if (!$firstFlag) {
        $sqlForData .= ", ";
      }

      $sqlForData .= $fieldName;
      $firstFlag = false;

      $fields[] = [
        "name" => $fieldName,
        "voucherInput" => $voucherInput
      ];

      if ($voucherInput == 2) {
        // 码表录入
        $sqlForData .= ", {$fieldName}_name";
      }
    }

    $sqlForData .= " from " . $tableName;
    $sqlForData .= " where je_number = -1000"; // je_number = -1000：期初建账数据

    // 按子账簿层级排序
    if (count($subAccLevelFields) > 0) {
      $sqlForData .= " order by ";
      foreach ($subAccLevelFields as $i => $v) {
        if ($i > 0) {
          $sqlForData .= ",";
        }
        $sqlForData .= $v;
      }
    }
    $data = $db->query($sqlForData);
    foreach ($data as $v) {
      $item = [];
      foreach ($fields as $f) {
        $name = $f["name"];

        if ($name == "subject_code") {
          $item["subject_code"] = $v["subject_code"] . " - " . $subjectName;
          continue;
        }

        $voucherInput = $f["voucherInput"];
        if ($voucherInput == 2) {
          // 码表录入
          $name2 = $name . "_name";
          $item[$name] = $v[$name] . " - " . $v[$name2];
        } else {
          $item[$name] = $v[$name];
        }
      }

      $result[] = $item;
    }

    return $result;
  }

  /**
   * 查询账样列
   */
  public function queryFmtColsForGlAcc($params)
  {
    $db = $this->db;

    $companyId = $params["companyId"];
    $subjectCode = $params["subjectCode"];

    $result = [];

    $sql = "select id 
            from t_acc_fmt 
            where company_id = '%s' and subject_code = '%s' ";
    $data = $db->query($sql, $companyId, $subjectCode);
    if (!$data) {
      return $result;
    }

    $fmtId = $data[0]["id"];

    $ignorFields = [
      "voucher_year",
      "voucher_month",
      "voucher_number",
      "je_number",
      "voucher_word",
      "acc_user_name",
      "biz_user_name"
    ];
    $sql = "select id, caption, db_field_name, col_width
            from t_acc_fmt_cols
            where fmt_id = '%s' and show_order > 0
            order by show_order ";
    $data = $db->query($sql, $fmtId);
    foreach ($data as $v) {
      $fieldName = $v["db_field_name"];
      if (in_array($fieldName, $ignorFields)) {
        continue;
      }

      $result[] = [
        "id" => $v["id"],
        "caption" => $v["caption"],
        "fieldName" => $fieldName,
        "colWidth" => $v["col_width"],
      ];
    }

    return $result;
  }

  /**
   * 完成总分类账建账
   */
  public function commitAccGlInit(&$params)
  {
    $db = $this->db;

    $companyId = $params["companyId"];
    $sql = "select name 
            from t_org
            where id = '%s' and parent_id is null ";
    $data = $db->query($sql, $companyId);
    if (!$data) {
      return $this->badParam("companyId");
    }

    $companyName = $data[0]["name"];

    // 检查明细分类账建账是否完成
    $sql = "select acc_detail_inited, acc_gl_inited
            from t_acc_init
            where company_id = '%s' ";
    $data = $db->query($sql, $companyId);
    if (!$data) {
      return $this->bad("<span style='color:red'>{$companyName}</span> 明细分类账还没有录入数据");
    }
    $detailInited = $data[0]["acc_detail_inited"];
    if ($detailInited != 1) {
      return $this->bad("<span style='color:red'>{$companyName}</span> 明细分类账还没有完成建账");
    }

    $glInited = $data[0]["acc_gl_inited"];
    if ($glInited == 1) {
      return $this->bad("<span style='color:red'>{$companyName}</span> 总分类账已经完成建账了");
    }

    // 把数据重新生成一遍
    $rc = $this->genInitData($companyId);
    if ($rc) {
      return $rc;
    }

    $sql = "update t_acc_init
            set acc_gl_inited = 1, acc_gl_inited_dt = now()
            where company_id = '%s' ";
    $rc = $db->execute($sql, $companyId);
    if ($rc === false) {
      return $this->sqlError(__METHOD__, __LINE__);
    }

    // 业务日志
    $log = "[$companyName] - 总分类账完成建账";
    $params["log"] = $log;

    // 操作成功
    return null;
  }

  /**
   * 取消总分类账建账
   */
  public function cancelAccGlInit(&$params)
  {
    $db = $this->db;

    $companyId = $params["companyId"];
    $sql = "select name 
            from t_org
            where id = '%s' and parent_id is null ";
    $data = $db->query($sql, $companyId);
    if (!$data) {
      return $this->badParam("companyId");
    }

    $companyName = $data[0]["name"];

    $sql = "select acc_gl_inited
            from t_acc_init
            where company_id = '%s' ";
    $data = $db->query($sql, $companyId);
    if (!$data) {
      return $this->bad("<span style='color:red'>{$companyName}</span> 总分类账还没有完成建账，无需取消建账");
    }
    $inited = $data[0]["acc_gl_inited"];
    if ($inited == 0) {
      return $this->bad("<span style='color:red'>{$companyName}</span> 总分类账还没有完成建账，无需取消建账");
    }

    // 检查是否有了日常的业务，如果有则不能取消标志
    $sql = "select db_table_name_prefix
        from t_acc_fmt
        where company_id = '%s' ";
    $data = $db->query($sql, $companyId);
    foreach ($data as $v) {
      $tableName = $v["db_table_name_prefix"];

      $sql = "select count(*) as cnt from {$tableName} where je_number > 0";
      $d = $db->query($sql);
      $cnt = $d["cnt"];
      if ($cnt > 0) {
        return $this->bad("已经有了日常业务发生，不能取消建账标志了");
      }
    }

    $sql = "update t_acc_init 
            set acc_gl_inited = 0, acc_gl_inited_dt = null
            where company_id = '%s' ";
    $rc = $db->execute($sql, $companyId);
    if ($rc === false) {
      return $this->sqlError(__METHOD__, __LINE__);
    }

    // 业务日志
    $log = "[$companyName] - 取消总分类账建账";
    $params["log"] = $log;

    // 操作成功
    return null;
  }
}
